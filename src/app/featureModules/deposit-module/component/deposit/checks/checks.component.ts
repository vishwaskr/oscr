import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';
import Swal from 'sweetalert2';


export interface CheckDepositList {
  Index: number;
  BankId: number;
  DepositId: number;
  PaymentFormId: number;
  PickupPaymentFormId: number;
  CheckDepositAmount: number;
  CheckSuggestAmount: number;
  RegisterNumber: number;
  RegisterNumberList: string;
  SiteName: string;
  Type: string;
  StatusName: string;
}

export interface CHECKSubmitData {
  BankId: number;
  PickupPaymentFormId: number;
}

@Component({
  selector: 'app-checks',
  templateUrl: './checks.component.html',
  styleUrls: ['./checks.component.css']
})
export class ChecksComponent implements OnInit {

  checkDepositShow:boolean=false;
  checkDepositSlipShow:boolean=false;
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  
  checksDataSource:any;
  checksDepositListDataSource:any;
  checkIndexArray:Array<any> = [];
  checkSubmitButtonEnaDis:boolean=false;
  isChanged:boolean=false;
  selection: any;
 //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private router:Router, public depositService:DepositService) { }

  displayedColumns: string[] = ['select', 'site_name', 'register', 'suggested', 'actual'];
  
  displayedColumns2: string[] = ['site_name', 'register', 'suggested', 'actual', 'status', 'action'];
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.checksDataSource = new MatTableDataSource<CheckDepositList>([]);
    this.selection = new SelectionModel<CheckDepositList>(true, []);
    
    this.getCMOandDepositTotal();
    this.getCheckDepositList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
   //Select all module-----------------------------------------
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.checksDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.checksDataSource.data.forEach(row => this.selection.select(row));
  }
  
  
    /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CheckDepositList): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    
    //For getting selected site id------------------------
    if(this.selection.isSelected(row)==true) {
     this.getCheckIds(true,row.Index);
    }
    else{
     this.getCheckIds(false,row.Index);
     for( var i = 0; i < this.checkIndexArray.length; i++){ 
          if ( this.checkIndexArray[i] === row.Index) {
            this.checkIndexArray.splice(i, 1); 
          }
        }
    }
    //-----------------------------------------------------
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.Index + 1}`;
  }
  
  //-----------------------------------------------------------------
    //Get Pickup ids---------------------------------------------------
  getCheckIds(event,amex_id){
    //console.log(event+'-'+pickup_id);
   if (event==true){
    this.checkIndexArray.push(amex_id);
    }
    else{
      for( var i = 0; i < this.checkIndexArray.length; i++){ 
          if ( this.checkIndexArray[i] === amex_id) {
            this.checkIndexArray.splice(i, 1); 
          }
        }
    }
    
    if (this.checkIndexArray.length >=1) {
      this.checkSubmitButtonEnaDis = false;
     // this.isChanged=true;
    }
    else{
      this.checkSubmitButtonEnaDis = true;
      //this.isChanged=false;
    }

  }
  //-----------------------------------------------------------------
  
  //Get Check deposit list-------------------------------------------
  getCheckDepositList(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var checkDepositListArray=[];
    var checkDepositSlipListArray=[];
    
    const checkDepositDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //call api-----------------
    this.depositService.getCheckDepositList(checkDepositDetails).subscribe(res=>{
       console.log(res);
       this.progressBarHide();
       if(res.status=='success') {
        let i=0;
        let j=0;
        
        var checkIndexIds= [];
        //New deposit------------------------
        if (res.data.NewDeposit.length >0) {
          res.data.NewDeposit.forEach(function (value) {
          let checkData =  {} as CheckDepositList;
              checkData.Index = i;
              checkData.BankId = value.BankId;
              checkData.DepositId = value.DepositId;
              checkData.PaymentFormId = value.PaymentFormId;
              checkData.PickupPaymentFormId = value.PickupPaymentFormId;
              checkData.CheckDepositAmount = value.CheckDepositAmount;
              checkData.CheckSuggestAmount = value.CheckSuggestAmount;
              checkData.RegisterNumber = value.RegisterNumber;
              checkData.RegisterNumberList = value.RegisterNumberList;
              checkData.SiteName = value.SiteName;
              checkData.Type = value.Type;
              
              checkIndexIds.push(i);
              
              checkDepositListArray.push(checkData);
              
              i++;
          });
          //console.log(checkDepositListArray);
          var CHECK_DATA: CheckDepositList[] = checkDepositListArray;
          this.checksDataSource = new MatTableDataSource<CheckDepositList>(CHECK_DATA);
          this.selection = new SelectionModel<CheckDepositList>(true, CHECK_DATA);
          this.checkDepositShow=true;
          
          for(let b = 0; b < checkIndexIds.length; b++){
          this.checkIndexArray.push(checkIndexIds[b]);
          }
        }
        else{
          this.checkDepositShow=false;
        }
        //--------------------------------
        //Old Deposit---------------------
          if (res.data.OldDeposit.length >0) {
          res.data.OldDeposit.forEach(function (value) {
          let checkData =  {} as CheckDepositList;
              checkData.Index = j;
              checkData.BankId = value.BankId;
              checkData.DepositId = value.DepositId;
              checkData.PaymentFormId = value.PaymentFormId;
              checkData.PickupPaymentFormId = value.PickupPaymentFormId;
              checkData.CheckDepositAmount = value.CheckDepositAmount;
              checkData.CheckSuggestAmount = value.CheckSuggestAmount;
              checkData.RegisterNumber = value.RegisterNumber;
              checkData.RegisterNumberList = value.RegisterNumberList;
              checkData.SiteName = value.SiteName;
              checkData.Type = value.Type;
              checkData.StatusName = value.StatusName;
              
              checkDepositSlipListArray.push(checkData);
              
              j++;
          });
          //console.log(checkDepositListArray);
          var CHECK_DATA: CheckDepositList[] = checkDepositSlipListArray;
          this.checksDepositListDataSource = new MatTableDataSource<CheckDepositList>(CHECK_DATA);
          
          this.checkDepositSlipShow=true;
          
        }
        else{
          this.checkDepositSlipShow=false;
        }
        //--------------------------------
       }
       else{
        this.checkDepositShow=false;
        this.checkDepositSlipShow=false;
       }
    });
    //-------------------------
  }
  //-----------------------------------------------------------------
  //Check Submit-----------------------------------------------------
  checkSubmit(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let checkTotalAmt=0;
    var checkDataSubmitArray=[];
    var uniqueCheckIds = this.checkIndexArray.filter((v, i, a) => a.indexOf(v) === i);
    let PaymentFormId=+(document.getElementById("payment_form_id_0") as HTMLInputElement).value;
    
    uniqueCheckIds.forEach(function (i) {
     let BankId=+(document.getElementById("bank_id_"+i) as HTMLInputElement).value;
     let PickupPaymentFormId=+(document.getElementById("pickup_payment_form_id_"+i) as HTMLInputElement).value; 
     let checkAmount=+(document.getElementById("check_actual_"+i) as HTMLInputElement).value;
     checkTotalAmt=checkTotalAmt+checkAmount;
     
     let CHECKSubmitData =  {} as CHECKSubmitData;
         CHECKSubmitData.BankId=BankId;
         CHECKSubmitData.PickupPaymentFormId=PickupPaymentFormId;
         
        checkDataSubmitArray.push(CHECKSubmitData);
         
     });
   // console.log(checkDataSubmitArray);
    const checkSubmitDetails = {
      'branchId': branchId,
      'createBy': this.authUserId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate,
      'paymentFormId': PaymentFormId,
      'checkAmount' : checkTotalAmt,
      'checkDetail': JSON.stringify(checkDataSubmitArray)
    }
    //console.log(checkSubmitDetails);
    this.depositService.submitCheckDeposit(checkSubmitDetails).subscribe(res=>{
     // console.log(res);
     this.progressBarHide();
       if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.isChanged=false;
        this.getCMOandDepositTotal();
        setTimeout(() => {
          this.getCheckDepositList();
          this.successmessage='';
        }, 2000);
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        this.checkSubmitButtonEnaDis=false;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
    });
    
  }
  //-----------------------------------------------------------------
    //Edit AMEX Deposit-----------------------------------------
  editCHECKDeposit(depositId){
   //console.log('DepositID:'+depositId);
   //button----
   document.getElementById("action-box-"+depositId).style.display="none";
   document.getElementById("save-box-"+depositId).style.display="block";
   //----------
   //input-----
   document.getElementById("up_check-lable-"+depositId).style.display="none";
   document.getElementById("up_check-input-"+depositId).style.display="block";
   //----------
  }
  //-----------------------------------------------------------------
  //Edit AMEX Deposit Cancel-----------------------------------------
  cancelCHECKDeposit(depositId){
   //Button---
   document.getElementById("action-box-"+depositId).style.display="block";
   document.getElementById("save-box-"+depositId).style.display="none";
   //---------
   //input-----
   document.getElementById("up_check-lable-"+depositId).style.display="block";
   document.getElementById("up_check-input-"+depositId).style.display="none";
   //----------
  }
  //-----------------------------------------------------------------
    //Update AMEX deposit----------------------------------------------
  updateCheckDeposit(depositId){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let check_actual_amt = +(document.getElementById("up_check_"+depositId) as HTMLInputElement).value;
   // console.log("amex_actual_amt:"+amex_actual_amt);
    const updateCheckDetails = {
      'depositId': depositId,
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'checkAmount': check_actual_amt,
      'depositDate': this.currentDate
    }
     console.log(updateCheckDetails);
     this.depositService.updateCheckDeposit(updateCheckDetails).subscribe(res=>{
     this.progressBarHide();
     console.log(res);
        if(res.status=='success') {
        //Button---------------
        document.getElementById("action-box-"+depositId).style.display="block";
        document.getElementById("save-box-"+depositId).style.display="none";
        //---------------------
        //input-----
        document.getElementById("up_check-lable-"+depositId).style.display="block";
        document.getElementById("up_check-input-"+depositId).style.display="none";
        //----------
        
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.getCMOandDepositTotal();
        this.getCheckDepositList();
        setTimeout(() => {
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
     });
  }
  //-----------------------------------------------------------------
  //Change Branch----------------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    if (this.isChanged ==false) {
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    this.getCMOandDepositTotal();
    this.getCheckDepositList();
    }
    else{
      Swal.fire({
        //title: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Save',
        cancelButtonText: 'Ignore'
        }).then((result) => {
          if (result.value) {
            //this.isChanged=false;
            //return false;
            this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
          }
          else {
            this.filterBranchId=BranchId;
            localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
            this.getCMOandDepositTotal();
            this.getCheckDepositList();
            this.isChanged=false;
          }
        });
    }
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getCheckDepositList();
  }
  //----------------------------------------------------------------
  //Check Is Change-------------------------------------------------
  checkChange(){
    this.isChanged=true;
  }
  //----------------------------------------------------------------
  //Route-----------------------------------------------------------
  routeLink(route){
    if (route=='../deposit' && this.isChanged ==false) {
     this.router.navigate(['../deposit']) 
    }
    else if (route=='../deposit/checks' && this.isChanged == false) {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='../deposit/cc-handhelds' && this.isChanged == false) {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='../deposit/pos' && !this.isChanged) {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='../deposit/amex' && this.isChanged == false) {
      this.router.navigate(['../deposit/amex']) 
    }
    else if(route=='../deposit/other-daily-deposits' && this.isChanged == false){
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    else{
      this.routeConfirm(route);  
    }
  }
  //----------------------------------------------------------
  //Route Confirm alert---------------------------------------
  routeConfirm(link){
     Swal.fire({
    // title: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Ignore'
    }).then((result) => {
      if (!result.value) {
        this.router.navigate([link]) 
      }
      //else {
      //  this.router.navigate([link]) 
      //}
    });
  }
  //----------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      //console.log(res);
      if(res.status=='success') {
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }

}
