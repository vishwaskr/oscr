import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';
import Swal from 'sweetalert2';

//Pos---------------------
export interface POSDataList {
  Index: number;
  DepositId: number;
  PosId: number;
  PosName: string;
  RegisterId: number;
  RegisterNumber: any;
  SiteId: number;
  SiteName : string;
  DebitSuggestAmount: number;
  DebitDepositAmount: number;
  VisaMasterDiscoverSuggestAmount: number;
  VisaMasterDiscoverDepositAmount: number;
  StatusName: string;
}

export interface POSSubmitData {
  PosId: number;
  RegisterId: number;
  SiteId: number;
  VisaMasterDiscoverDepositAmount: number;
  DebitDepositAmount: number;
}
//------------------------

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.css']
})
export class PosComponent implements OnInit {
  
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  posDataSource:any;
  posDataListSource:any;
  isChanged:boolean=false;
  showPOSdata:boolean=false;
  showPOSdatalist:boolean=false;
  isReviewdIndexArray:Array<any> = [];
  totalPOSResult:number;
  posSubmitButtonEnaDis:boolean=true;
   //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private router:Router, public depositService:DepositService) { }
  
  //Table columns for POS-------------
  displayedColumnsPOS: string[] = ['pos_name', 'site_name', 'register','vi_mc_di_suggested', 'vi_mc_di_actual', 'debit_suggested', 'debit_actual','is_reviewd'];
  displayedColumnsPOSList: string[] = ['pos_name', 'site_name', 'register','vi_mc_di_suggested', 'vi_mc_di_actual', 'debit_suggested', 'debit_actual','status','action'];
  //------------------------------------
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getCMOandDepositTotal();
    this.getPosDepositList();
  }
  //---------------------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Get POS Deposit list--------------------------------------------
  getPosDepositList(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var posDataArray=[];
    var posDataListArray=[];
    var posDepositDataArray=[];
    
    const posDepositDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
     //call api--------------
    this.depositService.getPosDepositList(posDepositDetails).subscribe(res=>{
     this.progressBarHide();
      //console.log(res);
       if(res.status=='success') {
       let j=0;
       var POS_DATA: POSDataList[]=[];
       //new---------------------------------------
       if (res.data.NewDeposit) {
        res.data.NewDeposit.forEach(function (value) {
         let posData =  {} as POSDataList;
          posData.Index=j;
          posData.DepositId=value.DepositId;
          posData.PosId=value.PosId;
          posData.PosName=value.PosName;
          posData.RegisterId=value.RegisterId;
          posData.RegisterNumber=value.RegisterNumber;
          posData.SiteId=value.SiteId;
          posData.SiteName=value.SiteName;
          posData.DebitDepositAmount=value.DebitDepositAmount;
          posData.DebitSuggestAmount=value.DebitSuggestAmount;
          posData.VisaMasterDiscoverDepositAmount=value.VisaMasterDiscoverDepositAmount;
          posData.VisaMasterDiscoverSuggestAmount=value.VisaMasterDiscoverSuggestAmount;
          
          posDataArray.push(posData);
          j++;
        });
        this.totalPOSResult=j;
        var POS_DATA: POSDataList[]=posDataArray;
        this.posDataSource = POS_DATA;
        this.showPOSdata=true;
       }
       else{
        this.showPOSdata=false;
        var POS_DATA: POSDataList[]=[];
       }
       this.posDataSource = POS_DATA;
       
       //if (this.totalPOSResult > 0) {
       // 
       //}
       //-------------------------------------
       //old----------------------------------
       if (res.data.OldDeposit) {
        res.data.OldDeposit.forEach(function (value) {
         let posListData =  {} as POSDataList;
          posListData.DepositId=value.DepositId;
          posListData.PosId=value.PosId;
          posListData.PosName=value.PosName;
          posListData.RegisterId=value.RegisterId;
          posListData.RegisterNumber=value.RegisterNumber;
          posListData.SiteId=value.SiteId;
          posListData.SiteName=value.SiteName;
          posListData.DebitDepositAmount=value.DebitDepositAmount;
          posListData.DebitSuggestAmount=value.DebitSuggestAmount;
          posListData.VisaMasterDiscoverDepositAmount=value.VisaMasterDiscoverDepositAmount;
          posListData.VisaMasterDiscoverSuggestAmount=value.VisaMasterDiscoverSuggestAmount;
          posListData.StatusName = value.StatusName;
          
          posDataListArray.push(posListData);
         
        });
        var POS_DATA_LIST: POSDataList[]=posDataListArray;
        this.posDataListSource=POS_DATA_LIST;
        this.showPOSdatalist=true;
       }
       else{
        var POS_DATA_LIST: POSDataList[]=[];
        this.posDataListSource=POS_DATA_LIST;
        this.showPOSdatalist=false;
       }
       //-------------------------------------
       }
       else{
        this.showPOSdata=false;
        this.showPOSdatalist=false;
        var POS_DATA: POSDataList[]=[];
        this.posDataSource = POS_DATA;
        var POS_DATA_LIST: POSDataList[]=[];
        this.posDataListSource=POS_DATA_LIST;
       }
       
    });
  }
  //----------------------------------------------------------------
  
  //POSFormSubmit---------------------------------------------------
  posFormSubmit(){
    this.posSubmitButtonEnaDis=true;
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
     
    //For getting vi_mc_di actual amount------------
     var posSubmitDataArray=[];
     for( var i = 0; i < this.isReviewdIndexArray.length; i++){
        let posId= +(document.getElementById("pos_id_"+i) as HTMLInputElement).value;
        let vi_mc_di_actual_amt = +(document.getElementById("vi_mc_di_"+i) as HTMLInputElement).value;
        let debit_amt = +(document.getElementById("debit_amt_"+i) as HTMLInputElement).value;
        let register_id = +(document.getElementById("register_id_"+i) as HTMLInputElement).value;
        let site_id = +(document.getElementById("site_id_"+i) as HTMLInputElement).value;
      
          let POSSubmitData =  {} as POSSubmitData;
             POSSubmitData.RegisterId = register_id;
             POSSubmitData.SiteId = site_id;
             POSSubmitData.PosId = posId;
             POSSubmitData.VisaMasterDiscoverDepositAmount = vi_mc_di_actual_amt;
             POSSubmitData.DebitDepositAmount = debit_amt;
             
             posSubmitDataArray.push(POSSubmitData);
     }
    //---------------------------------------------
     const posSubmitDetails = {
      'branchId': branchId,
      'createBy': this.authUserId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate,
      'posDetails': JSON.stringify(posSubmitDataArray)
    }
    //console.log(posSubmitDetails);
    //Call api---------------------------------------
    this.depositService.submitPOSDeposit(posSubmitDetails).subscribe(res=>{
     this.progressBarHide();
     // console.log(res);
       if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.isChanged=false;
        this.getCMOandDepositTotal();
        setTimeout(() => {
          this.getPosDepositList();
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        this.posSubmitButtonEnaDis=false;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
       
    });
    //----------------------------------------------- 
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  //Edit POS Deposit------------------------------------------------
  editPOSDeposit(depositId){
   //console.log('DepositID:'+depositId);
   //button----
   document.getElementById("action-box-"+depositId).style.display="none";
   document.getElementById("save-box-"+depositId).style.display="block";
   //----------
   //input-----
   document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="none";
   document.getElementById("up_vi-mc-di-input-"+depositId).style.display="block";
   
   document.getElementById("up_debit-lable-"+depositId).style.display="none";
   document.getElementById("up_debit-input-"+depositId).style.display="block";
   //----------
  }
  //-----------------------------------------------------------------
  //Edit CC-Handheld Deposit Cancel----------------------------------
  cancelPOSDeposit(depositId){
   //Button---
   document.getElementById("action-box-"+depositId).style.display="block";
   document.getElementById("save-box-"+depositId).style.display="none";
   //---------
   //input-----
   document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="block";
   document.getElementById("up_vi-mc-di-input-"+depositId).style.display="none";
   document.getElementById("up_debit-lable-"+depositId).style.display="block";
   document.getElementById("up_debit-input-"+depositId).style.display="none";
   //----------
  }
  //----------------------------------------------------------------
  updatePOSDeposit(depositId){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let vi_mc_di_actual_amt = +(document.getElementById("up_vi_mc_di_"+depositId) as HTMLInputElement).value;
    let debit_amt = +(document.getElementById("up_debit_amt_"+depositId) as HTMLInputElement).value;
    
    const updatePOSDetails = {
      'depositId': depositId,
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'visaMasterDiscoverDepositAmount': vi_mc_di_actual_amt,
      'debitDepositAmount': debit_amt,
      'depositDate': this.currentDate
    }
   // console.log(updatePOSDetails);
   //Call api------------
    this.depositService.updatePOSDeposit(updatePOSDetails).subscribe(res=>{
     this.progressBarHide();
      //console.log(res);
       if(res.status=='success') {
        //Button---------------
        document.getElementById("action-box-"+depositId).style.display="block";
        document.getElementById("save-box-"+depositId).style.display="none";
        //---------------------
        //input-----
        document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="block";
        document.getElementById("up_vi-mc-di-input-"+depositId).style.display="none";
        document.getElementById("up_debit-lable-"+depositId).style.display="block";
        document.getElementById("up_debit-input-"+depositId).style.display="none";
        //----------
        
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.getCMOandDepositTotal();
        this.getPosDepositList();
        setTimeout(() => {
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
       
    });
    //--------------------
    
  }
  //----------------------------------------------------------------
  
  //Change Branch----------------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    if (this.isChanged ==false) {
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    this.getCMOandDepositTotal();
    this.getPosDepositList();
    }
    else{
       Swal.fire({
       // title: 'Are you want to save?',
        text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Save',
        cancelButtonText: 'Ignore'
        }).then((result) => {
          if (result.value) {
            this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
          }
          else {
            this.filterBranchId=BranchId;
            localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
            this.getCMOandDepositTotal();
            this.getPosDepositList();
            this.isChanged=false;
          }
        });
    }
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getPosDepositList();
   
  }
  //----------------------------------------------------------
  
  //Check Is Change-------------------------------------------------
  checkChange(){
    this.isChanged=true;
  }
  //----------------------------------------------------------------
  //Route-----------------------------------------------------------
  routeLink(route){
    if (route=='../deposit' && this.isChanged ==false) {
     this.router.navigate(['../deposit']) 
    }
     else if (route=='../deposit/checks' && this.isChanged == false) {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='../deposit/cc-handhelds' && this.isChanged == false) {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='../deposit/pos' && !this.isChanged) {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='../deposit/amex' && this.isChanged == false) {
      this.router.navigate(['../deposit/amex']) 
    }
    else if(route=='../deposit/other-daily-deposits' && this.isChanged == false){
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    else{
      this.routeConfirm(route);  
    }
  }
  //----------------------------------------------------------
  //Route Confirm alert---------------------------------------
  routeConfirm(link){
     Swal.fire({
    //title: 'Are you want to save?',
    text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Ignore'
    }).then((result) => {
      if (result.value) {
        
      }
      else {
        this.router.navigate([link]) 
      }
    });
  }
  //----------------------------------------------------------
    //Is Reviewd Function--------------------------------------------
  isReviewed(event,Index){
    
    if (event.checked==true) {
      this.isReviewdIndexArray.push(Index);
    }
    else{
      for( var i = 0; i < this.isReviewdIndexArray.length; i++){ 
        if ( this.isReviewdIndexArray[i] === Index) {
          this.isReviewdIndexArray.splice(i, 1); 
        }
      }
    }
   //CC Submit button Enabled/Disabled------------------------------
   if (this.totalPOSResult==this.isReviewdIndexArray.length) {
    this.posSubmitButtonEnaDis=false;
    this.isChanged=true;
   }
   else{
    
    this.posSubmitButtonEnaDis=true;
   }
   //---------------------------------------------------------------
  }
  //----------------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      if(res.status=='success') {
       (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
       (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
   progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  isInt(n) {
   return n % 1 === 0;
  }

}
