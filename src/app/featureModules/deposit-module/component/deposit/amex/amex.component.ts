import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';
import Swal from 'sweetalert2';

//------------------------------
export interface AMEXDataList {
  Index:number;
  CloseId : number;
  DepositId:number;
  BankId:number;
  AmexDepositAmount:number;
  AmexSuggestAmount:number;
  PosId:number;
  RegisterId:number;
  RegisterNumber:number;
  SiteId:number;
  SiteName:number;
  StatusId:number;
  StatusName:string;
}

export interface AMEXSubmitData {
  BankId:number;
  CloseId:number;
  PosId:number;
}
//------------------------

@Component({
  selector: 'app-amex',
  templateUrl: './amex.component.html',
  styleUrls: ['./amex.component.css']
})
export class AmexComponent implements OnInit {
  
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  amexDataSource:any;
  amexSlipDataSource:any;
  amexIds:Array<any> = [];
  selection: any;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  submitButtonDisable:boolean=false;
  showAmexDeposit:boolean=false;
  showAmexDepositList:boolean=false;
  isChanged:boolean=false;
  
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private router:Router, public depositService:DepositService) { }
   
  //Table columns for AMEX-------------
  displayedColumnsAMEX: string[] = ['select', 'site_name', 'register', 'amex_suggested', 'amex_actual'];
  displayedColumnsAMEXSlip: string[] = ['site_name', 'register', 'amex_suggested','amex_actual','status','action'];
  //------------------------------------
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.amexDataSource = new MatTableDataSource<AMEXDataList>([]);
    this.selection = new SelectionModel<AMEXDataList>(true, []);
    
   // this.amexDataSource = AMEX_DATA;
    this.getCMOandDepositTotal();
    this.getAmexDepositList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Select all module-----------------------------------------
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.amexDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.amexDataSource.data.forEach(row => this.selection.select(row));
  }
  
  
    /** The label for the checkbox on the passed row */
  checkboxLabel(row?: AMEXDataList): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    
    //For getting selected site id------------------------
    if(this.selection.isSelected(row)==true) {
     this.getAmexIds(true,row.Index);
    }
    else{
     this.getAmexIds(false,row.Index);
     for( var i = 0; i < this.amexIds.length; i++){ 
          if ( this.amexIds[i] === row.Index) {
            this.amexIds.splice(i, 1); 
          }
        }
    }
    //-----------------------------------------------------
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.Index + 1}`;
  }
  
  //----------------------------------------------------------
  //Get AMEX deposit list-------------------------------------
  getAmexDepositList(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var amexDepositListArray=[];
    var amexDepositSlipListArray=[];
    
    const amexDepositDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    // console.log(amexDepositDetails);
    //call api----------------------
    this.depositService.getAmexDepositList(amexDepositDetails).subscribe(res=>{
     this.progressBarHide();
     // console.log(res);
       if(res.status=='success') {
        let j=0;
        let i=0;
        var amexCloseIds= [];
        //New deposit-----------------------------------
        if (res.data.NewDeposit.length >0) {
          res.data.NewDeposit.forEach(function (value) {
          let amexData =  {} as AMEXDataList;
              amexData.Index=j;
              amexData.AmexDepositAmount=value.AmexDepositAmount;
              amexData.AmexSuggestAmount=value.AmexSuggestAmount;
              amexData.CloseId=value.CloseId;
              amexData.DepositId=value.DepositId;
              amexData.BankId=value.BankId;
              amexData.PosId=value.PosId;
              amexData.RegisterId=value.RegisterId;
              amexData.RegisterNumber=value.RegisterNumber;
              amexData.SiteId=value.SiteId;
              amexData.SiteName=value.SiteName;
              amexData.StatusId=value.StatusId;
              amexData.StatusName=value.StatusName;
              
              amexCloseIds.push(j);
              
              amexDepositListArray.push(amexData);
              
              j++;
          });
          var AMEX_DATA: AMEXDataList[] = amexDepositListArray;
          this.amexDataSource = new MatTableDataSource<AMEXDataList>(AMEX_DATA);
          this.selection = new SelectionModel<AMEXDataList>(true, AMEX_DATA);
          this.showAmexDeposit=true;
          
          for(let b = 0; b < amexCloseIds.length; b++){
          this.amexIds.push(amexCloseIds[b]);
          }
          
        }
        else{
          this.showAmexDeposit=false;
        }
        //---------------------------------------------
        //Old Deposit----------------------------------
        if (res.data.OldDeposit.length >0) {
           res.data.OldDeposit.forEach(function (value) {
            let amexData =  {} as AMEXDataList;
              amexData.Index=i;
              amexData.AmexDepositAmount=value.AmexDepositAmount;
              amexData.AmexSuggestAmount=value.AmexSuggestAmount;
              amexData.CloseId=value.CloseId;
              amexData.DepositId=value.DepositId;
              amexData.BankId=value.BankId;
              amexData.PosId=value.PosId;
              amexData.RegisterId=value.RegisterId;
              amexData.RegisterNumber=value.RegisterNumberList;
              amexData.SiteId=value.SiteId;
              amexData.SiteName=value.SiteName;
              amexData.StatusId=value.StatusId;
              amexData.StatusName=value.StatusName;
              
              amexDepositSlipListArray.push(amexData);
              
              i++;
          });
         
          var AMEX_OLD_DATA: AMEXDataList[] = amexDepositSlipListArray;
          this.amexSlipDataSource = new MatTableDataSource<AMEXDataList>(AMEX_OLD_DATA);
          this.showAmexDepositList=true;
        }
        else{
          this.showAmexDepositList=false;
        }
        //---------------------------------------------
       }
       else{
        this.showAmexDeposit=false;
        this.showAmexDepositList=false;
       }
       
    });
    //------------------------------
  }
  //----------------------------------------------------------
  //Get Amex ids---------------------------------------------------
  getAmexIds(event,amex_id){
    //console.log(event+'-'+pickup_id);
   if (event==true){
    this.amexIds.push(amex_id);
    }
    else{
      for( var i = 0; i < this.amexIds.length; i++){ 
          if ( this.amexIds[i] === amex_id) {
            this.amexIds.splice(i, 1); 
          }
        }
    }
    
    if (this.amexIds.length >=1) {
      this.submitButtonDisable = false;
     // this.isChanged=true;
    }
    else{
      this.submitButtonDisable = true;
      //this.isChanged=false;
    }

  }
  //-----------------------------------------------------------------
  //Amex Form Submit-------------------------------------------------
  amexFormSubmit(){
    
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let amexTotalAmt=0;
    var amexDataSubmitArray=[];
    var uniqueAmexIds = this.amexIds.filter((v, i, a) => a.indexOf(v) === i);
    
    uniqueAmexIds.forEach(function (i) {
     let CloseId=+(document.getElementById("close_id_"+i) as HTMLInputElement).value; 
     let BankId=+(document.getElementById("bank_id_"+i) as HTMLInputElement).value;
     let PosId=+(document.getElementById("pos_id_"+i) as HTMLInputElement).value;
     let amexAmount=+(document.getElementById("amex_actual_"+i) as HTMLInputElement).value;
     amexTotalAmt=amexTotalAmt+amexAmount;
     
     let AMEXSubmitData =  {} as AMEXSubmitData;
         AMEXSubmitData.BankId=BankId;
         AMEXSubmitData.PosId=PosId;
         AMEXSubmitData.CloseId=CloseId;
         
      amexDataSubmitArray.push(AMEXSubmitData);
         
     });
    
    const amexSubmitDetails = {
      'branchId': branchId,
      'createBy': this.authUserId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate,
      'amexAmount' : amexTotalAmt,
      'amexDepositDetails': JSON.stringify(amexDataSubmitArray)
    }
    //console.log(amexSubmitDetails);
   //call api----------------------
   
    this.depositService.submitAmexDeposit(amexSubmitDetails).subscribe(res=>{
     this.progressBarHide();
       if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.isChanged=false;
        this.getCMOandDepositTotal();
        setTimeout(() => {
          this.getAmexDepositList();
          this.successmessage='';
        }, 2000);
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        this.submitButtonDisable=false;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
    });
    
   //------------------------------
    
  }
  //-----------------------------------------------------------------
  //Edit AMEX Deposit-----------------------------------------
  editAMEXDeposit(depositId){
   //console.log('DepositID:'+depositId);
   //button----
   document.getElementById("action-box-"+depositId).style.display="none";
   document.getElementById("save-box-"+depositId).style.display="block";
   //----------
   //input-----
   document.getElementById("amex-lable-"+depositId).style.display="none";
   document.getElementById("amex-input-"+depositId).style.display="block";
   //----------
  }
  //-----------------------------------------------------------------
  //Edit AMEX Deposit Cancel----------------------------------
  cancelAMEXDeposit(depositId){
   //Button---
   document.getElementById("action-box-"+depositId).style.display="block";
   document.getElementById("save-box-"+depositId).style.display="none";
   //---------
   //input-----
   document.getElementById("amex-lable-"+depositId).style.display="block";
   document.getElementById("amex-input-"+depositId).style.display="none";
   //----------
  }
  //-----------------------------------------------------------------
  //Update AMEX deposit----------------------------------------------
  updateAmexDeposit(depositId){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let amex_actual_amt = +(document.getElementById("up_amex_"+depositId) as HTMLInputElement).value;
   // console.log("amex_actual_amt:"+amex_actual_amt);
    const updateAmexDetails = {
      'depositId': depositId,
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'amexAmount': amex_actual_amt,
      'depositDate': this.currentDate
    }
     //console.log(updateAmexDetails);
     this.depositService.updateAmexDeposit(updateAmexDetails).subscribe(res=>{
     this.progressBarHide();
    // console.log(res);
        if(res.status=='success') {
        //Button---------------
        document.getElementById("action-box-"+depositId).style.display="block";
        document.getElementById("save-box-"+depositId).style.display="none";
        //---------------------
        //input-----
        document.getElementById("amex-lable-"+depositId).style.display="block";
        document.getElementById("amex-input-"+depositId).style.display="none";
        //----------
        
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.getCMOandDepositTotal();
        this.getAmexDepositList();
        setTimeout(() => {
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
     });
  }
  //-----------------------------------------------------------------
  //Route------------------------------------------------------------
  routeLink(route){
    if (route=='../deposit' && this.isChanged ==false) {
     this.router.navigate(['../deposit']) 
    }
    else if (route=='../deposit/checks' && this.isChanged == false) {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='../deposit/cc-handhelds' && this.isChanged == false) {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='../deposit/pos' && !this.isChanged) {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='../deposit/amex' && this.isChanged == false) {
      this.router.navigate(['../deposit/amex']) 
    }
    else if(route=='../deposit/other-daily-deposits' && this.isChanged == false){
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    else{
      this.routeConfirm(route);  
    }
  }
  //----------------------------------------------------------
  //Route Confirm alert-------------------------------------
  routeConfirm(link){
     Swal.fire({
    //title: 'Are you want to save?',
    text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Ignore'
    }).then((result) => {
      if (result.value) {
        
      }
      else {
        this.router.navigate([link]) 
      }
    });
  }
  //----------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      if(res.status=='success') {
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
    //Change Branch----------------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    if (this.isChanged ==false) {
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    this.getCMOandDepositTotal();
    this.getAmexDepositList();
    }
    else{
      Swal.fire({
       // title: 'Are you want to save?',
        text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Save',
        cancelButtonText: 'Ignore'
        }).then((result) => {
          if (result.value) {
            //this.isChanged=false;
            //return false;
            this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
          }
          else {
            this.filterBranchId=BranchId;
            localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
            this.getCMOandDepositTotal();
            this.getAmexDepositList();
            this.isChanged=false;
          }
        });
    }
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getAmexDepositList();
  }
  //----------------------------------------------------------
  //Check Is Change-------------------------------------------------
  checkChange(){
    this.isChanged=true;
  }
  //----------------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }
  
}
