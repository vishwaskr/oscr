import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcHandheldsComponent } from './cc-handhelds.component';

describe('CcHandheldsComponent', () => {
  let component: CcHandheldsComponent;
  let fixture: ComponentFixture<CcHandheldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcHandheldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcHandheldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
