import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';
import Swal from 'sweetalert2';

export interface CCHandheldList {
  Index: number;
  BankId: number;
  CloseId : number;
  DepositId: number;
  SiteId: number;
  SiteName: string;
  RegisterId: number;
  RegisterNumber: number;
  CardHandheldTidNumber: number;
  DebitSuggestAmount: number;
  DebitDepositAmount: number;
  BatchoutNumber: number;
  VisaMasterDiscoverSuggestAmount: number;
  VisaMasterDiscoverDepositAmount: number;
  StatusName: string;
}

export interface CCHandheldSubmitData {
  BankId: number;
  CloseId : number;
  RegisterId : number;
  SiteId : number;
  BatchoutNumber : string;
  CardHandheldTidNumber : string;
  VisaMasterDiscoverDepositAmount : number;
  DebitAmount : number;
}

@Component({
  selector: 'app-cc-handhelds',
  templateUrl: './cc-handhelds.component.html',
  styleUrls: ['./cc-handhelds.component.css']
})
export class CcHandheldsComponent implements OnInit {
  
  ccDepositShow:boolean=false;
  ccDepositSlipShow:boolean=false;
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  
  ccHandheldDataSource:any;
  ccHandheldDepositListDataSource:any;
  ccIdsArray:Array<any> = [];
  totalCCHandheldResult:number;
  ccSubmitButtonEnaDis:boolean=false;
  isChanged:boolean=false;
  selection: any;
 //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private router:Router, public depositService:DepositService) { }
  
  //For CC-Handheld list table------------
  displayedColumns3: string[] = ['select','site_name', 'register',  'device_id', 'batch_no', 'vi_mc_di_suggested', 'vi_mc_di_actual', 'debit_suggested', 'debit_actual'];
  displayedColumns4: string[] = ['site_name', 'register',  'device_id', 'batch_no', 'vi_mc_di_suggested', 'vi_mc_di_actual', 'debit_suggested', 'debit_actual','status','action'];
  //----------------------------------

  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.ccHandheldDataSource = new MatTableDataSource<CCHandheldList>([]);
    this.selection = new SelectionModel<CCHandheldList>(true, []);
    
    this.getCMOandDepositTotal();
    this.getCCHandheldDepositList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Select all module-----------------------------------------
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.ccHandheldDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.ccHandheldDataSource.data.forEach(row => this.selection.select(row));
  }
  
  
    /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CCHandheldList): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    
    //For getting selected site id------------------------
    if(this.selection.isSelected(row)==true) {
     this.selectCCids(true,row.Index);
    }
    else{
     this.selectCCids(false,row.Index);
     for( var i = 0; i < this.ccIdsArray.length; i++){ 
          if ( this.ccIdsArray[i] === row.Index) {
            this.ccIdsArray.splice(i, 1); 
          }
        }
    }
    //-----------------------------------------------------
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.Index + 1}`;
  }
  
  
  //Is Reviewd Function--------------------------------------------
  selectCCids(event,Index){
    
    if (event==true) {
      this.ccIdsArray.push(Index);
    }
    else{
      for( var i = 0; i < this.ccIdsArray.length; i++){ 
        if ( this.ccIdsArray[i] === Index) {
          this.ccIdsArray.splice(i, 1); 
        }
      }
    }
   //CC Submit button Enabled/Disabled------------------------------
   if (this.ccIdsArray.length >=1) {
    this.ccSubmitButtonEnaDis=false;
    //this.isChanged=true;
   }
   else{
    this.ccSubmitButtonEnaDis=true;
   }
   //---------------------------------------------------------------
  }
  //----------------------------------------------------------------
  
  //CC-Handheld Deposit----------------------------------------------
  //Fetch list-----------------
  getCCHandheldDepositList(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var ccHandheldDataArray=[];
    var ccHandheldDepositDataArray=[];
    
    const ccHandheldDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //call api--------------
    this.depositService.getCCHandheldDepositList(ccHandheldDetails).subscribe(res=>{
       this.progressBarHide();
       //console.log(res);
       if(res.status=='success') {
        //For new cchandheld list---------------------------
        let j=0;
        
        if (res.data.NewDeposit.length > 0) {
        res.data.NewDeposit.forEach(function (value) {
         let ccHandheldData =  {} as CCHandheldList;
          ccHandheldData.Index=j;
          ccHandheldData.BankId = value.BankId;
          ccHandheldData.CloseId = value.CloseId;
          ccHandheldData.BatchoutNumber=value.BatchoutNumber;
          ccHandheldData.CardHandheldTidNumber=value.CardHandheldTidNumber;
          ccHandheldData.DebitSuggestAmount=value.DebitSuggestAmount;
          ccHandheldData.RegisterId=value.RegisterId;
          ccHandheldData.RegisterNumber=value.RegisterNumber;
          ccHandheldData.SiteId=value.SiteId;
          ccHandheldData.SiteName=value.SiteName;
          ccHandheldData.VisaMasterDiscoverSuggestAmount=value.VisaMasterDiscoverSuggestAmount;
          
          //var employeeArr=[];
          //var employeeIMGArr=[];
          //let i=0;
          //value.Employee.forEach(function (emp) {
          //  employeeArr.push(emp.Name);
          //  employeeIMGArr[i]=emp.ProfileImage;
          //  i++;
          //});
          //ccHandheldData.EmployeeImage=employeeIMGArr;
          //ccHandheldData.EmployeeName=employeeArr;
          
          ccHandheldDataArray.push(ccHandheldData);
          j++;
        });
        this.totalCCHandheldResult=j;
        var CCHANDHELD_DATA: CCHandheldList[]=ccHandheldDataArray;
        this.ccHandheldDataSource = new MatTableDataSource<CCHandheldList>(CCHANDHELD_DATA);
        this.selection = new SelectionModel<CCHandheldList>(true, CCHANDHELD_DATA);
        this.ccDepositShow=true;
        }
        else{
          this.ccDepositShow=false;
        }
        
        //-----------------------------------------------
        //For ccHandheld deposit list--------------------
        if (res.data.OldDeposit.length > 0) {
        res.data.OldDeposit.forEach(function (value) {
          let ccHandheldDepositData =  {} as CCHandheldList;
          ccHandheldDepositData.CloseId = value.CloseId;
          ccHandheldDepositData.DepositId = value.DepositId;
          ccHandheldDepositData.BatchoutNumber=value.BatchoutNumber;
          ccHandheldDepositData.CardHandheldTidNumber=value.CardHandheldTidNumber;
          ccHandheldDepositData.DebitSuggestAmount=value.DebitSuggestAmount;
          ccHandheldDepositData.DebitDepositAmount=value.DebitDepositAmount;
          ccHandheldDepositData.RegisterId=value.RegisterId;
          ccHandheldDepositData.RegisterNumber=value.RegisterNumberList;
          ccHandheldDepositData.SiteId=value.SiteId;
          ccHandheldDepositData.SiteName=value.SiteName;
          ccHandheldDepositData.VisaMasterDiscoverSuggestAmount=value.VisaMasterDiscoverSuggestAmount;
          ccHandheldDepositData.VisaMasterDiscoverDepositAmount=value.VisaMasterDiscoverDepositAmount;
          ccHandheldDepositData.StatusName=value.StatusName;
          
          ccHandheldDepositDataArray.push(ccHandheldDepositData);
          
        });
        var CCHANDHELD_DEPOSIT_DATA: CCHandheldList[]=ccHandheldDepositDataArray;
        this.ccHandheldDepositListDataSource = CCHANDHELD_DEPOSIT_DATA;
        this.ccDepositSlipShow=true;
        }
        else{
          this.ccDepositSlipShow=false;
        }
        //-----------------------------------------------
       }
       else{
        this.ccDepositSlipShow=false;
        this.ccDepositShow=false;
       }
    });
    //----------------------
  }
  //---------------------

  //----------------------------------------------------------------
  //CCHandheldFormSubmit--------------------------------------------
  CCHandheldFormSubmit(){
    this.ccSubmitButtonEnaDis=true;
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
     
    //For getting vi_mc_di actual amount------------
     var ccHandheldSubmitDataArray=[];
  
     var ccIdsIds = this.ccIdsArray.filter((v, i, a) => a.indexOf(v) === i);
     let visaMasterDiscoverDepositAmount=0;
     let debitAmount=0;
      ccIdsIds.forEach(function (i) {
        let tid=(document.getElementById("tid_"+i) as HTMLInputElement).value;
        let vi_mc_di_actual_amt = +(document.getElementById("vi_mc_di_"+i) as HTMLInputElement).value;
        let debit_amt = +(document.getElementById("debit_amt_"+i) as HTMLInputElement).value;
        let batchout_no = (document.getElementById("batchout_no_"+i) as HTMLInputElement).value;
        let register_id = +(document.getElementById("register_id_"+i) as HTMLInputElement).value;
        let site_id = +(document.getElementById("site_id_"+i) as HTMLInputElement).value;
        let close_id = +(document.getElementById("close_id_"+i) as HTMLInputElement).value;
        let bank_id = +(document.getElementById("bank_id_"+i) as HTMLInputElement).value;
        
        visaMasterDiscoverDepositAmount=visaMasterDiscoverDepositAmount+vi_mc_di_actual_amt;
        debitAmount=debitAmount+debit_amt;
        
        let CCHandheldSubmitData =  {} as CCHandheldSubmitData;
             CCHandheldSubmitData.BankId = bank_id;
             CCHandheldSubmitData.CloseId = close_id;
             CCHandheldSubmitData.BatchoutNumber = batchout_no;
             CCHandheldSubmitData.CardHandheldTidNumber = tid;
             
             ccHandheldSubmitDataArray.push(CCHandheldSubmitData);
      });
      //console.log(ccHandheldSubmitDataArray);
    //----------------------------------------------
    const ccHandheldSubmitDetails = {
      'branchId': branchId,
      'createBy': this.authUserId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate,
      'visaMasterDiscoverDepositAmount': visaMasterDiscoverDepositAmount,
      'debitAmount':debitAmount,
      'cardHandheldDetails': JSON.stringify(ccHandheldSubmitDataArray)
    }
    //Call api---------------------------------------
   // console.log(ccHandheldSubmitDetails);
    
    this.depositService.submitCCHandheldDeposit(ccHandheldSubmitDetails).subscribe(res=>{
      console.log(res);
     this.progressBarHide();
       if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.isChanged=false;
        this.getCMOandDepositTotal();
        setTimeout(() => {
          this.getCCHandheldDepositList();
          this.successmessage='';
        }, 2000);
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        this.ccSubmitButtonEnaDis=false;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       } 
    });
    
    //----------------------------------------------- 
  }
  //-----------------------------------------------------------------
  //Edit CC-Handheld Deposit-----------------------------------------
  editCChandheldDeposit(depositId){
   //console.log('DepositID:'+depositId);
   //button----
   document.getElementById("action-box-"+depositId).style.display="none";
   document.getElementById("save-box-"+depositId).style.display="block";
   //----------
   //input-----
   document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="none";
   document.getElementById("up_vi-mc-di-input-"+depositId).style.display="block";
   
   document.getElementById("up_debit-lable-"+depositId).style.display="none";
   document.getElementById("up_debit-input-"+depositId).style.display="block";
   //----------
  }
  //-----------------------------------------------------------------
  //Edit CC-Handheld Deposit Cancel----------------------------------
  cancelCChandheldDeposit(depositId){
   //Button---
   document.getElementById("action-box-"+depositId).style.display="block";
   document.getElementById("save-box-"+depositId).style.display="none";
   //---------
   //input-----
   document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="block";
   document.getElementById("up_vi-mc-di-input-"+depositId).style.display="none";
   document.getElementById("up_debit-lable-"+depositId).style.display="block";
   document.getElementById("up_debit-input-"+depositId).style.display="none";
   //----------
  }
  //-----------------------------------------------------------------
  //Update CC-Handheld Deposit---------------------------------------
  updateCChandheldDeposit(depositId){
    this.progressBarShow();
   
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let vi_mc_di_actual_amt = +(document.getElementById("up_vi_mc_di_"+depositId) as HTMLInputElement).value;
    let debit_amt = +(document.getElementById("up_debit_amt_"+depositId) as HTMLInputElement).value;
    //console.log(vi_mc_di_actual_amt+'-'+debit_amt);
    
    const updateCCHandheldDetails = {
      'depositId': depositId,
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'visaMasterDiscoverAmount': vi_mc_di_actual_amt,
      'debitAmount': debit_amt,
      'depositDate': this.currentDate
    }
    //console.log(updateCCHandheldDetails);
    //Call api------------
    this.depositService.updateCChandheldDeposit(updateCCHandheldDetails).subscribe(res=>{
     this.progressBarHide();
      //console.log(res);
       if(res.status=='success') {
        //Button---------------
        document.getElementById("action-box-"+depositId).style.display="block";
        document.getElementById("save-box-"+depositId).style.display="none";
        //---------------------
        //input-----
        document.getElementById("up_vi-mc-di-lable-"+depositId).style.display="block";
        document.getElementById("up_vi-mc-di-input-"+depositId).style.display="none";
        document.getElementById("up_debit-lable-"+depositId).style.display="block";
        document.getElementById("up_debit-input-"+depositId).style.display="none";
        //----------
        
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.getCMOandDepositTotal();
        this.getCCHandheldDepositList();
        setTimeout(() => {
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
       
    });
    //--------------------
    
  }
  //-----------------------------------------------------------------
  
  //Change Branch----------------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    if (this.isChanged ==false) {
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    this.getCMOandDepositTotal();
    this.getCCHandheldDepositList();
    }
    else{
      Swal.fire({
        //title: 'Are you want to save?',
        text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Save',
        cancelButtonText: 'Ignore'
        }).then((result) => {
          if (result.value) {
            //this.isChanged=false;
            //return false;
            this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
          }
          else {
            this.filterBranchId=BranchId;
            localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
            this.getCMOandDepositTotal();
            this.getCCHandheldDepositList();
            this.isChanged=false;
          }
        });
    }
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getCCHandheldDepositList();
  }
  //----------------------------------------------------------
  //Check Is Change-------------------------------------------------
  checkChange(){
    this.isChanged=true;
  }
  //----------------------------------------------------------------
  //Route-----------------------------------------------------
  routeLink(route){
    if (route=='../deposit' && this.isChanged ==false) {
     this.router.navigate(['../deposit']) 
    }
    else if (route=='../deposit/checks' && this.isChanged == false) {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='../deposit/cc-handhelds' && this.isChanged == false) {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='../deposit/pos' && !this.isChanged) {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='../deposit/amex' && this.isChanged == false) {
      this.router.navigate(['../deposit/amex']) 
    }
    else if(route=='../deposit/other-daily-deposits' && this.isChanged == false){
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    else{
      this.routeConfirm(route);  
    }
  }
  //----------------------------------------------------------
  //Route Confirm alert---------------------------------------
  routeConfirm(link){
     Swal.fire({
    //title: 'Are you want to save?',
    text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Ignore'
    }).then((result) => {
      if (result.value) {
        
      }
      else {
        this.router.navigate([link]) 
      }
    });
  }
  //----------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      //console.log(res);
      if(res.status=='success') {
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }

}
