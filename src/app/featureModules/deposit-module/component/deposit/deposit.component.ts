import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';

export interface NewDepositData {
  EmployeeName : string;
  EmployeeImage : string;
  PickupId : number;
  PickupNumber : number;
  RegisterNumber : number;
  SiteName : string;
  SuggestedAmount : number;
  TypeName : string;
}


export interface DepositSlip {
  ArmoredDate: string;
  Bag: number;
  Comment: string;
  DepositAmount: number;
  DepositDate: any;
  DepositId: number;
  PickupPerson: string;
  TrackingNumber: string;
  StatusName: string;
}


@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})


export class DepositComponent implements OnInit {
  
  newDepositShowHide:boolean=false;
  showCashCoinDeposit:boolean=false;
  showCashCoinDepositList:boolean=false;
  
  pickupIds:Array<any> = [];
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  pickupDataSource:any;
  depositSlipDataSource:any;
  ccHandheldDataSource:any;
  ccHandheldDepositListDataSource:any;
  posDataSource:any;
  amexDataSource:any;
  isReviewdIndexArray:Array<any> = [];
  totalCCHandheldResult:number;
  ccSubmitButtonEnaDis:boolean=true;
   
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private router:Router, public dialog: MatDialog, public depositService:DepositService) {}

  //Pickup list table-----------------
  displayedColumns: string[] = ['select','picup_number','pickup_type', 'site_name',  'register_number', 'pickup_person', 'amount'];
  //----------------------------------
  //For Deposit list table------------
  displayedColumns2: string[] = ['tracking_no', 'deposit_amount',  'armored_date', 'bag', 'pickup_person', 'comment', 'status', 'action'];
  //----------------------------------
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getCMOandDepositTotal();
    this.getCashCoinsPickupList();
  }
  //-----------------------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Get Cash-Coins pickup lists--------------------------------------
  getCashCoinsPickupList(){
   this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const pickupDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
   // console.log(pickupDetails);
    var pickupListArray= [];
    var depositSlipArray= [];
    
    //call api---------------------
    this.depositService.getCashCoinPickupList(pickupDetails).subscribe(res=>{
     this.progressBarHide();
    // console.log(res);
     if(res.status=='success') {
      //For Pickup list--------------------------------
      if (res.data.NewDeposit.length >0) {
      res.data.NewDeposit.forEach(function (value) {
        let pickupData =  {} as NewDepositData;
          pickupData.EmployeeName = value.Employee[0]['Name'];
          pickupData.EmployeeImage = value.Employee[0]['ProfileImage'];
          pickupData.PickupId = value.PickupId;
          pickupData.PickupNumber = value.PickupNumber;
          pickupData.RegisterNumber = value.RegisterNumber;
          pickupData.SiteName = value.SiteName;
          pickupData.SuggestedAmount = value.SuggestedAmount;
          pickupData.TypeName = value.TypeName;
          
          pickupListArray.push(pickupData);
      });
      var PICKUP_DATA: NewDepositData[] = pickupListArray;
      this.pickupDataSource = new MatTableDataSource<NewDepositData>(PICKUP_DATA);
      this.showCashCoinDeposit=true;
      }
      else{
      this.showCashCoinDeposit=false;
      }
      
      //----------------------------------------------
      //For Deposit Slip------------------------------
      if (res.data.OldDeposit.length > 0) {
        res.data.OldDeposit.forEach(function (value) {
        let depositSlip =  {} as DepositSlip;
            depositSlip.ArmoredDate='';
            if (value.ArmoredDate!=null) {
              depositSlip.ArmoredDate=moment(value.ArmoredDate).format(dateFormat);
            }
            depositSlip.Bag= value.Bag;
            depositSlip.Comment= value.Comment;
            depositSlip.DepositAmount= value.DepositAmount;
            depositSlip.DepositDate = moment(value.DepositDate).format(dateFormat);
            depositSlip.DepositId = value.DepositId;
            depositSlip.PickupPerson = value.PickupPerson;
            depositSlip.TrackingNumber = value.TrackingNumber;
            depositSlip.StatusName = value.StatusName;
            
            depositSlipArray.push(depositSlip);
        });
        this.showCashCoinDepositList=true;
        var DEPOSIT_SLIP_DATA: DepositSlip[] = depositSlipArray;
        this.depositSlipDataSource = new MatTableDataSource<DepositSlip>(DEPOSIT_SLIP_DATA);
      }
      else{
        this.showCashCoinDepositList=false;
      }
      //----------------------------------------------
      
     }
     else{
      var DEPOSIT_DATA: NewDepositData[] = [];
      this.pickupDataSource = new MatTableDataSource<NewDepositData>(PICKUP_DATA);
      
      var DEPOSIT_SLIP_DATA: DepositSlip[] = [];
      this.depositSlipDataSource = new MatTableDataSource<DepositSlip>(DEPOSIT_SLIP_DATA);
     }
     
    });
    //-----------------------------
  }
  //-----------------------------------------------------------------
  //Get Pickup ids---------------------------------------------------
  getPickupIds(event,pickup_id){
    //console.log(event+'-'+pickup_id);
   if (event.source._checked==true){
    this.pickupIds.push(pickup_id);
    }
    else{
      for( var i = 0; i < this.pickupIds.length; i++){ 
          if ( this.pickupIds[i] === pickup_id) {
            this.pickupIds.splice(i, 1); 
          }
        }
    }
    
    if (this.pickupIds.length >=1) {
      this.newDepositShowHide = true;
    }
    else{
      this.newDepositShowHide = false;
    }
    //console.log(this.pickupIds);
    sessionStorage.setItem("pickupIds",JSON.stringify(this.pickupIds));
  }
  //-----------------------------------------------------------------
  
  //For open new deposit popup---------------------------------------
  openNewDepositPopup(){
    const dialogRef = this.dialog.open(NewDeposit, {
      disableClose: true,
      width: '',
      data: {}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      this.pickupIds=[];
      if (sessionStorage.getItem("dialogOpend")=='false') {
        this.newDepositShowHide=false;
        this.getCashCoinsPickupList();
      }
    });
  }
  //-----------------------------------------------------------------
  //Open edit cash coin deposit--------------------------------------
  openEditCashCoinPopup(){
   const dialogRef = this.dialog.open(EditCashCoinDeposit, {
      disableClose: true,
      width: '',
      data: {}
    });
   
    dialogRef.afterClosed().subscribe(result => {
      if (sessionStorage.getItem("dialogOpend")=='false') {
        sessionStorage.setItem("depositId",'');
        this.getCashCoinsPickupList();
      }
    });
  }
  //-----------------------------------------------------------------
  //Get tab Index----------------------------------------------------
  getTabIndex($event){
    //console.log($event.index);
    if ($event.index==0) {
      sessionStorage.setItem("pickupIds",'');
    }
    if ($event.index==1) { //Call getCCHandheldDepositList function
     // this.getCCHandheldDepositList();
    }
  }
  //-----------------------------------------------------------------
  //Edit Cash-Coin Deposit-------------------------------------------
  editCashCoinDeposit(depositId){
   // console.log('DepositId:'+depositId);
    sessionStorage.setItem("depositId",JSON.stringify(depositId));
    this.openEditCashCoinPopup();
  }
  //-----------------------------------------------------------------

  //Reload Function--------------------------------------------------
  reload(){
   this.ccSubmitButtonEnaDis=true;
   this.newDepositShowHide=false;
   this.getCashCoinsPickupList();
  // this.getCCHandheldDepositList(); 
  }
  //-----------------------------------------------------------------

  //Change Branch----------------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    //this.getIssuedBank();
    this.getCMOandDepositTotal();
    this.getCashCoinsPickupList();
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getCashCoinsPickupList();
  }
  //----------------------------------------------------------
  //Route-----------------------------------------------------
  routeLink(route){
    if (route=='cashcoin') {
     this.router.navigate(['../deposit']) 
    }
    else if (route=='../deposit/checks') {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='cc-handhelds') {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='pos') {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='amex') {
      this.router.navigate(['../deposit/amex']) 
    }
    else{
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    
  }
  //----------------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      if(res.status=='success') {
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }
  
  
}

//---------------------------------------------------------------------


//New deposit----------------------------------------------------------
export interface NewDepositData {
  
}

export interface PickupID {
 PickupId: number; 
}

export interface denominationDetail {
 HundredsCount:number;
 FiftiesCount:number;
 TwentiesCount:number;
 TensCount:number;
 FivesCount:number;
 TwosCount:number;
 OnesCount:number;
 QuartersCount:number;
 NickelsCount:number;
 DimesCount:number;
 PenniesCount:number;
 ChangeAmount:number;
}

@Component({
  selector: 'new-deposit',
  templateUrl: './new-deposit.html',
  styleUrls: ['./deposit.component.css']
})

export class NewDeposit implements OnInit {
  
  cashCoinDepositForm: FormGroup;
  pickupIds: any;
  authUserId:any;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
   showErrorMsg: boolean = false;
   errormessage:string;
   successmessage:string;
   submitDisabled:boolean;
  
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    change: number=0;
    totalCash:any;
    
  //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
  //------------------------------------
  
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<NewDeposit>,@Inject(MAT_DIALOG_DATA) public data: NewDepositData, public depositService:DepositService) {}
  //Popup close---------------
  onNoClick(): void {
    this.dialogRef.close();
  }
  //-------------------------
  ngOnInit() {
    
    this.pickupIds=JSON.parse(sessionStorage.getItem("pickupIds"));
    
    var pickupIdArr=[];
    this.pickupIds.forEach(function (value) {
      //console.log(value);
      let pickupIds =  {} as PickupID;
        pickupIds.PickupId = value;
        pickupIdArr.push(pickupIds);
    });
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getCashCoinDetails(pickupIdArr);
   // console.log(this.pickupIds);
    //CashCoin Deposit Form------------------------
    this.cashCoinDepositForm = new FormGroup({
      trackingNo: new FormControl(),
      SuggestHundredsCount: new FormControl(0),
      SuggestFiftiesCount: new FormControl(0),
      SuggestTwentiesCount: new FormControl(0),
      SuggestTensCount: new FormControl(0),
      SuggestFivesCount: new FormControl(0),
      SuggestTwosCount: new FormControl(0),
      SuggestOnesCount: new FormControl(0),
      SuggestQuartersCount: new FormControl(0),
      SuggestDimesCount: new FormControl(0),
      SuggestNickelsCount: new FormControl(0),
      SuggestPenniesCount: new FormControl(0),
      
      SuggestHundredsAmount: new FormControl(0),
      SuggestFiftiesAmount: new FormControl(0),
      SuggestTwentiesAmount: new FormControl(0),
      SuggestTensAmount: new FormControl(0),
      SuggestFivesAmount: new FormControl(0),
      SuggestTwosAmount: new FormControl(0),
      SuggestOnesAmount: new FormControl(0),
      SuggestQuartersAmount: new FormControl(0),
      SuggestDimesAmount: new FormControl(0),
      SuggestNickelsAmount: new FormControl(0),
      SuggestPenniesAmount: new FormControl(0),
      SuggestChangeAmount: new FormControl(0),
      
      hundredsCount: new FormControl(0),
      fiftiesCount: new FormControl(0),
      twentiesCount: new FormControl(0),
      tensCount: new FormControl(0),
      fivesCount: new FormControl(0),
      twosCount: new FormControl(0),
      onesCount: new FormControl(0),
      quartersCount: new FormControl(0),
      dimesCount: new FormControl(0),
      nicklesCount: new FormControl(0),
      penniesCount: new FormControl(0),
      
      hundreds: new FormControl(0),
      fifties: new FormControl(0),
      twenties: new FormControl(0),
      tens: new FormControl(0),
      fives: new FormControl(0),
      twos: new FormControl(0),
      ones: new FormControl(0),
      quarters: new FormControl(0),
      dimes: new FormControl(0),
      nickles: new FormControl(0),
      pennies: new FormControl(0),
      change: new FormControl()
    });
    //--------------------------------------------- 
  }
  //Get Cash Coin Details-------------------------
  getCashCoinDetails(pickupIdArr){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const cashCoinDetails = {
      'branchId' : branchId,
      'authUserId' : this.authUserId,
      'pickupList' : JSON.stringify(pickupIdArr),
      'depositDate': JSON.parse(sessionStorage.getItem("currentDate"))
    }
    //console.log(cashCoinDetails);
   //Call api----------------------------------
   this.depositService.getCashCoinDetails(cashCoinDetails).subscribe(res=>{
    //console.log(res);
    if(res.status=='success') {
      //Suggest Count---------------
      this.cashCoinDepositForm.controls['SuggestHundredsCount'].setValue(res.data.SuggestCount.HundredsCount);
      this.cashCoinDepositForm.controls['SuggestFiftiesCount'].setValue(res.data.SuggestCount.FiftiesCount);
      this.cashCoinDepositForm.controls['SuggestTwentiesCount'].setValue(res.data.SuggestCount.TwentiesCount);
      this.cashCoinDepositForm.controls['SuggestTensCount'].setValue(res.data.SuggestCount.TensCount);
      this.cashCoinDepositForm.controls['SuggestFivesCount'].setValue(res.data.SuggestCount.FivesCount);
      this.cashCoinDepositForm.controls['SuggestTwosCount'].setValue(res.data.SuggestCount.TwosCount);
      this.cashCoinDepositForm.controls['SuggestOnesCount'].setValue(res.data.SuggestCount.OnesCount);
      this.cashCoinDepositForm.controls['SuggestQuartersCount'].setValue(res.data.SuggestCount.QuartersCount);
      this.cashCoinDepositForm.controls['SuggestDimesCount'].setValue(res.data.SuggestCount.DimesCount);
      this.cashCoinDepositForm.controls['SuggestNickelsCount'].setValue(res.data.SuggestCount.NickelsCount);
      this.cashCoinDepositForm.controls['SuggestPenniesCount'].setValue(res.data.SuggestCount.PenniesCount);
      //----------------------------
      //Suggest Amount--------------
      this.cashCoinDepositForm.controls['SuggestHundredsAmount'].setValue(res.data.SuggestAmount.HundredsAmount);
      this.cashCoinDepositForm.controls['SuggestFiftiesAmount'].setValue(res.data.SuggestAmount.FiftiesAmount);
      this.cashCoinDepositForm.controls['SuggestTwentiesAmount'].setValue(res.data.SuggestAmount.TwentiesAmount);
      this.cashCoinDepositForm.controls['SuggestTensAmount'].setValue(res.data.SuggestAmount.TensAmount);
      this.cashCoinDepositForm.controls['SuggestFivesAmount'].setValue(res.data.SuggestAmount.FivesAmount);
      this.cashCoinDepositForm.controls['SuggestTwosAmount'].setValue(res.data.SuggestAmount.TwosAmount);
      this.cashCoinDepositForm.controls['SuggestOnesAmount'].setValue(res.data.SuggestAmount.OnesAmount);
      this.cashCoinDepositForm.controls['SuggestQuartersAmount'].setValue(res.data.SuggestAmount.QuartersAmount);
      this.cashCoinDepositForm.controls['SuggestDimesAmount'].setValue(res.data.SuggestAmount.DimesAmount);
      this.cashCoinDepositForm.controls['SuggestNickelsAmount'].setValue(res.data.SuggestAmount.NickelsAmount);
      this.cashCoinDepositForm.controls['SuggestPenniesAmount'].setValue(res.data.SuggestAmount.PenniesAmount);
      this.cashCoinDepositForm.controls['SuggestChangeAmount'].setValue(res.data.SuggestAmount.ChangeAmount);
      this.totalCash=res.data.SuggestAmount.TotalCash;
      //-----------------------------------------
      //Deposit Cash coin Count-----------------------
     this.cashCoinDepositForm.controls['hundredsCount'].setValue(res.data.DepositCount.HundredsCount);
     this.cashCoinDepositForm.controls['fiftiesCount'].setValue(res.data.DepositCount.FiftiesCount);
     this.cashCoinDepositForm.controls['twentiesCount'].setValue(res.data.DepositCount.TwentiesCount);
     this.cashCoinDepositForm.controls['tensCount'].setValue(res.data.DepositCount.TensCount);
     this.cashCoinDepositForm.controls['fivesCount'].setValue(res.data.DepositCount.FivesCount);
     this.cashCoinDepositForm.controls['twosCount'].setValue(res.data.DepositCount.TwosCount);
     this.cashCoinDepositForm.controls['onesCount'].setValue(res.data.DepositCount.OnesCount);
     this.cashCoinDepositForm.controls['quartersCount'].setValue(res.data.DepositCount.QuartersCount);
     this.cashCoinDepositForm.controls['dimesCount'].setValue(res.data.DepositCount.DimesCount);
     this.cashCoinDepositForm.controls['nicklesCount'].setValue(res.data.DepositCount.NickelsCount);
     this.cashCoinDepositForm.controls['penniesCount'].setValue(res.data.DepositCount.PenniesCount);
      //Deposit Cash coin amount-----------------------
      this.cashCoinDepositForm.controls['hundreds'].setValue(res.data.DepositAmount.HundredsAmount);
      this.cashCoinDepositForm.controls['fifties'].setValue(res.data.DepositAmount.FiftiesAmount);
      this.cashCoinDepositForm.controls['twenties'].setValue(res.data.DepositAmount.TwentiesAmount);
      this.cashCoinDepositForm.controls['tens'].setValue(res.data.DepositAmount.TensAmount);
      this.cashCoinDepositForm.controls['fives'].setValue(res.data.DepositAmount.FivesAmount);
      this.cashCoinDepositForm.controls['twos'].setValue(res.data.DepositAmount.TwosAmount);
      this.cashCoinDepositForm.controls['ones'].setValue(res.data.DepositAmount.OnesAmount);
      this.cashCoinDepositForm.controls['quarters'].setValue(res.data.DepositAmount.QuartersAmount);
      this.cashCoinDepositForm.controls['dimes'].setValue(res.data.DepositAmount.DimesAmount);
      this.cashCoinDepositForm.controls['nickles'].setValue(res.data.DepositAmount.NickelsAmount);
      this.cashCoinDepositForm.controls['pennies'].setValue(res.data.DepositAmount.PenniesAmount);
      this.cashCoinDepositForm.controls['change'].setValue(res.data.DepositAmount.ChangeAmount);
      
     //-----------------------------------------
    }
   });
   //------------------------------------------
   
  }
  //----------------------------------------------
   //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundredsError=false;
     this.hundreds=(hundredsCount * 100);
     this.cashCoinDepositForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.cashCoinDepositForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.hundredsBillError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.cashCoinDepositForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsError=true;
     this.noError=true;
     this.hundredsCount=0;
     this.cashCoinDepositForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
     if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fiftiesError=false;
     this.fifties=(fiftiesCount * 50);
     this.cashCoinDepositForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.cashCoinDepositForm.controls['fifties'].setValue(this.fifties);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
     if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
    }
    if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.fiftiesBillError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.cashCoinDepositForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.cashCoinDepositForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twentiesError=false;
     this.twenties=(twentiesCount * 20);
     this.cashCoinDepositForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.cashCoinDepositForm.controls['twenties'].setValue(this.twenties);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.twentiesBillError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.cashCoinDepositForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.cashCoinDepositForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tensError=false;
     this.tens=(tensCount * 10);
     this.cashCoinDepositForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.cashCoinDepositForm.controls['tens'].setValue(this.tens);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.tensBillError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.cashCoinDepositForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.cashCoinDepositForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fivesError=false;
     this.fives=(fivesCount * 5);
     this.cashCoinDepositForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.cashCoinDepositForm.controls['fives'].setValue(this.fives);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.fivesBillError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.cashCoinDepositForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.cashCoinDepositForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twosError=false;
     this.twos=(twosCount * 2);
     this.cashCoinDepositForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.cashCoinDepositForm.controls['twos'].setValue(this.twos);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.twosBillError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.cashCoinDepositForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.cashCoinDepositForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=(onesCount * 1);
     this.cashCoinDepositForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.cashCoinDepositForm.controls['ones'].setValue(this.ones);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.cashCoinDepositForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.cashCoinDepositForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=(quartersCount * 0.25);
     this.cashCoinDepositForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.cashCoinDepositForm.controls['quarters'].setValue(this.quarters);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.cashCoinDepositForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.cashCoinDepositForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
    }
    else if(dimesCount > 0 && CheckIsInt(dimesCount)) {
     this.noError=false;
     this.dimesBillError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.cashCoinDepositForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.cashCoinDepositForm.controls['dimes'].setValue(this.dimes);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
    }    
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.cashCoinDepositForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.cashCoinDepositForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
    }  
    else if(nicklesCount > 0 && CheckIsInt(nicklesCount)) {
     this.noError=false;
     this.nicklesBillError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.cashCoinDepositForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.cashCoinDepositForm.controls['nickles'].setValue(this.nickles);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.cashCoinDepositForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.cashCoinDepositForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
    }
    else if(penniesCount > 0 && CheckIsInt(penniesCount)) {
     this.noError=false;
     this.penniesBillError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.cashCoinDepositForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.cashCoinDepositForm.controls['pennies'].setValue(this.pennies);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
    }  
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.cashCoinDepositForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.cashCoinDepositForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //Calculate Total---------------------------------------
  calculateTotal(){
    //totalCash
   var total=0;
       total=+(this.cashCoinDepositForm.value.hundreds+this.cashCoinDepositForm.value.fifties+this.cashCoinDepositForm.value.twenties+this.cashCoinDepositForm.value.tens+this.cashCoinDepositForm.value.fives+
               this.cashCoinDepositForm.value.twos+this.cashCoinDepositForm.value.ones+this.cashCoinDepositForm.value.quarters+this.cashCoinDepositForm.value.nickles+
               this.cashCoinDepositForm.value.dimes+this.cashCoinDepositForm.value.pennies+this.cashCoinDepositForm.value.change);
       this.totalCash=total;
  }
  //------------------------------------------------------
  //Cash Coin Deposit Form Submit-------------------------
    cashCoinDepositFormSubmit(){
     this.submitDisabled=true;
     this.progressBarShow();
     let cashCoinData =  {} as denominationDetail;
         cashCoinData.HundredsCount = this.cashCoinDepositForm.value.hundredsCount;
         cashCoinData.FiftiesCount = this.cashCoinDepositForm.value.fiftiesCount;
         cashCoinData.TwentiesCount = this.cashCoinDepositForm.value.twentiesCount;
         cashCoinData.TensCount = this.cashCoinDepositForm.value.tensCount;
         cashCoinData.FivesCount = this.cashCoinDepositForm.value.fivesCount;
         cashCoinData.TwosCount = this.cashCoinDepositForm.value.twosCount;
         cashCoinData.OnesCount = this.cashCoinDepositForm.value.onesCount;
         cashCoinData.QuartersCount = this.cashCoinDepositForm.value.quartersCount;
         cashCoinData.NickelsCount = this.cashCoinDepositForm.value.nicklesCount;
         cashCoinData.DimesCount = this.cashCoinDepositForm.value.dimesCount;
         cashCoinData.PenniesCount = this.cashCoinDepositForm.value.penniesCount;
         cashCoinData.ChangeAmount = this.cashCoinDepositForm.value.change;
     
         //console.log(cashCoinData);
         if (!this.noError) {
          var pickupIdArr=[];
          this.pickupIds.forEach(function (value) {
            let pickupIds =  {} as PickupID;
              pickupIds.PickupId = value;
              pickupIdArr.push(pickupIds);
          });
          
          var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
            const cashCoinDetails = {
              'branchId' : branchId,
              'authUserId' : this.authUserId,
              'createBy' : this.authUserId,
              'trackingNumber' : this.cashCoinDepositForm.value.trackingNo,
              'pickupList' : JSON.stringify(pickupIdArr),
              'depositDate': JSON.parse(sessionStorage.getItem("currentDate")),
              'denominationDetail': JSON.stringify(cashCoinData)
            }
            //console.log(cashCoinDetails);
            //Call Api-------------------
              this.depositService.submitNewDeposit(cashCoinDetails).subscribe(res=>{
                this.progressBarHide();
                 //console.log(res);
                 if(res.status=='success') {
                   this.showErrorMsg=false;
                   sessionStorage.setItem("dialogOpend",'false');
                   this.successmessage=res.message;
                   this.getCMOandDepositTotal()
                   setTimeout(() => {
                     this.dialogRef.close();
                    }, 2000);
                 }
                 else{
                   this.submitDisabled=false;
                   this.showErrorMsg=true;
                   this.errormessage=res.message;
                 }
               });
            //---------------------------
         }
         else{
          this.submitDisabled=false;
          this.progressBarHide();
          //console.log('Somthing error!!!');
           this.showErrorMsg=true;
           this.errormessage='Somthing error occured';
         }
         
         setTimeout(() => {
         this.showErrorMsg=false;
         }, 2000);
    }
  //------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': JSON.parse(sessionStorage.getItem("currentDate"))
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      //console.log(res);
      depositTotal=res.data.DepositTotal;
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
     });
    //-------------------
  }
  //----------------------------------------------------------------
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }
  
}
//****************************************************************************
//Edit CashCoin Deposit------------------------------------
@Component({
  selector: 'edit-cashcoin-deposit',
  templateUrl: './edit-cashcoin-deposit.html',
  styleUrls: ['./deposit.component.css']
})

export class EditCashCoinDeposit implements OnInit {
  
  editCashCoinDepositForm: FormGroup;
  authUserId:any;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
   showErrorMsg: boolean = false;
   errormessage:string;
   successmessage:string;
   submitDisabled:boolean;
    
    armoredDate : any;
    
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    change: number=0;
    totalCash: any;
    
  //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
  //------------------------------------
  
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<EditCashCoinDeposit>,@Inject(MAT_DIALOG_DATA) public data: NewDepositData, public depositService:DepositService) {}
  //Popup close---------------
  onNoClick(): void {
    this.dialogRef.close();
  }
  //-------------------------
   ngOnInit() {
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getEditCashCoinDetails();
    //CashCoin Deposit Form------------------------
    this.editCashCoinDepositForm = new FormGroup({
      trackingNo: new FormControl(),
      ArmoredDate: new FormControl(),
      Bag: new FormControl(),
      PickupPerson: new FormControl(),
      Comment: new FormControl(),
      SuggestHundredsCount: new FormControl(0),
      SuggestFiftiesCount: new FormControl(0),
      SuggestTwentiesCount: new FormControl(0),
      SuggestTensCount: new FormControl(0),
      SuggestFivesCount: new FormControl(0),
      SuggestTwosCount: new FormControl(0),
      SuggestOnesCount: new FormControl(0),
      SuggestQuartersCount: new FormControl(0),
      SuggestDimesCount: new FormControl(0),
      SuggestNickelsCount: new FormControl(0),
      SuggestPenniesCount: new FormControl(0),
      
      SuggestHundredsAmount: new FormControl(0),
      SuggestFiftiesAmount: new FormControl(0),
      SuggestTwentiesAmount: new FormControl(0),
      SuggestTensAmount: new FormControl(0),
      SuggestFivesAmount: new FormControl(0),
      SuggestTwosAmount: new FormControl(0),
      SuggestOnesAmount: new FormControl(0),
      SuggestQuartersAmount: new FormControl(0),
      SuggestDimesAmount: new FormControl(0),
      SuggestNickelsAmount: new FormControl(0),
      SuggestPenniesAmount: new FormControl(0),
      SuggestChangeAmount: new FormControl(0),
      
      hundredsCount: new FormControl(0),
      fiftiesCount: new FormControl(0),
      twentiesCount: new FormControl(0),
      tensCount: new FormControl(0),
      fivesCount: new FormControl(0),
      twosCount: new FormControl(0),
      onesCount: new FormControl(0),
      quartersCount: new FormControl(0),
      dimesCount: new FormControl(0),
      nicklesCount: new FormControl(0),
      penniesCount: new FormControl(0),
      
      hundreds: new FormControl(0),
      fifties: new FormControl(0),
      twenties: new FormControl(0),
      tens: new FormControl(0),
      fives: new FormControl(0),
      twos: new FormControl(0),
      ones: new FormControl(0),
      quarters: new FormControl(0),
      dimes: new FormControl(0),
      nickles: new FormControl(0),
      pennies: new FormControl(0),
      change: new FormControl()
    });
    //--------------------------------------------- 
   }
   //Get cashcoin deposit details------------------
   getEditCashCoinDetails(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var depositId=JSON.parse(sessionStorage.getItem("depositId"));
    const cashCoinDetails = {
      'branchId' : branchId,
      'authUserId' : this.authUserId,
      'depositId' : depositId,
      'depositDate': JSON.parse(sessionStorage.getItem("currentDate"))
      
    }
     //call api--------------
    this.depositService.getCashCoinDepositDetails(cashCoinDetails).subscribe(res=>{
      //console.log(res);
       if(res.status=='success') {
        this.editCashCoinDepositForm.controls['trackingNo'].setValue(res.data.TrackingNumber);
        this.editCashCoinDepositForm.controls['ArmoredDate'].setValue(res.data.ArmoredDate);
        this.armoredDate='';
        if (res.data.ArmoredDate!=null) {
          this.armoredDate=moment(res.data.ArmoredDate).format(dateFormat);
        }
        
        
        this.editCashCoinDepositForm.controls['Bag'].setValue(res.data.Bag);
        this.editCashCoinDepositForm.controls['PickupPerson'].setValue(res.data.PickupPerson);
        this.editCashCoinDepositForm.controls['Comment'].setValue(res.data.Comment);
        //Suggest Count---------------
        this.editCashCoinDepositForm.controls['SuggestHundredsCount'].setValue(res.data.SuggestCount.HundredsCount);
        this.editCashCoinDepositForm.controls['SuggestFiftiesCount'].setValue(res.data.SuggestCount.FiftiesCount);
        this.editCashCoinDepositForm.controls['SuggestTwentiesCount'].setValue(res.data.SuggestCount.TwentiesCount);
        this.editCashCoinDepositForm.controls['SuggestTensCount'].setValue(res.data.SuggestCount.TensCount);
        this.editCashCoinDepositForm.controls['SuggestFivesCount'].setValue(res.data.SuggestCount.FivesCount);
        this.editCashCoinDepositForm.controls['SuggestTwosCount'].setValue(res.data.SuggestCount.TwosCount);
        this.editCashCoinDepositForm.controls['SuggestOnesCount'].setValue(res.data.SuggestCount.OnesCount);
        this.editCashCoinDepositForm.controls['SuggestQuartersCount'].setValue(res.data.SuggestCount.QuartersCount);
        this.editCashCoinDepositForm.controls['SuggestDimesCount'].setValue(res.data.SuggestCount.DimesCount);
        this.editCashCoinDepositForm.controls['SuggestNickelsCount'].setValue(res.data.SuggestCount.NickelsCount);
        this.editCashCoinDepositForm.controls['SuggestPenniesCount'].setValue(res.data.SuggestCount.PenniesCount);
        //----------------------------
        //Suggest Amount--------------
        this.editCashCoinDepositForm.controls['SuggestHundredsAmount'].setValue(res.data.SuggestAmount.HundredsAmount);
        this.editCashCoinDepositForm.controls['SuggestFiftiesAmount'].setValue(res.data.SuggestAmount.FiftiesAmount);
        this.editCashCoinDepositForm.controls['SuggestTwentiesAmount'].setValue(res.data.SuggestAmount.TwentiesAmount);
        this.editCashCoinDepositForm.controls['SuggestTensAmount'].setValue(res.data.SuggestAmount.TensAmount);
        this.editCashCoinDepositForm.controls['SuggestFivesAmount'].setValue(res.data.SuggestAmount.FivesAmount);
        this.editCashCoinDepositForm.controls['SuggestTwosAmount'].setValue(res.data.SuggestAmount.TwosAmount);
        this.editCashCoinDepositForm.controls['SuggestOnesAmount'].setValue(res.data.SuggestAmount.OnesAmount);
        this.editCashCoinDepositForm.controls['SuggestQuartersAmount'].setValue(res.data.SuggestAmount.QuartersAmount);
        this.editCashCoinDepositForm.controls['SuggestDimesAmount'].setValue(res.data.SuggestAmount.DimesAmount);
        this.editCashCoinDepositForm.controls['SuggestNickelsAmount'].setValue(res.data.SuggestAmount.NickelsAmount);
        this.editCashCoinDepositForm.controls['SuggestPenniesAmount'].setValue(res.data.SuggestAmount.PenniesAmount);
        this.editCashCoinDepositForm.controls['SuggestChangeAmount'].setValue(res.data.SuggestAmount.ChangeAmount);
        this.totalCash=res.data.SuggestAmount.TotalCash;
        //-----------------------------------------
        //Deposit Cash coin Count-----------------------
       this.editCashCoinDepositForm.controls['hundredsCount'].setValue(res.data.DepositCount.HundredsCount);
       this.editCashCoinDepositForm.controls['fiftiesCount'].setValue(res.data.DepositCount.FiftiesCount);
       this.editCashCoinDepositForm.controls['twentiesCount'].setValue(res.data.DepositCount.TwentiesCount);
       this.editCashCoinDepositForm.controls['tensCount'].setValue(res.data.DepositCount.TensCount);
       this.editCashCoinDepositForm.controls['fivesCount'].setValue(res.data.DepositCount.FivesCount);
       this.editCashCoinDepositForm.controls['twosCount'].setValue(res.data.DepositCount.TwosCount);
       this.editCashCoinDepositForm.controls['onesCount'].setValue(res.data.DepositCount.OnesCount);
       this.editCashCoinDepositForm.controls['quartersCount'].setValue(res.data.DepositCount.QuartersCount);
       this.editCashCoinDepositForm.controls['dimesCount'].setValue(res.data.DepositCount.DimesCount);
       this.editCashCoinDepositForm.controls['nicklesCount'].setValue(res.data.DepositCount.NickelsCount);
       this.editCashCoinDepositForm.controls['penniesCount'].setValue(res.data.DepositCount.PenniesCount);
        //Deposit Cash coin amount-----------------------
        this.editCashCoinDepositForm.controls['hundreds'].setValue(res.data.DepositAmount.HundredsAmount);
        this.editCashCoinDepositForm.controls['fifties'].setValue(res.data.DepositAmount.FiftiesAmount);
        this.editCashCoinDepositForm.controls['twenties'].setValue(res.data.DepositAmount.TwentiesAmount);
        this.editCashCoinDepositForm.controls['tens'].setValue(res.data.DepositAmount.TensAmount);
        this.editCashCoinDepositForm.controls['fives'].setValue(res.data.DepositAmount.FivesAmount);
        this.editCashCoinDepositForm.controls['twos'].setValue(res.data.DepositAmount.TwosAmount);
        this.editCashCoinDepositForm.controls['ones'].setValue(res.data.DepositAmount.OnesAmount);
        this.editCashCoinDepositForm.controls['quarters'].setValue(res.data.DepositAmount.QuartersAmount);
        this.editCashCoinDepositForm.controls['dimes'].setValue(res.data.DepositAmount.DimesAmount);
        this.editCashCoinDepositForm.controls['nickles'].setValue(res.data.DepositAmount.NickelsAmount);
        this.editCashCoinDepositForm.controls['pennies'].setValue(res.data.DepositAmount.PenniesAmount);
        this.editCashCoinDepositForm.controls['change'].setValue(res.data.DepositAmount.ChangeAmount);
         //-----------------------------------------
       }
    });
    
   }
   //----------------------------------------------
  //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundredsError=false;
     this.hundreds=(hundredsCount * 100);
     this.editCashCoinDepositForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.editCashCoinDepositForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.hundredsBillError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.editCashCoinDepositForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsError=true;
     this.noError=true;
     this.hundredsCount=0;
     this.editCashCoinDepositForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
     if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fiftiesError=false;
     this.fifties=(fiftiesCount * 50);
     this.editCashCoinDepositForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.editCashCoinDepositForm.controls['fifties'].setValue(this.fifties);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
     if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
    }
    if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.fiftiesBillError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.editCashCoinDepositForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.editCashCoinDepositForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twentiesError=false;
     this.twenties=(twentiesCount * 20);
     this.editCashCoinDepositForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.editCashCoinDepositForm.controls['twenties'].setValue(this.twenties);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.twentiesBillError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.editCashCoinDepositForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.editCashCoinDepositForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tensError=false;
     this.tens=(tensCount * 10);
     this.editCashCoinDepositForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.editCashCoinDepositForm.controls['tens'].setValue(this.tens);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.tensBillError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.editCashCoinDepositForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.editCashCoinDepositForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fivesError=false;
     this.fives=(fivesCount * 5);
     this.editCashCoinDepositForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.editCashCoinDepositForm.controls['fives'].setValue(this.fives);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.fivesBillError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.editCashCoinDepositForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.editCashCoinDepositForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twosError=false;
     this.twos=(twosCount * 2);
     this.editCashCoinDepositForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.editCashCoinDepositForm.controls['twos'].setValue(this.twos);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.twosBillError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.editCashCoinDepositForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.editCashCoinDepositForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=(onesCount * 1);
     this.editCashCoinDepositForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.editCashCoinDepositForm.controls['ones'].setValue(this.ones);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.editCashCoinDepositForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.editCashCoinDepositForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=(quartersCount * 0.25);
     this.editCashCoinDepositForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.editCashCoinDepositForm.controls['quarters'].setValue(this.quarters);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.editCashCoinDepositForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.editCashCoinDepositForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
    }
    else if(dimesCount > 0 && CheckIsInt(dimesCount)) {
     this.noError=false;
     this.dimesBillError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.editCashCoinDepositForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.editCashCoinDepositForm.controls['dimes'].setValue(this.dimes);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
    }    
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.editCashCoinDepositForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.editCashCoinDepositForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
    }  
    else if(nicklesCount > 0 && CheckIsInt(nicklesCount)) {
     this.noError=false;
     this.nicklesBillError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.editCashCoinDepositForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.editCashCoinDepositForm.controls['nickles'].setValue(this.nickles);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.editCashCoinDepositForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.editCashCoinDepositForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
    }
    else if(penniesCount > 0 && CheckIsInt(penniesCount)) {
     this.noError=false;
     this.penniesBillError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.editCashCoinDepositForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.editCashCoinDepositForm.controls['pennies'].setValue(this.pennies);
    }
    this.calculateTotal();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
    }  
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.editCashCoinDepositForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.editCashCoinDepositForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.calculateTotal();
  }
  //------------------------------------------------------
   //Calculate Total---------------------------------------
  calculateTotal(){
    //totalCash
   var total=0;
       total=+(this.editCashCoinDepositForm.value.hundreds+this.editCashCoinDepositForm.value.fifties+this.editCashCoinDepositForm.value.twenties+this.editCashCoinDepositForm.value.tens+this.editCashCoinDepositForm.value.fives+
               this.editCashCoinDepositForm.value.twos+this.editCashCoinDepositForm.value.ones+this.editCashCoinDepositForm.value.quarters+this.editCashCoinDepositForm.value.nickles+
               this.editCashCoinDepositForm.value.dimes+this.editCashCoinDepositForm.value.pennies+this.editCashCoinDepositForm.value.change);
       this.totalCash=total;
  }
  //------------------------------------------------------
  //Submit Edit CashCoin Deposit--------------------------
  editCashCoinDepositFormSubmit(){
    
    this.submitDisabled=true;
     this.progressBarShow();
     let cashCoinData =  {} as denominationDetail;
         cashCoinData.HundredsCount = this.editCashCoinDepositForm.value.hundredsCount;
         cashCoinData.FiftiesCount = this.editCashCoinDepositForm.value.fiftiesCount;
         cashCoinData.TwentiesCount = this.editCashCoinDepositForm.value.twentiesCount;
         cashCoinData.TensCount = this.editCashCoinDepositForm.value.tensCount;
         cashCoinData.FivesCount = this.editCashCoinDepositForm.value.fivesCount;
         cashCoinData.TwosCount = this.editCashCoinDepositForm.value.twosCount;
         cashCoinData.OnesCount = this.editCashCoinDepositForm.value.onesCount;
         cashCoinData.QuartersCount = this.editCashCoinDepositForm.value.quartersCount;
         cashCoinData.NickelsCount = this.editCashCoinDepositForm.value.nicklesCount;
         cashCoinData.DimesCount = this.editCashCoinDepositForm.value.dimesCount;
         cashCoinData.PenniesCount = this.editCashCoinDepositForm.value.penniesCount;
         cashCoinData.ChangeAmount = this.editCashCoinDepositForm.value.change;
     
         //console.log(cashCoinData);
         if (!this.noError) {
      
          var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
          var depositId=JSON.parse(sessionStorage.getItem("depositId"));
          
            const cashCoinDetails = {
              'depositId' : depositId,
              'branchId' : branchId,
              'authUserId' : this.authUserId,
              'updateBy' : this.authUserId,
              'trackingNumber' : this.editCashCoinDepositForm.value.trackingNo,
              'depositDate': JSON.parse(sessionStorage.getItem("currentDate")),
              'bag': this.editCashCoinDepositForm.value.Bag,
              'pickupPersion': this.editCashCoinDepositForm.value.PickupPerson,
              'comment': this.editCashCoinDepositForm.value.Comment,
              'armoredDate': this.armoredDate,
              'denominationDetail': JSON.stringify(cashCoinData)
            }
            //console.log(cashCoinDetails);
            //Call Api-------------------
              this.depositService.submitEditCashCoinDeposit(cashCoinDetails).subscribe(res=>{
                this.progressBarHide();
                 //console.log(res);
                 if(res.status=='success') {
                   this.showErrorMsg=false;
                   sessionStorage.setItem("dialogOpend",'false');
                   this.successmessage=res.message;
                   this.getCMOandDepositTotal();
                   setTimeout(() => {
                     this.dialogRef.close();
                    }, 2000);
                 }
                 else{
                   this.submitDisabled=false;
                   this.showErrorMsg=true;
                   this.errormessage=res.message;
                 }
               });
            //---------------------------
         }
         else{
          this.submitDisabled=false;
          this.progressBarHide();
          //console.log('Somthing error!!!');
           this.showErrorMsg=true;
           this.errormessage='Somthing error occured';
         }
         
         setTimeout(() => {
         this.showErrorMsg=false;
         }, 2000);
  }
  //------------------------------------------------------
  
    //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': JSON.parse(sessionStorage.getItem("currentDate"))
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      if(res.status=='success') {
      (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
      (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
  
    //Change date------------------------------------------------------
  onChangeArmoredDate(type: string, event: MatDatepickerInputEvent<Date>) {
  // console.log(event.value);
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.armoredDate=formattedDate;
   //var aDate   = moment(momentDate, 'MM/DD/YYYY', true);
   //var isValid = aDate.isValid();
   //console.log(isValid);
  }
  //----------------------------------------------------------
   
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }
}
//--------------------------------------------------------
