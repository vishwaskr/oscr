import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../../constants.js";
import { DepositService } from 'src/app/featureModules/deposit-module/service/deposit.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';
import Swal from 'sweetalert2';

export interface otherDepositFormfields {
  type : string;
  name : string;
  label : string;
  suggestedAmt : number;
  depositAmt : number;
}

export interface otherDepositSlipList {
 DepositId : number;
 PaymentFormsId : number;
 PaymentFormName : string;
 DepositAmount : number;
 SuggestAmount : number;
 StatusId : number;
 StatusName : string;
}

export interface DepositFormSubmitDetails {
  PaymentFormsId: number;
  Amount: number;
}

@Component({
  selector: 'app-other-daily-deposits',
  templateUrl: './other-daily-deposits.component.html',
  styleUrls: ['./other-daily-deposits.component.css']
})
export class OtherDailyDepositsComponent implements OnInit {

  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("currentDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  authUserId:any;
  otherSubmitButtonEnaDis: boolean=true;
  isChanged:boolean=false;
  showDepositdForm:boolean=false;
  showDepositData:boolean=false;
  otherDepositDataSource:any;
   //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  otherDepositForm: FormGroup;
  OtherDepositFormsArray:any;
  public otherDepositFormfields: any[];
  
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  constructor(private fb: FormBuilder,private router:Router, public depositService:DepositService) { }
  
  displayedColumnsOtherDeposit: string[] = ['form_name', 'suggested', 'actual', 'status','action'];
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    sessionStorage.setItem("currentDate",JSON.stringify(this.currentDate));
    }
    else{
    this.date = new FormControl(new Date());
    sessionStorage.setItem("currentDate",JSON.stringify(this.date.value));
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getCMOandDepositTotal();
    this.getOtherDepositList();
    
    //Deposit form------------------------
    this.otherDepositForm = this.fb.group({
      otherDepositFormfields: new FormControl(JSON.stringify(this.otherDepositFormfields))
    });
    //------------------------------------
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Get Other Deposit List-------------------------------------
  getOtherDepositList(){
  this.progressBarShow();
  var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  var OtherDepositDetailsArray=[];
  
    const otherDepositDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //console.log(otherDepositDetails);
    //call api------
    this.depositService.getOtherDepositList(otherDepositDetails).subscribe(res=>{
      this.progressBarHide();
      // console.log(res);
       if(res.status=='success') {
        //New deposit-------------------------------
        if (res.data.NewDeposit.length > 0) {
        this.showDepositdForm=true;
        this.otherSubmitButtonEnaDis=false;
        res.data.NewDeposit.forEach(function (value) {
            let otherDepositFormsData =  {} as otherDepositFormfields;
                otherDepositFormsData.type='text';
                otherDepositFormsData.label=value.PaymentFormName;
                otherDepositFormsData.name=value.PaymentFormsId;
                otherDepositFormsData.depositAmt=value.DepositAmount;
                otherDepositFormsData.suggestedAmt=value.SuggestAmount;
                
                OtherDepositDetailsArray.push(otherDepositFormsData);
          });
         this.otherDepositFormfields=OtherDepositDetailsArray;
         console.log(OtherDepositDetailsArray);
          for (let i = 0; i < OtherDepositDetailsArray.length; i++) {
           this.otherDepositForm.addControl(OtherDepositDetailsArray[i].name, new FormControl(OtherDepositDetailsArray[i].suggestedAmt));
           this.otherDepositForm.controls[OtherDepositDetailsArray[i].name].setValue(OtherDepositDetailsArray[i].suggestedAmt); 
          }
        }
        else{
          this.showDepositdForm=false;
        }
        //-----------------------------------------
        //Old Deposit------------------------------
         var depositDataArray=[];
        if (res.data.OldDeposit.length > 0) {
         res.data.OldDeposit.forEach(function (value) {
           let otherDepositSlipList =  {} as otherDepositSlipList;
              otherDepositSlipList.DepositId = value.DepositId;
              otherDepositSlipList.PaymentFormsId = value.PaymentFormsId;
              otherDepositSlipList.PaymentFormName = value.PaymentFormName;
              otherDepositSlipList.DepositAmount = value.DepositAmount;
              otherDepositSlipList.SuggestAmount = value.SuggestAmount;
              otherDepositSlipList.StatusId=value.StatusId;
              otherDepositSlipList.StatusName=value.StatusName;
              
              depositDataArray.push(otherDepositSlipList);
         });
          var OTHER_DEPOSIT_DATA: otherDepositSlipList[]=depositDataArray;
          this.otherDepositDataSource = OTHER_DEPOSIT_DATA;
          this.showDepositData=true;
        }
        else{
          this.showDepositData=false;
        }
        //-----------------------------------------
       }
       else{
        this.showDepositdForm=false;
        this.showDepositData=false;
        this.otherDepositFormfields=[];
        this.otherDepositForm = this.fb.group({
          otherDepositFormfields: new FormControl(JSON.stringify(this.otherDepositFormfields))
        });
       }
    });
    //--------------
  }
  //-----------------------------------------------------------
  //Submit Other Deposit---------------------------------------
  submitOtherDeposit(){
    this.otherSubmitButtonEnaDis=true;
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    //console.log(this.otherDepositForm.value);
    var frmVal=JSON.stringify(this.otherDepositForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var frmVal2=JSON.stringify(this.otherDepositFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var otherDepositFormSubmitDetailsArr=[];
    var otherDepositTotal=0;
    for (var key2 in frmValObj2) {
      let otherDepositFormSubmitDetails = {} as DepositFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      //console.log("keyName: "+keyName);
      otherDepositFormSubmitDetails.PaymentFormsId=keyName;
      otherDepositFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
    //  otherDepositTotal=otherDepositTotal+frmValObj[JSON.stringify(keyName)]
      
      otherDepositFormSubmitDetailsArr.push(otherDepositFormSubmitDetails);
    }
   // this.otherDepositTotal=+otherDepositTotal;
   // this.OtherDepositFormsArray=otherDepositFormSubmitDetailsArr;
     const otherDepoisitDetails = {
      'branchId' : branchId,
      'createBy' : this.authUserId,
      'authUserId' : this.authUserId,
      'depositDate': this.currentDate,
      'otherDepositDetail' : JSON.stringify(otherDepositFormSubmitDetailsArr)
     }
     console.log(otherDepoisitDetails);
     //call api----------
     this.depositService.submitOtherDeposit(otherDepoisitDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
        this.otherSubmitButtonEnaDis=false;
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.isChanged=false;
        this.getCMOandDepositTotal();
        setTimeout(() => {
          this.getOtherDepositList();
          this.successmessage='';
        }, 2000);
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        this.otherSubmitButtonEnaDis=false;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
     });
     //------------------
  }
  //-----------------------------------------------------------
   //Edit Other Deposit-----------------------------------------
  editDeposit(depositId){
   //console.log('DepositID:'+depositId);
   //button----
   document.getElementById("action-box-"+depositId).style.display="none";
   document.getElementById("save-box-"+depositId).style.display="block";
   //----------
   //input-----
   document.getElementById("deposit-lable-"+depositId).style.display="none";
   document.getElementById("deposit-input-"+depositId).style.display="block";
   //----------
  }
  //-----------------------------------------------------------------
  //Edit Other Deposit Cancel----------------------------------------
  cancelDeposit(depositId){
   //Button---
   document.getElementById("action-box-"+depositId).style.display="block";
   document.getElementById("save-box-"+depositId).style.display="none";
   //---------
   //input-----
   document.getElementById("deposit-lable-"+depositId).style.display="block";
   document.getElementById("deposit-input-"+depositId).style.display="none";
   //----------
  }
  //-----------------------------------------------------------------
  //Update Other deposit----------------------------------------------
  updateDeposit(depositId){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let actual_amt = +(document.getElementById("up_deposit_"+depositId) as HTMLInputElement).value;
   // console.log("amex_actual_amt:"+amex_actual_amt);
    const updateDepositDetails = {
      'depositId': depositId,
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'amount': actual_amt,
      'depositDate': this.currentDate
    }
     //console.log(updateAmexDetails);
     this.depositService.updateOtherDeposit(updateDepositDetails).subscribe(res=>{
     this.progressBarHide();
    // console.log(res);
        if(res.status=='success') {
        //Button---------------
        document.getElementById("action-box-"+depositId).style.display="block";
        document.getElementById("save-box-"+depositId).style.display="none";
        //---------------------
        //input-----
        document.getElementById("deposit-lable-"+depositId).style.display="block";
        document.getElementById("deposit-input-"+depositId).style.display="none";
        //----------
        
        this.showErrorMsg=false;
        this.successmessage=res.message;
        this.getCMOandDepositTotal();
        this.getOtherDepositList();
        setTimeout(() => {
          this.successmessage='';
        }, 2000);
        
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
     });
  }
  //-----------------------------------------------------------------
   //Check Is Change-------------------------------------------
  checkChange(){
    this.isChanged=true;
  }
  //-----------------------------------------------------------
   //Route-----------------------------------------------------
  routeLink(route){
    if (route=='../deposit' && this.isChanged ==false) {
     this.router.navigate(['../deposit']) 
    }
     else if (route=='../deposit/checks' && this.isChanged == false) {
      this.router.navigate(['../deposit/checks']) 
    }
    else if (route=='../deposit/cc-handhelds' && this.isChanged == false) {
      this.router.navigate(['../deposit/cc-handhelds']) 
    }
    else if (route=='../deposit/pos' && !this.isChanged) {
      this.router.navigate(['../deposit/pos']) 
    }
    else if (route=='../deposit/amex' && this.isChanged == false) {
      this.router.navigate(['../deposit/amex']) 
    }
    else if(route=='../deposit/other-daily-deposits' && this.isChanged == false){
      this.router.navigate(['../deposit/other-daily-deposits']) 
    }
    else{
      this.routeConfirm(route);  
    }
  }
  //----------------------------------------------------------
  //Route Confirm alert---------------------------------------
  routeConfirm(link){
     Swal.fire({
    // title: 'Are you want to save?',
    text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Ignore'
    }).then((result) => {
      if (result.value) {
        
      }
      else {
        this.router.navigate([link]) 
      }
    });
  }
  //----------------------------------------------------------
  
  //Change Branch---------------------------------------------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    if (this.isChanged ==false) {
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    this.getCMOandDepositTotal();
    this.getOtherDepositList();
    }
    else{
       Swal.fire({
        //title: 'Are you want to save?',
        text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Save',
        cancelButtonText: 'Ignore'
        }).then((result) => {
          if (result.value) {
            this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
          }
          else {
            this.filterBranchId=BranchId;
            localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
            this.getCMOandDepositTotal();
            this.getOtherDepositList();
            this.isChanged=false;
          }
        });
    }
  }
  //-----------------------------------------------------------------
  //Change date------------------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("currentDate",JSON.stringify(momentDate));
   this.getCMOandDepositTotal();
   this.getOtherDepositList();
  }
  //----------------------------------------------------------
  //Get CMO Total and Deposit Total---------------------------------
  getCMOandDepositTotal(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var cmoTotal=0;
    var depositTotal=0;
    const Details = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'depositDate': this.currentDate
    }
    //Call api-----------
     this.depositService.getCMOandDepositTotal(Details).subscribe(res=>{
      if(res.status=='success') {
       (document.getElementById("cmoTotal") as HTMLInputElement).value=res.data.CMOTotal;
       (document.getElementById("depositTotal") as HTMLInputElement).value=res.data.DepositTotal;
      }
      else{
       (document.getElementById("cmoTotal") as HTMLInputElement).value='0';
       (document.getElementById("depositTotal") as HTMLInputElement).value='0'; 
      }
     });
    //-------------------
  }
  //----------------------------------------------------------------
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  isInt(n) {
   return n % 1 === 0;
  }

}
