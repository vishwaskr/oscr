import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherDailyDepositsComponent } from './other-daily-deposits.component';

describe('OtherDailyDepositsComponent', () => {
  let component: OtherDailyDepositsComponent;
  let fixture: ComponentFixture<OtherDailyDepositsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherDailyDepositsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherDailyDepositsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
