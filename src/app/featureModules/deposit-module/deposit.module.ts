import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { DepositComponent } from './component/deposit/deposit.component';
import {DepositRoutingModule} from './deposit-routing/deposit-routing.module';
import {MatProgressBarModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';


import {NewDeposit} from './component/deposit/deposit.component';
import {EditCashCoinDeposit} from './component/deposit/deposit.component';
import { CcHandheldsComponent } from './component/deposit/cc-handhelds/cc-handhelds.component';
import { PosComponent } from './component/deposit/pos/pos.component';
import { AmexComponent } from './component/deposit/amex/amex.component';
import { OtherDailyDepositsComponent } from './component/deposit/other-daily-deposits/other-daily-deposits.component';
import { ChecksComponent } from './component/deposit/checks/checks.component';

@NgModule({
  declarations: [DepositComponent, NewDeposit,EditCashCoinDeposit, CcHandheldsComponent, PosComponent, AmexComponent, OtherDailyDepositsComponent, ChecksComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatDialogModule,
    DepositRoutingModule,
    MatProgressBarModule,
    MatTabsModule
  ],
  entryComponents: [NewDeposit,EditCashCoinDeposit]
})
export class DepositModule { }
