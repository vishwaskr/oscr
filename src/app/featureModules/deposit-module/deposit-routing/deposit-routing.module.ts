import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Route} from '@angular/router';

//importing Component
import {DepositComponent} from '../component/deposit/deposit.component';
import {ChecksComponent} from '../component/deposit/checks/checks.component';
import {CcHandheldsComponent} from '../component/deposit/cc-handhelds/cc-handhelds.component';
import {PosComponent} from '../component/deposit/pos/pos.component';
import {AmexComponent} from '../component/deposit/amex/amex.component';
import {OtherDailyDepositsComponent} from '../component/deposit/other-daily-deposits/other-daily-deposits.component';

const route : Route [] = [
  {
    path:'',
    redirectTo:'deposit',
    pathMatch:'full'
  },
  {
    path:'checks',
    component:ChecksComponent
  },
  {
    path:'deposit',
    component:DepositComponent
  },
  {
    path:'cc-handhelds',
    component:CcHandheldsComponent
  },
  {
    path:'pos',
    component:PosComponent
  },
  {
    path:'amex',
    component:AmexComponent
  },
  {
    path:'other-daily-deposits',
    component:OtherDailyDepositsComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class DepositRoutingModule { }
