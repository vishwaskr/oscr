import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

//httpHeaders options------------------------------------
const httpOptions = {
  headers: new HttpHeaders({
    "Accept": "application/json"
  }),
 withCredentials: true
};
//-------------------------------------------------------
//API base url-------------------------------------------
var apiUrl=environment.apiHost;
//-------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class DepositService {

  constructor(private http: HttpClient) { }
  
  //Get CMO and Deposit-------------------------
  getCMOandDepositTotal(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
    return this.http.get(apiUrl+`branch/deposit?`+body,httpOptions)
  }
  //--------------------------------------------
  
  //Get Cash Coin Pickup list-------------------
  getCashCoinPickupList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
    return this.http.get(apiUrl+`branch/deposit/cashcoin?`+body,httpOptions)
  }
  //---------------------------------------------
  //Get Cash Coin Details------------------------
  getCashCoinDetails(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate+"&pickupList="+data.pickupList;
    return this.http.get(apiUrl+`branch/deposit/cashcoin?`+body,httpOptions)
  }
  //---------------------------------------------
  //Submit New Deposit---------------------------
   submitNewDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
  
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('trackingNumber', data.trackingNumber);
    formData.append('pickupList', data.pickupList);
    formData.append('depositDate', data.depositDate);
    formData.append('denominationDetail', data.denominationDetail);
    //console.log(data);
    return this.http.post(apiUrl+`branch/deposit/cashcoin`,formData,httpOptions)
   }
  //---------------------------------------------
  //Get CashCoin Deposit Details-----------------
  getCashCoinDepositDetails(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate+"&depositId="+data.depositId;
    return this.http.get(apiUrl+`branch/deposit/cashcoin?`+body,httpOptions)
  }
  //---------------------------------------------
  //Submit New Deposit---------------------------
   submitEditCashCoinDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
  
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('trackingNumber', data.trackingNumber);
    formData.append('bag', data.bag);
    formData.append('pickupPersion', data.pickupPersion);
    formData.append('comment', data.comment);
    formData.append('armoredDate', data.armoredDate);
    formData.append('depositDate', data.depositDate);
    formData.append('denominationDetail', data.denominationDetail);
    //console.log(data);
    return this.http.put(apiUrl+`branch/deposit/cashcoin`,formData,httpOptions)
   }
  //---------------------------------------------
  //Get CC Handheld Deposit list-----------------
  getCCHandheldDepositList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
    return this.http.get(apiUrl+`branch/deposit/card?`+body,httpOptions)
  }
  //---------------------------------------------
  //Submit cchandheld deposit--------------------
  submitCCHandheldDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
  
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('depositDate', data.depositDate);
    formData.append('visaMasterDiscoverDepositAmount', data.visaMasterDiscoverDepositAmount);
    formData.append('debitAmount', data.debitAmount);
    formData.append('cardHandheldDetails', data.cardHandheldDetails);
    //console.log(data);
    return this.http.post(apiUrl+`branch/deposit/card`,formData,httpOptions)
   }
  //---------------------------------------------
  //Update Deposit-------------------------------
  updateCChandheldDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('depositDate', data.depositDate);
    formData.append('visaMasterDiscoverAmount', data.visaMasterDiscoverAmount);
    formData.append('debitAmount', data.debitAmount);
    
    return this.http.put(apiUrl+`branch/deposit/card`,formData,httpOptions)
   }
  //---------------------------------------------
  //POS Deposit----------------------------------
   //Get POS Deposit list------------------------
    getPosDepositList(data):Observable<any>{
      var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
      return this.http.get(apiUrl+`branch/deposit/pos?`+body,httpOptions)
    }
  //---------------------------------------------
  //Submit cchandheld deposit--------------------
  submitPOSDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
  
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('depositDate', data.depositDate);
    formData.append('posDetails', data.posDetails);
    
    return this.http.post(apiUrl+`branch/deposit/pos`,formData,httpOptions)
   }
  //---------------------------------------------
  //Update Deposit-------------------------------
  updatePOSDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('depositDate', data.depositDate);
    formData.append('visaMasterDiscoverDepositAmount', data.visaMasterDiscoverDepositAmount);
    formData.append('debitDepositAmount', data.debitDepositAmount);
    
    return this.http.put(apiUrl+`branch/deposit/pos`,formData,httpOptions)
   }
  //---------------------------------------------
  //AMEX Deposit---------------------------------
  //Get AMEX Deposit list-----------------------
    getAmexDepositList(data):Observable<any>{
      var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
      return this.http.get(apiUrl+`branch/deposit/amex?`+body,httpOptions)
    }
  //---------------------------------------------
  //Submit Amex deposit--------------------
  submitAmexDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
  
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('depositDate', data.depositDate);
    formData.append('amexAmount', data.amexAmount);
    formData.append('amexDepositDetails', data.amexDepositDetails);
    
    return this.http.post(apiUrl+`branch/deposit/amex`,formData,httpOptions)
   }
  //---------------------------------------------
  //Update Amex Deposit-------------------------------
  updateAmexDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('depositDate', data.depositDate);
    formData.append('amexAmount', data.amexAmount);
    
    return this.http.put(apiUrl+`branch/deposit/amex`,formData,httpOptions)
   }
  //---------------------------------------------
  //Other Deposit--------------------------------
  //Get other deposit list-----------------------
  getOtherDepositList(data):Observable<any>{
      var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
      return this.http.get(apiUrl+`branch/deposit/other?`+body,httpOptions)
    }
  //---------------------------------------------
  //Submit other deposit-------------------------
  submitOtherDeposit(data):Observable<any>{ 
    const formData: FormData = new FormData();
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('depositDate', data.depositDate);
    formData.append('otherDepositDetail', data.otherDepositDetail);
    
    return this.http.post(apiUrl+`branch/deposit/other`,formData,httpOptions)
   }
  //---------------------------------------------
   //Update Other Deposit-------------------------------
  updateOtherDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('depositDate', data.depositDate);
    formData.append('amount', data.amount);
    
    return this.http.put(apiUrl+`branch/deposit/other`,formData,httpOptions)
   }
  //---------------------------------------------
  //CHECK Deposit---------------------------------
  //Get CHECK Deposit list-----------------------
    getCheckDepositList(data):Observable<any>{
      var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&depositDate="+data.depositDate;
      return this.http.get(apiUrl+`branch/deposit/check?`+body,httpOptions)
    }
  //---------------------------------------------
  //Submit other deposit-------------------------
  submitCheckDeposit(data):Observable<any>{ 
    const formData: FormData = new FormData();
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('depositDate', data.depositDate);
    formData.append('paymentFormId', data.paymentFormId);
    formData.append('checkAmount', data.checkAmount);
    formData.append('checkDetail', data.checkDetail);
    
    return this.http.post(apiUrl+`branch/deposit/check`,formData,httpOptions)
   }
  //---------------------------------------------
  //Update Check Deposit-------------------------------
  updateCheckDeposit(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('depositDate', data.depositDate);
    formData.append('checkAmount', data.checkAmount);
    
    return this.http.put(apiUrl+`branch/deposit/check`,formData,httpOptions)
   }
  //---------------------------------------------
  
}
