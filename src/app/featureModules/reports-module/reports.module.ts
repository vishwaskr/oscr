import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { ReportsRoutingModule } from './reports-routing/reports-routing.module';
import { ReportsComponent } from './component/reports/reports.component';


@NgModule({
  declarations: [ReportsComponent],
  imports: [
    SharedModule,
    CommonModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
