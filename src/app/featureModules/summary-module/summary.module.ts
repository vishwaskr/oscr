import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatProgressBarModule} from '@angular/material';
import { SummaryComponent } from './component/summary/summary.component';
import { SharedModule } from '../../shared/shared.module';
import {SummaryRoutingModule} from './summary-routing/summary-routing.module';
import { AlertComponent } from './component/alert/alert/alert.component';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [SummaryComponent, AlertComponent],
  imports: [
    CommonModule,
    MatProgressBarModule,
    SharedModule,
    SummaryRoutingModule,
    NgxMaskModule
  ]
})
export class SummaryModule { }
