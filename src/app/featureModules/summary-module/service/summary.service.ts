import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

//httpHeaders options------------------------------------
const httpOptions = {
  headers: new HttpHeaders({
    "Accept": "application/json"
  }),
 withCredentials: true
};

//-------------------------------------------------------
//API base url-------------------------------------------
var apiUrl=environment.apiHost;
//-------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class SummaryService {

  constructor(private http: HttpClient) { }
  //Get Summary details-------------------
  getSummaryDetails(data):Observable<any>{
    var apiUrl=environment.apiHost;
    var body ="branchId="+data.branchId+"&authUserId="+data.authUserId+"&fromDate="+data.fromDate+"&toDate="+data.toDate;
    return this.http.get(apiUrl+`branch/summary?`+ body,httpOptions);
  }
  //------------------------------------
  
  //Update summary data-----------------
  updateSummary(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('depositId', data.depositId);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('summaryId', data.summaryId);
    formData.append('attendance', data.attendance);
    formData.append('weather', data.weather);
    formData.append('comment', data.comment);
    
    return this.http.put(apiUrl+`branch/summary`,formData,httpOptions)
   }
  //------------------------------------
  //Get weather-------------------------
  getWeather(data):Observable<any>{
    
    return this.http.get('https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=d7c0e9516d1b50af1dbd0d77815a37cc');
  }
  //------------------------------------
  //Submit to Corporate-----------------
  submit_to_corporate(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('date', data.date);
    
    return this.http.post(apiUrl+`branch/submittocorporate`,formData,httpOptions)

   }
  //------------------------------------
}
