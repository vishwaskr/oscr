import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../constants.js";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import * as jq from 'jquery';
import * as moment from 'moment';

import { SummaryService } from 'src/app/featureModules/summary-module/service/summary.service';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';

export interface SUMMARY {
  SummaryId: number;
  Attendance: number;
  CMOTotal: number;
  Comment: string;
  SummaryDate: any;
  DepositTotal: number;
  Documents: string;
  GSRTotal: number;
  Status: string;
  Weather: number;
}


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})

export class SummaryComponent implements OnInit {

  oscrSummaryFilterForm: FormGroup;
  startDate:any;
  endDate:any;
  userBranchArr:any;
  filterBranchId: number;
  currentBranchName:string;
  summaryDataSource: any;
  authUserId:any;
  summaryTableShow: boolean=false;
  toDate:any;
  fromDate:any;
  fromDate2:any;
  currentDate = moment(new Date()).format(dateFormat);
  defaultOSCRSummaryDay:number=1;
  //Progres----------------
   progressShow: boolean;
   mode = 'determinate';
   value = 50;
   bufferValue = 95;
 //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;
  
  temp:number;
  
  constructor(public summaryService: SummaryService, public getDefaultConfigService: SettingsService) { }

  
   displayedColumns: string[] = ['date', 'attendance', 'weather', 'cmoTotal', 'depositTotal', 'gsrTotal', 'documents', 'status', 'comment'];
  
   @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
   @ViewChild(MatSort, {static: false}) sort: MatSort;
   
  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    //Fetch all branches for current user----------
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //---------------------------------------------
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
    this.currentBranchName=this.userBranchArr[0]['Branch_Name'];
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
   
    //Form controller------------------------------
   
    
    this.toDate = new FormControl(new Date());
    this.fromDate=moment(new Date()).subtract(this.defaultOSCRSummaryDay,'days').format(dateFormat);
    this.fromDate2 = new FormControl(new Date(this.fromDate));
    //console.log('fromDate:'+moment(new Date(prvData)).format(dateFormat));
    
    this.oscrSummaryFilterForm = new FormGroup({
      filterBranchId: new FormControl(this.filterBranchId),
      startDate: new FormControl(this.fromDate2.value),
      endDate: new FormControl(this.toDate.value)
    })
    //---------------------------------------------
    
     this.getDefaultConfig();
     
     this.getWeather();
  }
  //-----------------------------------------------
  
   //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   console.log('IDD:'+idd);
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Datpicker function-----------------------------
  
  events: string[] = [];

  onChangeStartDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.startDate=formattedDate;
   this.fromDate=formattedDate;
   this.fromDate2=formattedDate;
   this.getSummaryDetails();
  }
  
  onChangeEndDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.endDate=formattedDate;
   this.toDate=formattedDate;
   this.getSummaryDetails();
  }
  //-----------------------------------------------
  
  //Get Default config------------------------------
  getDefaultConfig(){
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const configDetails = {
      'branchId'  : branchId,
      'authUserId': this.authUserId
    }
    
    this.getDefaultConfigService.getDefaultConfig(configDetails).subscribe(res=>{
        if(res.status=='success') {
          this.defaultOSCRSummaryDay = res.data.DefaultOSCRSummaryDay;
         
          this.fromDate=moment(new Date()).subtract(this.defaultOSCRSummaryDay,'days').format(dateFormat);
          this.fromDate2 = new FormControl(new Date(this.fromDate));
          this.oscrSummaryFilterForm.controls['startDate'].setValue(this.fromDate2.value);
          this.getSummaryDetails();
        }
      });
    
  }
  //------------------------------------------------
  //Get summary details----------------------------
  getSummaryDetails(){
    
    this.progressBarShow();
   // var prvData=moment(new Date()).subtract(5,'days').format(dateFormat);
    localStorage.setItem('currentBranchId', JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', JSON.stringify(this.currentBranchName));
    //console.log(this.startDate+'--'+this.endDate);
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const summaryDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'fromDate': this.fromDate,
      'toDate': moment(this.toDate).format(dateFormat)
     }
  //  console.log(summaryDetails);
    var summaryDataArray= [];
    this.summaryService.getSummaryDetails(summaryDetails).subscribe(res=>{
    this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
        if (res.data.length > 0) {
          res.data.forEach(function (value) {
           let summaryData =  {} as SUMMARY;
               summaryData.SummaryId = value.SummaryId;
               summaryData.Attendance = value.Attendance;
               summaryData.CMOTotal = value.CMOTotal;
               summaryData.Comment = value.Comment;
               summaryData.SummaryDate = moment(value.SummaryDate).format(dateFormat);
               summaryData.DepositTotal = value.DepositTotal;
               
               summaryData.Documents = "No";
               if (value.Documents) {
                summaryData.Documents = "Yes";
               }
               
               summaryData.GSRTotal = value.GSRTotal;
               summaryData.Status = value.Status;
               summaryData.Weather = value.Weather;
               
               summaryDataArray.push(summaryData);
          });
          
          var SUMMARY_DATA: SUMMARY[] = summaryDataArray;
          this.summaryDataSource = new MatTableDataSource<SUMMARY>(SUMMARY_DATA);
          this.summaryDataSource.paginator = this.paginator;
          this.summaryDataSource.sort = this.sort;
          this.summaryTableShow=true;
        }
        else{
         this.summaryTableShow=false; 
        }
      }
    })
    
  }
  //----------------------------------------------
  //Edit Attendance-------------------------------
  editAtten(summaryId){
    document.getElementById("edit-box-"+summaryId).style.display="none";
    document.getElementById("save-box-"+summaryId).style.display="flex";
  }
  cancelAtten(summaryId){
    document.getElementById("edit-box-"+summaryId).style.display="flex";
    document.getElementById("save-box-"+summaryId).style.display="none";
  }
  //----------------------------------------------
  //Edit Weather-------------------------------
  editWeather(summaryId){
    document.getElementById("w-edit-box-"+summaryId).style.display="none";
    document.getElementById("w-save-box-"+summaryId).style.display="flex";
  }
  cancelWeather(summaryId){
    document.getElementById("w-edit-box-"+summaryId).style.display="flex";
    document.getElementById("w-save-box-"+summaryId).style.display="none";
  }
  //----------------------------------------------
  //Edit Comment-------------------------------
  editComment(summaryId){
    document.getElementById("comment-edit-box-"+summaryId).style.display="none";
    document.getElementById("comment-save-box-"+summaryId).style.display="flex";
  }
  cancelComment(summaryId){
    document.getElementById("comment-edit-box-"+summaryId).style.display="flex";
    document.getElementById("comment-save-box-"+summaryId).style.display="none";
  }
  //----------------------------------------------
  //Save Attendance-------------------------------
  saveSummaryInfo(summaryId){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    let attenData = +(document.getElementById("attn-input-"+summaryId) as HTMLInputElement).value;
    let weatherData = +(document.getElementById("w-input-"+summaryId) as HTMLInputElement).value;
    var comment = (document.getElementById("comment-input-"+summaryId) as HTMLInputElement).value;
    
    const submitDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'updateBy': this.authUserId,
      'summaryId': summaryId,
      'attendance': attenData,
      'weather': weatherData,
      'comment': comment
     }
     //console.log(submitDetails);
     //call api-------------------------
     this.summaryService.updateSummary(submitDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
        this.getDefaultConfig();
        document.getElementById("edit-box-"+summaryId).style.display="flex";
        document.getElementById("save-box-"+summaryId).style.display="none";
        this.showErrorMsg=false;
        this.successmessage=res.message;
        setTimeout(() => {
          this.successmessage='';
        }, 2000); 
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
     });
     //---------------------------------
     
   // console.log('attenData:'+attenData);
  }
  //----------------------------------------------
  //Get weather-----------------------------------
  getWeather(){
    this.summaryService.getWeather('').subscribe(res=>{
      //kelvin to fahrenheit
      //(278K - 273.15) � 9/5 + 32 = 40.73�F
      let tempK = res.main.temp;
      let tempF = (tempK - 273.15) * 9/5 + 32;
      this.temp = parseFloat(tempF.toFixed(2));
      console.log('Temp(F):'+this.temp);
    });
  }
  //----------------------------------------------
  //Submit to Corporate---------------------------
  submit_to_corporate(){
   this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const cropDetails = {
      'branchId'  : branchId,
      'date' : moment(this.toDate).format(dateFormat),
      'authUserId': this.authUserId
    }
   // console.log(cropDetails);
   this.summaryService.submit_to_corporate(cropDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        setTimeout(() => {
          this.successmessage='';
          this.getSummaryDetails();
        }, 2000); 
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
   });
   
  }
  //----------------------------------------------
  //Get summary details when branch change--------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    this.filterBranchId=BranchId;
    this.currentBranchName=Branch_Name;
    
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', Branch_Name);
    
    
    this.getSummaryDetails();
  }
  //----------------------------------------------
  
   
 //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //-------------------------------- 
}


