import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Route} from '@angular/router';

// importing component
import {SummaryComponent} from '../component/summary/summary.component';

//importing guard
import {AuthGuard} from '../../../core/auth/auth.guard';

const route:Route[] =[
  {
    path:'',
    redirectTo:'summary',
    pathMatch:'full'
  },
  {
    path:'summary',
    component:SummaryComponent,
    canActivate:[AuthGuard]
  }  
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class SummaryRoutingModule { }
