import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormGroup, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import * as jq from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-default-config',
  templateUrl: './default-config.component.html',
  styleUrls: ['./default-config.component.css']
})
export class DefaultConfigComponent implements OnInit {
  
  defaultConfigForm: FormGroup;
  authUserId:any;
  signOutAmount:number;
  summaryDays:number;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
   //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
  //-----------------------
  
  constructor(public getDefaultConfigService: SettingsService, public updateDefaultConfigService: SettingsService,public router: Router) { }

  ngOnInit() {
    this.side_slide();
     this.authUserId=JSON.parse(localStorage.getItem('UserId'));
     this.getDefaultConfig();
    //Form controller------------------------------
    this.createForm();
    //---------------------------------------------
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Get Default config------------------------------
  getDefaultConfig(){
    this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const configDetails = {
      'branchId'  : branchId,
      'authUserId': this.authUserId
    }
    
    this.getDefaultConfigService.getDefaultConfig(configDetails).subscribe(res=>{
      this.progressBarHide();
        if(res.status=='success') {
         this.signOutAmount = res.data.DefaultSignOutAmount
         this.summaryDays = res.data.DefaultOSCRSummaryDay
          //Form controller------------------------------
          this.createForm();
          //---------------------------------------------
        }
        else{
         
        }
        
      });
    
  }
  //------------------------------------------------
    
    
    //Submit Default Config------------------------
    defaultConfigFormSubmit(){
      if(this.defaultConfigForm.valid){
        this.progressBarShow();
         var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
         
        if(this.defaultConfigForm.valid){
         //console.log(this.defaultConfigForm);
        const configDetails = {
        'signOutAmount'  : this.defaultConfigForm.value.signOutAmount,
        'defaultBranchSummaryDay'  : this.defaultConfigForm.value.summaryDays,
        'branchId'  : branchId,
        'updateBy'  : this.authUserId,
        'authUserId': this.authUserId
        }
       
       //console.log(configDetails);
       
        this.updateDefaultConfigService.updateDefaultConfig(configDetails).subscribe(res=>{
        this.progressBarHide();
          if(res.status=='success') {
            this.showErrorMsg=false;
            this.successmessage= 'Success! Config Updated.';
            
            setTimeout(() => {
             this.router.navigate(['/setting/setting']);
            }, 2000);
            
          }
          else{
           this.showErrorMsg=true;
           this.errormessage= res.message;
          }
          
        });
        }
      }
    }
    //---------------------------------------------
    
    //Create Form----------------------------------
    createForm(){
     this.defaultConfigForm = new FormGroup({
      signOutAmount: new FormControl(this.signOutAmount,[Validators.required,Validators.pattern(/^[0-9]*$/)]),
      summaryDays: new FormControl(this.summaryDays,[Validators.required,Validators.pattern(/^[0-9]*$/)])
    }) 
    }
    //---------------------------------------------
    
    //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------

}
