import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormGroup, FormControl, FormArray, FormBuilder, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as jq from 'jquery';

export interface POS {
  posId: string;
  posName: string;
}

@Component({
  selector: 'app-edit-register',
  templateUrl: './edit-register.component.html',
  styleUrls: ['./edit-register.component.css']
})

export class EditRegisterComponent implements OnInit {

  editRegisterForm: FormGroup;
  noOfRegister: number;
  siteId:string;
  siteName:string;
  regfields:any;
  regDeta:any;
  posOptions:any;
  authUserId:any;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
  constructor(private fb: FormBuilder, public getPOSListService: SettingsService, public updateRegisterService: SettingsService, public router: Router) { }

  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.siteName=localStorage.getItem("siteName");
    this.siteId=localStorage.getItem("siteId");
    this.getPosList();
    this.regDeta=JSON.parse(localStorage.getItem("regData"));
    this.noOfRegister=this.regDeta.length
    this.createForm(this.noOfRegister);
     
    // console.log(this.regDeta[0]);
    // console.log(this.regDeta[0].POSModel.POSId);
     for (let i = 0; i < this.noOfRegister; i++) {
      
       var RegisterId= this.regDeta[i].RegisterId;
       var RegisterNumber=this.regDeta[i].RegisterNumber;
       var POSId=this.regDeta[i].POSModel.POSId;
       var isactive=this.regDeta[i].IsActive;
       this.addRegister(RegisterId,RegisterNumber,POSId,isactive);
    }
    
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
  get RegFields() {
    return this.editRegisterForm.get('regfields') as FormArray
  }
  
  addRegister(RegisterId,RegisterNumber,POSId,isactive) {
     const register = this.fb.group({ 
      registerId: [RegisterNumber],
      posId: [POSId],
      isActive:[isactive]
    })

    this.RegFields.push(register)
  }
  
   //deleteRegister(i){
   // this.noOfRegister=this.noOfRegister-1;
   // this.RegFields.removeAt(i)
   //}
  
  //On change register no-------------------------
  //onChangeRegisterNo(event){
  //  
  //  //console.log(event.target.value);
  //  let noOfRegisterP=0;
  //  
  //  noOfRegisterP=event.target.value;
  //
  //  this.createForm(noOfRegisterP);
  //   
  //  //For add--------------------------------------
  //  for (let i = 0; i < noOfRegisterP; i++) {
  //  //this.addRegister(i+1);
  //  }
  //  
  //  //---------------------------------------------
  //}
  
  createForm(noOfRegister){
    this.noOfRegister=noOfRegister;
    this.editRegisterForm = this.fb.group({
      //noOfRegister: this.noOfRegister,
      regfields: this.fb.array([],[Validators.required])
    })
  }
  //----------------------------------------------
  
  //Submit Register form--------------------------
  submitRegisterForm(){
  if(this.editRegisterForm.valid){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    this.regfields=this.editRegisterForm.value.regfields;
    //console.log("BranchID: "+branchId);
    const RegSiteDetails = {
        'branchId': branchId,
        'siteId': this.siteId,
        'regfields': JSON.stringify(this.regfields),
        'authUserId': this.authUserId
    }
   // console.log(RegSiteDetails);
    //call api------------
      this.updateRegisterService.updateRegisterToSite(RegSiteDetails).subscribe(res=>{
      // console.log(res);
         if(res.status=='success') {
           this.showErrorMsg=false;
           this.successmessage= 'Success! Register Updated.';
         
          setTimeout(() => {
           this.router.navigate(['/setting/manage-register']);
          }, 2000);
         }
         else{
         this.showErrorMsg=true;
         this.errormessage= res.message;
        }
         
      });
     //------------------ 
    
    }
    
  }
  //----------------------------------------------
  
  //Get POS List----------------------------------
  getPosList(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const posDetails = {
        'branchId': branchId,
        'authUserId': this.authUserId
    }
    var posArray= [];
    //call api------------
      this.getPOSListService.getPOSList(posDetails).subscribe(res=>{
       //console.log(res);
         if(res.status=='success') {
          //console.log(res.data);
          res.data.forEach(function (value) {
            let posData =  {} as POS;
            
            posData.posId = value.POSId;
            posData.posName = value.POSName;
            
            posArray.push(posData);
           
            });
         
            this.posOptions = posArray;
            
        } 
      });
    //-------------------
  }
  //----------------------------------------------

}
