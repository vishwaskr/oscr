import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import * as jq from 'jquery';
import { Router } from '@angular/router';

export interface RegisterSiteList {
  SerialNo: number;
  SiteName: string;
  SiteId: number;
  NoofRegister: number;
  BranchWiseSiteId : number;
}

enum SiteType {
    ExistsSite  = 1,
    NoExistSite = 2,
    ActiveSite  = 3,
}


@Component({
  selector: 'app-manage-register',
  templateUrl: './manage-register.component.html',
  styleUrls: ['./manage-register.component.css']
})
export class ManageRegisterComponent implements OnInit {
  
  dataSource:any;
  siteName:string;
  authUserId:any;
  
  userBranchArr:any;
  filterBranchId: number;
  
  //Progres----------------
   progressShow: boolean;
   mode = 'determinate';
   value = 50;
   bufferValue = 95;
   //-----------------------
  
  constructor(public getRegisterSiteListService: SettingsService, public getRegisterDetailsService: SettingsService, public router: Router) { }
  
  displayedColumns: string[] = ['serialNo', 'siteName', 'noOfRegister', 'action'];
  
  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    //Fetch all branches for current user----------
  this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
  //---------------------------------------------
  //For default selected branch------------------
  this.filterBranchId=this.userBranchArr[0]['BranchId'];
  //---------------------------------------------
  var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  if (branchId!='') {
    this.filterBranchId=branchId;
  }
    this.getRegisterSiteList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
  getRegisterSiteList(){
    
  // var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
   this.progressBarShow();
  
  const SiteDetails = {
      'branchId': this.filterBranchId,
      'siteListType': SiteType.ActiveSite,
      'authUserId': this.authUserId
     }
     //console.log(RegisterSiteDetails);
     var siteArray= [];
    //call api------------
      this.getRegisterSiteListService.getRegisterSiteList(SiteDetails).subscribe(res=>{
        this.progressBarHide();
       // console.log(res);
         if(res.status=='success') {
          let i=1;
            res.data.forEach(function (value) {
            let siteData =  {} as RegisterSiteList;
            
            siteData.SerialNo = i;
            siteData.BranchWiseSiteId = value.BranchWiseSiteId;
            siteData.SiteName = value.SiteName;
            siteData.SiteId = value.SiteId;
            siteData.NoofRegister = value.NoofRegister;
            
            siteArray.push(siteData);
            i++;
            });
            
            var SITE_DATA: RegisterSiteList[] = siteArray;
            this.dataSource = SITE_DATA;
            
        }
        else{
          
        } 
      });
      
    //this.dataSource = ELEMENT_DATA; 
  }
  
  //Add Register----------------------------------------
  addRegister(siteName,siteId){
   localStorage.setItem("siteName",siteName);
   localStorage.setItem("siteId",siteId);
   this.router.navigate(['/setting/add-register']);  
  }
  //----------------------------------------------------
  //EditRegister----------------------------------------
  editRegister(siteName,siteId){
   localStorage.setItem("siteName",siteName);
   localStorage.setItem("siteId",siteId);
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const registerDetails = {
      'branchId'  : branchId,
      'siteId'  : siteId,
      'authUserId': this.authUserId
     }
    
    //Call api--------------------
   this.getRegisterDetailsService.getRegisterDetails(registerDetails).subscribe(res=>{
   //console.log(res);
   localStorage.setItem("regData",JSON.stringify(res.data));
   this.router.navigate(['/setting/edit-register']); 
  });
     
  }
  //----------------------------------------------------
  //Get Registered Site list when branch change---------
  changeBranch(BranchId,Branch_Name){
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', Branch_Name);
    this.getRegisterSiteList();
  }
  //----------------------------------------------------
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  

}
