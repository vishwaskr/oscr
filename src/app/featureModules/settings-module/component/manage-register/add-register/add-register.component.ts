import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormGroup, FormControl, FormArray, FormBuilder, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as jq from 'jquery';

export interface POS {
  posId: string;
  posName: string;
}

@Component({
  selector: 'app-add-register',
  templateUrl: './add-register.component.html',
  styleUrls: ['./add-register.component.css']
})
export class AddRegisterComponent implements OnInit {
  
  addRegisterForm: FormGroup;
  noOfRegister: number;
  siteId:string;
  siteName:string;
  regfields:any;
  authUserId:any;
  posOptions:any;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
  constructor(private fb: FormBuilder,public getPOSListService: SettingsService,public addRegisterToSiteService: SettingsService, public router: Router) { }

  ngOnInit() {
    this.side_slide();
    this.siteName=localStorage.getItem("siteName");
    this.siteId=localStorage.getItem("siteId");
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getPosList();
    
    this.createForm(0);
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------

   get RegisterFields() {
    return this.addRegisterForm.get('regfields') as FormArray
  }
  
  addRegister(i) {
     const register = this.fb.group({ 
      registerId: [i],
      posId: []
    })

    this.RegisterFields.push(register)
  }
  
   deleteRegister(i){
    this.noOfRegister=this.noOfRegister-1;
    this.RegisterFields.removeAt(i)
   }
  
  //On change register no-------------------------
  onChangeRegisterNo(event){
    
    //console.log(event.target.value);
    let noOfRegisterP=0;
    
    noOfRegisterP=event.target.value;
  
    this.createForm(noOfRegisterP);
     
    //For add--------------------------------------
    for (let i = 0; i < noOfRegisterP; i++) {
    this.addRegister(i+1);
    }
    
    //---------------------------------------------
  }
  
  createForm(noOfRegister){
    this.noOfRegister=noOfRegister;
    this.addRegisterForm = this.fb.group({
      noOfRegister: [this.noOfRegister,[Validators.required,Validators.pattern(/^[0-9]*$/)]],
      regfields: this.fb.array([],[Validators.required])
    })
  }
  //----------------------------------------------
  //Get POS List----------------------------------
  getPosList(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const posDetails = {
        'branchId': branchId,
        'authUserId': this.authUserId
    }
    var posListArray= [];
    //call api------------
      this.getPOSListService.getPOSList(posDetails).subscribe(res=>{
       //console.log(res);
         if(res.status=='success') {
          //console.log(res.data);
          res.data.forEach(function (value) {
            let posData =  {} as POS;
            
            posData.posId = value.POSId;
            posData.posName = value.POSName;
            
            posListArray.push(posData);
           
            });
         
            this.posOptions = posListArray;
            
        } 
      });
    //-------------------
  }
  //----------------------------------------------
  //Submit Register form--------------------------
  submitRegisterForm(){
  if(this.addRegisterForm.valid){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    this.regfields=this.addRegisterForm.value.regfields;
    const RegSiteDetails = {
        'branchId': branchId,
        'siteId': this.siteId,
        'registerDetails': JSON.stringify(this.regfields),
        'authUserId': this.authUserId
    }
    //console.log(RegSiteDetails);
    
    //call api------------
      this.addRegisterToSiteService.addRegisterToSite(RegSiteDetails).subscribe(res=>{
       //console.log(res);
         if(res.status=='success') {
           this.showErrorMsg=false;
           this.successmessage= 'Success! Register Added.';
         
          setTimeout(() => {
           this.router.navigate(['/setting/manage-register']);
          }, 2000);
         }
         else{
         this.showErrorMsg=true;
         this.errormessage= res.message;
        }
         
      });
     //------------------ 
    }
  }
  //----------------------------------------------
  
  
}
