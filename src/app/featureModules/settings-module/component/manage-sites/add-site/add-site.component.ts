import { Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {ErrorStateMatcher} from '@angular/material/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import * as jq from 'jquery';
import { Router } from '@angular/router';

export interface PeriodicElement {
  SiteID: number;
  SiteName: string;
  SiteDescription: string;
  IsActive: boolean;
}


@Component({
  selector: 'app-add-site',
  templateUrl: './add-site.component.html',
  styleUrls: ['./add-site.component.css']
})
export class AddSiteComponent implements OnInit {
  
  dataSource: any;
  selection: any;
  selectedSiteId: Array<number> = [];
  UniqeSelectedSiteId: Array<number> = [];
  submitDisabled:boolean;
  authUserId:any;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
  //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
 //-----------------------
 
  constructor(public getAllSiteListService: SettingsService,public addSiteToBranchService: SettingsService, public router: Router) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'siteId', 'siteName', 'siteDescreption'];
  
  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getAllSiteList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    
    //For getting selected site id------------------------
    if(this.selection.isSelected(row)==true) {
      this.selectedSiteId.push(row.SiteID);
    //  console.log(row.SiteID+": Checked");
    }
    else{
     //console.log(row.SiteID+": UnChecked");
        for( var i = 0; i < this.selectedSiteId.length; i++){ 
          if ( this.selectedSiteId[i] === row.SiteID) {
            this.selectedSiteId.splice(i, 1); 
          }
        }
    }
    //-----------------------------------------------------
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.SiteID + 1}`;
  }
  
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }
  
  //Get All site list---------------------------------------
  getAllSiteList(){
     this.progressBarShow();
    //getAllSiteListService
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const siteDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     var siteArray= [];
    //call api------------
      this.getAllSiteListService.getAllSiteList(siteDetails).subscribe(res=>{
        //console.log(res);
         this.progressBarHide();
         
        if(res.status=='success') {
         
          res.data.forEach(function (value) {
          let siteData =  {} as PeriodicElement;
          
          siteData.SiteID = value.SiteId;
          siteData.SiteName = value.SiteName;
          siteData.SiteDescription = value.SiteDescription;
          siteData.IsActive = value.IsActive;
          
          siteArray.push(siteData);
        });
       
         var ELEMENT_DATA: PeriodicElement[] = siteArray;
         this.dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
         this.selection = new SelectionModel<PeriodicElement>(true, []);
         this.dataSource.paginator = this.paginator; 
        }
        else{
          
        }
      });
    //--------------------
  }
  //--------------------------------------------------------
  //Submit site---------------------------------------------
  submitSite(){
    this.submitDisabled=true;
    
    this.progressBarShow();
    
    this.UniqeSelectedSiteId=[];
    //Create unique id-----------
    for(var i=0; i < this.selectedSiteId.length; i++){
        if(this.UniqeSelectedSiteId.indexOf(this.selectedSiteId[i]) === -1) {
            this.UniqeSelectedSiteId.push(this.selectedSiteId[i]);
        }
    }
    //---------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var siteIds=this.UniqeSelectedSiteId.join(',');
   
    const siteDetails = {
      'branchId': branchId,
      'siteList': siteIds,
      'createBy': this.authUserId,
      'authUserId': this.authUserId
    }
    // console.log(siteDetails);
    this.addSiteToBranchService.addSiteToBranch(siteDetails).subscribe(res=>{
      
      this.progressBarHide();
      
      this.submitDisabled=false;
        //console.log(res);
        if(res.status=='success') {
        //this.getAllSiteList();
        this.showErrorMsg=false;
        this.successmessage= 'Success! Site Added.';
          setTimeout(() => {
           this.router.navigate(['/setting/manage-sites']);
          }, 2000);
        
        }
        else{
          this.showErrorMsg=true;
          this.errormessage= res.message;  
        }
    });
  }
  //--------------------------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------

}
