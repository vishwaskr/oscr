import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import * as jq from 'jquery';


export interface SiteTableElement {
  BranchWiseSiteId: number;
  SiteName: string;
  SiteDescription: string;
  SiteTypes: obj[];
  IsOccasionalSite:string;
  IsActive: boolean;
  
}

interface obj {
  TypeName: string,
  TypeId: number
}

@Component({
  selector: 'app-manage-sites',
  templateUrl: './manage-sites.component.html',
  styleUrls: ['./manage-sites.component.css']
})

export class ManageSitesComponent implements OnInit {
  
  dataSource:any;
  IsActive:boolean;
  authUserId:any;
  userBranchArr:any;
  filterBranchId: number;
  //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
  //-----------------------
 
 // all site types
  allSiteTypes : obj[];

 
  constructor(public getAssignedSiteListService: SettingsService, public changeBranchSiteStatusService: SettingsService, public enableSiteTypeService:SettingsService,public getAllSiteTypeService: SettingsService) { }
  
  displayedColumns: string[] = ['siteId', 'siteName', 'siteStatus', 'IsOccasionalSite', 'siteType'];
  
  ngOnInit() {
  this.side_slide();
  this.authUserId=JSON.parse(localStorage.getItem('UserId'));
  //Fetch all branches for current user----------
  this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
  //---------------------------------------------
  //For default selected branch------------------
  this.filterBranchId=this.userBranchArr[0]['BranchId'];
  //---------------------------------------------
  var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  if (branchId!='') {
    this.filterBranchId=branchId;
  }
  
  //Get all Site types---------------------------------------
   this.getAllSiteTypeService.getAllSiteType(this.authUserId).subscribe(res=>{
    //this.allSiteTypes =[{TypeName: 'Food', TypeId: 1}, {TypeName: 'Retail', TypeId: 2}];
    //console.log(res);
    if(res.status=='success') {
     this.allSiteTypes =res.data;
    }
  });
  //---------------------------------------------------------
  this.getAssignedSiteList();
  
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
   
  comparer(o1: obj, o2: obj): boolean {
    // if possible compare by object's name, and not by reference.
    return o1 && o2 ? o1.TypeName === o2.TypeName : o2 === o2;
  }
  
  //Get branch assigned site list--------------------------------------
  getAssignedSiteList(){
    
  //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  this.progressBarShow();
    const siteDetails = {
      'branchId': this.filterBranchId,
      'siteListType': 1,
      'authUserId': this.authUserId
    }
   // console.log(siteDetails);
    var siteArray= [];
    //call api------------
      this.getAssignedSiteListService.getAssignedSiteList(siteDetails).subscribe(res=>{
        this.progressBarHide();
        //console.log(res);
        if(res.status=='success') {
            res.data.forEach(function (value) {
            let siteData =  {} as SiteTableElement;
          
            siteData.BranchWiseSiteId = value.BranchWiseSiteId;
            siteData.SiteName = value.SiteName;
            siteData.SiteDescription = value.SiteDescription;
            siteData.SiteTypes = value.SiteTypes;
            siteData.IsOccasionalSite='No';
            if (value.IsOccasionalSite) {
              siteData.IsOccasionalSite = 'Yes';
            }
            siteData.IsActive = value.IsActive;
            //siteData.SiteTypes = [{TypeName: 'Retail', TypeId: 1}];
           // console.log(value.SiteTypes);
            siteArray.push(siteData);
            });
           
            //this.selectedObjects = [{TypeName: 'Retail', TypeId: 1}];  
            
            var ELEMENT_DATA: SiteTableElement[] = siteArray;
            this.dataSource = ELEMENT_DATA; 
        }
      });
      //---------------------
  }
 //-------------------------------------------------------------------
  //Branch wise site status change-------------------------------------
  branchSiteStatusChange(event,BranchWiseSiteId){
    this.progressBarShow();
    //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    
    const siteDetails = {
      'branchId'  : this.filterBranchId,
      'branchWiseSiteId' :BranchWiseSiteId,
      'updateBy' : this.authUserId,
      'isActive' : event.checked,
      'authUserId': this.authUserId
    }
    
     //Call api--------------------
    this.changeBranchSiteStatusService.changeBranchSiteStatus(siteDetails).subscribe(res=>{
   // console.log(res);
     this.progressBarHide();
     if(res.status=='success') {
      
     }
     else{
      
     }
   });
    //console.log(BranchWiseSiteId);
  }
  //-------------------------------------------------------------------
  //Enable site type---------------------------------------------------
  enableSiteType(event,BranchWiseSiteId){
    
    if(event.isUserInput) {
      //console.log(event.source.value, event.source.selected);
      this.progressBarShow();
      
      const siteDetails = {
      'branchWiseSiteId': BranchWiseSiteId,
      'createBy' : this.authUserId,
      'isActive' : event.source.selected,
      'typeId' : event.source.value.TypeId,
      'authUserId': this.authUserId
      }
     
    //Call api--------------------
    this.enableSiteTypeService.enableSiteType(siteDetails).subscribe(res=>{
   // console.log(res);
     this.progressBarHide();
    
     if(res.status=='success') {
     this.getAssignedSiteList(); 
     }
     else{
      
     }
    });
    
      
    }
  }
  //-------------------------------------------------------------------
  //Get payment form list when branch change--------
  changeBranch(BranchId,Branch_Name){
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', Branch_Name);
    this.getAssignedSiteList();
  }
  //----------------------------------------------
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
}
