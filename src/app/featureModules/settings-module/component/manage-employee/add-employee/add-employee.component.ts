import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import { Router } from '@angular/router';
import * as jq from 'jquery';

export interface Role {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  
  addEmployeeForm: FormGroup;
  employeeName: string;
  employeeUserName: string;
  //employeeEmail: string;
  employeeId: string;
  employeeRole : number;
  employeeImage : any;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  fileToUpload: File = null;
  imageFileName:string='Upload Profile Image';
  empState:boolean; //If true then add
  readonly:boolean;
  empId:number;
  empDet: any;
  url: string;
  authUserId:any;

  

 constructor(public addEmployeeService: SettingsService,public updateEmployeeService: SettingsService, public router: Router) { }

 ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    if (localStorage.getItem("empState")=='add') {
     this.empState=true;
     this.readonly=false;
    }
    if (localStorage.getItem("empState")=='edit') {
      this.empState=false;
      this.readonly=true;
      this.empId=JSON.parse(localStorage.getItem("empId"));
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      
      this.empDet=JSON.parse(localStorage.getItem("empData"));
      
      //console.log(this.empDet);
      this.employeeName=this.empDet.Name;
      this.employeeUserName=this.empDet.UserName;
      if (this.empDet.EmployeeId=='null') {
         this.employeeId='';
      }
      else{
        this.employeeId=this.empDet.EmployeeId;
      }
      this.employeeRole=this.empDet.RoleId;
      this.employeeImage=this.empDet.ProfileImage;

    }
    
    //Form controller------------------------------
    this.addEmployeeForm = new FormGroup({
      employeeName: new FormControl(this.employeeName,[Validators.required]),
     // employeeEmail: new FormControl(this.employeeEmail, [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      employeeUserName: new FormControl(this.employeeUserName,[Validators.required]),
      employeeId: new FormControl(this.employeeId),
      employeeRole: new FormControl(this.employeeRole,[Validators.required]),
      employeeImage: new FormControl(this.employeeImage)
    })
    
    //---------------------------------------------
   
  }
  //----------------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------

  //matcher = new MyErrorStateMatcher();
  
  //Roles----------------------------------------------
  roles: Role[] = [
    {value: 4, viewValue: 'Others'}
  ];
  //---------------------------------------------------
  //Submit emoloyee form-------------------------------
  employeeFormSubmit(){
    
    if(this.addEmployeeForm.valid){
      //console.log(this.addEmployeeForm);
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      
     //Call api--------------------
     
    if (localStorage.getItem("empState")=='add') { //For add employee
       const empDetails = {
      'name'      : this.addEmployeeForm.value.employeeName,
      'userName'  : this.addEmployeeForm.value.employeeUserName,
      'branchId'  : branchId,
      'createBy'  : this.authUserId,
      'roleId'    : this.addEmployeeForm.value.employeeRole,
      'employeeId'  : this.addEmployeeForm.value.employeeId,
      'authUserId': this.authUserId
     }
     
      this.addEmployeeService.addEmployee(empDetails,this.fileToUpload).subscribe(res=>{
       //console.log(res);
        if(res.status=='success') {
          this.showErrorMsg=false;
          this.successmessage= 'Success! Employee Added.';
         
          setTimeout(() => {
           this.router.navigate(['/setting/manage-employee']);
          }, 2000);
          
        }
        else{
         this.showErrorMsg=true;
         this.errormessage= res.message;
        }
        
      });
    }
    else{ //For update employee
      const empDetails = {
      'userId'    : this.empId,
      'name'      : this.addEmployeeForm.value.employeeName,
      'userName'  : this.addEmployeeForm.value.employeeUserName,
      'branchId'  : branchId,
      'updateBy'  : this.authUserId,
      'roleId'    : this.addEmployeeForm.value.employeeRole,
      'employeeId'  : this.addEmployeeForm.value.employeeId,
      'userOldImage' : this.employeeImage,
      'authUserId': this.authUserId
     }
    // console.log(empDetails);
     this.updateEmployeeService.updateEmployee(empDetails,this.fileToUpload).subscribe(res=>{
       // console.log(res);
        if(res.status=='success') {
          this.showErrorMsg=false;
          this.successmessage= res.message;
         
          setTimeout(() => {
           this.router.navigate(['/setting/manage-employee']);
          }, 2000);
          
          
        }
        else{
         this.showErrorMsg=true;
         this.errormessage= res.message;
        }
        
      }); 
    }
     //----------------------------
     
    }
    else{
      this.showErrorMsg=true;
    }
  }
  //---------------------------------------------------
  //For file upload------------------------------------
  handleFileInput(files: FileList) {
    //console.log(files.item(0));
    var reader = new FileReader();
    reader.readAsDataURL(files.item(0));
     
    var fd=files.item(0);
    this.imageFileName=fd.name;
    this.fileToUpload = files.item(0);
    
    reader.onload = (event) => { // called once readAsDataURL is completed
        this.employeeImage=reader.result;
      }
  }
  //--------------------------------------------------  
}
