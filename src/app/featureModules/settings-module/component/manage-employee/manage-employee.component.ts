import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {ErrorStateMatcher} from '@angular/material/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import * as jq from 'jquery';
import Swal from 'sweetalert2';

export interface EmployeeTableElement {
  userid:number;
  name: string;
  image: string;
  employeeId: string;
  username: string;
  role:string;
}

@Component({
  selector: 'app-manage-employee',
  templateUrl: './manage-employee.component.html',
  styleUrls: ['./manage-employee.component.css']
})
export class ManageEmployeeComponent implements OnInit {

 employeeListArr: string[];
 dataSource: any;
 empState:string;
 authUserId:any;
 //Progres----------------
 progressShow: boolean;
 mode = 'determinate';
 value = 50;
 bufferValue = 95;
 //-----------------------
 
 
 
  constructor(public employeeService: SettingsService,public getEmployeeService: SettingsService,public deleteEmployeeService: SettingsService, public router: Router) { }
  
  displayedColumns: string[] = ['image', 'employeeId', 'name', 'username', 'role', 'action'];
 
   @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
   
   
  ngOnInit() {
     this.side_slide();
     this.authUserId=JSON.parse(localStorage.getItem('UserId'));
     this.getEmployeeList();
  }
  //------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------

  getEmployeeList(){
    
    this.progressBarShow();
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var empArray= [];
    var defaultUserImage = 'assets/img/profile.jpg';
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.employeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      this.progressBarHide();
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as EmployeeTableElement;
          empData.userid = value.UserId;
          empData.image = value.ProfileImage;
          if (value.ProfileImage=='') {
            empData.image=defaultUserImage;
          }
          empData.name = value.Name;
          
          if (value.EmployeeId=='null') {
           empData.employeeId=''; 
          }
          else{
           empData.employeeId = value.EmployeeId; 
          }
          
          
          empData.username = value.UserName;
          empData.role = value.RoleName;
          
          empArray.push(empData);
        });
       
         var ELEMENT_DATA: EmployeeTableElement[] = empArray;
         this.dataSource = new MatTableDataSource<EmployeeTableElement>(ELEMENT_DATA);
         this.dataSource.paginator = this.paginator;
      }
      else{
       //console.log('Error');
      }
      
    })
    //--------------------
  }

  //Add employee--------------------
  addEmployee(){
   this.empState='add';
   localStorage.setItem("empState",this.empState);
   this.router.navigate(['/setting/add-employee']); 
  }
  //--------------------------------
  //Edit employee-------------------
  editEmployee(empId){
   this.empState='edit';
   localStorage.setItem("empState",this.empState);
   localStorage.setItem("empId",JSON.stringify(empId));
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const empDetails = {
      'branchId'  : branchId,
      'userId'  : empId,
      'authUserId': this.authUserId
     }
    
  //Call api--------------------
   this.getEmployeeService.getEmployeeDetails(empDetails).subscribe(res=>{
   //console.log(res);
   localStorage.setItem("empData",JSON.stringify(res.data));
   this.router.navigate(['/setting/edit-employee']); 
  });
   
  }
  //--------------------------------
  //Delete employee-----------------
  deleteEmployee(empId){
    //console.log(empId);
  var branchId=JSON.parse(localStorage.getItem("currentBranchId")); 
  Swal.fire({
  title: 'Are you sure?',
 // text: 'You will not be able to recover this imaginary file!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
      this.deleteEmployeeServiceApi(empId,branchId);
      //Swal.fire(
      //  'Deleted!',
      //  'Your imaginary file has been deleted.',
      //  'success'
      //)
    }
    //else if (result.dismiss === Swal.DismissReason.cancel) {
    //  Swal.fire(
    //    'Cancelled',
    //    'Your imaginary file is safe :)',
    //    'error'
    //  )
    //}
  });
    
}
//------------------------------
deleteEmployeeServiceApi(empId,branchId){
   const employeeDetails = {
      'userId': empId,
      'branchId'  : branchId,
      'authUserId': this.authUserId
     }
     //console.log(employeeDetails);
    //call api------------
      this.deleteEmployeeService.deleteEmployee(employeeDetails).subscribe(res=>{
       // console.log(res);
         if(res.status=='success') {
          Swal.fire(
            'Deleted!',
            res.message,
            'success'
          )
          this.getEmployeeList(); //Refresh employee list
         }
         else{
          Swal.fire(
            'Cancelled!',
            res.message,
            'error'
          )
         }
      });
    //------------------
}
  
//--------------------------------
progressBarShow(){
 this.progressShow=true;
 this.mode="indeterminate"; 
}
progressBarHide(){
  this.mode="determinate";
  this.progressShow=false;
}
//--------------------------------

}

