import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCardHandheldComponent } from './manage-card-handheld.component';

describe('ManageCardHandheldComponent', () => {
  let component: ManageCardHandheldComponent;
  let fixture: ComponentFixture<ManageCardHandheldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCardHandheldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCardHandheldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
