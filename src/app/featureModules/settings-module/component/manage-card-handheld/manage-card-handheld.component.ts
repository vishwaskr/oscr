import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as jq from 'jquery';
import Swal from 'sweetalert2';

export interface CardHandheldDataList {
  cardid:number;
  slno: number;
  tid: string;
  deviceName: string;
}


@Component({
  selector: 'app-manage-card-handheld',
  templateUrl: './manage-card-handheld.component.html',
  styleUrls: ['./manage-card-handheld.component.css']
})
export class ManageCardHandheldComponent implements OnInit {
  
  dataSource:any;
  authUserId:any;
  cardState:string;

  //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
  //-----------------------
 
  constructor(public getAssignedCardHandheldListService: SettingsService,public getCardHandheldDetailsService: SettingsService,public deleteCardHandheldDetailsService: SettingsService, public router: Router) { }
  
  displayedColumns: string[] = ['slno', 'tid', 'deviceType', 'action'];
  
  ngOnInit() {
     this.side_slide();
     this.authUserId=JSON.parse(localStorage.getItem('UserId'));
     this.getAssignedCardHandheldList();
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
    
  //Get Assigned CardHandheld list-------------------------------
  getAssignedCardHandheldList(){
  var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  this.progressBarShow();
    const cardDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
    }
    var cardHandheldDataArray= [];
    //call api------------
      this.getAssignedCardHandheldListService.getAssignedCardHandheldList(cardDetails).subscribe(res=>{
        this.progressBarHide();
       // console.log(res);
        if(res.status=='success') {
            res.data.forEach(function (value) {
            let cardData =  {} as CardHandheldDataList;
          
            cardData.cardid = value.CardHandheldId;
            cardData.slno = value.CardHandheldSerialNumber;
            cardData.tid = value.CardHandheldTidNumber;
            cardData.deviceName = value.DeviceName;
            
            cardHandheldDataArray.push(cardData);
            });
            
            var CARDHANDHELD_DATA: CardHandheldDataList[] = cardHandheldDataArray;
            this.dataSource = CARDHANDHELD_DATA;
            
        }
        else{
          
        }
      });
      //---------------------
  }
  //-------------------------------------------------------------
  //Add CardHandHeld--------------------
  addCardHandHeld(){
   this.cardState='add';
   localStorage.setItem("cardState",this.cardState);
   this.router.navigate(['/setting/add-card-handheld']); 
  }
  //-----------------------------------
  //Edit CardHandHeld------------------
  editCardHandheld(cardHandId){
   this.cardState='edit';
  localStorage.setItem("cardState",this.cardState);
  var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
  localStorage.setItem("cardHandId",JSON.stringify(cardHandId));
  
  const cardDetails = {
      'branchId'  : branchId,
      'cardHandheldId'  : cardHandId,
      'authUserId': this.authUserId
     }
    
     //Call api--------------------
   this.getCardHandheldDetailsService.getCardHandheldDetails(cardDetails).subscribe(res=>{
   console.log(res);
   localStorage.setItem("cardData",JSON.stringify(res.data));
   this.router.navigate(['/setting/edit-card-handheld']);
   
  });
  
  }
  //-----------------------------------
  //Delete CardHandHeld----------------
  deleteCardHandHeld(cardHandId){
 //  console.log(cardHandId);
    Swal.fire({
    title: 'Are you sure?',
   // text: 'You will not be able to recover this imaginary file!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.deleteCardHandHeldServiceApi(cardHandId);
      }
    });
  }
  //-----------------------------------
  //Delete api call--------------------
  deleteCardHandHeldServiceApi(cardHandId){
      const cardDetails = {
      'cardHandId': cardHandId,
      'authUserId': this.authUserId
     }
     //console.log(employeeDetails);
    //call api------------
      this.deleteCardHandheldDetailsService.deleteCardHandheld(cardDetails).subscribe(res=>{
        //console.log(res);
         if(res.status=='success') {
          Swal.fire(
            'Deleted!',
             res.message,
            'success'
          )
          this.getAssignedCardHandheldList(); //Refresh employee list
         }
         else{
          Swal.fire(
            'Cancelled!',
            res.message,
            'error'
          )
         }
      });
    //------------------
  }
  //-----------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
   
}
