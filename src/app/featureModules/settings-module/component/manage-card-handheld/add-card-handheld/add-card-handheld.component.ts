import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormGroup, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import * as jq from 'jquery';
import { Router } from '@angular/router';

export interface DEVICE_TYPE {
  DeviceName: string;
  DeviceTypeId: number;
}

@Component({
  selector: 'app-add-card-handheld',
  templateUrl: './add-card-handheld.component.html',
  styleUrls: ['./add-card-handheld.component.css']
})
export class AddCardHandheldComponent implements OnInit {
  
  authUserId:any;
  addCardHandheldForm: FormGroup;
  slno:number;
  tid:string;
  cardDeviceType:any;
  note:string;
  
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  cardState:boolean; //If true then add
  cardDet: any;
  cardHandId:number;
  cardDeviceTypeId:number;
  
  constructor(public getCardDeviceTypeListService: SettingsService, public addCardHandHeldService: SettingsService,public updateCardHandHeldService: SettingsService, public router: Router) { }

  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    if (localStorage.getItem("cardState")=='add') {
     this.cardState=true;
    }
    if (localStorage.getItem("cardState")=='edit') {
      this.cardState=false;
      this.cardHandId=JSON.parse(localStorage.getItem("cardHandId"));
      this.cardDet=JSON.parse(localStorage.getItem("cardData"));
      //console.log(this.cardDet);
      this.slno=this.cardDet.CardHandheldSerialNumber;
      this.tid=this.cardDet.CardHandheldTidNumber;
      this.note=this.cardDet.Note;
      this.cardDeviceTypeId=this.cardDet.DeviceId;
      
    }
    
    this.getCardDeviceTypeList();
    
    
     //Form controller------------------------------
    this.addCardHandheldForm = new FormGroup({
      slno: new FormControl(this.slno,[Validators.required]),
      tid: new FormControl(this.tid,[Validators.required]),
      note: new FormControl(this.note),
      cardDeviceType: new FormControl(this.cardDeviceTypeId,[Validators.required])
    })
    
    //---------------------------------------------
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------

  //Get Card Device type list---------------------------------
  getCardDeviceTypeList(){
    const cardDeviceDetails = {
        'authUserId': this.authUserId
    }
    var cardArray= [];
    //call api------------
      this.getCardDeviceTypeListService.getCardDeviceTypeList(cardDeviceDetails).subscribe(res=>{
      // console.log(res);
         if(res.status=='success') {
          //console.log(res.data);
          res.data.forEach(function (value) {
            let deviceData =  {} as DEVICE_TYPE;
            
            deviceData.DeviceTypeId = value.DeviceTypeId;
            deviceData.DeviceName = value.DeviceName;
            
            cardArray.push(deviceData);
           
            });
         
            this.cardDeviceType = cardArray;
            
        } 
      });
    //-------------------
  }
  //----------------------------------------------------------
  //Submit Card HandHeld Form---------------------------------
  submitCardHandheldForm(){
    if(this.addCardHandheldForm.valid){
     
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      
      if (localStorage.getItem("cardState")=='add') { //For add card handheld
        const cardDetails = {
        'slno' : this.addCardHandheldForm.value.slno,
        'tid'  : this.addCardHandheldForm.value.tid,
        'note'  : this.addCardHandheldForm.value.note,
        'cardDeviceTypeId'  : this.addCardHandheldForm.value.cardDeviceType,
        'branchId'  : branchId,
        'authUserId': this.authUserId
        }
      // console.log(cardDetails);
       
        this.addCardHandHeldService.addCardHandHeld(cardDetails).subscribe(res=>{
         
          if(res.status=='success') {
            this.showErrorMsg=false;
            this.successmessage= 'Success! CardHandHeld Added.';
           
            setTimeout(() => {
             this.router.navigate(['/setting/manage-card-handheld']);
            }, 2000);
            
          }
          else{
           this.showErrorMsg=true;
           this.errormessage= res.message;
          }
          
        });
      }
      else{ //Update card handheld
         const cardDetails = {
        'CardHandheldId' : this.cardHandId,
        'slno' : this.addCardHandheldForm.value.slno,
        'tid'  : this.addCardHandheldForm.value.tid,
        'note'  : this.addCardHandheldForm.value.note,
        'cardDeviceTypeId'  : this.addCardHandheldForm.value.cardDeviceType,
        //'branchId'  : branchId,
        'authUserId': this.authUserId
        }
     // console.log(cardDetails);
       
        this.updateCardHandHeldService.updateCardHandHeld(cardDetails).subscribe(res=>{
         //console.log(res);
          if(res.status=='success') {
            this.showErrorMsg=false;
            this.successmessage= 'Success! CardHandHeld Updated.';
           
            setTimeout(() => {
             this.router.navigate(['/setting/manage-card-handheld']);
            }, 2000);
            
          }
          else{
           this.showErrorMsg=true;
           this.errormessage= res.message;
          }
          
        }); 
        
      }
    }
  }
  //----------------------------------------------------------
}
