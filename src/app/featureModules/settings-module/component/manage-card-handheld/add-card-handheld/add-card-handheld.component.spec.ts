import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCardHandheldComponent } from './add-card-handheld.component';

describe('AddCardHandheldComponent', () => {
  let component: AddCardHandheldComponent;
  let fixture: ComponentFixture<AddCardHandheldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCardHandheldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCardHandheldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
