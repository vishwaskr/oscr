import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import * as jq from 'jquery';

export interface BATCHOUT_TYPE {
  BatchTypeName: string;
  BatchTypeId: number;
}

export interface PosTableElement {
  posId: number;
  posName: string;
  batchoutTypeId: number
  IsActive: boolean;
  
}


@Component({
  selector: 'app-manage-pos',
  templateUrl: './manage-pos.component.html',
  styleUrls: ['./manage-pos.component.css']
})
export class ManagePosComponent implements OnInit {
  
  authUserId:any;
  dataSource:any;
  batchOutTypes:any;
  
  //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
  //-----------------------

  
  constructor(public getBatchoutTypeService: SettingsService, public getHQPosListService: SettingsService, public addPOStoBranchService: SettingsService) { }
  
  displayedColumns: string[] = ['slno', 'posName', 'posStatus', 'batchoutType'];
  
  ngOnInit() {
    this.side_slide();
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getBatchoutType();
    this.getPosList();
  }
  //-------------------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
  //Get pos list-------------------------------------------------
  getPosList(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const posDetails = {
      'authUserId': this.authUserId,
      'branchId'  : branchId
    }
    var posArray= [];
    //call api------------
      this.getHQPosListService.getHQPOSList(posDetails).subscribe(res=>{
       // console.log(res);
       if(res.status=='success') {
            res.data.forEach(function (value) {
            let posData =  {} as PosTableElement;
          
            posData.posId = value.PosId;
            posData.posName = value.PosName;
            posData.IsActive = value.IsActive;
            posData.batchoutTypeId = value.BatchoutTypeId;
            posArray.push(posData);
            
            });
            
            //Data source or table--------------------------------
            var ELEMENT_DATA: PosTableElement[] = posArray;
            this.dataSource = ELEMENT_DATA;
            //----------------------------------------------------
        }
      });
  }
  //-------------------------------------------------------------
  //Get POS batchout type list-----------------------------------
  getBatchoutType(){
    const batchoutDetails = {
      'authUserId': this.authUserId
    }
    var batchArray= [];
    this.getBatchoutTypeService.getBatchoutType(batchoutDetails).subscribe(res=>{
      //console.log(res);
       if(res.status=='success') {
        res.data.forEach(function (value) {
            let batchData =  {} as BATCHOUT_TYPE;
            
            batchData.BatchTypeId = value.BatchoutTypeId;
            batchData.BatchTypeName = value.BatchoutTypeName;
            
            batchArray.push(batchData);
           
            });
         
            this.batchOutTypes = batchArray;
       }
    });
  }
  //-------------------------------------------------------------
  
  //POS Status change--------------------------------------------
  changePosStatus(event,posId,batchOutTypeId){
    
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    //console.log(event.checked);
    //console.log(posId);
    //console.log(batchOutTypeId);
    
    const posDetails = {
        'posId' : posId,
        'batchoutTypeId' : batchOutTypeId,
        'branchId' : branchId,
        'isActive' : event.checked,
        'createBy' : this.authUserId,
        'authUserId' : this.authUserId
    }
       //console.log(posDetails);
       
        this.addPOStoBranchService.addPOStoBranch(posDetails).subscribe(res=>{
          //console.log(res);
        this.getPosList();
        this.progressBarHide();
          
          
        });
  }
  //-------------------------------------------------------------
  //Change Batchout Type-----------------------------------------
  changeBatchoutType(event,posId,IsActive){
  //  console.log(posId);
    //console.log(event);
   this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
   var batchOutTypeId=event.source.value;
   
   const posDetails = {
        'posId' : posId,
        'batchoutTypeId' : batchOutTypeId,
        'branchId' : branchId,
        'isActive' :IsActive,
        'createBy' : this.authUserId,
        'authUserId' : this.authUserId
    }
       
    this.addPOStoBranchService.addPOStoBranch(posDetails).subscribe(res=>{
     // console.log(res);
      this.getPosList();
      this.progressBarHide();
    });

  }
  //-------------------------------------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------

}
