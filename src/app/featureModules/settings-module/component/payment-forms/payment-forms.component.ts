import { Component, OnInit } from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../constants.js";
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { SettingsService } from 'src/app/featureModules/settings-module/service/settings.service';
import * as jq from 'jquery';
import * as moment from 'moment';

export interface PaymentFormDataList {
  PaymentFormId: number;
  PaymentFormName: string;
  IsActive:boolean;
  IsDefault:boolean;
}


@Component({
  selector: 'app-payment-forms',
  templateUrl: './payment-forms.component.html',
  styleUrls: ['./payment-forms.component.css']
})

export class PaymentFormsComponent implements OnInit {

  dataSource: any;
  IsActive: boolean;
  authUserId:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("registerDate"));
  date:any;
  userBranchArr:any;
  filterBranchId: number;
  currentBranchName:string;
  //Progres----------------
  progressShow: boolean;
  mode = 'determinate';
  value = 50;
  bufferValue = 95;
  //-----------------------
  currentDate = moment(new Date()).format(dateFormat);
  
  constructor(public getPaymentFormsService: SettingsService,public changePaymentFormsStatusService: SettingsService) { }
  
  displayedColumns: string[] = ['serialNo', 'formName', 'formStatus'];
  
  ngOnInit() {
    this.side_slide();
     //Fetch all branches for current user----------
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //---------------------------------------------
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
    this.currentBranchName=this.userBranchArr[0]['Branch_Name'];
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getPaymentFormsList(); 
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
  //Get payment forms list------------------------------
  getPaymentFormsList(){
    
  this.progressBarShow();
  
  var formDataArray= [];
  
  
    const formDetails = {
      'branchId': this.filterBranchId,
      'authUserId': this.authUserId,
      'formType': 1
     }
     //console.log(formDetails);
    //call api------------
      this.getPaymentFormsService.getPaymentFormsList(formDetails).subscribe(res=>{
        
        this.progressBarHide();
        //console.log(res);
        if(res.status=='success') {
        res.data.forEach(function (value) {
          let formData =  {} as PaymentFormDataList;
          formData.PaymentFormId = value.PaymentFormId;
          formData.PaymentFormName = value.PaymentFormName;
          formData.IsActive = value.IsActive;
          formData.IsDefault = value.IsDefault;
          
          formDataArray.push(formData);
        });
       
         var PAYMENT_FORM_DATA: PaymentFormDataList[] = formDataArray;
         this.dataSource = PAYMENT_FORM_DATA;
      
      }
      else{
        
      }
      
      });
  }
  //---------------------------------------------------
  //Function for change form status--------------------
  paymentFromsStatus(event,PaymentFormId){
    //prgress show--------
    this.progressBarShow();
    //--------------------- 
   // console.log(this.IsActive+', FormID: '+PaymentFormId);
       const paymentformDetails = {
      'branchId'  : this.filterBranchId,
      'paymentFormId' :PaymentFormId,
      'createBy'  : this.authUserId,
      'updateBy' : this.authUserId,
      'isActive' : event.checked,
      'authUserId': this.authUserId
     }
    
     //Call api--------------------
    this.changePaymentFormsStatusService.changePaymentFormStatus(paymentformDetails).subscribe(res=>{
      
    this.progressBarHide();
    
     if(res.status=='success') {
      
     }
     else{
      
     }
   });
  }
  //----------------------------------------------------
  
   //Get payment form list when branch change--------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    this.filterBranchId=BranchId;
    this.currentBranchName=Branch_Name;
    this.getPaymentFormsList();
  }
  //----------------------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
}
