import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Route} from '@angular/router';

//importing component
import {SettingsComponent} from '../component/settings/settings.component';
import {ManageEmployeeComponent} from '../component/manage-employee/manage-employee.component';
import {AddEmployeeComponent} from '../component/manage-employee/add-employee/add-employee.component';
import {PaymentFormsComponent} from '../component/payment-forms/payment-forms.component';
import {AddPaymentFormComponent} from '../component/payment-forms/add-payment-form/add-payment-form.component';
import {ManageSitesComponent} from '../component/manage-sites/manage-sites.component';
import {AddSiteComponent} from '../component/manage-sites/add-site/add-site.component';
import {ManageRegisterComponent} from '../component/manage-register/manage-register.component';
import {AddRegisterComponent} from '../component/manage-register/add-register/add-register.component';
import {EditRegisterComponent} from '../component/manage-register/edit-register/edit-register.component';
import {ManageCardHandheldComponent} from '../component/manage-card-handheld/manage-card-handheld.component';
import {AddCardHandheldComponent} from '../component/manage-card-handheld/add-card-handheld/add-card-handheld.component';
import {DefaultConfigComponent} from '../component/default-config/default-config.component';
import {ManagePosComponent} from '../component/manage-pos/manage-pos.component';

import {AuthGuard} from '../../../core/auth/auth.guard';

const route:Route[] =[
  {
    path:'',
    redirectTo:'setting',
    pathMatch:'full'
  },
  {
    path:'setting',
    component:SettingsComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'manage-employee',
    component:ManageEmployeeComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'add-employee',
    component:AddEmployeeComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'edit-employee',
    component:AddEmployeeComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'payment-form',
    component:PaymentFormsComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'add-payment-form',
    component:AddPaymentFormComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'manage-sites',
    component:ManageSitesComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'add-site',
    component:AddSiteComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'manage-register',
    component:ManageRegisterComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'add-register',
    component:AddRegisterComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'edit-register',
    component:EditRegisterComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'manage-card-handheld',
    component:ManageCardHandheldComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'add-card-handheld',
    component:AddCardHandheldComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'edit-card-handheld',
    component:AddCardHandheldComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'default-config',
    component:DefaultConfigComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'manage-pos',
    component:ManagePosComponent,
    canActivate:[AuthGuard]
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class SettingsRoutingModule { }
