import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

//httpHeaders options------------------------------------
const httpOptions = {
  headers: new HttpHeaders({
    "Accept": "application/json"
  }),
 withCredentials: true
};
//-------------------------------------------------------
//API base url-------------------------------------------
var apiUrl=environment.apiHost;
//-------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }
  //Employee management-----------------------------------------------------------------------
  //Get employees------------------------------
  getEmployeeList(data):Observable<any>{
    
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId;
    return this.http.get(apiUrl+`branch/employee?`+body,httpOptions)
  
  }
  //-------------------------------------------
  
  //Add employees------------------------------
  addEmployee(data,fileToUpload):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('userImage', '');
    if (fileToUpload) {
    formData.append('userImage', fileToUpload, fileToUpload.name);
    }
    
    formData.append('name', data.name);
    formData.append('userName', data.userName);
    formData.append('employeeId', data.employeeId);
    formData.append('branchId', data.branchId);
    formData.append('createBy', data.createBy);
    formData.append('roleId', data.roleId);
    formData.append('userId', data.userId);
    formData.append('authUserId', data.authUserId);
    
    //console.log(data);
    return this.http.post(apiUrl+`branch/employee`,formData,httpOptions)
  }
  //-------------------------------------------
  //Get Employee Details for edit--------------
  getEmployeeDetails(data):Observable<any>{
   
    var body = "branchId=" + data.branchId + "&userId="+data.userId + "&authUserId="+data.authUserId;
    return this.http.get(apiUrl+`branch/employee?`+body,httpOptions)
  }
  //-------------------------------------------
  //Update employees---------------------------
  updateEmployee(data,fileToUpload):Observable<any>{
    
    const formData: FormData = new FormData();
    formData.append('userNewImage', '');
    if (fileToUpload) {
     formData.append('userNewImage', fileToUpload, fileToUpload.name);
    }
   
    formData.append('authUserId', data.authUserId);
    formData.append('userId', data.userId);
    formData.append('name', data.name);
    formData.append('userName', data.userName);
    formData.append('employeeId', data.employeeId);
    formData.append('branchId', data.branchId);
    formData.append('updateBy', data.updateBy);
    formData.append('roleId', data.roleId);
    formData.append('userOldImage', data.userOldImage);
   //console.log(data);
    return this.http.put(apiUrl+`branch/employee`,formData,httpOptions)
  }
  //---------------------------------------------
  //Delete employee------------------------------
   deleteEmployee(data):Observable<any>{
   
    var body = "userId=" + data.userId + "&branchId=" + data.branchId + "&authUserId="+data.authUserId;
    return this.http.delete(apiUrl+`branch/employee?`+body,httpOptions)
  
   }
  //---------------------------------------------
  //End Employee management----------------------------------------------------------------------------
  //###################################################################################################
  //Payment forms--------------------------------------------------------------------------------------
  //Get branch wise payment forms list-----------
  getPaymentFormsList(data):Observable<any>{
  
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId + "&formType="+data.formType;
  
    return this.http.get(apiUrl+`branch/paymentform?`+body,httpOptions)
  }
  //---------------------------------------------
  //Change status--------------------------------
   changePaymentFormStatus(data):Observable<any>{
   
    const formData: FormData = new FormData();
    
    formData.append('authUserId', data.authUserId);
    formData.append('branchId', data.branchId);
    formData.append('createBy', data.createBy);
    formData.append('updateBy', data.updateBy);
    formData.append('paymentFormId', data.paymentFormId);
    formData.append('isActive', data.isActive);

   // console.log(fileToUpload);
    return this.http.post(apiUrl+`branch/paymentform`,formData,httpOptions)
  }
  //---------------------------------------------
  //End Payment Forms-----------------------------------------------------------------------------------
  //####################################################################################################
  //Site management-------------------------------------------------------------------------------------
  //Get all unassigned site list------------------
  getAllSiteList(data):Observable<any>{
   
    var body = "branchId=" + data.branchId + "&siteListType=2" + "&authUserId="+data.authUserId;
  
    return this.http.get(apiUrl+`branch/site?`+body,httpOptions)
  }
  //-----------------------------------------------
  //Assign site to branch--------------------------
  addSiteToBranch(data):Observable<any>{
   
   //siteList, branchId,createBy
     const formData: FormData = new FormData();
     
     formData.append('authUserId', data.authUserId);
     formData.append('branchId', data.branchId);
     formData.append('siteList', data.siteList);
     formData.append('createBy', data.createBy);
    
    return this.http.post(apiUrl+`branch/site`,formData,httpOptions)
  }
  //-----------------------------------------------
  //Get assigned site list-------------------------
  getAssignedSiteList(data):Observable<any>{
  
    var body = "branchId=" + data.branchId + "&siteListType="+data.siteListType + "&authUserId="+data.authUserId;
  
    return this.http.get(apiUrl+`branch/site?`+body,httpOptions)
  }
  //-----------------------------------------------
  //Change branch site status----------------------
  changeBranchSiteStatus(data):Observable<any>{
   
    const formData: FormData = new FormData();
    
    formData.append('authUserId', data.authUserId);
    formData.append('branchId', data.branchId);
    formData.append('updateBy', data.updateBy);
    formData.append('branchWiseSiteId', data.branchWiseSiteId);
    formData.append('isActive', data.isActive);
   
    return this.http.put(apiUrl+`branch/site`,formData,httpOptions)
  }
  //-----------------------------------------------
  //Get all site types-----------------------------
  getAllSiteType(authUserId):Observable<any>{
    
  var body = "authUserId="+authUserId;
  return this.http.get(apiUrl+`site/type?`+body,httpOptions)

  }
  //-----------------------------------------------
  //Enable site type-------------------------------
  //enableSiteType
  enableSiteType(data):Observable<any>{
   
    const formData: FormData = new FormData();
    
    formData.append('authUserId', data.authUserId);
    formData.append('branchWiseSiteId', data.branchWiseSiteId);
    formData.append('createBy', data.createBy);
    formData.append('typeId', data.typeId);
    formData.append('isActive', data.isActive);
    
    return this.http.post(apiUrl+`branch/site/type`,formData,httpOptions)
  }
  //-----------------------------------------------
  //End site management-----------------------------------------------------------------------------------
  //######################################################################################################
  //------------------------------------------------------------------------------------------------------
  //Manage Register---------------------------------------------------------------------------------------
  //Get Site for manage register---------------
  getRegisterSiteList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId+ "&siteListType=" +data.siteListType;
    
    return this.http.get(apiUrl+`branch/site?`+body,httpOptions)
  }
  //-------------------------------------------
  //Add register-------------------------------
  addRegisterToSite(data):Observable<any>{
    const formData: FormData = new FormData();

    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.authUserId);
    formData.append('siteId', data.siteId);
    formData.append('registerDetails', data.registerDetails);
    
   // console.log(formData);
    return this.http.post(apiUrl+`branch/site/register`,formData,httpOptions)
  }
  //-------------------------------------------
  //Get Register Details by site id------------
  getRegisterDetails(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId+ "&siteId=" +data.siteId + "&registerType=1";
    return this.http.get(apiUrl+`branch/site/register?`+body,httpOptions)
  }
  //-------------------------------------------
  //Update register-------------------------------
  updateRegisterToSite(data):Observable<any>{
    const formData: FormData = new FormData();

    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.authUserId);
    formData.append('siteId', data.siteId);
    formData.append('registerDetails', data.regfields);
    
   // console.log(formData);
    return this.http.put(apiUrl+`branch/site/register`,formData,httpOptions)
  }
  //-------------------------------------------
  //End Manage Register-----------------------------------------------------------------------------------
  //######################################################################################################
  //Start Manage CardHandheld-----------------------------------------------------------------------------
  //Add CardHandheld-------------------------------------------
  addCardHandHeld(data):Observable<any>{
    const formData: FormData = new FormData();

    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.authUserId);
    formData.append('cardhandheldSerialNumber', data.slno);
    formData.append('cardhandheldTidNumber', data.tid);
    formData.append('note', data.note);
    formData.append('deviceTypeId', data.cardDeviceTypeId);
    
   // console.log(formData);
    return this.http.post(apiUrl+`branch/card`,formData,httpOptions)
  }
  //-----------------------------------------------------------
  //Get CardHandheld Details by id-----------------------------
  getCardHandheldDetails(data):Observable<any>{
    var body ="authUserId=" + data.authUserId + "&branchId=" + data.branchId + "&cardHandheldId="+data.cardHandheldId;
    
    return this.http.get(apiUrl+`branch/card?`+body,httpOptions)
  }
  //-----------------------------------------------------------
  //Update CardHandHeld----------------------------------------
  updateCardHandHeld(data):Observable<any>{
    const formData: FormData = new FormData();

   // formData.append('branchId', data.branchId);
    formData.append('cardHandheldId', data.CardHandheldId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.authUserId);
    formData.append('cardhandheldSerialNumber', data.slno);
    formData.append('cardhandheldTidNumber', data.tid);
    formData.append('note', data.note);
    formData.append('deviceTypeId', data.cardDeviceTypeId);
    
    return this.http.put(apiUrl+`branch/card`,formData,httpOptions)
  }
  //-----------------------------------------------------------
   //Delete CardHandHeld---------------------------------------
   deleteCardHandheld(data):Observable<any>{
   
    var body = "cardHandheldId=" + data.cardHandId + "&authUserId="+data.authUserId;
    return this.http.delete(apiUrl+`branch/card?`+body,httpOptions)
  
   }
  //-----------------------------------------------------------
  //Get Card HandHeld device type------------------------------
  getCardDeviceTypeList(data):Observable<any>{
    var body = "authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`handheld/device/type?`+body,httpOptions)
  }
  //-----------------------------------------------------------
  //Get assigned cardhandheld details--------------------------
   getAssignedCardHandheldList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`branch/card?`+body,httpOptions)
  }
  //------------------------------------------------------------
  //------------------------------------------------------------------------------------------------------
  //######################################################################################################
  //------------------------------------------------------------------------------------------------------
  //Start Manage POS--------------------------------------------------------------------------------------
  //Get POS Batchout Type---------------------------
  getBatchoutType(data):Observable<any>{
    var body = "authUserId=" + data.authUserId;
    return this.http.get(apiUrl+`hq/pos/batchouttype?`+body,httpOptions)
  }
  //------------------------------------------------
  //Get HQ POS list---------------------------------
   getHQPOSList(data):Observable<any>{
    var body ="branchId=" + data.branchId + "&authUserId=" + data.authUserId;
    return this.http.get(apiUrl+`hq/pos?`+body,httpOptions)
  }
  //------------------------------------------------
  //Get branch wise pos list------------------------
  getPOSList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" + data.authUserId;
    return this.http.get(apiUrl+`branch/pos?`+body,httpOptions)
  }
  //------------------------------------------------
  //Add POS to Branch-------------------------------
  addPOStoBranch(data):Observable<any>{
    const formData: FormData = new FormData();

    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.authUserId);
    formData.append('isActive', data.isActive);
    formData.append('posId', data.posId);
    formData.append('batchoutTypeId', data.batchoutTypeId);
    
   // console.log(formData);
    return this.http.post(apiUrl+`branch/pos`,formData,httpOptions)
  }
  //------------------------------------------------
  //End Manage POS----------------------------------------------------------------------------------------
  //Get default config------------------------------
  getDefaultConfig(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" + data.authUserId;
    return this.http.get(apiUrl+`branch/defaultsetting?`+body,httpOptions)
  }
  //------------------------------------------------
  //Update Default Config---------------------------
  updateDefaultConfig(data):Observable<any>{
    const formData: FormData = new FormData();

    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.authUserId);
    formData.append('signOutAmount', data.signOutAmount);
    formData.append('defaultBranchSummaryDay', data.defaultBranchSummaryDay);
    
   // console.log(formData);
    return this.http.post(apiUrl+`branch/defaultsetting`,formData,httpOptions)
  }
  //------------------------------------------------
  
}
