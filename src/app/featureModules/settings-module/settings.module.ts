import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import { SettingsComponent } from './component/settings/settings.component';
import { SharedModule } from '../../shared/shared.module';
import {SettingsRoutingModule} from './settings-routing/settings-routing.module';
import { ManageEmployeeComponent } from './component/manage-employee/manage-employee.component';
import { AddEmployeeComponent } from './component/manage-employee/add-employee/add-employee.component';
import { PaymentFormsComponent } from './component/payment-forms/payment-forms.component';
import { AddPaymentFormComponent } from './component/payment-forms/add-payment-form/add-payment-form.component';
import { ManageSitesComponent } from './component/manage-sites/manage-sites.component';
import { AddSiteComponent } from './component/manage-sites/add-site/add-site.component';
import { ManageRegisterComponent } from './component/manage-register/manage-register.component';
import { AddRegisterComponent } from './component/manage-register/add-register/add-register.component';
import { ManageCardHandheldComponent } from './component/manage-card-handheld/manage-card-handheld.component';
import { AddCardHandheldComponent } from './component/manage-card-handheld/add-card-handheld/add-card-handheld.component';
import { DefaultConfigComponent } from './component/default-config/default-config.component';
import { ManagePosComponent } from './component/manage-pos/manage-pos.component';
import {MatProgressBarModule} from '@angular/material';
import { EditRegisterComponent } from './component/manage-register/edit-register/edit-register.component'

@NgModule({
  declarations: [
    SettingsComponent,
    ManageEmployeeComponent,
    AddEmployeeComponent,
    PaymentFormsComponent,
    AddPaymentFormComponent,
    ManageSitesComponent,
    AddSiteComponent,
    ManageRegisterComponent,
    AddRegisterComponent,
    ManageCardHandheldComponent,
    AddCardHandheldComponent,
    DefaultConfigComponent,
    ManagePosComponent,
    EditRegisterComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    SettingsRoutingModule,
    SharedModule,
    MatProgressBarModule
  ]
})
export class SettingsModule { }
