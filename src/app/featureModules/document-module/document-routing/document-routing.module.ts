import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule , Route} from '@angular/router';

// impprting component
import {DocumentComponent} from '../component/document/document.component';

const route:Route[] = [
  {
    path:'',
    redirectTo:'document',
    pathMatch:'full'
  },
  {
    path:'document',
    component:DocumentComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class DocumentRoutingModule { }
