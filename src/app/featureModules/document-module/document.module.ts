import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {MatProgressBarModule} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { DocumentComponent } from './component/document/document.component';
import {imageViewer} from './component/document/document.component';
import {DocumentRoutingModule} from './document-routing/document-routing.module';

@NgModule({
  declarations: [DocumentComponent,imageViewer],
  imports: [
    CommonModule,
    DocumentRoutingModule,
    SharedModule,
    MatProgressBarModule,
    MatDialogModule
  ],
  entryComponents: [imageViewer]
})
export class DocumentModule { }
