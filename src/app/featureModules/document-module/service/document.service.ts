import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

//httpHeaders options------------------------------------
const httpOptions = {
  headers: new HttpHeaders({
    "Accept": "application/json"
  }),
 withCredentials: true
};
//-------------------------------------------------------
//API base url-------------------------------------------
var apiUrl=environment.apiHost;
//-------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http: HttpClient) { }
  
  //Get Document List------------------------------------
  getDocumentList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&bankDate="+data.bankDate;
    return this.http.get(apiUrl+`branch/document?`+body,httpOptions)
  }
  //-----------------------------------------------------
 //Add Document------------------------------------------
  uploadDocument(data,fileToUpload):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('documentFile', '');
    if (fileToUpload) {
    formData.append('documentFile', fileToUpload, fileToUpload.name);
    }
    formData.append('branchId', data.branchId);
    formData.append('createBy', data.createBy);
    formData.append('authUserId', data.authUserId);
    formData.append('fileName', data.fileName);
    formData.append('documentType', data.documentType);
    formData.append('bankDate', data.bankDate);
    
    //console.log(data);
    return this.http.post(apiUrl+`branch/document`,formData,httpOptions)
  }
  //--------------------------------------------
  //Delete document------------------------------
   deleteDocument(data):Observable<any>{
    var body = "documentId=" + data.documentId + "&filePath=" + data.filePath + "&bankDate=" + data.bankDate + "&authUserId="+data.authUserId;
    return this.http.delete(apiUrl+`branch/document?`+body,httpOptions)
   }
  //---------------------------------------------
}
