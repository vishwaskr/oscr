import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../constants.js";
import { DocumentService } from 'src/app/featureModules/document-module/service/document.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import * as jq from 'jquery';
import * as moment from 'moment-timezone';
import Swal from 'sweetalert2';

import { DomSanitizer } from '@angular/platform-browser';


export interface DocumentData {
 DocumentId: number;
 DocumentType: string;
 DocumentTypeId: number;
 FileName: string;
 FilePath: string;
 FileType: string;
 isImage: boolean;
 isDownloadable: boolean;
 DefaultImg: string;
}

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  
  authUserId:any;
  filterBranchId:number;
  userBranchArr:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("registerDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  fileToUpload: File = null;
  imageFileName:any;
  
  ReportsArray:Array<any> = [];
  showReport: boolean=false;
  
  BatchoutSlipArray:Array<any> = [];
  showBatchoutSlip: boolean=false;
  
  ZReportLogArray:Array<any> = [];
  showZReportLog: boolean=false;
  
  DepositSlipsArray:Array<any> = [];
  showDepositSlip: boolean=false;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  showErrorMsg:boolean=true;
  errormessage:string;
  successmessage:string;

  
  constructor(private sanitizer:DomSanitizer,public DocumentService: DocumentService, public dialog: MatDialog) { }

    //Image Viewer------------------------------------------
    imageViewer(fileurl,isImage): void {
      const dialogRef = this.dialog.open(imageViewer, {
        width: '',
        data: {filePath:fileurl,isImage:isImage}
      });
    
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }
  //------------------------------------------------------
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    }
    else{
    this.date = new FormControl(new Date());
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getDocumentList();
  }
  //----------------------------------------------------------
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Get document list-----------------------------------------
  getDocumentList(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var ReportsArray=[];
    var BatchoutSlipArray=[];
    var ZReportLogArray=[];
    var DepositSlipsArray=[];
    var DefaultImg = 'assets/img/word-exel.jpg';
    
    const documentDetails = {
      'branchId' : branchId,
      'authUserId' : this.authUserId,
      'bankDate' : this.currentDate
    }
    //console.log(documentDetails);
    
    //Call API------------------------
    this.DocumentService.getDocumentList(documentDetails).subscribe(res=>{
      this.progressBarHide();
      console.log(res);
      if(res.status=='success') {
        //For Reports document-----------------------------
        if (res.data.Reports.length > 0) {
          res.data.Reports.forEach(function (value) {
            let reportData =  {} as DocumentData;
                reportData.DocumentId=value.DocumentId;
                reportData.DocumentType=value.DocumentType;
                reportData.DocumentTypeId=value.DocumentTypeId;
                reportData.FileName=value.FileName;
                reportData.FilePath=value.FilePath;
                reportData.FileType=value.FileType;
                reportData.isImage=false;
                if (value.FileType=='image/jpeg') {
                 reportData.isImage=true;
                 reportData.DefaultImg=value.FilePath;
                }
                reportData.isDownloadable=false;
                if (value.FileType=='application/vnd.open') {
                 reportData.isDownloadable=true;
                 reportData.isImage=true;
                 reportData.DefaultImg=DefaultImg;
                }
               ReportsArray.push(reportData);
          });
          this.showReport=true;
          this.ReportsArray=ReportsArray;
        }
        else{
         this.showReport=false; 
        }
      //--------------------------------------------------
      //For BatchoutSlip-----------------------------
        if (res.data.BatchOutSlips.length > 0) {
          res.data.BatchOutSlips.forEach(function (value) {
            let batchoutData =  {} as DocumentData;
                batchoutData.DocumentId=value.DocumentId;
                batchoutData.DocumentType=value.DocumentType;
                batchoutData.DocumentTypeId=value.DocumentTypeId;
                batchoutData.FileName=value.FileName;
                batchoutData.FilePath=value.FilePath;
                batchoutData.FileType=value.FileType;
                batchoutData.isImage=false;
                if (value.FileType=='image/jpeg') {
                 batchoutData.isImage=true;
                 batchoutData.DefaultImg=value.FilePath;
                }
                batchoutData.isDownloadable=false;
                if (value.FileType=='application/vnd.open') {
                 batchoutData.isDownloadable=true;
                 batchoutData.isImage=true;
                 batchoutData.DefaultImg=DefaultImg;
                }
               BatchoutSlipArray.push(batchoutData);
          });
          this.showBatchoutSlip=true;
          this.BatchoutSlipArray=BatchoutSlipArray;
        }
        else{
         this.showBatchoutSlip=false; 
        }
      //--------------------------------------------------
      //For ZReportLog------------------------------------
        if (res.data.ZReportLog.length > 0) {
          res.data.ZReportLog.forEach(function (value) {
            let zReportData =  {} as DocumentData;
                zReportData.DocumentId=value.DocumentId;
                zReportData.DocumentType=value.DocumentType;
                zReportData.DocumentTypeId=value.DocumentTypeId;
                zReportData.FileName=value.FileName;
                zReportData.FilePath=value.FilePath;
                zReportData.FileType=value.FileType;
                zReportData.isImage=false;
                if (value.FileType=='image/jpeg') {
                 zReportData.isImage=true;
                 zReportData.DefaultImg=value.FilePath;
                }
                
                zReportData.isDownloadable=false;
                if (value.FileType=='application/vnd.open') {
                 zReportData.isDownloadable=true;
                 zReportData.isImage=true;
                 zReportData.DefaultImg=DefaultImg;
                }
                
               ZReportLogArray.push(zReportData);
          });
          this.showZReportLog=true;
          this.ZReportLogArray=ZReportLogArray;
        }
        else{
         this.showZReportLog=false; 
        }
      //--------------------------------------------------
      //For DepositSlips----------------------------------
        if (res.data.DepositSlips.length > 0) {
          res.data.DepositSlips.forEach(function (value) {
            let DepositSlipsData =  {} as DocumentData;
                DepositSlipsData.DocumentId=value.DocumentId;
                DepositSlipsData.DocumentType=value.DocumentType;
                DepositSlipsData.DocumentTypeId=value.DocumentTypeId;
                DepositSlipsData.FileName=value.FileName;
                DepositSlipsData.FilePath=value.FilePath;
                DepositSlipsData.FileType=value.FileType;
                DepositSlipsData.isImage=false;
                if (value.FileType=='image/jpeg') {
                 DepositSlipsData.isImage=true;
                 DepositSlipsData.DefaultImg=value.FilePath;
                }
                
                DepositSlipsData.isDownloadable=false;
                if (value.FileType=='application/vnd.open') {
                 DepositSlipsData.isDownloadable=true;
                 DepositSlipsData.isImage=true;
                 DepositSlipsData.DefaultImg=DefaultImg;
                }
                
               DepositSlipsArray.push(DepositSlipsData);
          });
          this.showDepositSlip=true;
          this.DepositSlipsArray=DepositSlipsArray;
        }
        else{
         this.showDepositSlip=false; 
        }
      //--------------------------------------------------
      }
      else{
        this.showZReportLog=false;
        this.showReport=false;
        this.showBatchoutSlip=false;
        this.showDepositSlip=false; 
      }
    });
    //--------------------------------
  }
  //----------------------------------------------------------
  //Upload document-------------------------------------------
  uploadDocument(documentType){
   this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
   const documentFileDetails = {
      'branchId' : branchId,
      'authUserId' : this.authUserId,
      'bankDate' : this.currentDate,
      'createBy' : this.authUserId,
      'fileName' : this.imageFileName,
      'documentType' : documentType
    }
    //console.log(documentFileDetails);
    //Call API-------------------------
    this.DocumentService.uploadDocument(documentFileDetails,this.fileToUpload).subscribe(res=>{
      this.progressBarHide();
        if(res.status=='success') {
        this.showErrorMsg=false;
        this.successmessage=res.message;
        setTimeout(() => {
          this.getDocumentList();
          this.successmessage='';
        }, 2000);
       }
       else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
    });
    //---------------------------------
    
  }
  //----------------------------------------------------------
  //Deelete Document------------------------------------------
  deleteDocument(DocumentId,filePath){
    Swal.fire({
    title: 'Are you sure?',
    //text: 'You have unsaved changes. Kindly save the changes by clicking on Submit, else those will be lost.',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Delete',
    cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
        this.deleteDocumentService(DocumentId,filePath);
      }
    });
  }
  deleteDocumentService(DocumentId,filePath){
    this.progressBarShow();
    const documentDeleteDetails = {
      'authUserId' : this.authUserId,
      'bankDate' : this.currentDate,
      'documentId' : DocumentId,
      'filePath' : filePath
    }
    //Call api---------------
    this.DocumentService.deleteDocument(documentDeleteDetails).subscribe(res=>{
      //console.log(res);
      this.progressBarHide();
        if(res.status=='success') {
          this.showErrorMsg=false;
            this.successmessage=res.message;
            setTimeout(() => {
              this.getDocumentList();
              this.successmessage='';
            }, 2000);
        }
        else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
        setTimeout(() => {
           this.showErrorMsg=false;
           this.errormessage='';
        }, 2000);
       }
    });
    //-----------------------
  }
  //----------------------------------------------------------
  //For file upload-------------------------------------------
  handleFileInput(files: FileList,documentType) {
    //console.log(files.item(0));
    var reader = new FileReader();
    reader.readAsDataURL(files.item(0));
     
    var fd=files.item(0);
    this.imageFileName=fd.name;
    this.fileToUpload = files.item(0);
    //reader.onload = (event) => { // called once readAsDataURL is completed
    //    this.employeeImage=reader.result;
    //}
  //  console.log('documentType:'+documentType);
    this.uploadDocument(documentType);
  }
  //---------------------------------------------------------- 
  //Change date-----------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("registerDate",JSON.stringify(momentDate));
   this.getDocumentList();
   
  }
  //----------------------------------------------------------
  //Get summary details when branch change--------------------
  changeBranch(BranchId,Branch_Name){
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', Branch_Name);
    this.getDocumentList();
  }
  //----------------------------------------------------------
  //Get secure url--------------------------------------------
  getSecureUrl(url){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  //return '';
  }
  //----------------------------------------------------------
  //----------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //----------------------------------------------------------
}

//Image Viewer------------------------------------------------
@Component({
  selector: 'imageViewer',
  templateUrl: './imageViewer.html',
  styleUrls: ['./document.component.css']
})
export class imageViewer {

  constructor(public dialogRef: MatDialogRef<imageViewer>, @Inject(MAT_DIALOG_DATA) public data: '') {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
//----------------------------------------------------------
