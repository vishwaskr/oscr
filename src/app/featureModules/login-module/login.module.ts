import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
//import { ParticlesModule } from 'angular-particle';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './component/login/login.component';
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
import {LoginRoutingModule} from './login-routing/login-routing.module';
import {SharedModule} from '../../shared/shared.module';
//import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import {MatProgressBarModule} from '@angular/material'
import { DeviceDetectorModule } from 'ngx-device-detector';

@NgModule({
  declarations: [LoginComponent, ForgotPasswordComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    //NgBootstrapFormValidationModule.forRoot(),
   // NgBootstrapFormValidationModule,
    DeviceDetectorModule.forRoot(),
    MatProgressBarModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
],
  exports:[LoginRoutingModule]
})
export class LoginModule { }
