import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Route} from '@angular/router';

// import component
import {LoginComponent} from '../component/login/login.component';
import{ForgotPasswordComponent} from '../component/forgot-password/forgot-password.component'

//import guard
import {AuthGuard} from '../../../core/auth/auth.guard';

const route:Route[] =[
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  },
  {
    path:'login',
    component:LoginComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'forgott-password',
    component:ForgotPasswordComponent
  }
]



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class LoginRoutingModule { }
