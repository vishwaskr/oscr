import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';


//const crypto = require('crypto')
const accessKey = "OSCR";
const secretKey = "techexactly";
const companyCode="abc";

const httpOptions = {
  headers: new HttpHeaders({
     "Accept": "application/json"
  }),
 withCredentials: true
};

  
@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private http: HttpClient) { }
  getUserDetails(data):Observable<any>{
    var apiUrl=environment.apiHost;
    const formData: FormData = new FormData();
    formData.append('username', data.username);
    formData.append('password', data.password);
    formData.append('devicesname', data.devicesname);
    formData.append('broswerinfo', data.broswerinfo);
    formData.append('ipaddress', data.ipaddress);
    
    return this.http.post(apiUrl+`login`,formData, httpOptions)
  }
}
