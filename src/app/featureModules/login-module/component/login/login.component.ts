import { Component, OnInit,OnChanges } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
//import { NgxSpinnerService } from 'ngx-spinner';
import { LoginService } from 'src/app/featureModules/login-module/service/login.service';
import {Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthState } from '../../../../core/auth/auth.model';
import {ActionAuthLogin} from '../../../../core/auth/auth.action';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { DeviceDetectorService } from 'ngx-device-detector';
import {Md5} from "md5-typescript";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnChanges {

  oscrLoginForm: FormGroup;
  state: Observable<AuthState>;
  //isLogin: boolean = false;
  message: string;
  showErrorMsg: boolean;
  ipAddress: any;
  deviceInfo = null;
  broswerinfo: string;
  rememberMe: boolean;
  username: string;
  password: string;
 // rememberMechecked: boolean;
  

  //Main Progress bar settings-------------------
   mainProgressShow:boolean;
   main_mode = 'indeterminate';
  //---------------------------------------------
  //Login process Progress bar settings----------
   progressShow:boolean;
   mode = 'determinate';
   value = 50;
   bufferValue = 95;
  //---------------------------------------------
  
  constructor(private deviceService: DeviceDetectorService,private http: HttpClient,public loginService: LoginService,private router:Router, private store:Store<AuthState>) {
    this.mainProgressShow=true;
    
    
    //For getting ip address---------------------------------
    this.http.get<{ip:string}>('https://jsonip.com')
    .subscribe( data => {
      this.ipAddress = data.ip
      sessionStorage.setItem('ipAddress', this.ipAddress);
    })
    //-------------------------------------------------------
    this.getBrowserDetails();
   // this.particleParams();
   }
  //Get browser info--------------------------------------
   getBrowserDetails() {
      this.deviceInfo = this.deviceService.getDeviceInfo();
      const isMobile = this.deviceService.isMobile();
      const isTablet = this.deviceService.isTablet();
      const isDesktopDevice = this.deviceService.isDesktop();
      //console.log(isDesktopDevice);
      this.broswerinfo=this.deviceInfo.browser+"-"+this.deviceInfo.browser_version;
     // console.log(this.deviceInfo);
      //console.log(isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
      //console.log(isTablet);  // returns if the device us a tablet (iPad etc)
      //console.log(isDesktopDevice); // returns if the app is running on a Desktop browser.
    }
    //---------------------------------------------------------

  ngOnInit() {
    //this.rememberMechecked= JSON.parse(localStorage.getItem('rememberMe'));
    ////check remember--------------------------------
    //if (this.rememberMechecked) {
    //  this.username=localStorage.getItem('username');
    //  this.password=localStorage.getItem('password');
    //}
    //----------------------------------------------
    this.oscrLoginForm = new FormGroup({
      userName: new FormControl(this.username,[Validators.required]),
      password:new FormControl(this.password,[Validators.required])
     // rememberMe:new FormControl(this.rememberMechecked)
    })
    
    this.mainProgressShow=false;
    
  }
  
  ngOnChanges(change:any){
    console.log(change);
  }

  oscrLoginFormSubmit(){
   //console.log(this.oscrLoginForm);
    if(this.oscrLoginForm.valid){
      this.progressShow=true;
      this.mode="indeterminate";
      this.showErrorMsg=false;
      //this.rememberMechecked=this.oscrLoginForm.value.rememberMe;
      const userDetails = {
      'username': this.oscrLoginForm.value.userName,
      'password': this.hasPassword(this.oscrLoginForm.value.password),
      'devicesname' : 'web',
      'broswerinfo' : this.broswerinfo,
      'ipaddress' : this.ipAddress
     }
     
     
     this.loginService.getUserDetails(userDetails).subscribe(res=>{
      //console.log(res);
       this.mode="determinate";
       this.progressShow=false;
      if(res.status=='success') {
        localStorage.setItem('isLogin', JSON.stringify({isLogin:'yes'}));
        sessionStorage.setItem('UserName', res.data.Name);
        localStorage.setItem('UserId', JSON.stringify(res.data.UserID));
        //sessionStorage.setItem('UserBranch', res.data.Branch);
        localStorage.setItem("UserBranch", JSON.stringify(res.data.Branch));
        
        localStorage.setItem('currentBranchId', JSON.stringify(res.data.Branch[0]['BranchId']));
        localStorage.setItem('currentBranchName', JSON.stringify(res.data.Branch[0]['Branch_Name']));
        
        //if (this.rememberMechecked) {
        //  localStorage.setItem('rememberMe', JSON.stringify(this.rememberMechecked));
        //  localStorage.setItem('username', this.oscrLoginForm.value.userName);
        //  localStorage.setItem('password', this.oscrLoginForm.value.password);
        //}
        //else{
        //  localStorage.setItem('rememberMe', JSON.stringify(false));
        //  localStorage.setItem('username', '');
        //  localStorage.setItem('password', ''); 
        //}
        
        this.store.dispatch(new ActionAuthLogin);
        this.router.navigate(['/summary'])
      }
      else if (res.status=='alreadylogin') {
        this.message=res.message;
      }
      else{
        this.message='Username or Password mismatch!';
      }
      
    })
     
    }
    else{
       this.showErrorMsg=true;
    }
  }
  
  //Password converted to md5----------------
  hasPassword(password){
    return Md5.init(password);
  }
  //-----------------------------------------

}
