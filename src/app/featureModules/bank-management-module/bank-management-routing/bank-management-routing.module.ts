import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Route} from '@angular/router';

// importing component
import {BankManagementComponent} from '../component/bank-management/bank-management.component';
import {IssueBankBulkComponent} from '../component/issue-bank-bulk/issue-bank-bulk.component';
import {BankSummaryComponent} from '../component/bank-summary/bank-summary.component';

//importing guard
import {AuthGuard} from '../../../core/auth/auth.guard';

const route:Route[] = [
  {
    path:'',
    redirectTo:'bank',
    pathMatch:'full'
  },
  {
    path:'bank',
    component:BankManagementComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'issue-bank-bulk',
    component:IssueBankBulkComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'bank-summary',
    component:BankSummaryComponent,
    canActivate:[AuthGuard]
  }
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports:[RouterModule]
})
export class BankManagementRoutingModule { }
