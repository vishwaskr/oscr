import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bank-toolbar',
  templateUrl: './bank-toolbar.component.html',
  styleUrls: ['./bank-toolbar.component.css']
})
export class BankToolbarComponent implements OnInit {

  @Input() pname:string;
  constructor() { }

  ngOnInit() {
    this.pname="Palash";
  }

}
