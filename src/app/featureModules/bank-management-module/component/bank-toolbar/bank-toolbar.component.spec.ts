import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankToolbarComponent } from './bank-toolbar.component';

describe('BankToolbarComponent', () => {
  let component: BankToolbarComponent;
  let fixture: ComponentFixture<BankToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
