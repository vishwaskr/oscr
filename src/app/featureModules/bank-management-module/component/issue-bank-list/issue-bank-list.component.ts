import { Component, OnInit } from '@angular/core';

export interface IssueBanlListElement {
  date: string;
  site: string;
  type: string;
  register: number;
  employee: string;
  lastPickup : string;
  pickups : number;
  pickupTotal : number;
  newPickup : string;
  //approve: string;
}

const ISSUE_BANK_LIST_DATA: IssueBanlListElement[] = [
  {date: '10-06-2019', site: 'Site1', type: 'Food', register: 1, employee: 'Palash', lastPickup: '10-06-2019', pickups: 3, pickupTotal: 4, newPickup:'',},
  {date: '10-06-2019', site: 'Site2', type: 'Food', register: 1, employee: 'Raju', lastPickup: '10-06-2019', pickups: 5, pickupTotal: 5, newPickup:'',},
];

@Component({
  selector: 'app-issue-bank-list',
  templateUrl: './issue-bank-list.component.html',
  styleUrls: ['./issue-bank-list.component.css']
})
export class IssueBankListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  displayedColumns: string[] = ['date', 'site', 'type', 'register', 'employee', 'lastPickup', 'pickups', 'pickupTotal', 'newPickup', 'summary', 'close', 'delete'];
  dataSource = ISSUE_BANK_LIST_DATA;
}
