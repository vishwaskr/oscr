import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueBankListComponent } from './issue-bank-list.component';

describe('IssueBankListComponent', () => {
  let component: IssueBankListComponent;
  let fixture: ComponentFixture<IssueBankListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueBankListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueBankListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
