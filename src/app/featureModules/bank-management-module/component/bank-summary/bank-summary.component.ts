import {  Component, OnInit, Input ,Inject, ViewChild } from '@angular/core';
import { dateFormat , CheckIsInt } from "../../../../constants.js";
import { BankManagementService } from 'src/app/featureModules/bank-management-module/service/bank-management.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatTableDataSource} from '@angular/material/table';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
//import * as moment from 'moment';
import * as jq from 'jquery';
import * as moment from 'moment-timezone';

export interface BankSummaryData {
  BankId: number;
  BranchName : string;
  BankIssueDate: any;
  SiteName: string;
  RegisterNumber:number;
  PickupDate: any;
  PickupId: number;
  PickupNumber: number;
  PickupTime: any;
  PickupTotal: number;
  Type: string;
}


@Component({
  selector: 'app-bank-summary',
  templateUrl: './bank-summary.component.html',
  styleUrls: ['./bank-summary.component.css']
})
export class BankSummaryComponent implements OnInit {
  
  filterBranchId:number;
  userBranchArr:any;
  authUserId:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("registerDate"));
  date:any;
  currentDate = moment(new Date()).format(dateFormat);
  bankDate:any;
  bankId:any;
  dataSource:any;
  bankSummaryShow: boolean=false;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
  
  constructor(public BankServiceData: BankManagementService, public dialog: MatDialog) { }
  displayedColumns: string[] = ['registerDate', 'siteName', 'registerNumber', 'pickupNumber','pickupDate','pickupTime','totalPickup','action'];
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    }
    else{
    this.date = new FormControl(new Date());
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.bankDate=sessionStorage.getItem('bankDate');
    this.bankId=JSON.parse(sessionStorage.getItem('bankId'));
    this.getBankSummary(this.bankId,this.bankDate);
    //console.log(this.bankId+"--"+this.bankDate);
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   console.log('IDD:'+idd);
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  
  //Get summary details when branch change--------------------
  changeBranch(BranchId,Branch_Name){
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
  }
  //----------------------------------------------------------
   //Change date----------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   sessionStorage.setItem("registerDate",JSON.stringify(momentDate));
  }
  
  //Get Bank Summary-----------------------------------------
  getBankSummary(bankId,bankDate){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var bankDataArray= [];
    
    const bankDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'bankIssueDate': bankDate,
      'bankId': bankId
     }
    //API call------------------------
    this.BankServiceData.getBankSummary(bankDetails).subscribe(res=>{
      this.progressBarHide();
      console.log(res);
     if(res.status=='success') {
       var tz = moment.tz.guess();
       this.bankSummaryShow=true;
        res.data.forEach(function (value) {
        let bankData =  {} as BankSummaryData;
        
        bankData.BankId = value.BankId;
        bankData.BranchName = value.BranchName;
        bankData.BankIssueDate = moment(new Date(value.BankIssueDate)).format(dateFormat);
        bankData.SiteName = value.SiteName;
        bankData.RegisterNumber = value.RegisterNumber;
        bankData.PickupDate = moment(new Date(value.PickupDate)).format(dateFormat);
        bankData.PickupId = value.PickupId;
        bankData.PickupNumber = value.PickupNumber;
        
        var PickupTime='';
        if (value.PickupTime!='') {
        var gmtDateTime = moment.utc(value.PickupTime)
        PickupTime = moment.tz(gmtDateTime, tz).format("hh:mm a");
        }
        bankData.PickupTime = PickupTime;
        
        bankData.PickupTotal = value.PickupTotal;
        bankData.Type = value.Type;
        
        bankDataArray.push(bankData);
      
        });
        
        var SUMMARY_DATA: BankSummaryData[] = bankDataArray;
        this.dataSource = new MatTableDataSource<BankSummaryData>(SUMMARY_DATA);
       }
       else{
        this.bankSummaryShow=false;
        var SUMMARY_DATA: BankSummaryData[] = [];
        this.dataSource = new MatTableDataSource<BankSummaryData>(SUMMARY_DATA);
       }
    });
    //-------------------------------------------------------
     
  }
  //---------------------------------------------------------
  //Open new pickup dialog-----------------------------------
  openNewPickupDialog(branchName,siteName,registerNumber,pickupDate,pickupTime,pickupTotal,pickupNumber): void {
     var pickupDate2=moment(pickupDate).format(dateFormat);
    // var currentTime=moment(new Date(currentTime)).format("hh:mm a");
    const dialogRef = this.dialog.open(EditNewPickup, {
      disableClose: true,
      width: '',
      data: {branchName:branchName,siteName:siteName,registerNumber:registerNumber,pickupDate:pickupDate2,pickupTime:pickupTime,pickupNumber:pickupNumber,pickupTotal:pickupTotal}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (sessionStorage.getItem("dialogOpend")=='false') {
        this.getBankSummary(this.bankId,this.bankDate);
      }
    });
    
  }
  //------------------------------------------------------
  //Bank Close Popup--------------------------------------
  openBankCloseDialog(siteName): void {
    
    const dialogRef = this.dialog.open(EditBankClose, {
      width: '',
      data: {siteName:siteName}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      //if (sessionStorage.getItem("dialogOpend")=='false') {
      //  this.getIssuedBank();
      //}
    });
  }
  //------------------------------------------------------
  //Open edit popup---------------------------------------
  openEditPopup(branchName,bankId,bankIssueDate,type,pickupId,siteName,registerNumber,pickupDate,pickupTime,pickupTotal,pickupNumber){
    //console.log(type+"---"+pickupId);
   sessionStorage.setItem("pickupId",JSON.stringify(pickupId));
   sessionStorage.setItem("bankId",JSON.stringify(bankId));
   sessionStorage.setItem("bankIssueDate",JSON.stringify(bankIssueDate));
    if (type=='Pickup') {
      this.openNewPickupDialog(branchName,siteName,registerNumber,pickupDate,pickupTime,pickupTotal,pickupNumber);
    }
    else{
      this.openBankCloseDialog(siteName);
    }
  }
  //-------------------------------------------------------
  //-------------------------------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //-------------------------------------------------------

}
//---------------------------------------------------------
//Start edit new pickup------------------------------------
export interface NewPickUpData {
  BankId : number;
  branchId : number;
  branchName : string;
  siteName : string;
  siteId : number;
  registerNumber : number;
  registerId : number;
  pickupCount : number;
}

export interface PickupEmployee {
  FK_Employee_ID:number;
  name: string;
}

export interface DenominationDetail {
  HundredsCount : number;
  FiftiesCount : number;
  TwentiesCount : number;
  TensCount : number;
  FivesCount : number;
  TwosCount : number;
  OnesCount : number;
  QuartersCount : number;
  NickelsCount : number;
  DimesCount : number;
  PenniesCount : number;
  ChecksAmount : number;
  GiftCertificates : number;
  ChangeAmount : number;
}


@Component({
  selector: 'edit-new-pickup',
  templateUrl: './edit-new-pickup.html',
  styleUrls: ['./bank-summary.component.css']
})

export class EditNewPickup implements OnInit {
  
    //Progres----------------
     progressShow: boolean;
     mode = 'determinate';
     value = 50;
     bufferValue = 95;
     //-----------------------
     showErrorMsg: boolean = false;
     errormessage:string;
     successmessage:string;
  
    //pickupForm: FormGroup;
    denominationsForm: FormGroup;
    employees: any;
    authUserId: any;
    amountBills: number=1;
    bankIssueDate:any;
    BankPaymentFormsArray:any;
    pickUpEmployeeId:number;
    public paymentFormfields: any[];
    
    isChange:boolean=false;
    
    //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
    //------------------------------------
     //denominations Variables---
    denominationDetailArray: any;
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    
    
    change: number=0;
    checks: number;
    total: number=0;
    denominationsTotalAmount: number=0;
    
    constructor(public dialogRef: MatDialogRef<EditNewPickup>,@Inject(MAT_DIALOG_DATA) public data: NewPickUpData, public getEmployeeService: BankManagementService,public getPaymentFormList: BankManagementService,public submitNewpickupService: BankManagementService,public getPickupDetailsData: BankManagementService) {}
   
    
  onNoClick(): void {
    this.dialogRef.close();
  }
  
   ngOnInit() {
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getEmployeeList();
    this.getPickupDetails();
    //denominationsForm----------------------------
     this.denominationsForm = new FormGroup({
      pickUpEmployee: new FormControl(this.pickUpEmployeeId,[Validators.required]),
      hundreds: new FormControl(0),
      fifties: new FormControl(0),
      twenties: new FormControl(0),
      tens: new FormControl(0),
      fives: new FormControl(0),
      twos: new FormControl(0),
      ones: new FormControl(0),
      quarters: new FormControl(0),
      dimes: new FormControl(0),
      nickles: new FormControl(0),
      pennies: new FormControl(0),
      
      hundredsCount: new FormControl(0),
      fiftiesCount: new FormControl(0),
      twentiesCount: new FormControl(0),
      tensCount: new FormControl(0),
      fivesCount: new FormControl(0),
      twosCount: new FormControl(0),
      onesCount: new FormControl(0),
      quartersCount: new FormControl(0),
      dimesCount: new FormControl(0),
      nicklesCount: new FormControl(0),
      penniesCount: new FormControl(0),
      
      change: new FormControl(0),
      //checks: new FormControl(''),
      paymentFormfields: new FormControl(JSON.stringify(this.paymentFormfields))
    })
  }
  //------------------------------------------------------------
  //Get pickup details------------------------------------------
  getPickupDetails(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
     //sessionStorage.setItem("pickupId",JSON.stringify(pickupId));
     //sessionStorage.setItem("bankId",JSON.stringify(bankId));
     //sessionStorage.setItem("bankIssueDate",JSON.stringify(bankIssueDate));
    var pickupDetailsArray= [];
    const pickupDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'pickupId': JSON.parse(sessionStorage.getItem("pickupId")),
      'bankIssueDate': JSON.parse(sessionStorage.getItem("bankIssueDate"))
    }
    //call api--------------
    this.getPickupDetailsData.getPickupDetailsData(pickupDetails).subscribe(res=>{
      //console.log(res);
      if(res.status=='success') {
        
        this.pickUpEmployeeId=res.data.UserId;
        this.denominationsForm.controls['pickUpEmployee'].setValue(res.data.UserId);
        //Denomination-----------------------------
         this.denominationsForm.controls['hundreds'].setValue(res.data.DenominationAmount.HundredsAmount);
         this.denominationsForm.controls['fifties'].setValue(res.data.DenominationAmount.FiftiesAmount);
         this.denominationsForm.controls['twenties'].setValue(res.data.DenominationAmount.TwentiesAmount);
         this.denominationsForm.controls['tens'].setValue(res.data.DenominationAmount.TensAmount);
         this.denominationsForm.controls['fives'].setValue(res.data.DenominationAmount.FivesAmount);
         this.denominationsForm.controls['twos'].setValue(res.data.DenominationAmount.TwosAmount);
         this.denominationsForm.controls['ones'].setValue(res.data.DenominationAmount.OnesAmount);
         this.denominationsForm.controls['quarters'].setValue(res.data.DenominationAmount.QuartersAmount);
         this.denominationsForm.controls['dimes'].setValue(res.data.DenominationAmount.DimesAmount);
         this.denominationsForm.controls['nickles'].setValue(res.data.DenominationAmount.NickelsAmount);
         this.denominationsForm.controls['pennies'].setValue(res.data.DenominationAmount.PenniesAmount);
         this.denominationsForm.controls['change'].setValue(res.data.DenominationAmount.ChangeAmount);
         this.denominationsTotalAmount=res.data.DenominationAmount.TotalCash;
         //console.log(res.data.DenominationAmount.TotalCash);
         this.total=res.data.DenominationAmount.TotalCash;
        //-----------------------------------------
        //Denomination Count-----------------------
        this.denominationsForm.controls['hundredsCount'].setValue(res.data.DenominationCount.HundredsCount);
        this.denominationsForm.controls['fiftiesCount'].setValue(res.data.DenominationCount.FiftiesCount);
        this.denominationsForm.controls['twentiesCount'].setValue(res.data.DenominationCount.TwentiesCount);
        this.denominationsForm.controls['tensCount'].setValue(res.data.DenominationCount.TensCount);
        this.denominationsForm.controls['fivesCount'].setValue(res.data.DenominationCount.FivesCount);
        this.denominationsForm.controls['twosCount'].setValue(res.data.DenominationCount.TwosCount);
        this.denominationsForm.controls['onesCount'].setValue(res.data.DenominationCount.OnesCount);
        this.denominationsForm.controls['quartersCount'].setValue(res.data.DenominationCount.QuartersCount);
        this.denominationsForm.controls['dimesCount'].setValue(res.data.DenominationCount.DimesCount);
        this.denominationsForm.controls['nicklesCount'].setValue(res.data.DenominationCount.NickelsCount);
        this.denominationsForm.controls['penniesCount'].setValue(res.data.DenominationCount.PenniesCount);
        //-----------------------------------------
        var BankPaymentFormsArray= [];
              //bank payment forms-----------------------
          res.data.PaymentForms.forEach(function (value) {
            let bankFormsData =  {} as paymentFormfields;
                bankFormsData.type='text';
                bankFormsData.label=value.PaymentFormName;
                bankFormsData.name=value.PaymentFormId;
                bankFormsData.value=value.Amount;
                BankPaymentFormsArray.push(bankFormsData);
          });
          this.paymentFormfields=BankPaymentFormsArray;
          for (let i = 0; i < BankPaymentFormsArray.length; i++) {
           this.denominationsForm.addControl(BankPaymentFormsArray[i].name, new FormControl(BankPaymentFormsArray[i].Amount));
           this.denominationsForm.controls[BankPaymentFormsArray[i].name].setValue(BankPaymentFormsArray[i].value); 
          }
        
        
      }
    });
  }
  //------------------------------------------------------------
  
  //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=(hundredsCount * 100);
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
     this.hundredsError=true;
     this.noError=true;
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
    if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=(fiftiesCount * 50);
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
    if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=(twentiesCount * 20);
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=(tensCount * 10);
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=(fivesCount * 5);
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=(twosCount * 2);
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=(onesCount * 1);
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=(quartersCount * 0.25);
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else if(dimesCount > 0 && CheckIsInt(dimesCount)) {
     this.noError=false;
     this.dimesBillError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }    
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }  
    else if(nicklesCount > 0 && CheckIsInt(nicklesCount)) {
     this.noError=false;
     this.nicklesBillError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else if(penniesCount > 0 && CheckIsInt(penniesCount)) {
     this.noError=false;
     this.penniesBillError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }  
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //Submit-------------------------------------------------------
  denominationsFormSubmit(){
    var frmVal=JSON.stringify(this.denominationsForm.value);
    var frmValObj=JSON.parse(frmVal);
    var denominationDetailArray = [];
    //console.log(frmValObj);
    let sum = 0;
    var countVariable = ["pickUpEmployee","dimesCount","fiftiesCount","fivesCount","nicklesCount","onesCount","penniesCount","quartersCount","tensCount","twentiesCount","twosCount","hundredsCount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!countVariable.includes(key)) {
          //console.log(key + " -> " + frmValObj[key]);
          let amount=frmValObj[key];
          sum = sum + (+amount);
        }
          
      }
    }
    //Collection data---------------
    let denominationData =  {} as DenominationDetail;
      denominationData.HundredsCount = frmValObj['hundredsCount'];
      denominationData.FiftiesCount = frmValObj['fiftiesCount'];
      denominationData.TwentiesCount = frmValObj['twentiesCount'];
      denominationData.TensCount = frmValObj['tensCount'];
      denominationData.FivesCount = frmValObj['fivesCount'];
      denominationData.TwosCount = frmValObj['twosCount'];
      denominationData.OnesCount = frmValObj['onesCount'];
      denominationData.QuartersCount = frmValObj['quartersCount'];
      denominationData.NickelsCount = frmValObj['nicklesCount'];
      denominationData.DimesCount = frmValObj['dimesCount'];
      denominationData.PenniesCount = frmValObj['penniesCount'];
      denominationData.ChangeAmount = frmValObj['change'];

      this.denominationDetailArray=denominationData;
      //console.log(denominationData);
    //------------------------------
    //Collecting payments form data---------
    var frmVal2=JSON.stringify(this.paymentFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var paymentFormSubmitDetailsArr=[];
   
    for (var key2 in frmValObj2) {
      let paymentFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      paymentFormSubmitDetails.PaymentFormId=keyName;
      paymentFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      if (frmValObj[JSON.stringify(keyName)] > 0) {
       paymentFormSubmitDetailsArr.push(paymentFormSubmitDetails);
      }
      
    }
    this.BankPaymentFormsArray=paymentFormSubmitDetailsArr;
    this.denominationsTotalAmount=+sum.toFixed(2);
    this.total=+sum.toFixed(2);
    document.getElementById('pickupTotal').innerHTML = JSON.stringify(this.total);
  }
  //--------------------------------------------------------------
  //Pickup Submit-------------------------------------------------
  pickupFormSubmit(){
    this.denominationsFormSubmit();
    if (this.denominationsForm.valid && !this.noError) {
     if (this.total < 1 ) {
       this.showErrorMsg=true;
       this.errormessage= 'Please enter currency denominations';
         //setTimeout(() => {
         //this.showErrorMsg=false;
         //}, 2000);
      }
      else {
      this.progressBarShow();
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      const newPickupDetails = {
         'pickupId' : sessionStorage.getItem("pickupId"),
         'denominationDetail':JSON.stringify(this.denominationDetailArray),
         'paymentFormDetail':JSON.stringify(this.BankPaymentFormsArray),
         'bankIssueDate': JSON.parse(sessionStorage.getItem("bankIssueDate")),
         'pickupBy' : this.denominationsForm.value.pickUpEmployee,
         'createBy' : this.authUserId,
         'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
         'registerId' : JSON.parse(sessionStorage.getItem("registerId")),
         'siteId' : JSON.parse(sessionStorage.getItem("siteId")),
         'branchId' : branchId,
         'verifiedBy' : this.authUserId,
         'accessTypeId' : 1,
         'authUserId' : this.authUserId,
         'isChange' : this.isChange
        }
       // console.log(newPickupDetails);
        this.submitNewpickupService.submitEditPickup(newPickupDetails).subscribe(res=>{
         this.progressBarHide();
         console.log(res);
         if(res.status=='success') {
             this.showErrorMsg=false;
             this.successmessage= 'Success! Pickup Updated.';
             sessionStorage.setItem("dialogOpend",'false');
             setTimeout(() => {
              this.dialogRef.close();
             }, 2000);
           }
           else{
            this.showErrorMsg=true;
            this.errormessage= res.message;
           }
        });
      //-------------------------
       }
      }
      else{
      this.progressBarHide();
      this.showErrorMsg=true;
      this.errormessage= 'Some errors occurd...';
    }
  }
  
  //--------------------------------------------------------------
  //Is any changes made-------------------------------------------
  isAmountChange(){
    this.isChange=true;
  }
  //--------------------------------------------------------------
  //Integer checking----------------------------------------------
  //isInt(n) {
  // return n % 1 === 0;
  //}
  //--------------------------------------------------------------
  //--------------------------------------------------------------
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as PickupEmployee;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      }
      
    })
    //----------------------------------------------------
  }
  //------------------------------------------------------
  //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------
}
//---------------------------------------------------------------
//Edit Close Bank------------------------------------------------
export interface bankEditData {
  BankId : number;
  branchId : number;
  branchName : string;
  siteName : string;
  siteId : number;
  registerNumber : number;
  registerId : number;
  pickupCount : number;
}

export interface Employee {
  UserId: number;
  name: string;
}

export interface BankDetails {
  registerStartingGT : number;
  registerEndingGT : number;
  registerZNumber : string;
  registerTranx : number;
  registerNoSales : number;
  registerReading : number;
}

export interface BankPaymentForms {
BranchPaymentFormId : number;
PaymentFormName : string;
Amount : number;
}

export interface paymentFormfields {
  type : string;
  name : string;
  label : string;
  value : number;
}

export interface otherChargeFormfields {
  type : string;
  name : string;
  label : string;
  value : number;
}

export interface cardHandheldDetails {
  AmexAmount : number;
  BatchoutNumber : string;
  CardHandheldTidNumber : string;
  CloseCardHandheldID : number;
  DebitAmount : number;
  DiscoverAmount : number;
  MasterCardAmount : number;
  TotalCardHandheldAmount : number;
  VisaAmount : number;
}

export interface POS {
 BatchoutNumber : string;
 AmexAmount : number;
 DebitAmount : number;
 DiscoverAmount : number;
 MasterCardAmount : number;
 VisaAmount : number;
 TotalCardHandheldAmount : number;
 OtherChargeAmount : number;
 TotalPOSAmount : number;
}

export interface PickupList {
  PickupNumber: number;
  PickupTotal: number;
}

export interface DenominationDetail {
  HundredsCount: number;
  FiftiesCount: number;
  TwentiesCount: number;
  FivesCount : number;
  TwosCount: number;
  OnesCount: number;
  QuartersCount: number;
  NickelsCount: number;
  DimesCount: number;
  PenniesCount: number;
  ChangeAmount: number;
}


export interface PaymentFormSubmitDetails {
  PaymentFormId: number;
  Amount: number;
}

@Component({
  selector: 'edit-bank-close',
  templateUrl: './edit-bank-close.html',
  styleUrls: ['./bank-summary.component.css']
})

export class EditBankClose implements OnInit {
    
    errorArray:Array<any> = [];
    errormessage: string;
    successmessage: string
    showErrorMsg: boolean;
    
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
 //-----------------------
 
  registerSummaryForm: FormGroup;
  salesSummaryForm: FormGroup;
  denominationsForm: FormGroup;
  cardHandheldform: FormGroup;
  posForm: FormGroup;
  otherChargesForm: FormGroup;
  BankCloseCashId: number;
  pickupList: any;
  
  BankPaymentFormsArray:any;
  public paymentFormfields: any[];
  OtherChargesFormsArray:any;
  public otherChargeFormfields: any[];
  
    authUserId:number;
    employees:any;
    
    cardHandheldFieldsitems:any;
    cardHandheldTotal:number=0;
    cardHandheldFieldsDataArray:any;
    otherChargesTotal:number=0;
    
   //Register Variables--------
    registerStartingGT: number;
    registerEndingGT: number;
    registerZNumber: string='';
    registerTranx: number;
    registerNoSales: number;
    registerReading: number;
   //--------------------------
   //Sales Summary Variables---
   //difference: number;
   totalPickup: number=0;
   totalDrawer: number=0;
   adjustments: number=0;
   adjustmentComments: string;
   ccAdjustments: number=0;
   ccAdjustmentComments: string;
   overShort: number=0;
   overShortComments: string;
   cashier: any;
   cashierDataArray: any;
   cashierArr: string[];
   cashierList:PickupEmployee[];
   //--------------------------
   isChange:boolean=false;
   //Error flags---------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
    adjustmentCommentError:boolean=false;
    ccAdjustmentCommentError:boolean=false;
    overShortCommentError:boolean=false;
    ccHandheldBatchoutError:boolean=false;
    ccHandheldAmountError:boolean=false;
    registerStartingGTError:boolean=false;
    registerEndingGTError:boolean=false;
    registerZNumberError:boolean=false;
    registerTranxError:boolean=false;
    registerNoSalesError:boolean=false;
    registerReadingError:boolean=false;
    //------------------------------------
   //denominations Variables---
    denominationDetailArray: any;
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    
    
    change: number=0;
    checks: number;
    total: number=0;
    denominationsTotalAmount: number=0;
   //-----------------------
   //POS form variables--------
   posBatchoutNumberError: boolean;
   posName: string;
   BatchoutNumber: string;
   VisaAmount: number=0;
   MasterCardAmount: number=0;
   DiscoverAmount: number=0;
   AmexAmount: number=0;
   DebitAmount: number=0;
   totalPOS: number=0;
   posDetails : any;
   //--------------------------
   constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<EditBankClose>,@Inject(MAT_DIALOG_DATA) public data: bankEditData, public getEmployeeService: BankManagementService,public BankServiceData: BankManagementService) {}
   
    ngOnInit() {
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getEmployeeList();
    this.getCloseBankDetails();
    
    //Register Summary Form------------------------
    this.registerSummaryForm = new FormGroup({
      BankCloseCashId: new FormControl(this.BankCloseCashId),
      registerStartingGT: new FormControl(this.registerStartingGT,[Validators.required]),
      registerEndingGT: new FormControl(this.registerEndingGT,[Validators.required]),
      registerZNumber: new FormControl(this.registerZNumber,[Validators.required]),
      registerTranx: new FormControl(this.registerTranx,[Validators.required]),
      registerNoSales: new FormControl(this.registerNoSales,[Validators.required]),
      registerReading: new FormControl(this.registerReading,[Validators.required])
    });
    //---------------------------------------------
    //Sales Summary Form---------------------------
    this.salesSummaryForm = new FormGroup({
     // difference: new FormControl(this.difference),
      totalPickup: new FormControl(this.totalPickup),
      totalDrawer: new FormControl(this.totalDrawer),
      adjustments: new FormControl(this.adjustments),
      adjustmentComments: new FormControl(this.adjustmentComments),
      ccAdjustments: new FormControl(this.ccAdjustments),
      ccAdjustmentComments: new FormControl(this.ccAdjustmentComments),
      overShort: new FormControl(this.overShort),
      overShortComments: new FormControl(this.overShortComments),
      cashier: new FormControl(this.overShortComments)
    });
    //---------------------------------------------
    //denominationsForm----------------------------
     this.denominationsForm = new FormGroup({
      hundreds: new FormControl(this.hundreds),
      fifties: new FormControl(this.fifties),
      twenties: new FormControl(this.twenties),
      tens: new FormControl(this.tens),
      fives: new FormControl(this.fives),
      twos: new FormControl(this.twos),
      ones: new FormControl(this.ones),
      quarters: new FormControl(this.quarters),
      dimes: new FormControl(this.dimes),
      nickles: new FormControl(this.nickles),
      pennies: new FormControl(this.pennies),
      
      hundredsCount: new FormControl(this.hundredsCount),
      fiftiesCount: new FormControl(this.fiftiesCount),
      twentiesCount: new FormControl(this.twentiesCount),
      tensCount: new FormControl(this.tensCount),
      fivesCount: new FormControl(this.fivesCount),
      twosCount: new FormControl(this.twosCount),
      onesCount: new FormControl(this.onesCount),
      quartersCount: new FormControl(this.quartersCount),
      dimesCount: new FormControl(this.dimesCount),
      nicklesCount: new FormControl(this.nicklesCount),
      penniesCount: new FormControl(this.penniesCount),
      
      change: new FormControl(this.change),
      //checks: new FormControl(''),
      paymentFormfields: new FormControl(JSON.stringify(this.paymentFormfields))
    })
    //-------------------------------------------
    //POS Form-----------------------------------
   
    this.posForm = new FormGroup({
      BatchoutNumber: new FormControl(this.BatchoutNumber),
      VisaAmount: new FormControl(this.VisaAmount),
      MasterCardAmount: new FormControl(this.MasterCardAmount),
      DiscoverAmount: new FormControl(this.DiscoverAmount),
      AmexAmount: new FormControl(this.AmexAmount),
      DebitAmount: new FormControl(this.DebitAmount),
      TotalCardHandheldAmount: new FormControl(this.totalPOS)
    });
    //---------------------------------------------
    
    //CardHandheld Form----------------------------
    this.cardHandheldform = this.fb.group({
      cardHandheldFieldsitems: this.fb.array([])
    });
    //-------------------------------------------
    
     //Other charges forms------------------------
    this.otherChargesForm = this.fb.group({
      otherChargeFormfields: new FormControl(JSON.stringify(this.otherChargeFormfields))
    });
    //-------------------------------------------
    
   }
   
  get CardHandheldFields() {
    return this.cardHandheldform.get('cardHandheldFieldsitems') as FormArray
  }
  
  addCardHendheldField(CardHandheldTidNumber,CloseCardHandheldID,BatchoutNumber,AmexAmount,DebitAmount,DiscoverAmount,MasterCardAmount,VisaAmount,TotalCardHandheldAmount) {
      if (BatchoutNumber==0) {
       BatchoutNumber=''; 
      }
     const CardHendheldField = this.fb.group({ 
      CardHandheldTidNumber: [CardHandheldTidNumber],
      CloseCardHandheldID: [CloseCardHandheldID],
      BatchoutNumber: [BatchoutNumber],
      AmexAmount: [AmexAmount],
      DebitAmount: [DebitAmount],
      DiscoverAmount: [DiscoverAmount],
      MasterCardAmount: [MasterCardAmount],
      TotalCardHandheldAmount: [TotalCardHandheldAmount],
      VisaAmount: [VisaAmount]
    })

    this.CardHandheldFields.push(CardHendheldField)
  }
  
  
   
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as PickupEmployee;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      } 
    })
    
  }
  //------------------------------------------------------
  //Get Close Bank Details--------------------------------
  getCloseBankDetails(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var BankPaymentFormsArray=[];
    var OtherChargeDetailsArray=[];
    var cardhandheldFormsDataArray=[];
    var cashierDataArray=[];
    var cashierArr=[];
    var pickupDataArr=[];
    const bankDetails = {
      'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
     //call api------------
      this.BankServiceData.getCloseBankData(bankDetails).subscribe(res=>{
      this.progressBarHide();
      console.log(res);
      if(res.status=='success') {
          this.BankCloseCashId = res.data.BankCloseCashId;
          this.registerSummaryForm.controls['BankCloseCashId'].setValue(res.data.BankCloseCashId);
          this.registerStartingGT = res.data.StartingGT;
          this.registerSummaryForm.controls['registerStartingGT'].setValue(res.data.StartingGT);
          this.registerEndingGT = res.data.EndingGT;
          this.registerSummaryForm.controls['registerEndingGT'].setValue(res.data.EndingGT);
          
          //this.registerZNumber = res.data.RegisterZNumber;
          var RegisterZNumber=null;
          
          if (res.data.RegisterZNumber=='null' || res.data.RegisterZNumber== null) {
          // RegisterZNumber = res.data.RegisterZNumber;
           
          }
          else{
            RegisterZNumber = res.data.RegisterZNumber;
          }
          
          this.registerSummaryForm.controls['registerZNumber'].setValue(RegisterZNumber);
          
          this.registerTranx = res.data.RegisterTranx;
          this.registerSummaryForm.controls['registerTranx'].setValue(res.data.RegisterTranx);
          this.registerNoSales = res.data.RegisterNoSales;
          this.registerSummaryForm.controls['registerNoSales'].setValue(res.data.RegisterNoSales);
          this.registerReading = res.data.Sales;
          this.registerSummaryForm.controls['registerReading'].setValue(res.data.Sales);
          
          //this.difference = 0;
          this.totalPickup = res.data.TotalPickup;
          this.salesSummaryForm.controls['totalPickup'].setValue(res.data.TotalPickup);
          this.totalDrawer = res.data.TotalDrawer;
          this.salesSummaryForm.controls['totalDrawer'].setValue(res.data.TotalDrawer);
          this.adjustments = res.data.Adjustment;
          this.salesSummaryForm.controls['adjustments'].setValue(res.data.Adjustment);
          this.adjustmentComments = res.data.AdjustmentComment;
          var AdjustmentComment = null;
          if (res.data.AdjustmentComment!='null') {
            AdjustmentComment=res.data.AdjustmentComment;
          }
          this.salesSummaryForm.controls['adjustmentComments'].setValue(AdjustmentComment);
         
          this.ccAdjustments = res.data.CCAdjustment;
          this.salesSummaryForm.controls['ccAdjustments'].setValue(res.data.CCAdjustment);
          this.ccAdjustmentComments = res.data.CCAdjustmentComment;
          var CCAdjustmentComment = null;
          if (res.data.CCAdjustmentComment!='null') {
            CCAdjustmentComment = res.data.CCAdjustmentComment;
          }
          this.salesSummaryForm.controls['ccAdjustmentComments'].setValue(CCAdjustmentComment);
          
          this.overShort = res.data.OverShort;
          this.salesSummaryForm.controls['overShort'].setValue(res.data.OverShort);
          
          this.overShortComments = res.data.OverShortComment;
          var overShortComments=null;
          if (res.data.OverShortComment!='') {
            overShortComments=res.data.OverShortComment;
          }
          this.salesSummaryForm.controls['overShortComments'].setValue(overShortComments);
          
          //bank payment forms-----------------------
          res.data.BankPaymentForms.forEach(function (value) {
            //console.log(value);
            let bankFormsData =  {} as paymentFormfields;
                
                bankFormsData.type='text';
                bankFormsData.label=value.PaymentFormName;
                bankFormsData.name=value.PaymentFormId;
                bankFormsData.value=value.Amount;
                
                BankPaymentFormsArray.push(bankFormsData);
          });
          this.paymentFormfields=BankPaymentFormsArray;
          //console.log(BankPaymentFormsArray);
          for (let i = 0; i < BankPaymentFormsArray.length; i++) {
           // console.log(BankPaymentFormsArray[i].name);
           this.denominationsForm.addControl(BankPaymentFormsArray[i].name, new FormControl(BankPaymentFormsArray[i].Amount));
           this.denominationsForm.controls[BankPaymentFormsArray[i].name].setValue(BankPaymentFormsArray[i].value); 
          }
          //-----------------------------------------
          //otherChargeFormfields--------------------
          var otherChargesTotal=0;
          res.data.BankOtherChargeDetails.forEach(function (value) {
            //BankOtherChargeDetailsArray
            let otherChargeFormsData =  {} as otherChargeFormfields;
                otherChargeFormsData.type='text';
                otherChargeFormsData.label=value.PaymentFormName;
                otherChargeFormsData.name=value.PaymentFormId;
                otherChargeFormsData.value=value.Amount;
                
                otherChargesTotal=otherChargesTotal+value.Amount;
                
                OtherChargeDetailsArray.push(otherChargeFormsData);
          });
          this.otherChargesTotal=+otherChargesTotal;
          this.otherChargeFormfields=OtherChargeDetailsArray;
          for (let i = 0; i < OtherChargeDetailsArray.length; i++) {
           this.otherChargesForm.addControl(OtherChargeDetailsArray[i].name, new FormControl(OtherChargeDetailsArray[i].Amount));
           this.otherChargesForm.controls[OtherChargeDetailsArray[i].name].setValue(OtherChargeDetailsArray[i].value); 
          }
          //-----------------------------------------
          //CardHandheld-----------------------------
         var totalCardHandheldTotal=0;
          res.data.CardHandheldDetails.forEach(function (value) {
            let cardhandheldFormsData =  {} as cardHandheldDetails;
              cardhandheldFormsData.AmexAmount = value.AmexAmount;
              cardhandheldFormsData.BatchoutNumber = value.BatchoutNumber;
              cardhandheldFormsData.CardHandheldTidNumber = value.CardHandheldTidNumber;
              cardhandheldFormsData.CloseCardHandheldID = value.CloseCardHandheldID;
              cardhandheldFormsData.DebitAmount = value.DebitAmount;
              cardhandheldFormsData.DiscoverAmount = value.DiscoverAmount;
              cardhandheldFormsData.MasterCardAmount = value.MasterCardAmount;
              cardhandheldFormsData.VisaAmount = value.VisaAmount;
              cardhandheldFormsData.TotalCardHandheldAmount = value.TotalCardHandheldAmount;
              totalCardHandheldTotal = totalCardHandheldTotal+value.TotalCardHandheldAmount;
              cardhandheldFormsDataArray.push(cardhandheldFormsData);
             
          });
          
          this.cardHandheldTotal=totalCardHandheldTotal;
          
         // console.log(cardhandheldFormsDataArray);
         for (let i = 0; i < cardhandheldFormsDataArray.length; i++) {
          //CardHandheldTidNumber,CloseCardHandheldID,BatchoutNumber,AmexAmount,DebitAmount,DiscoverAmount,MasterCardAmount,VisaAmount,TotalCardHandheldAmount
         
          this.addCardHendheldField(cardhandheldFormsDataArray[i].CardHandheldTidNumber, cardhandheldFormsDataArray[i].CloseCardHandheldID, cardhandheldFormsDataArray[i].BatchoutNumber, cardhandheldFormsDataArray[i].AmexAmount, cardhandheldFormsDataArray[i].DebitAmount, cardhandheldFormsDataArray[i].DiscoverAmount, cardhandheldFormsDataArray[i].MasterCardAmount, cardhandheldFormsDataArray[i].VisaAmount, cardhandheldFormsDataArray[i].TotalCardHandheldAmount);
         }
        //-----------------------------------------
        //Denomination-----------------------------
         this.denominationsForm.controls['hundreds'].setValue(res.data.Denomination.HundredsAmount);
         this.denominationsForm.controls['fifties'].setValue(res.data.Denomination.FiftiesAmount);
         this.denominationsForm.controls['twenties'].setValue(res.data.Denomination.TwentiesAmount);
         this.denominationsForm.controls['tens'].setValue(res.data.Denomination.TensAmount);
         this.denominationsForm.controls['fives'].setValue(res.data.Denomination.FivesAmount);
         this.denominationsForm.controls['twos'].setValue(res.data.Denomination.TwosAmount);
         this.denominationsForm.controls['ones'].setValue(res.data.Denomination.OnesAmount);
         this.denominationsForm.controls['quarters'].setValue(res.data.Denomination.QuartersAmount);
         this.denominationsForm.controls['dimes'].setValue(res.data.Denomination.DimesAmount);
         this.denominationsForm.controls['nickles'].setValue(res.data.Denomination.NickelsAmount);
         this.denominationsForm.controls['pennies'].setValue(res.data.Denomination.PenniesAmount);
         this.denominationsForm.controls['change'].setValue(res.data.Denomination.ChangeAmount);
         this.denominationsTotalAmount=res.data.Denomination.TotalCash;
        //-----------------------------------------
        //Denomination Count-----------------------
        this.denominationsForm.controls['hundredsCount'].setValue(res.data.DenominationCount.HundredsCount);
        this.denominationsForm.controls['fiftiesCount'].setValue(res.data.DenominationCount.FiftiesCount);
        this.denominationsForm.controls['twentiesCount'].setValue(res.data.DenominationCount.TwentiesCount);
        this.denominationsForm.controls['tensCount'].setValue(res.data.DenominationCount.TensCount);
        this.denominationsForm.controls['fivesCount'].setValue(res.data.DenominationCount.FivesCount);
        this.denominationsForm.controls['twosCount'].setValue(res.data.DenominationCount.TwosCount);
        this.denominationsForm.controls['onesCount'].setValue(res.data.DenominationCount.OnesCount);
        this.denominationsForm.controls['quartersCount'].setValue(res.data.DenominationCount.QuartersCount);
        this.denominationsForm.controls['dimesCount'].setValue(res.data.DenominationCount.DimesCount);
        this.denominationsForm.controls['nicklesCount'].setValue(res.data.DenominationCount.NickelsCount);
        this.denominationsForm.controls['penniesCount'].setValue(res.data.DenominationCount.PenniesCount);
        //-----------------------------------------
        //POS--------------------------------------
          let posFormsData =  {} as POS;
          posFormsData.BatchoutNumber=res.data.PosDetails.BatchoutNumber;
          posFormsData.AmexAmount=res.data.PosDetails.AmexAmount;
          posFormsData.DebitAmount=res.data.PosDetails.DebitAmount;
          posFormsData.DiscoverAmount=res.data.PosDetails.DiscoverAmount;
          posFormsData.MasterCardAmount=res.data.PosDetails.MasterCardAmount;
          posFormsData.VisaAmount=res.data.PosDetails.VisaAmount;
          posFormsData.TotalCardHandheldAmount=res.data.PosDetails.TotalCardHandheldAmount;
          
          //set form value----------------------------
          this.posName=res.data.PosDetails.POSName;
          this.posForm.controls['BatchoutNumber'].setValue(res.data.PosDetails.BatchoutNumber);
          this.posForm.controls['VisaAmount'].setValue(res.data.PosDetails.VisaAmount);
          this.posForm.controls['MasterCardAmount'].setValue(res.data.PosDetails.MasterCardAmount);
          this.posForm.controls['DiscoverAmount'].setValue(res.data.PosDetails.DiscoverAmount);
          this.posForm.controls['AmexAmount'].setValue(res.data.PosDetails.AmexAmount);
          this.posForm.controls['DebitAmount'].setValue(res.data.PosDetails.DebitAmount);
          
          this.totalPOS = res.data.PosDetails.TotalCardHandheldAmount;
          //------------------------------------------
          //console.log(posFormsData);
       
        //-----------------------------------------
        //Cashier----------------------------------
        res.data.Cashier.forEach(function (value) {
          let cashierData =  {} as PickupEmployee;
          cashierData.FK_Employee_ID=value.UserId;
          cashierArr.push(value.UserId);
          cashierDataArray.push(cashierData);
        });
        this.cashierList=cashierDataArray;
        this.cashierArr=cashierArr;
        //console.log(this.cashierList);
        //-----------------------------------------
        //Pickup List------------------------------
        res.data.PickupList.forEach(function (value) {
          let pickupData =  {} as PickupList;
           pickupData.PickupNumber = value.PickupNumber;
           pickupData.PickupTotal = value.PickupTotal;
           pickupDataArr.push(pickupData);   
        });
        this.pickupList = pickupDataArr;
        //-----------------------------------------
      }
      else{
       //console.log('Error');
      }
      //console.log(bankDetailsArray);
    })
     
  }
  //------------------------------------------------------
  
  //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundredsError=false;
     this.hundreds=(hundredsCount * 100);
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.hundredsBillError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsError=true;
     this.noError=true;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
     if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fiftiesError=false;
     this.fifties=(fiftiesCount * 50);
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
     if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.fiftiesBillError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twentiesError=false;
     this.twenties=(twentiesCount * 20);
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.twentiesBillError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tensError=false;
     this.tens=(tensCount * 10);
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.tensBillError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fivesError=false;
     this.fives=(fivesCount * 5);
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.fivesBillError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twosError=false;
     this.twos=(twosCount * 2);
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.twosBillError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.onesError=false;
     this.ones=(onesCount * 1);
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.onesBillError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quartersError=false;
     this.quarters=(quartersCount * 0.25);
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.quartersBillError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else if(dimesCount > 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimesError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }     
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.dimesBillError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }  
    else if(nicklesCount > 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nicklesError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.nicklesBillError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else if(penniesCount > 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.penniesError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }    
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.penniesBillError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  
  
  //DenominationsFormSubmit-------------------------------
  denominationsFormSubmit(){
    //console.log(this.denominationsForm.value);
    if (!this.noError) {
    var frmVal=JSON.stringify(this.denominationsForm.value);
    var frmValObj=JSON.parse(frmVal);
    var denominationDetailArray = [];
    //console.log(frmValObj);
    let sum = 0;
    var countVariable = ["dimesCount","fiftiesCount","fivesCount","nicklesCount","onesCount","penniesCount","quartersCount","tensCount","twentiesCount","twosCount","hundredsCount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!countVariable.includes(key)) {
          //console.log(key + " -> " + frmValObj[key]);
          let amount=frmValObj[key];
          sum = sum + (+amount);
        }
          
      }
    }
    //Collection data---------------
    let denominationData =  {} as DenominationDetail;
      denominationData.HundredsCount = frmValObj['hundredsCount'];
      denominationData.FiftiesCount = frmValObj['fiftiesCount'];
      denominationData.TwentiesCount = frmValObj['twentiesCount'];
      denominationData.TensCount = frmValObj['tensCount'];
      denominationData.FivesCount = frmValObj['fivesCount'];
      denominationData.TwosCount = frmValObj['twosCount'];
      denominationData.OnesCount = frmValObj['onesCount'];
      denominationData.QuartersCount = frmValObj['quartersCount'];
      denominationData.NickelsCount = frmValObj['nicklesCount'];
      denominationData.DimesCount = frmValObj['dimesCount'];
      denominationData.PenniesCount = frmValObj['penniesCount'];
      denominationData.ChangeAmount = frmValObj['change'];
      
     // denominationDetailArray.push(denominationData);
      this.denominationDetailArray=denominationData;
    //------------------------------
    //Collecting payments form data---------
    var frmVal2=JSON.stringify(this.paymentFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var paymentFormSubmitDetailsArr=[];
   
    for (var key2 in frmValObj2) {
      let paymentFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      paymentFormSubmitDetails.PaymentFormId=keyName;
      paymentFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      paymentFormSubmitDetailsArr.push(paymentFormSubmitDetails);
    }
    this.BankPaymentFormsArray=paymentFormSubmitDetailsArr;
    //console.log(paymentFormSubmitDetailsArr);
    //--------------------------------------
    
    this.denominationsTotalAmount=+sum.toFixed(2);
    this.calculateOverShort();
    this.errorArray.splice(2, 1); 
    }
    else{
      this.errorArray[2]='Some error in denominations';
    }
  }
  //------------------------------------------------------
  //Register Summary Form Submit--------------------------
  registerSummaryFormSubmit(){
    var registerStartingGT=this.registerSummaryForm.value.registerStartingGT;
    this.registerStartingGTError=false;
    if (registerStartingGT === null) {
     this.registerStartingGTError=true; 
    }
    
    var registerEndingGT=this.registerSummaryForm.value.registerEndingGT;
    this.registerEndingGTError=false;
    if (registerEndingGT === null) {
      this.registerEndingGTError=true;
    }
    
    var registerZNumber = this.registerSummaryForm.value.registerZNumber;
    this.registerZNumberError=false;
    if (registerZNumber == "" || registerZNumber === 'null') {
      this.registerZNumberError=true;
    }
    
    var registerTranx = this.registerSummaryForm.value.registerTranx;
    
    this.registerTranxError=false;
    if (registerTranx === null) {
      this.registerTranxError=true;
    }
    
    var registerNoSales = this.registerSummaryForm.value.registerNoSales;
    this.registerNoSalesError=false;
    if (registerNoSales === null) {
      this.registerNoSalesError=true;
    }
    
    var registerReading = this.registerSummaryForm.value.registerReading;
    this.registerReadingError=false;
    if (registerReading === null) {
      this.registerReadingError=true;
    }
    
    if(this.registerSummaryForm.valid) {
      
      if (registerStartingGT > registerEndingGT) {
        this.registerStartingGTError=true;
        this.errorArray[9]="Register Starting GT Can't be grater than Ending GT ";
      }
      else{
       this.errorArray.splice(9, 1);
       this.registerStartingGTError=false; 
      }
      this.errorArray.splice(8, 1);
    }
    else{
     this.errorArray[8]='Please fill all register summary fields';
    }
    
    this.calculateOverShort();
   
  }
  //------------------------------------------------------
  //Sales Summary Form Submit-----------------------------
  salesSummaryFormSubmit(){
    //console.log(this.salesSummaryForm);
    var frmVal=JSON.stringify(this.salesSummaryForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var cashierDataArr=[];
    
    for (var key in frmValObj['cashier']) {
      let cashierData =  {} as Employee;
      cashierData.UserId = frmValObj['cashier'][key].FK_Employee_ID;
      cashierDataArr.push(cashierData);
    }
    
    //ccAdjustmentCommentError
    //CC adjustment comment null checking-----------------
    var ccAdjustmentAmt=this.salesSummaryForm.value.ccAdjustments;
    var ccAdjustmentComment=this.salesSummaryForm.value.ccAdjustmentComments;
    if ((ccAdjustmentAmt > 0 || ccAdjustmentAmt < 0 && ccAdjustmentAmt !='') && (ccAdjustmentComment=='' || ccAdjustmentComment==null)) {
      this.ccAdjustmentCommentError=true;
      this.errorArray[6]='Please enter CC Adjustment comment';
    }
    else{
     this.errorArray.splice(6, 1); 
     this.ccAdjustmentCommentError=false;
    }
    //----------------------------------------------------
    //Adjustment Comment null checking--------------------
    var adjustmentAmt=this.salesSummaryForm.value.adjustments;
    var adjustmentComment=this.salesSummaryForm.value.adjustmentComments;
    if ((adjustmentAmt > 0 || adjustmentAmt < 0 && adjustmentAmt !='') && (adjustmentComment=='' || adjustmentComment==null)) {
      this.errorArray[5]='Please enter Adjustment comment';
      this.adjustmentCommentError=true;
    }
    else{
     this.errorArray.splice(5, 1); 
     this.adjustmentCommentError=false;
    }
    //----------------------------------------------------
   //overshort null checking------------------------------
    var overShortAmt=this.salesSummaryForm.value.overShort;
    var overShortComment=this.salesSummaryForm.value.overShortComments;
    if ((overShortAmt >= 10 ) && (overShortComment=='' || overShortComment==null)) {
      this.errorArray[0]='Please enter over/short comments';
      this.overShortCommentError=true;
    }
    else{
     this.errorArray.splice(0, 1); 
     this.overShortCommentError=false;
    }
    //----------------------------------------------------
    
    this.cashierDataArray=cashierDataArr;
    this.calculateOverShort();
  }
  //------------------------------------------------------
  //POS Form submit---------------------------------------
  posFormSubmit(){
   // console.log(this.posForm.value);
    var frmVal=JSON.stringify(this.posForm.value);
    var frmValObj=JSON.parse(frmVal);
    var sum = 0;
    var excludeVariable = ["BatchoutNumber","TotalCardHandheldAmount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!excludeVariable.includes(key)) {
         // console.log(key + " -> " + frmValObj[key]);
          var amount=frmValObj[key];
          sum = sum + (parseFloat(amount));
        }
          
      }
    }
    //For collecting data----------
    var posDataArray=[];
    let posData =  {} as POS;
    for (var key in frmValObj) {
      if (key=='BatchoutNumber') {
        posData.BatchoutNumber=frmValObj['BatchoutNumber'];
        if ((frmValObj['BatchoutNumber']=='' || frmValObj['BatchoutNumber']==null) && sum > 0) {
          this.errorArray[1]='Please enter POS Batch#';
          this.posBatchoutNumberError=true;
        }
        else{
          this.errorArray.splice(1, 1); 
          this.posBatchoutNumberError=false;
        }
      }
      if (key=='VisaAmount') {
        posData.VisaAmount=frmValObj['VisaAmount'];
      }
      if (key=='MasterCardAmount') {
        posData.MasterCardAmount=frmValObj['MasterCardAmount'];
      }
      if (key=='DiscoverAmount') {
        posData.DiscoverAmount=frmValObj['DiscoverAmount'];
      }
      if (key=='AmexAmount') {
        posData.AmexAmount=frmValObj['AmexAmount'];
      }
      if (key=='DebitAmount') {
        posData.DebitAmount=frmValObj['DebitAmount'];
      }
    }
    posData.OtherChargeAmount=0;
    posData.TotalPOSAmount=sum;
    posDataArray.push(posData);
    this.posDetails=posDataArray;
    
    //console.log(this.posDetails);
    //-----------------------------
   
    this.totalPOS=sum;
    this.posForm.controls['TotalCardHandheldAmount'].setValue(this.totalPOS);
    
    this.calculateOverShort();
  }
  //------------------------------------------------------
  //CardHandheld Form Submit------------------------------
  cardHandheldFormSubmit(){
   // console.log(this.cardHandheldform);
    let ccErrorCount=0;
    let ccAmtErrorCount=0;
    var cardHandheldFieldsArray=[];
    var frmVal=JSON.stringify(this.cardHandheldform.value.cardHandheldFieldsitems);
    var frmValObj=JSON.parse(frmVal);
    //console.log(frmValObj);
    let sum = 0;
    let cardHandheldTotal = 0;
    let i = 0;
    var excludeVariable = ["CardHandheldTidNumber","CloseCardHandheldID","BatchoutNumber","TotalCardHandheldAmount"];
    for (var fkey in frmValObj) {
      if (frmValObj.hasOwnProperty(fkey)) {
        //For total calculation------------
        for (var key in frmValObj[fkey]) {
          if (!excludeVariable.includes(key)) {
            //console.log(key + " -> " + frmValObj[fkey][key]);
            let amount=frmValObj[fkey][key];
            sum = sum + (+amount);
            cardHandheldTotal = cardHandheldTotal + (+amount);
          }
        }
        //----------------------------------
        //For collecting data---------------
        let cardHandheldData =  {} as cardHandheldDetails;
        var cardBerror=[];
        for (var key in frmValObj[fkey]) {
          if (key=='BatchoutNumber') {
             cardHandheldData.BatchoutNumber=frmValObj[fkey]['BatchoutNumber'];
             if (frmValObj[fkey]['BatchoutNumber'].length === 0 && sum > 0) {
                var element = document.getElementById('batch-out'+i);
                element.classList.add("error-input");
                ccErrorCount++;
             }
             else{
                var element = document.getElementById('batch-out'+i);
                element.classList.remove("error-input");
             }
             
             if (frmValObj[fkey]['BatchoutNumber'].length!== 0 && sum <= 0) {
              ccAmtErrorCount++;
             }
           }
            
           if (key=='CloseCardHandheldID') {
            cardHandheldData.CloseCardHandheldID=frmValObj[fkey]['CloseCardHandheldID'];
           }
           if (key=='BatchoutNumber') {
            cardHandheldData.BatchoutNumber=frmValObj[fkey]['BatchoutNumber'];
           }
           if (key=='AmexAmount') {
            cardHandheldData.AmexAmount=frmValObj[fkey]['AmexAmount'];
           }
           if (key=='DebitAmount') {
            cardHandheldData.DebitAmount=frmValObj[fkey]['DebitAmount'];
           }
           if (key=='DiscoverAmount') {
            cardHandheldData.DiscoverAmount=frmValObj[fkey]['DiscoverAmount'];
           }
           if (key=='MasterCardAmount') {
            cardHandheldData.MasterCardAmount=frmValObj[fkey]['MasterCardAmount'];
           }
           if (key=='VisaAmount') {
            cardHandheldData.VisaAmount=frmValObj[fkey]['VisaAmount'];
           }
           if (key=='TotalCardHandheldAmount') {
            cardHandheldData.TotalCardHandheldAmount=sum;
           }
        }
        //----------------------------------
        cardHandheldFieldsArray.push(cardHandheldData);
        
        document.getElementById('card-total'+i).innerHTML = JSON.stringify(sum);
        sum = 0;
       // console.log("CardH Total:"+sum);
      }
      i++;
    }
    
    if (ccErrorCount > 0) {
     this.errorArray[3]='Please enter all CC-Handheld Batch#';
     this.ccHandheldAmountError=true;
    }
    else{
       this.errorArray.splice(3, 1); 
       this.ccHandheldAmountError=false;
    }
    //console.log('ccAmtErrorCount:'+ccAmtErrorCount);
    if (ccAmtErrorCount > 0) {
      this.errorArray[4]='Please enter cc handheld details';
      this.ccHandheldBatchoutError=true;
    }
    else{
       this.errorArray.splice(4, 1); 
       this.ccHandheldBatchoutError=false;
    }
    
    this.cardHandheldFieldsDataArray=cardHandheldFieldsArray;
    this.cardHandheldTotal=cardHandheldTotal;
    this.calculateOverShort();
  }
  //------------------------------------------------------
  //RegisterSummary on change submit----------------------
  registerSummaryChange(){
   this.registerSummaryFormSubmit();
  }
  //------------------------------------------------------
   //Integer checking-------------------------------------
  //isInt(n) {
  // return n % 1 === 0;
  //}
  //------------------------------------------------------
   //Function for checking selected Cashier---------------
      comparer(o1: PickupEmployee, o2: PickupEmployee): boolean {
        // if possible compare by object's name, and not by reference.
        return o1 && o2 ? o1.FK_Employee_ID === o2.FK_Employee_ID : o2 === o2;
      }
    //----------------------------------------------------
  
  //Check for added Cashier-------------------------------
    checkDisabledCashier(FK_Employee_ID){
      if (this.cashierArr && this.cashierArr.includes(FK_Employee_ID)) {
       return true;
      }
      else{
       return false; 
      }
    }
    //-----------------------------------------------------
  calculateOverShort(){
    var totalDrawerC=0;
    var cardHandheldTotal= +this.cardHandheldTotal;
    var totalPOS = +this.totalPOS;
    var denominationsTotalAmount= +this.denominationsTotalAmount;
    
    totalDrawerC=(cardHandheldTotal + totalPOS + denominationsTotalAmount +this.totalPickup +this.otherChargesTotal);
     
    this.salesSummaryForm.controls['totalDrawer'].setValue(totalDrawerC.toFixed(2));
     
    var totalDrawer=this.salesSummaryForm.value.totalDrawer;
    var registerReading=this.registerSummaryForm.value.registerReading;
    var overshort=(parseFloat(totalDrawer) - parseFloat(registerReading));
   
    this.salesSummaryForm.controls['overShort'].setValue(overshort.toFixed(2));
  }
  //-------------------------------------------------------
  //Other charges form submit------------------------------
  otherChargesFormSubmit(){
  //  console.log(this.otherChargesForm.value);
  //Collecting payments form data---------
     var frmVal=JSON.stringify(this.otherChargesForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var frmVal2=JSON.stringify(this.otherChargeFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var otherChargesFormSubmitDetailsArr=[];
    var otherChargesTotal=0;
    for (var key2 in frmValObj2) {
      let otherChargesFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      //console.log("keyName: "+keyName);
      otherChargesFormSubmitDetails.PaymentFormId=keyName;
      otherChargesFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      otherChargesTotal=otherChargesTotal+frmValObj[JSON.stringify(keyName)]
      
      otherChargesFormSubmitDetailsArr.push(otherChargesFormSubmitDetails);
    }
    this.otherChargesTotal=+otherChargesTotal;
    this.OtherChargesFormsArray=otherChargesFormSubmitDetailsArr;
    this.calculateOverShort();
    //console.log(otherChargesFormSubmitDetailsArr);
    //--------------------------------------
  }
  //-------------------------------------------------------
  //Submit all forms---------------------------------------
  submitAllForms(){
    this.registerSummaryFormSubmit();
    this.salesSummaryFormSubmit();
    this.denominationsFormSubmit();
    this.cardHandheldFormSubmit();
    this.posFormSubmit();
    this.otherChargesFormSubmit();
    
    var Uniqerror = this.errorArray.filter((v, i, a) => a.indexOf(v) === i);
    let error=JSON.stringify(Uniqerror);
    
    if (this.registerSummaryForm.valid && this.posForm.valid && !this.noError && !this.posBatchoutNumberError && !this.overShortCommentError && !this.ccHandheldAmountError && !this.ccHandheldBatchoutError && !this.adjustmentCommentError && !this.ccAdjustmentCommentError && !this.registerStartingGTError) {
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    
    var cashierDetails='';
    if (this.cashierDataArray.length > 0) {
      cashierDetails=JSON.stringify(this.cashierDataArray);
    }
    //Close bank data--------------------
    const closeBankDetails = {
      'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId' : branchId,
      'createBy' : this.authUserId,
      'authUserId' : this.authUserId,
      'bankCloseCashId' : this.registerSummaryForm.value.BankCloseCashId,
      'startingGT' : this.registerSummaryForm.value.registerStartingGT,
      'endingGT' : this.registerSummaryForm.value.registerEndingGT,
      'registerTranx' : this.registerSummaryForm.value.registerTranx,
      'registerNoSales' : this.registerSummaryForm.value.registerNoSales,
      'sales' : this.registerSummaryForm.value.registerReading,
      'registerZNumber' : this.registerSummaryForm.value.registerZNumber,
      'totalDrawer' : this.salesSummaryForm.value.totalDrawer,
      'adjustment' : this.salesSummaryForm.value.adjustments,
      'cCAdjustment' : this.salesSummaryForm.value.ccAdjustments,
      'adjustmentComment' : this.salesSummaryForm.value.adjustmentComments,
      'ccAdjustmentComment' : this.salesSummaryForm.value.ccAdjustmentComments,
      'overShort' : this.salesSummaryForm.value.overShort,
      'overShortComment' : this.salesSummaryForm.value.overShortComments,
      'cashierDetails' : cashierDetails,
      'cardHandheldAmount' : JSON.stringify(this.cardHandheldFieldsDataArray),
      'denominationDetail' : JSON.stringify(this.denominationDetailArray),
      'paymentFormDetails' : JSON.stringify(this.BankPaymentFormsArray),
      'otherChargeDetails' : JSON.stringify(this.OtherChargesFormsArray),
      'isChange' : this.isChange,
      'posDetails' : JSON.stringify(this.posDetails)
     }
    //-----------------------------------
   // console.log(closeBankDetails);
    //API call---------------------------
    this.BankServiceData.submitCloseBank(closeBankDetails).subscribe(res=>{
     this.progressBarHide();
      console.log(res);
      if(res.status=='success') {
        this.showErrorMsg=false;
        sessionStorage.setItem("dialogOpend",'false');
        this.successmessage=res.message;
        setTimeout(() => {
          this.dialogRef.close();
         }, 2000);
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
      }
    });
    //-----------------------------------
    }
    else{
      this.showErrorMsg=true;
      this.errormessage=error;
    }
  }
  //--------------------------------------------------------
  //Is any changes made-------------------------------------------
  isAmountChange(){
    this.isChange=true;
  }
  //--------------------------------------------------------------
   //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------
}
//----------------------------------------------------------

