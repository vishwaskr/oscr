import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueBankBulkComponent } from './issue-bank-bulk.component';

describe('IssueBankBulkComponent', () => {
  let component: IssueBankBulkComponent;
  let fixture: ComponentFixture<IssueBankBulkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueBankBulkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueBankBulkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
