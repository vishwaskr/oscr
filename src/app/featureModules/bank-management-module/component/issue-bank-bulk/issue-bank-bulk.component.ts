import { Component, OnInit } from '@angular/core';
import { dateFormat } from "../../../../constants.js";
import { BankManagementService } from 'src/app/featureModules/bank-management-module/service/bank-management.service';
import {FormGroup, FormControl, FormArray, FormBuilder, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { Router } from '@angular/router';
import * as jq from 'jquery';
import * as moment from 'moment';

export interface EmployeeData {
  FK_Employee_ID:number;
  name: string;
}

export interface EmployeeIds {
  FK_Employee_ID:number;
}

export interface openSiteListData {
  Date: string;
  SiteId: number;
  SiteName: string;
  RegisterId: number
  RegisterNumber: number;
}

interface bankIssueDetails {
    FK_Site_ID: number;
    FK_Register_ID: number;
    Sign_Out_Amount:number;
    FK_Sign_out_Employee_ID:number;
    CashierDetails:any[];
}


@Component({
  selector: 'app-issue-bank-bulk',
  templateUrl: './issue-bank-bulk.component.html',
  styleUrls: ['./issue-bank-bulk.component.css']
})
export class IssueBankBulkComponent implements OnInit {
  
   userBranchArr:any;
   currentBranchName:string;
   sessionDate:string=JSON.parse(sessionStorage.getItem("registerDate"));
   date:any;
   time:any;
   currentDate = moment(new Date()).format(dateFormat);
   
  bankStatus:number=5; //5:open bank,6:closed bank
  
  noBankFound: boolean=true;
  selection: any;
  employees:any;
  employeeIds: Array<any> = [];
  cashierIds: Array<any> = [];
  registerIds:Array<any> = [];
  authUserId:any;
  signOutIpAddress: any;
  dataSource:any;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
 //-----------------------
 
  constructor(public getEmployeeService: BankManagementService, public getOpenSiteListService: BankManagementService, public issueBankService: BankManagementService, public router: Router) { }
  
  displayedColumns: string[] = ['date', 'site', 'register', 'employee', 'cashier', 'open'];
  
 
  ngOnInit() {
   this.side_slide();
    //For default selected branch------------------
    this.currentBranchName=localStorage.getItem("currentBranchName")
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getEmployeeList();
    if (this.sessionDate!==null) {
    //this.date = new FormControl(new Date(this.sessionDate));
     this.date=moment(new Date(this.sessionDate)).format(dateFormat);
    }
    else{
    //this.date = new FormControl(new Date());
    this.date=moment(new Date()).format(dateFormat);
    }
    
    this.time=moment().utc().format("hh:mm:ss");
    this.signOutIpAddress=sessionStorage.getItem('ipAddress');
   // console.log("Current Time: "+ sessionStorage.getItem('ipAddress'));
    this.dataSource = new MatTableDataSource<openSiteListData>([]);
    this.selection = new SelectionModel<openSiteListData>(true, []);
    
    this.getOpenSiteList();
   // console.log(this.currentBranchName);
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   console.log('IDD:'+idd);
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
  
  
    /** The label for the checkbox on the passed row */
  checkboxLabel(row?: openSiteListData): string {
    
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
   // console.log(this.selection.isSelected(row));
    //For getting selected site id------------------------
    if(this.selection.isSelected(row)==true) {
      //this.selectedSiteId.push(row.SiteID);
    // console.log(row.SiteId+": Checked");
     this.getRegisterAndSiteIds(true,row.RegisterId,row.SiteId);
    }
    else{
     //console.log(row.RegisterId+": UnChecked");
     this.getRegisterAndSiteIds(false,row.RegisterId,row.SiteId);
      var idd=row.RegisterId+'|'+row.SiteId;
        for( var i = 0; i < this.registerIds.length; i++){ 
          if ( this.registerIds[i] === idd) {
            this.registerIds.splice(i, 1); 
          }
        }
    }
    //-----------------------------------------------------
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.RegisterId + 1}`;
  }
  
  //Get opened site list for Issue Bank-----------------------
  getOpenSiteList(){
   this.progressBarShow();
   var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const openSiteDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'date': this.date
     }
     var siteListArray= [];
     var siteRegisterIds= [];
     //call api------------
      this.getOpenSiteListService.getOpenSiteList(openSiteDetails).subscribe(res=>{
     // console.log(res);
      this.progressBarHide();
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let siteData =  {} as openSiteListData;
          siteData.Date = moment(value.Date).format(dateFormat);
          siteData.RegisterId = value.RegisterId;
          siteData.RegisterNumber = value.RegisterNumber;
          siteData.SiteId = value.SiteId;
          siteData.SiteName = value.SiteName;
          
          siteRegisterIds.push(value.RegisterId+'|'+value.SiteId);
          
          siteListArray.push(siteData);
        });
      
        var SITE_DATA: openSiteListData[] = siteListArray;
        this.dataSource = new MatTableDataSource<openSiteListData>(SITE_DATA);
        this.selection = new SelectionModel<openSiteListData>(true, SITE_DATA);
  
        for(let i = 0; i < siteRegisterIds.length; i++){
          this.registerIds.push(siteRegisterIds[i]);
        }
      }
      else{
      this.showErrorMsg=true;
      this.errormessage='No Site And Register Found';
      }
      
    })
    //--------------------
  }
  //----------------------------------------------------------
  
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as EmployeeData;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      }
      
    })
    //--------------------
  }
  //------------------------------------------------------
  //Get RegisterId and SiteId-------------------------------------
  //getRegisterAndSiteIdsBK(event,registerId,site_id){
  //  var idd=registerId+'|'+site_id;
  //  if (event.source._checked==true){
  //  this.registerIds.push(idd);
  //  }
  //  else{
  //    for( var i = 0; i < this.registerIds.length; i++){ 
  //        if ( this.registerIds[i] === idd) {
  //          this.registerIds.splice(i, 1); 
  //        }
  //      }
  //  }
  //  //console.log(this.registerIds);
  //}
  
  
  getRegisterAndSiteIds(event,registerId,site_id){
    
    var idd=registerId+'|'+site_id;
    if (event==true){
    this.registerIds.push(idd);
    }
    else{
      for( var i = 0; i < this.registerIds.length; i++){ 
          if ( this.registerIds[i] === idd) {
            this.registerIds.splice(i, 1); 
          }
        }
    }
   // console.log('registerIds:'+this.registerIds);
  }
  //------------------------------------------------------
  //Get Employee id-------------------------------------
  getEmployeeId(event,registerId){
    this.employeeIds[registerId]=event.value; 
  }
  //----------------------------------------------------
  //Get Cashier id--------------------------------------
  getCashierIds(event,registerId) {
    var cashierArray= [];
    event.source.value.forEach(function (value) {
      let cashierData =  {} as EmployeeIds;
          cashierData.FK_Employee_ID=value
          cashierArray.push(cashierData);
      });
    this.cashierIds[registerId]=cashierArray;
  }
  //-----------------------------------------------------
  //Issue Bank Submit------------------------------------
  issueBankSubmit(){
    
   // console.log('Sub-registerIds:'+this.registerIds);
    
    var uniqueRegisterIds = this.registerIds.filter((v, i, a) => a.indexOf(v) === i);
   // console.log('uniqueRegisterIds:'+uniqueRegisterIds.length);
    if (uniqueRegisterIds.length == 0 && this.employeeIds.length == 0) {
      this.showErrorMsg=true;
      this.errormessage="Please select employee and check atleast one check box";
      setTimeout(() => {
         this.showErrorMsg=false;
      }, 2000);
    }
    else if (uniqueRegisterIds.length == 0) {
      this.showErrorMsg=true;
      this.errormessage="Please check atleast one checkbox";
      setTimeout(() => {
         this.showErrorMsg=false;
      }, 2000);
    }
    else if (this.employeeIds.length == 0) {
      this.showErrorMsg=true;
      this.errormessage="Please select employee";
      setTimeout(() => {
         this.showErrorMsg=false;
      }, 2000);
    }
    else {
      this.showErrorMsg=false;
    }
    
    let isAllEmployeeSelected=0;
    
    if (uniqueRegisterIds.length > 0 ) {
      
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      var issueBankArray= [];
      var employees=this.employeeIds;
      var cashiers=this.cashierIds;
      uniqueRegisterIds.forEach(function (value) {
      var res = value.split("|");
      //res[0]--registerId
      //res[1]--Siteid
      if (!employees[res[0]]) {
        isAllEmployeeSelected++;
        var element = document.getElementById(res[0]);
        element.classList.add("employeeError");
      }
      else{
        var element = document.getElementById(res[0]);
        element.classList.remove("employeeError");
      }
      
      
       let issueData =  {} as bankIssueDetails;
       issueData.FK_Register_ID = res[0];
       issueData.FK_Site_ID = res[1];
       issueData.Sign_Out_Amount = 0;
       issueData.FK_Sign_out_Employee_ID = employees[res[0]];
       
       if (cashiers[res[0]]!='') {
        issueData.CashierDetails = cashiers[res[0]];
       }
       else{
        issueData.CashierDetails = employees[res[0]];
       }
       
       issueBankArray.push(issueData);
       });
      //console.log(JSON.stringify(issueBankArray));
      //console.log(emp[1][0].FK_Employee_ID);
      
     // console.log(isAllEmployeeSelected);
      
     if (isAllEmployeeSelected==0) {
      this.progressBarShow();
       const issueBankDetails = {
         'bankIssueDetails' : JSON.stringify(issueBankArray),
         'branchId' : branchId,
         'authUserId' : this.authUserId,
         'createBy' : this.authUserId,
         'signOutAccessTypeId' : 1,
         'issueDate' : this.date,
         'issueTime' : this.time,
         'signOutIpAddress' : this.signOutIpAddress,
         'signOutIMEI' : '',
         'signOutAppVersion' : '',
         'userImage' : ''
        }
        
      //  console.log(issueBankDetails);
         //call api------------------------------
         
         this.issueBankService.issueBank(issueBankDetails).subscribe(res=>{
         
         this.progressBarHide();
           if(res.status=='success') {
             this.showErrorMsg=false;
             this.successmessage= 'Success! Bank Issued.';
            
             setTimeout(() => {
              this.router.navigate(['/bank-mangement/bank']);
             }, 2000);
           }
           else{
             this.showErrorMsg=true;
             this.errormessage= res.message;
           }
         });
         
         //-----------------------------
        }
        else{
          this.showErrorMsg=true;
          this.errormessage="Please select all employee";
          setTimeout(() => {
             this.showErrorMsg=false;
          }, 2000);
        }   
     }
     else{
      this.showErrorMsg=true;
      this.errormessage="Please check atleast one checkbox";
      setTimeout(() => {
         this.showErrorMsg=false;
      }, 2000);
     }
     
  }
  //-----------------------------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }
  //--------------------------------
  
}
