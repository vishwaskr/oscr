import { Component, OnInit, Input ,Inject, ViewChild} from '@angular/core';
import { dateFormat , CheckIsInt} from "../../../../constants.js";
import { BankManagementService } from 'src/app/featureModules/bank-management-module/service/bank-management.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FormGroup, FormControl, FormBuilder, FormGroupDirective, FormArray, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as jq from 'jquery';
//import * as moment from 'moment';
import * as moment from 'moment-timezone';

export interface IssueBankDataList {
  bankId:number;
  date: string;
  siteName: string;
  SiteId:number;
  register: number;
  registerId: number;
  employee: string;
  EmployeeImage:string;
  CashierName:string;
  lastPickup : string;
  pickups : number;
  pickupTotal : number;
  newPickup : string;
}

export interface NewPickUpData {
  BankId : number;
  branchId : number;
  branchName : string;
  siteName : string;
  siteId : number;
  registerNumber : number;
  registerId : number;
  pickupCount : number;
}

export interface bankEditData {
  BankId : number;
  branchId : number;
  branchName : string;
  siteName : string;
  siteId : number;
  registerNumber : number;
  registerId : number;
  pickupCount : number;
}

//New pickup-----------------------------------------------------
export interface PickupEmployee {
  FK_Employee_ID:number;
  name: string;
}

@Component({
  selector: 'app-bank-management',
  templateUrl: './bank-management.component.html',
  styleUrls: ['./bank-management.component.css']
})

export class BankManagementComponent implements OnInit {

  todaydate:Date = new Date();
  bankManagementFilterForm: FormGroup;
  name: string;
  filterBranchId:number;
  userBranchArr:any;
  
  dataSource:any;
  authUserId:any;
  sessionDate:string=JSON.parse(sessionStorage.getItem("registerDate"));
  date:any;
  bankStatus:number=5; //5:open bank,6:closed bank
  bankStatusMessage:string="No Banks Issued for ";
  
  currentDate = moment(new Date()).format(dateFormat);
  
  //serializedDate = new FormControl((new Date()).toISOString());
  noBankFound: boolean=true;
  
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
  //-----------------------
 //New Pickup-------------
  bankId: number;
  siteName:string;
  siteId:number;
  registerNumber:number;
  registerId:number;
  pickupCount:number;
 //-----------------------
  
  
  constructor(public getIssuedBankListService: BankManagementService, public getNewPickUpDataService: BankManagementService, public dialog: MatDialog, public router: Router) { }
  
   //Table Display Columns-------------
  displayedColumns: string[] = ['date', 'site', 'register', 'employee', 'cashier', 'lastPickup', 'pickups', 'pickupTotal', 'editBank', 'newPickup', 'summary', 'close'];
  //----------------------------------
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  //Open new pickup dialog--------------------------------
  openNewPickupDialog(bankId,siteName,siteId,registerNumber,registerId,pickupCount): void {
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var branchName=localStorage.getItem("currentBranchName");
    this.bankId=bankId;
    this.siteName=siteName;
    this.siteId=siteId;
    this.registerNumber=registerNumber;
    this.registerId=registerId;
    this.pickupCount=pickupCount;
    
    var currentDate=moment(new Date()).format(dateFormat);
    var pickupDate=moment(new Date()).format(dateFormat);
    
    var currentTime=moment().format("hh:mm a");
    
    sessionStorage.setItem("bankId",JSON.stringify(bankId));
    sessionStorage.setItem("siteId",JSON.stringify(siteId));
    sessionStorage.setItem("registerId",JSON.stringify(registerId));
    sessionStorage.setItem("bankIssueDate",JSON.stringify(this.currentDate));
    sessionStorage.setItem("dialogOpend",'true');
    
    //console.log("currentDate:"+this.currentDate);
    
    const dialogRef = this.dialog.open(NewPickup, {
      disableClose: true,
      width: '',
      data: {branchId:branchId,branchName:branchName,bankId:this.bankId,siteName:this.siteName,siteId:this.siteId,registerNumber:this.registerNumber,registerId:this.registerId,pickupCount:this.pickupCount,date:this.currentDate,pickupDate:pickupDate,time:currentTime}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (sessionStorage.getItem("dialogOpend")=='false') {
        this.getIssuedBank();
      }
     
    });
    
  }
  //------------------------------------------------------
  //Special Bank------------------------------------------
    openSpecialBank(): void {
      sessionStorage.setItem("bankIssueDate",JSON.stringify(this.currentDate));
      const dialogRef = this.dialog.open(SpecialBank, {
        width: '',
        data: {}
      });
    
      dialogRef.afterClosed().subscribe(result => {
        //console.log('The dialog was closed');
      });
    }
  //------------------------------------------------------
  
  //Bank Edit popup---------------------------------------
  openBankEditDialog(bankIssueDate,bankId,siteName,siteId,registerNumber,registerId,employeeName): void {
    
    sessionStorage.setItem("bankId",JSON.stringify(bankId));
    sessionStorage.setItem("siteId",JSON.stringify(siteId));
    sessionStorage.setItem("registerId",JSON.stringify(registerId));
    sessionStorage.setItem("registerNumber",JSON.stringify(registerNumber));
    sessionStorage.setItem("bankIssueDate",JSON.stringify(bankIssueDate));
    
    sessionStorage.setItem("dialogOpend",'true');
    
    const dialogRef = this.dialog.open(BankEdit, {
      width: '',
      data: {bankIssueDate:bankIssueDate,siteName:siteName,registerNumber:registerNumber,employeeName:employeeName}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (sessionStorage.getItem("dialogOpend")=='false') {
        this.getIssuedBank();
      }
    });
    
  }
  //------------------------------------------------------
  //Bank Close Popup--------------------------------------
  openBankCloseDialog(bankIssueDate,bankId,siteName,siteId,registerNumber,registerId,employeeName): void {
    
    sessionStorage.setItem("bankId",JSON.stringify(bankId));
    sessionStorage.setItem("siteId",JSON.stringify(siteId));
    sessionStorage.setItem("registerId",JSON.stringify(registerId));
    sessionStorage.setItem("bankIssueDate",JSON.stringify(bankIssueDate));
    
    const dialogRef = this.dialog.open(BankClose, {
      disableClose: true,
      width: '',
      data: {bankIssueDate:bankIssueDate,siteName:siteName,registerNumber:registerNumber,employeeName:employeeName}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (sessionStorage.getItem("dialogOpend")=='false') {
        this.getIssuedBank();
      }
    });
  }
  //------------------------------------------------------
  
  ngOnInit() {
    this.side_slide();
    this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
    //For default selected branch------------------
    this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    if (branchId!='') {
      this.filterBranchId=branchId;
    }
    
    if (this.sessionDate!==null) {
    this.date = new FormControl(new Date(this.sessionDate));
    this.currentDate=moment(this.date.value).format(dateFormat);
    }
    else{
    this.date = new FormControl(new Date());
    }
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
     this.bankManagementFilterForm = new FormGroup({
      filterBranchId: new FormControl(this.filterBranchId)
    })
     
    this.getIssuedBank();
    
  }
  
  //Sidebar nav-----------------------------------------------
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   console.log('IDD:'+idd);
   if (idd==1) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
   }
  }
  //----------------------------------------------------------
  //Change date-----------------------------------------------
  onChangeDate(type: string, event: MatDatepickerInputEvent<Date>) {
   const momentDate = new Date(event.value);
   const formattedDate = moment(momentDate).format(dateFormat);
   this.currentDate=formattedDate;
   this.getIssuedBank();
   sessionStorage.setItem("registerDate",JSON.stringify(momentDate));
   //sessionStorage.setItem("bankIssueDate",JSON.stringify(momentDate));
  }
  //----------------------------------------------------------
  //On change Register status---------------------------------
  onChangeRegister(){
   this.getIssuedBank();
   if (this.bankStatus==6) {
    this.bankStatusMessage="No Closed Bank for ";
  }
  else{
    this.bankStatusMessage="No Banks Issued for ";
  }
   //console.log('RegisterStatus: '+ this.bankStatus);
  }
  //----------------------------------------------------------
  
  //Get issued bank-------------------------------------------
  getIssuedBank(){
    this.progressBarShow();
    var defaultUserImage = 'assets/img/profile.jpg';
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
   //console.log('issuedBank');
    const bankDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId,
      'date': this.currentDate,
      'bankType': this.bankStatus
     }
     
    // console.log(bankDetails);
     var bankDataArray= [];
     this.getIssuedBankListService.getIssuedBankList(bankDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if (res.status=='NoBankFound') {
        this.noBankFound=true;
      }
      else{
        var tz = moment.tz.guess();
        this.noBankFound=false;
        
        res.data.forEach(function (value) {
        let bankData =  {} as IssueBankDataList;
        
        bankData.bankId = value.BankId;
        bankData.date = moment(new Date(value.Date)).format(dateFormat);
        bankData.siteName = value.SiteName;
        bankData.SiteId = value.SiteId;
        bankData.register = value.RegisterNumber;
        bankData.registerId = value.RegisterId;
        bankData.employee = value.EmployeeName;
        bankData.CashierName = value.CashierName;
        bankData.EmployeeImage = value.EmployeeImage;
        if (value.EmployeeImage=='') {
          bankData.EmployeeImage = defaultUserImage;
        }
        
        //console.log('LastPickupTime:'+value.LastPickupTime);
        
        var LastPickupTime='';
        if (value.LastPickupTime!='') {
          var gmtDateTime = moment.utc(value.LastPickupTime)
          LastPickupTime = moment.tz(gmtDateTime, tz).format("hh:mm a");
        }
       
        bankData.lastPickup = LastPickupTime;
        bankData.pickups = value.PickupCount;
        bankData.pickupTotal = value.PickupTotal;
        
        bankDataArray.push(bankData);
      
        });
        
        var BANK_DATA: IssueBankDataList[] = bankDataArray;
        this.dataSource = new MatTableDataSource<IssueBankDataList>(BANK_DATA);
        this.dataSource.paginator = this.paginator;
        
      }
     
     });
  }
  //----------------------------------------------------------
  
  //Get summary details when branch change--------
  changeBranch(BranchId,Branch_Name){
    //console.log(BranchId+":"+Branch_Name);
    this.filterBranchId=BranchId;
    localStorage.setItem("currentBranchId",JSON.stringify(this.filterBranchId));
    localStorage.setItem('currentBranchName', Branch_Name);
    //this.currentBranchName=Branch_Name;
    this.getIssuedBank();
  }
  //----------------------------------------------
  //Go to bank summary----------------------------
  goToBankSummary(bankId,bankDate){
   // console.log(bankId+"--"+bankDate);
    sessionStorage.setItem("bankId",JSON.stringify(bankId));
    sessionStorage.setItem("bankDate",bankDate);
    this.router.navigate(['/bank-mangement/bank-summary']); 
  }
  //----------------------------------------------
  
  //--------------------------------
  progressBarShow(){
   this.progressShow=true;
   this.mode="indeterminate"; 
  }
  progressBarHide(){
    this.mode="determinate";
    this.progressShow=false;
  }

  onKeydown(event: any) {
    console.log(event);
    let pincode=event.target.value;
    if(pincode.length > 9 || event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 97 && event.keyCode <= 122 ) {
      return false;
    }
   
  }
  //--------------------------------
}

//--------------------------------------------------------------------------------------
//Start New pickup----------------------------------------------------------------------
export interface DenominationDetail {
  HundredsCount : number;
  FiftiesCount : number;
  TwentiesCount : number;
  TensCount : number;
  FivesCount : number;
  TwosCount : number;
  OnesCount : number;
  QuartersCount : number;
  NickelsCount : number;
  DimesCount : number;
  PenniesCount : number;
  ChecksAmount : number;
  GiftCertificates : number;
  ChangeAmount : number;
}

export interface paymentFormfields {
  type : string;
  name : string;
  label : string;
  value : number;
}

@Component({
  selector: 'new-pickup',
  templateUrl: './new-pickup.html',
  styleUrls: ['./bank-management.component.css']
})

export class NewPickup implements OnInit {
  
    //Progres----------------
     progressShow: boolean;
     mode = 'determinate';
     value = 50;
     bufferValue = 95;
     //-----------------------
     showErrorMsg: boolean = false;
     errormessage:string;
     successmessage:string;
  
    //pickupForm: FormGroup;
    denominationsForm: FormGroup;
    employees: any;
    authUserId: any;
    amountBills: number=1;
    bankIssueDate:any;
    BankPaymentFormsArray:any;
    public paymentFormfields: any[];
    
    //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
    //------------------------------------
     //denominations Variables---
    denominationDetailArray: any;
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    
    
    change: number=0;
    checks: number;
    total: number=0;
    denominationsTotalAmount: number=0;
    
    
  constructor(public dialogRef: MatDialogRef<NewPickup>,@Inject(MAT_DIALOG_DATA) public data: NewPickUpData, public getEmployeeService: BankManagementService,public submitNewpickupService: BankManagementService,public getPaymentFormList: BankManagementService) {}
   
  onNoClick(): void {
    this.dialogRef.close();
  }
  
   ngOnInit() {
    
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    this.getEmployeeList();
    this.getPaymentForms();
        //denominationsForm----------------------------
     this.denominationsForm = new FormGroup({
      pickUpEmployee: new FormControl('',[Validators.required]),
      hundreds: new FormControl(0),
      fifties: new FormControl(0),
      twenties: new FormControl(0),
      tens: new FormControl(0),
      fives: new FormControl(0),
      twos: new FormControl(0),
      ones: new FormControl(0),
      quarters: new FormControl(0),
      dimes: new FormControl(0),
      nickles: new FormControl(0),
      pennies: new FormControl(0),
      
      hundredsCount: new FormControl(0),
      fiftiesCount: new FormControl(0),
      twentiesCount: new FormControl(0),
      tensCount: new FormControl(0),
      fivesCount: new FormControl(0),
      twosCount: new FormControl(0),
      onesCount: new FormControl(0),
      quartersCount: new FormControl(0),
      dimesCount: new FormControl(0),
      nicklesCount: new FormControl(0),
      penniesCount: new FormControl(0),
      
      change: new FormControl(0),
      //checks: new FormControl(''),
      paymentFormfields: new FormControl(JSON.stringify(this.paymentFormfields))
    })
  }
  //------------------------------------------------------------
  
  //Get payment forms-------------------------------------------
  getPaymentForms(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var BankPaymentFormsArray= [];
    const formDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
    }
    //Call api---------------------
     this.getPaymentFormList.getPaymentFormList(formDetails).subscribe(res=>{
      if(res.status=='success') {
      res.data.forEach(function (value) {
       let bankFormsData =  {} as paymentFormfields;
          bankFormsData.type='text';
          bankFormsData.label=value.PaymentFormName;
          bankFormsData.name=value.PaymentFormId;
          bankFormsData.value=value.Amount;
          
          BankPaymentFormsArray.push(bankFormsData);
        });
        this.paymentFormfields=BankPaymentFormsArray;
        
         for (let i = 0; i < BankPaymentFormsArray.length; i++) {
           this.denominationsForm.addControl(BankPaymentFormsArray[i].name, new FormControl(BankPaymentFormsArray[i].Amount));
          }
      }
      else{
       //console.log('Error');
      }
    })
    //-----------------------------
  }
  //------------------------------------------------------------
    //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=(hundredsCount * 100);
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
     this.hundredsError=true;
     this.noError=true;
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
    if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=(fiftiesCount * 50);
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
    if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=(twentiesCount * 20);
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=(tensCount * 10);
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=(fivesCount * 5);
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=(twosCount * 2);
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=(onesCount * 1);
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=(quartersCount * 0.25);
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else if(dimesCount > 0 && CheckIsInt(dimesCount)) {
     this.noError=false;
     this.dimesBillError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }    
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }  
    else if(nicklesCount > 0 && CheckIsInt(nicklesCount)) {
     this.noError=false;
     this.nicklesBillError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else if(penniesCount > 0 && CheckIsInt(penniesCount)) {
     this.noError=false;
     this.penniesBillError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }  
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //Submit-------------------------------------------------------
  denominationsFormSubmit(){
    var frmVal=JSON.stringify(this.denominationsForm.value);
    var frmValObj=JSON.parse(frmVal);
    var denominationDetailArray = [];
    //console.log(frmValObj);
    let sum = 0;
    var countVariable = ["pickUpEmployee","dimesCount","fiftiesCount","fivesCount","nicklesCount","onesCount","penniesCount","quartersCount","tensCount","twentiesCount","twosCount","hundredsCount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!countVariable.includes(key)) {
          //console.log(key + " -> " + frmValObj[key]);
          let amount=frmValObj[key];
          sum = sum + (+amount);
        }
          
      }
    }
    //Collection data---------------
    let denominationData =  {} as DenominationDetail;
      denominationData.HundredsCount = frmValObj['hundredsCount'];
      denominationData.FiftiesCount = frmValObj['fiftiesCount'];
      denominationData.TwentiesCount = frmValObj['twentiesCount'];
      denominationData.TensCount = frmValObj['tensCount'];
      denominationData.FivesCount = frmValObj['fivesCount'];
      denominationData.TwosCount = frmValObj['twosCount'];
      denominationData.OnesCount = frmValObj['onesCount'];
      denominationData.QuartersCount = frmValObj['quartersCount'];
      denominationData.NickelsCount = frmValObj['nicklesCount'];
      denominationData.DimesCount = frmValObj['dimesCount'];
      denominationData.PenniesCount = frmValObj['penniesCount'];
      denominationData.ChangeAmount = frmValObj['change'];

      this.denominationDetailArray=denominationData;
      //console.log(denominationData);
    //------------------------------
    //Collecting payments form data---------
    var frmVal2=JSON.stringify(this.paymentFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var paymentFormSubmitDetailsArr=[];
   
    for (var key2 in frmValObj2) {
      let paymentFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      paymentFormSubmitDetails.PaymentFormId=keyName;
      paymentFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      if (frmValObj[JSON.stringify(keyName)] > 0) {
       paymentFormSubmitDetailsArr.push(paymentFormSubmitDetails);
      }
      
    }
    this.BankPaymentFormsArray=paymentFormSubmitDetailsArr;
    this.denominationsTotalAmount=+sum.toFixed(2);
    this.total=+sum.toFixed(2);
  }
  //--------------------------------------------------------------
  //Pickup Submit-------------------------------------------------
  pickupFormSubmit(){
  
  if (this.denominationsForm.value.pickUpEmployee=='') {
       this.showErrorMsg=true;
       this.errormessage= 'Please select a Pickup Employee';
  }
  else{
    if (this.denominationsForm.valid && !this.noError) {
      if (this.total < 1 ) {
       this.showErrorMsg=true;
       this.errormessage= 'Please enter currency denominations';
         //setTimeout(() => {
         //this.showErrorMsg=false;
         //}, 2000);
      }
      else {
      this.progressBarShow();
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      const newPickupDetails = {
         'denominationDetail':JSON.stringify(this.denominationDetailArray),
         'paymentFormDetail':JSON.stringify(this.BankPaymentFormsArray),
         'bankIssueDate': JSON.parse(sessionStorage.getItem("bankIssueDate")),
         'pickupBy' : this.denominationsForm.value.pickUpEmployee,
         'createBy' : this.authUserId,
         'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
         'registerId' : JSON.parse(sessionStorage.getItem("registerId")),
         'siteId' : JSON.parse(sessionStorage.getItem("siteId")),
         'branchId' : branchId,
         'verifiedBy' : this.authUserId,
         'accessTypeId' : 1,
         'authUserId' : this.authUserId
        }
       // console.log(newPickupDetails);
        
        //console.log(newPickupDetails);
        this.submitNewpickupService.submitNewPickup(newPickupDetails).subscribe(res=>{
         this.progressBarHide();
         //console.log(res);
         if(res.status=='success') {
             this.showErrorMsg=false;
             this.successmessage= 'Success! Pickup Added.';
             sessionStorage.setItem("dialogOpend",'false');
             setTimeout(() => {
              this.dialogRef.close();
             }, 2000);
           }
           else{
            this.showErrorMsg=true;
            this.errormessage= res.message;
            setTimeout(() => {
            this.showErrorMsg=false;
            }, 2000);
           }
        });
      //-------------------------
      }
    }
    else{
      this.progressBarHide();
      this.showErrorMsg=true;
      this.errormessage= 'Some errors occurd...';
    }
  }
  }
  //--------------------------------------------------------------

  //Integer checking----------------------------------------------
  //isInt(n) {
  // return n % 1 === 0;
  //}
  //--------------------------------------------------------------
  //--------------------------------------------------------------
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as PickupEmployee;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      }
      
    })
    //----------------------------------------------------
  }
  //------------------------------------------------------
  //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------
}
//---------------------------------------------------------------
//Edit Bank------------------------------------------------------
export interface Employee {
  UserId: number;
  name: string;
}

export interface CardHandHeldList {
  CardHandheldTidNumber: string;
}

export interface EditBankCashierList {
  UserId: number;
}

export interface EditBankSiteList {
  SiteId: number;
  SiteName: string;
}

export interface EditBankRegisterList {
  RegisterId: number;
  RegisterNumber: number;
}

@Component({
  selector: 'bank-edit',
  templateUrl: './bank-edit.html',
  styleUrls: ['./bank-management.component.css']
})

export class BankEdit implements OnInit {
 
  filterBranchId:number;
  userBranchArr:any;
  
  authUserId:number;
  cardHandHeldList:any;
  bankCardHandHeldList:CardHandHeldList[];
  bankCardHandHeldListArr:string[];
  editBankCashierList:EditBankCashierList[];
  editBankCashierListArr:string[];
  editBankSiteList:any;
  editBankRegisterList:any
  editBankForm: FormGroup;
  employees:any;
  siteID:any;
  registerId:any;
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
 //-----------------------
  submitDisabled:boolean;
  showErrorMsg: boolean = false;
  errormessage:string;
  successmessage:string;
  
  constructor(public dialogRef: MatDialogRef<BankEdit>,@Inject(MAT_DIALOG_DATA) public data: bankEditData,public BankServiceData: BankManagementService) {}
   
    
  onNoClick(): void {
    this.dialogRef.close();
  }
  
   
   ngOnInit() {
      this.authUserId=JSON.parse(localStorage.getItem('UserId'));
      
      this.userBranchArr = JSON.parse(localStorage.getItem("UserBranch"));
      //For default selected branch------------------
      this.filterBranchId=this.userBranchArr[0]['BranchId'];
   
    //---------------------------------------------
      var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      if (branchId!='') {
        this.filterBranchId=branchId;
      }
    
      this.getBranchWiseSiteList(branchId);
      this.getBankWiseCardHandheldList(branchId);
      this.getBranchWiseCardhandheldList(branchId);
      this.getEmployeeList(branchId);
      
      this.siteID=sessionStorage.getItem("siteId");
      this.registerId=sessionStorage.getItem("registerId");
      
      this.editOnChangeSite(this.siteID);
      
      this.editBankForm = new FormGroup({
      cardHandHeldTID: new FormControl(''),
      cashierID: new FormControl(''),
      siteID: new FormControl(+this.siteID,[Validators.required]),
      registerId: new FormControl(+this.registerId,[Validators.required])
      });
      
     //console.log('registerNumber: '+this.registerNumber);
    }
    
     //Get Bank Wise CardHandHeld list-------------------------
    getBankWiseCardHandheldList(branchId){
      var bankId=JSON.parse(sessionStorage.getItem("bankId"));
      //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      var cardHandHeldListArray= [];
      var editBankCashierListArr=[];
      var bankCardHandHeldListArr=[];
      const cardHandHeldDetails = {
          'branchId': branchId,
          'authUserId': this.authUserId,
          'bankId': bankId,
          'bankIssueDate':JSON.parse(sessionStorage.getItem("bankIssueDate"))
      }
      
      //call api------------
          this.BankServiceData.getBankWiseCardHandheldList(cardHandHeldDetails).subscribe(res=>{
          //console.log(res);
          if(res.status=='success') {
            this.bankCardHandHeldList = res.data.CardHandheldTidNumber;
            this.editBankCashierList = res.data.BankCashier;
            
            res.data.BankCashier.forEach(function (value) {
              editBankCashierListArr.push(value.UserId);
            });
            
            res.data.CardHandheldTidNumber.forEach(function (value) {
              bankCardHandHeldListArr.push(value.CardHandheldTidNumber);
            });
            
          }
          else{
            this.bankCardHandHeldList=[];
            this.editBankCashierList=[];
            this.editBankCashierListArr=[];
            this.bankCardHandHeldListArr=[];
           //console.log('Error');
          }
        }) 
        this.editBankCashierListArr=editBankCashierListArr;
        this.bankCardHandHeldListArr=bankCardHandHeldListArr;
        //----------------------------------------------------
    }
    //--------------------------------------------------------
    
    
    //Get Branch Wise CardHandHeld list--------------------------
    getBranchWiseCardhandheldList(branchId){
      //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
        var cardHandHeldListArray= [];
        var bankId=JSON.parse(sessionStorage.getItem("bankId"));
        let bankCardHandHeldListArr=this.bankCardHandHeldList;
        const cardHandHeldDetails = {
          'branchId': branchId,
          'authUserId': this.authUserId,
          'bankId': bankId,
          'bankIssueDate':JSON.parse(sessionStorage.getItem("bankIssueDate"))
         }
         //console.log(cardHandHeldDetails);
         
        //call api------------
          this.BankServiceData.getBranchWiseCardHandheldList(cardHandHeldDetails).subscribe(res=>{
          //console.log(res);
          if(res.status=='success') {
            let i=0;
            res.data.forEach(function (value) {
              let cardHandHeldData =  {} as CardHandHeldList;
              cardHandHeldData.CardHandheldTidNumber = value.CardHandheldTidNumber;
              cardHandHeldListArray.push(cardHandHeldData);
              i++;
            });
           this.cardHandHeldList=cardHandHeldListArray;
          // console.log(this.cardHandHeldList);
          }
          else{
            this.cardHandHeldList=[];
           //console.log('Error');
          }
        })
        //----------------------------------------------------
    }
    //----------------------------------------------------------
    //Get Employee list-----------------------------------------
    getEmployeeList(branchId){
    
    //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.BankServiceData.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as Employee;
          empData.UserId = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
        this.employees=[];
       //console.log('Error');
      } 
    })
    //------------------
    
  }
  //------------------------------------------------------
  //Get branch wise site list-----------------------------
  getBranchWiseSiteList(branchId){
  // var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
   var siteArray= [];
   const siteDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
    }
    //console.log(siteDetails);
    //call api------------
      this.BankServiceData.getBranchWiseSiteList(siteDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let siteData =  {} as EditBankSiteList;
          siteData.SiteId = value.SiteId;
          siteData.SiteName = value.SiteName;
          siteArray.push(siteData);
        });
       this.editBankSiteList=siteArray;
      }
      else{
       this.editBankSiteList=[];
       this.siteID='';
      }
      
    })
    //------------------
  }
  //------------------------------------------------------
  //On change site----------------------------------------
  editOnChangeSite(siteID){
    //console.log(siteID);
  if (this.filterBranchId ==0) {
     this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
  }
  //else{
  //  var branchId=this.filterBranchId;
  //}
  
   var registerArray= [];
   const registerDetails = {
      'branchId': this.filterBranchId,
      'authUserId': this.authUserId,
      'siteId': siteID
    }
    
    //call api------------
      this.BankServiceData.getSiteWiseRegisterList(registerDetails).subscribe(res=>{
      //console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let registerData =  {} as EditBankRegisterList;
          registerData.RegisterId = value.RegisterId;
          registerData.RegisterNumber = value.RegisterNumber;
          registerArray.push(registerData);
        });
       this.editBankRegisterList=registerArray;
      }
      else{
       this.editBankRegisterList=[];
      }
      
    })
    //------------------
  }
  //------------------------------------------------------
  
   //Get summary details when branch change--------
  changeBranch(BranchId,Branch_Name){
    
    this.filterBranchId=BranchId;
    this.getBranchWiseSiteList(BranchId);
    this.employees=[];
    this.getEmployeeList(BranchId);
    this.getBranchWiseCardhandheldList(BranchId);
    this.editBankForm.get('siteID').setValue('');
    this.registerId='';
    this.editBankForm.get('registerId').setValue('');
    this.editBankRegisterList=[];
    this.editBankCashierListArr=[];
    this.editBankForm.get('cashierID').setValue('');
  
  }
  //----------------------------------------------
   
   //Function for checking selected Cashier------------------
      comparer2(o1: EditBankCashierList, o2: EditBankCashierList): boolean {
        // if possible compare by object's name, and not by reference.
        return o1 && o2 ? o1.UserId === o2.UserId : o2 === o2;
      }
    //--------------------------------------------------------
    
    //Function for checking selected card handheld------------
      comparer(o1: CardHandHeldList, o2: CardHandHeldList): boolean {
        // if possible compare by object's name, and not by reference.
        return o1 && o2 ? o1.CardHandheldTidNumber === o2.CardHandheldTidNumber : o2 === o2;
      }
    //--------------------------------------------------------
    
    //Submit Bank Edit Form-----------------------------------
    editBankFormSubmit(){
      
      this.submitDisabled=true;
      var cardHandHeldListArray= [];
      var cashierListArray= [];
      
      //var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
      
    if (this.filterBranchId ==0) {
     this.filterBranchId=JSON.parse(localStorage.getItem("currentBranchId"));
    }
      
     if (this.editBankForm.valid && this.siteID!='' && this.registerId!='') {
      this.progressBarShow();
      //console.log(this.editBankForm);
      this.editBankForm.value.cardHandHeldTID.forEach(function (value) {
        let cardHandHeldData =  {} as CardHandHeldList;
        cardHandHeldData.CardHandheldTidNumber = value.CardHandheldTidNumber;
        cardHandHeldListArray.push(cardHandHeldData);
      });
      
      this.editBankForm.value.cashierID.forEach(function (value) {
       let cashierData =  {} as Employee;
       cashierData.UserId=value.UserId;
       cashierListArray.push(cashierData);
      });
      
      
      const editBankDetails = {
        'cashierDetails':JSON.stringify(cashierListArray),
        'cardHandheldTidIdList':JSON.stringify(cardHandHeldListArray),
        'bankIssueDate': JSON.parse(sessionStorage.getItem("bankIssueDate")),
        'updateBy' : this.authUserId,
        'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
        'registerId' : this.editBankForm.value.registerId,
        'siteId' : this.siteID,
        'branchId' : this.filterBranchId,
        'authUserId' : this.authUserId
       }
       
       //console.log(editBankDetails);
        this.BankServiceData.editBank(editBankDetails).subscribe(res=>{
        this.progressBarHide();
        //this.submitDisabled=false;
       // console.log(res);
        if(res.status=='success') {
            this.showErrorMsg=false;
            this.successmessage= 'Success! Bank Updated.';
            sessionStorage.setItem("dialogOpend",'false');
            setTimeout(() => {
             this.dialogRef.close();
            }, 2000);
            
          }
          else{
           this.showErrorMsg=true;
           this.errormessage= res.message;
          }
          
       });
     }
     else{
      this.submitDisabled=false;
      this.showErrorMsg=true;
      this.errormessage= 'Some error occured.';
     }
     
    }
    //--------------------------------------------------------
    //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------
    
    //Check for added Card Handheld Number---------------------
    checkDisabled(CardHandheldTidNumber){
      if (this.bankCardHandHeldListArr && this.bankCardHandHeldListArr.includes(CardHandheldTidNumber)) {
       return true;
      }
      else
      {
        return false;
      }
    }
    //---------------------------------------------------------
    
    //Check for cashier---------------------------------------
    checkEditBankCashierDisabled(UserId){
      if (this.editBankCashierListArr && this.editBankCashierListArr.includes(UserId)) {
       return true;
      }
      else
      {
        return false;
      }
    }
    //---------------------------------------------------------

}
//---------------------------------------------------------------
//Close Bank-----------------------------------------------------
export interface BankDetails {
  registerStartingGT : number;
  registerEndingGT : number;
  registerZNumber : string;
  registerTranx : number;
  registerNoSales : number;
  registerReading : number;
}

export interface BankPaymentForms {
BranchPaymentFormId : number;
PaymentFormName : string;
Amount : number;
}

export interface paymentFormfields {
  type : string;
  name : string;
  label : string;
  value : number;
}

export interface otherChargeFormfields {
  type : string;
  name : string;
  label : string;
  value : number;
}

export interface cardHandheldDetails {
  AmexAmount : number;
  BatchoutNumber : string;
  CardHandheldTidNumber : string;
  CloseCardHandheldID : number;
  DebitAmount : number;
  DiscoverAmount : number;
  MasterCardAmount : number;
  TotalCardHandheldAmount : number;
  VisaAmount : number;
}

export interface POS {
 BatchoutNumber : string;
 AmexAmount : number;
 DebitAmount : number;
 DiscoverAmount : number;
 MasterCardAmount : number;
 VisaAmount : number;
 TotalCardHandheldAmount : number;
 OtherChargeAmount : number;
 TotalPOSAmount : number;
}

export interface PickupList {
  PickupNumber: number;
  PickupTotal: number;
}

export interface DenominationDetail {
  HundredsCount: number;
  FiftiesCount: number;
  TwentiesCount: number;
  FivesCount : number;
  TwosCount: number;
  OnesCount: number;
  QuartersCount: number;
  NickelsCount: number;
  DimesCount: number;
  PenniesCount: number;
  ChangeAmount: number;
}


export interface PaymentFormSubmitDetails {
  PaymentFormId: number;
  Amount: number;
}

@Component({
  selector: 'bank-close',
  templateUrl: './bank-close.html',
  styleUrls: ['./bank-management.component.css']
})

export class BankClose implements OnInit {
  
    errorArray:Array<any> = [];
    errormessage: string;
    successmessage: string
    showErrorMsg: boolean;
    panelOpenState = false;
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
 //-----------------------
 
  registerSummaryForm: FormGroup;
  salesSummaryForm: FormGroup;
  denominationsForm: FormGroup;
  cardHandheldform: FormGroup;
  posForm: FormGroup;
  otherChargesForm: FormGroup;
  BankCloseCashId: number;
  pickupList: any;
  
  BankPaymentFormsArray:any;
  public paymentFormfields: any[];
  OtherChargesFormsArray:any;
  public otherChargeFormfields: any[];
  
    authUserId:number;
    employees:any;
    
    cardHandheldFieldsitems:any;
    cardHandheldTotal:number=0;
    cardHandheldFieldsDataArray:any;
    otherChargesTotal:number=0;
    
   //Register Variables--------
    registerStartingGT: number;
    registerEndingGT: number;
    registerZNumber: string='';
    registerTranx: number;
    registerNoSales: number;
    registerReading: number;
   //--------------------------
   //Sales Summary Variables---
   //difference: number;
   totalPickup: number=0;
   totalDrawer: number=0;
   adjustments: number=0;
   adjustmentComments: string;
   ccAdjustments: number=0;
   ccAdjustmentComments: string;
   overShort: number=0;
   overShortComments: string;
   cashier: any;
   cashierDataArray: any;
   cashierArr: string[];
   cashierList:PickupEmployee[];
   //--------------------------
   isChange:boolean=false;
   //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
    adjustmentCommentError:boolean=false;
    ccAdjustmentCommentError:boolean=false;
    overShortCommentError:boolean=false;
    ccHandheldBatchoutError:boolean=false;
    ccHandheldAmountError:boolean=false;
    registerStartingGTError:boolean=false;
    registerEndingGTError:boolean=false;
    registerZNumberError:boolean=false;
    registerTranxError:boolean=false;
    registerNoSalesError:boolean=false;
    registerReadingError:boolean=false;
    //------------------------------------
   //denominations Variables---
    denominationDetailArray: any;
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    
    
    change: number=0;
    checks: number;
    total: number=0;
    denominationsTotalAmount: number=0;
   //-----------------------
   //POS form variables--------
   posBatchoutNumberError: boolean;
   posName: string;
   BatchoutNumber: string;
   VisaAmount: number=0;
   MasterCardAmount: number=0;
   DiscoverAmount: number=0;
   AmexAmount: number=0;
   DebitAmount: number=0;
   totalPOS: number=0;
   posDetails : any;
   //--------------------------
   constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<BankClose>,@Inject(MAT_DIALOG_DATA) public data: bankEditData, public getEmployeeService: BankManagementService,public BankServiceData: BankManagementService) {}
   
   ngOnInit() {
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getEmployeeList();
    this.getCloseBankDetails();
    
    //Register Summary Form------------------------
    this.registerSummaryForm = new FormGroup({
      BankCloseCashId: new FormControl(this.BankCloseCashId),
      registerStartingGT: new FormControl(this.registerStartingGT,[Validators.required]),
      registerEndingGT: new FormControl(this.registerEndingGT,[Validators.required]),
      registerZNumber: new FormControl(this.registerZNumber,[Validators.required]),
      registerTranx: new FormControl(this.registerTranx,[Validators.required]),
      registerNoSales: new FormControl(this.registerNoSales,[Validators.required]),
      registerReading: new FormControl(this.registerReading,[Validators.required])
    });
    //---------------------------------------------
    //Sales Summary Form---------------------------
    this.salesSummaryForm = new FormGroup({
     // difference: new FormControl(this.difference),
      totalPickup: new FormControl(this.totalPickup),
      totalDrawer: new FormControl(this.totalDrawer),
      adjustments: new FormControl(this.adjustments),
      adjustmentComments: new FormControl(this.adjustmentComments),
      ccAdjustments: new FormControl(this.ccAdjustments),
      ccAdjustmentComments: new FormControl(this.ccAdjustmentComments),
      overShort: new FormControl(this.overShort),
      overShortComments: new FormControl(this.overShortComments),
      cashier: new FormControl(this.overShortComments)
    });
    //---------------------------------------------
    //denominationsForm----------------------------
     this.denominationsForm = new FormGroup({
      hundreds: new FormControl(this.hundreds),
      fifties: new FormControl(this.fifties),
      twenties: new FormControl(this.twenties),
      tens: new FormControl(this.tens),
      fives: new FormControl(this.fives),
      twos: new FormControl(this.twos),
      ones: new FormControl(this.ones),
      quarters: new FormControl(this.quarters),
      dimes: new FormControl(this.dimes),
      nickles: new FormControl(this.nickles),
      pennies: new FormControl(this.pennies),
      
      hundredsCount: new FormControl(this.hundredsCount),
      fiftiesCount: new FormControl(this.fiftiesCount),
      twentiesCount: new FormControl(this.twentiesCount),
      tensCount: new FormControl(this.tensCount),
      fivesCount: new FormControl(this.fivesCount),
      twosCount: new FormControl(this.twosCount),
      onesCount: new FormControl(this.onesCount),
      quartersCount: new FormControl(this.quartersCount),
      dimesCount: new FormControl(this.dimesCount),
      nicklesCount: new FormControl(this.nicklesCount),
      penniesCount: new FormControl(this.penniesCount),
      
      change: new FormControl(this.change),
      //checks: new FormControl(''),
      paymentFormfields: new FormControl(JSON.stringify(this.paymentFormfields))
    })
    //-------------------------------------------
    //POS Form-----------------------------------
   
    this.posForm = new FormGroup({
      BatchoutNumber: new FormControl(this.BatchoutNumber),
      VisaAmount: new FormControl(this.VisaAmount),
      MasterCardAmount: new FormControl(this.MasterCardAmount),
      DiscoverAmount: new FormControl(this.DiscoverAmount),
      AmexAmount: new FormControl(this.AmexAmount),
      DebitAmount: new FormControl(this.DebitAmount),
      TotalCardHandheldAmount: new FormControl(this.totalPOS)
    });
    //---------------------------------------------
    
    //CardHandheld Form----------------------------
    this.cardHandheldform = this.fb.group({
      cardHandheldFieldsitems: this.fb.array([])
    });
   
    //-------------------------------------------
    //Other charges forms------------------------
    this.otherChargesForm = this.fb.group({
      otherChargeFormfields: new FormControl(JSON.stringify(this.otherChargeFormfields))
    });
    //-------------------------------------------
    
   }
   
  get CardHandheldFields() {
    return this.cardHandheldform.get('cardHandheldFieldsitems') as FormArray
  }
  
  addCardHendheldField(CardHandheldTidNumber,CloseCardHandheldID,BatchoutNumber,AmexAmount,DebitAmount,DiscoverAmount,MasterCardAmount,VisaAmount,TotalCardHandheldAmount) {
      if (BatchoutNumber==0) {
       BatchoutNumber=''; 
      }
     const CardHendheldField = this.fb.group({ 
      CardHandheldTidNumber: [CardHandheldTidNumber],
      CloseCardHandheldID: [CloseCardHandheldID],
      BatchoutNumber: [BatchoutNumber,[Validators.required]],
      AmexAmount: [AmexAmount],
      DebitAmount: [DebitAmount],
      DiscoverAmount: [DiscoverAmount],
      MasterCardAmount: [MasterCardAmount],
      TotalCardHandheldAmount: [TotalCardHandheldAmount],
      VisaAmount: [VisaAmount]
    })

    this.CardHandheldFields.push(CardHendheldField)
  }
  
  
   
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as PickupEmployee;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      } 
    })
    
  }
  //------------------------------------------------------
  //Get Close Bank Details--------------------------------
  getCloseBankDetails(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var BankPaymentFormsArray=[];
    var cardhandheldFormsDataArray=[];
    var OtherChargeDetailsArray=[];
    var cashierDataArray=[];
    var cashierArr=[];
    var pickupDataArr=[];
    const bankDetails = {
      'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
     //call api------------
      this.BankServiceData.getCloseBankData(bankDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
          this.BankCloseCashId = res.data.BankCloseCashId;
          this.registerSummaryForm.controls['BankCloseCashId'].setValue(res.data.BankCloseCashId);
          this.registerStartingGT = res.data.StartingGT;
          this.registerSummaryForm.controls['registerStartingGT'].setValue(res.data.StartingGT);
          this.registerEndingGT = res.data.EndingGT;
          this.registerSummaryForm.controls['registerEndingGT'].setValue(res.data.EndingGT);
          
          //this.registerZNumber = res.data.RegisterZNumber;
          var RegisterZNumber=null;
          
          if (res.data.RegisterZNumber=='null' || res.data.RegisterZNumber== null) {
          // RegisterZNumber = res.data.RegisterZNumber; 
          }
          else{
            RegisterZNumber = res.data.RegisterZNumber;
          }
          
          this.registerSummaryForm.controls['registerZNumber'].setValue(RegisterZNumber);
          
          this.registerTranx = res.data.RegisterTranx;
          this.registerSummaryForm.controls['registerTranx'].setValue(res.data.RegisterTranx);
          this.registerNoSales = res.data.RegisterNoSales;
          this.registerSummaryForm.controls['registerNoSales'].setValue(res.data.RegisterNoSales);
          this.registerReading = res.data.Sales;
          this.registerSummaryForm.controls['registerReading'].setValue(res.data.Sales);
          
          //this.difference = 0;
          this.totalPickup = res.data.TotalPickup;
          this.salesSummaryForm.controls['totalPickup'].setValue(res.data.TotalPickup);
          this.totalDrawer = res.data.TotalDrawer;
          this.salesSummaryForm.controls['totalDrawer'].setValue(res.data.TotalDrawer);
          this.adjustments = res.data.Adjustment;
          this.salesSummaryForm.controls['adjustments'].setValue(res.data.Adjustment);
          this.adjustmentComments = res.data.AdjustmentComment;
          var AdjustmentComment = null;
          if (res.data.AdjustmentComment!='null') {
            AdjustmentComment=res.data.AdjustmentComment;
          }
          this.salesSummaryForm.controls['adjustmentComments'].setValue(AdjustmentComment);
         
          this.ccAdjustments = res.data.CCAdjustment;
          this.salesSummaryForm.controls['ccAdjustments'].setValue(res.data.CCAdjustment);
          this.ccAdjustmentComments = res.data.CCAdjustmentComment;
          var CCAdjustmentComment = null;
          if (res.data.CCAdjustmentComment!='null') {
            CCAdjustmentComment = res.data.CCAdjustmentComment;
          }
          this.salesSummaryForm.controls['ccAdjustmentComments'].setValue(CCAdjustmentComment);
          
          this.overShort = res.data.OverShort;
          this.salesSummaryForm.controls['overShort'].setValue(res.data.OverShort);
          
          this.overShortComments = res.data.OverShortComment;
          var overShortComments=null;
          if (res.data.OverShortComment!='') {
            overShortComments=res.data.OverShortComment;
          }
          this.salesSummaryForm.controls['overShortComments'].setValue(overShortComments);
          
          //bank payment forms-----------------------
          res.data.BankPaymentForms.forEach(function (value) {
            //console.log(value);
            let bankFormsData =  {} as paymentFormfields;
                
                bankFormsData.type='text';
                bankFormsData.label=value.PaymentFormName;
                bankFormsData.name=value.PaymentFormId;
                bankFormsData.value=value.Amount;
                
                BankPaymentFormsArray.push(bankFormsData);
          });
          this.paymentFormfields=BankPaymentFormsArray;
          //console.log(BankPaymentFormsArray);
          for (let i = 0; i < BankPaymentFormsArray.length; i++) {
           // console.log(BankPaymentFormsArray[i].name);
           this.denominationsForm.addControl(BankPaymentFormsArray[i].name, new FormControl(BankPaymentFormsArray[i].Amount));
           this.denominationsForm.controls[BankPaymentFormsArray[i].name].setValue(BankPaymentFormsArray[i].value); 
          }
          //-----------------------------------------
          //otherChargeFormfields--------------------
          var otherChargesTotal=0;
          res.data.BankOtherChargeDetails.forEach(function (value) {
            //BankOtherChargeDetailsArray
            let otherChargeFormsData =  {} as otherChargeFormfields;
                otherChargeFormsData.type='text';
                otherChargeFormsData.label=value.PaymentFormName;
                otherChargeFormsData.name=value.PaymentFormId;
                otherChargeFormsData.value=value.Amount;
                
                otherChargesTotal=otherChargesTotal+value.Amount;
                
                OtherChargeDetailsArray.push(otherChargeFormsData);
          });
          this.otherChargesTotal=+otherChargesTotal;
          this.otherChargeFormfields=OtherChargeDetailsArray;
          for (let i = 0; i < OtherChargeDetailsArray.length; i++) {
           this.otherChargesForm.addControl(OtherChargeDetailsArray[i].name, new FormControl(OtherChargeDetailsArray[i].Amount));
           this.otherChargesForm.controls[OtherChargeDetailsArray[i].name].setValue(OtherChargeDetailsArray[i].value); 
          }
          //-----------------------------------------
          
          //CardHandheld-----------------------------
         var totalCardHandheldTotal=0;
          res.data.CardHandheldDetails.forEach(function (value) {
            let cardhandheldFormsData =  {} as cardHandheldDetails;
              cardhandheldFormsData.AmexAmount = value.AmexAmount;
              cardhandheldFormsData.BatchoutNumber = value.BatchoutNumber;
              cardhandheldFormsData.CardHandheldTidNumber = value.CardHandheldTidNumber;
              cardhandheldFormsData.CloseCardHandheldID = value.CloseCardHandheldID;
              cardhandheldFormsData.DebitAmount = value.DebitAmount;
              cardhandheldFormsData.DiscoverAmount = value.DiscoverAmount;
              cardhandheldFormsData.MasterCardAmount = value.MasterCardAmount;
              cardhandheldFormsData.VisaAmount = value.VisaAmount;
              cardhandheldFormsData.TotalCardHandheldAmount = value.TotalCardHandheldAmount;
              totalCardHandheldTotal = totalCardHandheldTotal+value.TotalCardHandheldAmount;
              cardhandheldFormsDataArray.push(cardhandheldFormsData);
             
          });
          
          this.cardHandheldTotal=totalCardHandheldTotal;
          
         // console.log(cardhandheldFormsDataArray);
         for (let i = 0; i < cardhandheldFormsDataArray.length; i++) {
          //CardHandheldTidNumber,CloseCardHandheldID,BatchoutNumber,AmexAmount,DebitAmount,DiscoverAmount,MasterCardAmount,VisaAmount,TotalCardHandheldAmount
         
          this.addCardHendheldField(cardhandheldFormsDataArray[i].CardHandheldTidNumber, cardhandheldFormsDataArray[i].CloseCardHandheldID, cardhandheldFormsDataArray[i].BatchoutNumber, cardhandheldFormsDataArray[i].AmexAmount, cardhandheldFormsDataArray[i].DebitAmount, cardhandheldFormsDataArray[i].DiscoverAmount, cardhandheldFormsDataArray[i].MasterCardAmount, cardhandheldFormsDataArray[i].VisaAmount, cardhandheldFormsDataArray[i].TotalCardHandheldAmount);
         }
        //-----------------------------------------
        //Denomination-----------------------------
         this.denominationsForm.controls['hundreds'].setValue(res.data.Denomination.HundredsAmount);
         this.denominationsForm.controls['fifties'].setValue(res.data.Denomination.FiftiesAmount);
         this.denominationsForm.controls['twenties'].setValue(res.data.Denomination.TwentiesAmount);
         this.denominationsForm.controls['tens'].setValue(res.data.Denomination.TensAmount);
         this.denominationsForm.controls['fives'].setValue(res.data.Denomination.FivesAmount);
         this.denominationsForm.controls['twos'].setValue(res.data.Denomination.TwosAmount);
         this.denominationsForm.controls['ones'].setValue(res.data.Denomination.OnesAmount);
         this.denominationsForm.controls['quarters'].setValue(res.data.Denomination.QuartersAmount);
         this.denominationsForm.controls['dimes'].setValue(res.data.Denomination.DimesAmount);
         this.denominationsForm.controls['nickles'].setValue(res.data.Denomination.NickelsAmount);
         this.denominationsForm.controls['pennies'].setValue(res.data.Denomination.PenniesAmount);
         this.denominationsForm.controls['change'].setValue(res.data.Denomination.ChangeAmount);
         this.denominationsTotalAmount=res.data.Denomination.TotalCash;
        //-----------------------------------------
        //Denomination Count-----------------------
        this.denominationsForm.controls['hundredsCount'].setValue(res.data.DenominationCount.HundredsCount);
        this.denominationsForm.controls['fiftiesCount'].setValue(res.data.DenominationCount.FiftiesCount);
        this.denominationsForm.controls['twentiesCount'].setValue(res.data.DenominationCount.TwentiesCount);
        this.denominationsForm.controls['tensCount'].setValue(res.data.DenominationCount.TensCount);
        this.denominationsForm.controls['fivesCount'].setValue(res.data.DenominationCount.FivesCount);
        this.denominationsForm.controls['twosCount'].setValue(res.data.DenominationCount.TwosCount);
        this.denominationsForm.controls['onesCount'].setValue(res.data.DenominationCount.OnesCount);
        this.denominationsForm.controls['quartersCount'].setValue(res.data.DenominationCount.QuartersCount);
        this.denominationsForm.controls['dimesCount'].setValue(res.data.DenominationCount.DimesCount);
        this.denominationsForm.controls['nicklesCount'].setValue(res.data.DenominationCount.NickelsCount);
        this.denominationsForm.controls['penniesCount'].setValue(res.data.DenominationCount.PenniesCount);
        //-----------------------------------------
        //POS--------------------------------------
          let posFormsData =  {} as POS;
          posFormsData.BatchoutNumber=res.data.PosDetails.BatchoutNumber;
          posFormsData.AmexAmount=res.data.PosDetails.AmexAmount;
          posFormsData.DebitAmount=res.data.PosDetails.DebitAmount;
          posFormsData.DiscoverAmount=res.data.PosDetails.DiscoverAmount;
          posFormsData.MasterCardAmount=res.data.PosDetails.MasterCardAmount;
          posFormsData.VisaAmount=res.data.PosDetails.VisaAmount;
          posFormsData.TotalCardHandheldAmount=res.data.PosDetails.TotalCardHandheldAmount;
          
          //set form value----------------------------
          this.posName=res.data.PosDetails.POSName;
          this.posForm.controls['BatchoutNumber'].setValue(res.data.PosDetails.BatchoutNumber);
          this.posForm.controls['VisaAmount'].setValue(res.data.PosDetails.VisaAmount);
          this.posForm.controls['MasterCardAmount'].setValue(res.data.PosDetails.MasterCardAmount);
          this.posForm.controls['DiscoverAmount'].setValue(res.data.PosDetails.DiscoverAmount);
          this.posForm.controls['AmexAmount'].setValue(res.data.PosDetails.AmexAmount);
          this.posForm.controls['DebitAmount'].setValue(res.data.PosDetails.DebitAmount);
          
          this.totalPOS = res.data.PosDetails.TotalCardHandheldAmount;
          //------------------------------------------
          //console.log(posFormsData);
       
        //-----------------------------------------
        //Cashier----------------------------------
        res.data.Cashier.forEach(function (value) {
          let cashierData =  {} as PickupEmployee;
          cashierData.FK_Employee_ID=value.UserId;
          cashierArr.push(value.UserId);
          cashierDataArray.push(cashierData);
        });
        this.cashierList=cashierDataArray;
        this.cashierArr=cashierArr;
        //console.log(this.cashierList);
        //-----------------------------------------
        //Pickup List------------------------------
        res.data.PickupList.forEach(function (value) {
          let pickupData =  {} as PickupList;
           pickupData.PickupNumber = value.PickupNumber;
           pickupData.PickupTotal = value.PickupTotal;
           pickupDataArr.push(pickupData);   
        });
        this.pickupList = pickupDataArr;
        //-----------------------------------------
      }
      else{
       //console.log('Error');
      }
      //console.log(bankDetailsArray);
    })
     
  }
  //------------------------------------------------------
  
  //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundredsError=false;
     this.hundreds=(hundredsCount * 100);
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.hundredsBillError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsError=true;
     this.noError=true;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
     if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fiftiesError=false;
     this.fifties=(fiftiesCount * 50);
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
     if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.fiftiesBillError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twentiesError=false;
     this.twenties=(twentiesCount * 20);
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.twentiesBillError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tensError=false;
     this.tens=(tensCount * 10);
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.tensBillError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fivesError=false;
     this.fives=(fivesCount * 5);
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.fivesBillError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twosError=false;
     this.twos=(twosCount * 2);
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.twosBillError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.onesError=false;
     this.ones=(onesCount * 1);
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.onesBillError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quartersError=false;
     this.quarters=(quartersCount * 0.25);
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.quartersBillError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else if(dimesCount > 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimesError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }     
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.dimesBillError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }  
    else if(nicklesCount > 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nicklesError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.nicklesBillError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else if(penniesCount > 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.penniesError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }    
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.penniesBillError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  
  
  //DenominationsFormSubmit-------------------------------
  denominationsFormSubmit(){
   // console.log(this.denominationsForm.value);
    if (!this.noError) {
    var frmVal=JSON.stringify(this.denominationsForm.value);
    var frmValObj=JSON.parse(frmVal);
    var denominationDetailArray = [];
    //console.log(frmValObj);
    let sum = 0;
    var countVariable = ["dimesCount","fiftiesCount","fivesCount","nicklesCount","onesCount","penniesCount","quartersCount","tensCount","twentiesCount","twosCount","hundredsCount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!countVariable.includes(key)) {
          //console.log(key + " -> " + frmValObj[key]);
          let amount=frmValObj[key];
          sum = sum + (+amount);
        }
          
      }
    }
    //Collection data---------------
    let denominationData =  {} as DenominationDetail;
      denominationData.HundredsCount = frmValObj['hundredsCount'];
      denominationData.FiftiesCount = frmValObj['fiftiesCount'];
      denominationData.TwentiesCount = frmValObj['twentiesCount'];
      denominationData.TensCount = frmValObj['tensCount'];
      denominationData.FivesCount = frmValObj['fivesCount'];
      denominationData.TwosCount = frmValObj['twosCount'];
      denominationData.OnesCount = frmValObj['onesCount'];
      denominationData.QuartersCount = frmValObj['quartersCount'];
      denominationData.NickelsCount = frmValObj['nicklesCount'];
      denominationData.DimesCount = frmValObj['dimesCount'];
      denominationData.PenniesCount = frmValObj['penniesCount'];
      denominationData.ChangeAmount = frmValObj['change'];
      
     // denominationDetailArray.push(denominationData);
      this.denominationDetailArray=denominationData;
    //------------------------------
    //Collecting payments form data---------
    var frmVal2=JSON.stringify(this.paymentFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var paymentFormSubmitDetailsArr=[];
   
    for (var key2 in frmValObj2) {
      let paymentFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      paymentFormSubmitDetails.PaymentFormId=keyName;
      paymentFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      paymentFormSubmitDetailsArr.push(paymentFormSubmitDetails);
    }
    this.BankPaymentFormsArray=paymentFormSubmitDetailsArr;
    //console.log(paymentFormSubmitDetailsArr);
    //--------------------------------------
    
    this.denominationsTotalAmount=+sum.toFixed(2);
    this.calculateOverShort();
    this.errorArray.splice(2, 1); 
    }
    else{
      this.errorArray[2]='Some error in denominations';
    }
    
  }
  //------------------------------------------------------
  //Register Summary Form Submit--------------------------
  registerSummaryFormSubmit(){
    
    var registerStartingGT=this.registerSummaryForm.value.registerStartingGT;
    this.registerStartingGTError=false;
    if (registerStartingGT === null) {
     this.registerStartingGTError=true; 
    }
    
    var registerEndingGT=this.registerSummaryForm.value.registerEndingGT;
    this.registerEndingGTError=false;
    if (registerEndingGT === null) {
      this.registerEndingGTError=true;
    }
    
    var registerZNumber = this.registerSummaryForm.value.registerZNumber;
    this.registerZNumberError=false;
    if (registerZNumber == "" || registerZNumber === 'null') {
      this.registerZNumberError=true;
    }
    
    var registerTranx = this.registerSummaryForm.value.registerTranx;
    
    this.registerTranxError=false;
    if (registerTranx === null) {
      this.registerTranxError=true;
    }
    
    var registerNoSales = this.registerSummaryForm.value.registerNoSales;
    this.registerNoSalesError=false;
    if (registerNoSales === null) {
      this.registerNoSalesError=true;
    }
    
    var registerReading = this.registerSummaryForm.value.registerReading;
    this.registerReadingError=false;
    if (registerReading === null) {
      this.registerReadingError=true;
    }
    
    if(this.registerSummaryForm.valid) {
      
      if (registerStartingGT > registerEndingGT) {
        this.registerStartingGTError=true;
        this.errorArray[9]="Register Starting GT Can't be grater than Ending GT ";
      }
      else{
       this.errorArray.splice(9, 1);
       this.registerStartingGTError=false; 
      }
      this.errorArray.splice(8, 1);
    }
    else{
     this.errorArray[8]='Please fill all register summary fields';
    }
    
    this.calculateOverShort();
   
  }
  //------------------------------------------------------
  //Sales Summary Form Submit-----------------------------
  salesSummaryFormSubmit(){
     
    //console.log(this.salesSummaryForm);
    var frmVal=JSON.stringify(this.salesSummaryForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var cashierDataArr=[];
    
    for (var key in frmValObj['cashier']) {
      let cashierData =  {} as Employee;
      cashierData.UserId = frmValObj['cashier'][key].FK_Employee_ID;
      cashierDataArr.push(cashierData);
    }
    
    //ccAdjustmentCommentError
    //CC adjustment comment null checking-----------------
    var ccAdjustmentAmt=this.salesSummaryForm.value.ccAdjustments;
    var ccAdjustmentComment=this.salesSummaryForm.value.ccAdjustmentComments;
    if ((ccAdjustmentAmt > 0 || ccAdjustmentAmt < 0 && ccAdjustmentAmt !='') && (ccAdjustmentComment=='' || ccAdjustmentComment==null)) {
      this.ccAdjustmentCommentError=true;
      this.errorArray[6]='Please enter CC Adjustment comment';
    }
    else{
     this.errorArray.splice(6, 1); 
     this.ccAdjustmentCommentError=false;
    }
    //----------------------------------------------------
    //Adjustment Comment null checking--------------------
    var adjustmentAmt=this.salesSummaryForm.value.adjustments;
    var adjustmentComment=this.salesSummaryForm.value.adjustmentComments;
    if ((adjustmentAmt > 0 || adjustmentAmt < 0 && adjustmentAmt !='') && (adjustmentComment=='' || adjustmentComment==null)) {
      this.adjustmentCommentError=true;
      this.errorArray[5]='Please enter Adjustment comment';
    }
    else{
     this.errorArray.splice(5, 1); 
     this.adjustmentCommentError=false;
    }
    //----------------------------------------------------
   //overshort null checking------------------------------
    var overShortAmt=this.salesSummaryForm.value.overShort;
    var overShortComment=this.salesSummaryForm.value.overShortComments;
    if ((overShortAmt >= 10) && (overShortComment=='' || overShortComment==null)) {
      this.overShortCommentError=true;
      this.errorArray[0]='Please enter over/short comments';
    }
    else{
     this.errorArray.splice(0, 1); 
     this.overShortCommentError=false;
    }
    //----------------------------------------------------
   
    this.cashierDataArray=cashierDataArr;
    this.calculateOverShort();
   
  }
  //------------------------------------------------------
  //POS Form submit---------------------------------------
  posFormSubmit(){
    
   // console.log(this.posForm.value);
    var frmVal=JSON.stringify(this.posForm.value);
    var frmValObj=JSON.parse(frmVal);
    var sum = 0;
    var excludeVariable = ["BatchoutNumber","TotalCardHandheldAmount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!excludeVariable.includes(key)) {
         // console.log(key + " -> " + frmValObj[key]);
          var amount=frmValObj[key];
          sum = sum + (parseFloat(amount));
        }
          
      }
    }
    //For collecting data----------
    var posDataArray=[];
    let posData =  {} as POS;
    for (var key in frmValObj) {
      if (key=='BatchoutNumber') {
        posData.BatchoutNumber=frmValObj['BatchoutNumber'];
        if ((frmValObj['BatchoutNumber']=='' || frmValObj['BatchoutNumber']==null) && sum > 0) {
          this.errorArray[1]='Please enter POS Batch#';
          this.posBatchoutNumberError=true;
          //this.noError=true;
        }
        else{
          this.errorArray.splice(1, 1); 
          this.posBatchoutNumberError=false;
          //this.noError=false;
        }
      }
      if (key=='VisaAmount') {
        posData.VisaAmount=frmValObj['VisaAmount'];
      }
      if (key=='MasterCardAmount') {
        posData.MasterCardAmount=frmValObj['MasterCardAmount'];
      }
      if (key=='DiscoverAmount') {
        posData.DiscoverAmount=frmValObj['DiscoverAmount'];
      }
      if (key=='AmexAmount') {
        posData.AmexAmount=frmValObj['AmexAmount'];
      }
      if (key=='DebitAmount') {
        posData.DebitAmount=frmValObj['DebitAmount'];
      }
    }
    posData.OtherChargeAmount=0;
    posData.TotalPOSAmount=sum;
    posDataArray.push(posData);
    this.posDetails=posDataArray;
    
    //console.log(this.posDetails);
    //-----------------------------
   
    this.totalPOS=sum;
    this.posForm.controls['TotalCardHandheldAmount'].setValue(this.totalPOS);
    
    this.calculateOverShort();
    
  }
  //------------------------------------------------------
  //CardHandheld Form Submit------------------------------
  cardHandheldFormSubmit(){
    
   // console.log(this.cardHandheldform);
    let ccErrorCount=0;
    let ccAmtErrorCount=0;
    var cardHandheldFieldsArray=[];
    var frmVal=JSON.stringify(this.cardHandheldform.value.cardHandheldFieldsitems);
    var frmValObj=JSON.parse(frmVal);
    //console.log(frmValObj);
    let sum = 0;
    let cardHandheldTotal = 0;
    let i = 0;
    var excludeVariable = ["CardHandheldTidNumber","CloseCardHandheldID","BatchoutNumber","TotalCardHandheldAmount"];
    for (var fkey in frmValObj) {
      if (frmValObj.hasOwnProperty(fkey)) {
        //For total calculation------------
        for (var key in frmValObj[fkey]) {
          if (!excludeVariable.includes(key)) {
            //console.log(key + " -> " + frmValObj[fkey][key]);
            let amount=frmValObj[fkey][key];
            sum = sum + (+amount);
            cardHandheldTotal = cardHandheldTotal + (+amount);
          }
        }
        //----------------------------------
        //For collecting data---------------
        let cardHandheldData =  {} as cardHandheldDetails;
        var cardBerror=[];
        for (var key in frmValObj[fkey]) {
          if (key=='BatchoutNumber') {
             cardHandheldData.BatchoutNumber=frmValObj[fkey]['BatchoutNumber'];
             if (frmValObj[fkey]['BatchoutNumber'].length === 0 && sum > 0) {
                var element = document.getElementById('batch-out'+i);
                element.classList.add("error-input");
                
                ccErrorCount++;
             }
             else{
                var element = document.getElementById('batch-out'+i);
                element.classList.remove("error-input");
             }
             
             //if ((frmValObj[fkey]['BatchoutNumber']!='' || frmValObj[fkey]['BatchoutNumber']!== null) && sum <= 0) {
             if (frmValObj[fkey]['BatchoutNumber'].length!== 0 && sum <= 0) {
              ccAmtErrorCount++;
             }
             
           }
            
           if (key=='CloseCardHandheldID') {
            cardHandheldData.CloseCardHandheldID=frmValObj[fkey]['CloseCardHandheldID'];
           }
           if (key=='BatchoutNumber') {
            cardHandheldData.BatchoutNumber=frmValObj[fkey]['BatchoutNumber'];
           }
           if (key=='AmexAmount') {
            cardHandheldData.AmexAmount=frmValObj[fkey]['AmexAmount'];
           }
           if (key=='DebitAmount') {
            cardHandheldData.DebitAmount=frmValObj[fkey]['DebitAmount'];
           }
           if (key=='DiscoverAmount') {
            cardHandheldData.DiscoverAmount=frmValObj[fkey]['DiscoverAmount'];
           }
           if (key=='MasterCardAmount') {
            cardHandheldData.MasterCardAmount=frmValObj[fkey]['MasterCardAmount'];
           }
           if (key=='VisaAmount') {
            cardHandheldData.VisaAmount=frmValObj[fkey]['VisaAmount'];
           }
           if (key=='TotalCardHandheldAmount') {
            cardHandheldData.TotalCardHandheldAmount=sum;
           }
        }
        //----------------------------------
        cardHandheldFieldsArray.push(cardHandheldData);
        
        document.getElementById('card-total'+i).innerHTML = JSON.stringify(sum);
        sum = 0;
       // console.log("CardH Total:"+sum);
      }
      i++;
    }
    //console.log('ccErrorCount:'+ccErrorCount);
    if (ccErrorCount > 0) {
     this.errorArray[3]='Please enter all CC-Handheld Batch#';
     this.ccHandheldAmountError=true;
    }
    else{
       this.errorArray.splice(3, 1); 
       this.ccHandheldAmountError=false;
    }
    //console.log('ccAmtErrorCount:'+ccAmtErrorCount);
    if (ccAmtErrorCount > 0) {
      this.errorArray[4]='Please enter cc handheld details';
      this.ccHandheldBatchoutError=true;
    }
    else{
       this.errorArray.splice(4, 1); 
       this.ccHandheldBatchoutError=false;
    }
    
    this.cardHandheldFieldsDataArray=cardHandheldFieldsArray;
    this.cardHandheldTotal=cardHandheldTotal;
    this.calculateOverShort();
    
  }
  //------------------------------------------------------
  //RegisterSummary on change submit----------------------
  registerSummaryChange(){
   this.registerSummaryFormSubmit();
  }
  //------------------------------------------------------
 
  //Function for checking selected Cashier---------------
      comparer(o1: PickupEmployee, o2: PickupEmployee): boolean {
        // if possible compare by object's name, and not by reference.
        return o1 && o2 ? o1.FK_Employee_ID === o2.FK_Employee_ID : o2 === o2;
      }
    //----------------------------------------------------
  
  //Check for added Cashier-------------------------------
    checkDisabledCashier(FK_Employee_ID){
      if (this.cashierArr && this.cashierArr.includes(FK_Employee_ID)) {
       return true;
      }
      else{
       return false; 
      }
    }
    //-----------------------------------------------------
  calculateOverShort(){
    var totalDrawerC=0;
    var cardHandheldTotal= +this.cardHandheldTotal;
    var totalPOS = +this.totalPOS;
    var denominationsTotalAmount= +this.denominationsTotalAmount;
    
    totalDrawerC=(cardHandheldTotal + totalPOS + denominationsTotalAmount +this.totalPickup +this.otherChargesTotal);
     
    this.salesSummaryForm.controls['totalDrawer'].setValue(totalDrawerC.toFixed(2));
     
    var totalDrawer=this.salesSummaryForm.value.totalDrawer;
    var registerReading=this.registerSummaryForm.value.registerReading;
    var overshort=(parseFloat(totalDrawer) - parseFloat(registerReading));
   
    this.salesSummaryForm.controls['overShort'].setValue(overshort.toFixed(2));
  }
  //-------------------------------------------------------
  //Other charges form submit------------------------------
  otherChargesFormSubmit(){
  //Collecting payments form data---------
     var frmVal=JSON.stringify(this.otherChargesForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var frmVal2=JSON.stringify(this.otherChargeFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var otherChargesFormSubmitDetailsArr=[];
    var otherChargesTotal=0;
    for (var key2 in frmValObj2) {
      let otherChargesFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      //console.log("keyName: "+keyName);
      otherChargesFormSubmitDetails.PaymentFormId=keyName;
      otherChargesFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      otherChargesTotal=otherChargesTotal+frmValObj[JSON.stringify(keyName)]
      
      otherChargesFormSubmitDetailsArr.push(otherChargesFormSubmitDetails);
    }
    this.otherChargesTotal=+otherChargesTotal;
    this.OtherChargesFormsArray=otherChargesFormSubmitDetailsArr;
    
    this.calculateOverShort();
    //console.log(otherChargesFormSubmitDetailsArr);
    //--------------------------------------
  }
  //-------------------------------------------------------
  //Submit all forms---------------------------------------
  submitAllForms(){
    
    this.registerSummaryFormSubmit();
    this.salesSummaryFormSubmit();
    this.denominationsFormSubmit();
    this.cardHandheldFormSubmit();
    this.posFormSubmit();
    this.otherChargesFormSubmit();
    //console.log(this.errorArray);
    
    var Uniqerror = this.errorArray.filter((v, i, a) => a.indexOf(v) === i);
    let error=JSON.stringify(Uniqerror);
    
    if (this.registerSummaryForm.valid && this.posForm.valid && !this.noError && !this.posBatchoutNumberError && !this.overShortCommentError && !this.ccHandheldAmountError && !this.ccHandheldBatchoutError && !this.adjustmentCommentError && !this.ccAdjustmentCommentError && !this.registerStartingGTError) {
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    
    var cashierDetails='';
    if (this.cashierDataArray.length > 0) {
      cashierDetails=JSON.stringify(this.cashierDataArray);
    }
    var isChange=false;
    if (this.registerSummaryForm.value.BankCloseCashId > 0 && this.isChange==true) {
      isChange=true;
    }
    //Close bank data--------------------
    const closeBankDetails = {
      'bankId' : JSON.parse(sessionStorage.getItem("bankId")),
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId' : branchId,
      'createBy' : this.authUserId,
      'authUserId' : this.authUserId,
      'bankCloseCashId' : this.registerSummaryForm.value.BankCloseCashId,
      'startingGT' : this.registerSummaryForm.value.registerStartingGT,
      'endingGT' : this.registerSummaryForm.value.registerEndingGT,
      'registerTranx' : this.registerSummaryForm.value.registerTranx,
      'registerNoSales' : this.registerSummaryForm.value.registerNoSales,
      'sales' : this.registerSummaryForm.value.registerReading,
      'registerZNumber' : this.registerSummaryForm.value.registerZNumber,
      'totalDrawer' : this.salesSummaryForm.value.totalDrawer,
      'adjustment' : this.salesSummaryForm.value.adjustments,
      'cCAdjustment' : this.salesSummaryForm.value.ccAdjustments,
      'adjustmentComment' : this.salesSummaryForm.value.adjustmentComments,
      'ccAdjustmentComment' : this.salesSummaryForm.value.ccAdjustmentComments,
      'overShort' : this.salesSummaryForm.value.overShort,
      'overShortComment' : this.salesSummaryForm.value.overShortComments,
      'cashierDetails' : cashierDetails,
      'cardHandheldAmount' : JSON.stringify(this.cardHandheldFieldsDataArray),
      'denominationDetail' : JSON.stringify(this.denominationDetailArray),
      'paymentFormDetails' : JSON.stringify(this.BankPaymentFormsArray),
      'isChange' : isChange,
      'otherChargeDetails' : JSON.stringify(this.OtherChargesFormsArray),
      'posDetails' : JSON.stringify(this.posDetails)
     }
    //-----------------------------------
    //console.log(closeBankDetails);
    //API call---------------------------
    this.BankServiceData.submitCloseBank(closeBankDetails).subscribe(res=>{
     this.progressBarHide();
     // console.log(res);
      if(res.status=='success') {
        this.showErrorMsg=false;
        sessionStorage.setItem("dialogOpend",'false');
        this.successmessage=res.message;
        setTimeout(() => {
          this.dialogRef.close();
         }, 2000);
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
      }
    });
    //-----------------------------------
    }
    else{
      this.showErrorMsg=true;
      this.errormessage=error;
    }
  }
  //--------------------------------------------------------
   //Is any changes made-------------------------------------------
  isAmountChange(){
    this.isChange=true;
  }
  //--------------------------------------------------------------
   //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
    //--------------------------------
}
//----------------------------------------------------------
//####################################################################################################################
//Special Bank----------------------------------------------
export interface POS {
  PosId: string;
  posName: string;
}

export interface CC {
  CardHandheldTidNumber: string;
}

export interface Event {
 EventId: number;
 Vendor: string;
 Event: string;
 Date: any;
 AmountReceived: number;
 Notes: string;
}

@Component({
  selector: 'special_bank',
  templateUrl: './special_bank.html',
  styleUrls: ['./bank-management.component.css']
})
export class SpecialBank {

    clickValue = 0;
    isMonthGreat = false;
    dateValue: boolean = true;
    errorArray:Array<any> = [];
    errormessage: string;
    successmessage: string
    showErrorMsg: boolean;
    panelOpenState = false;
    posOptions:any;
    ccOptions:any;
  //Progres----------------
    progressShow: boolean;
    mode = 'determinate';
    value = 50;
    bufferValue = 95;
 //-----------------------
  date: any;
  registerSummaryForm: FormGroup;
  salesSummaryForm: FormGroup;
  denominationsForm: FormGroup;
  cardHandheldform: FormGroup;
  eventForm: FormGroup;
  posForm: FormGroup;
  otherChargesForm: FormGroup;
  BankCloseCashId: number;
  
  BankPaymentFormsArray:any;
  public paymentFormfields: any[];
  OtherChargesFormsArray:any;
  public otherChargeFormfields: any[];
  
  eventFormsArray:any;
  
    authUserId:number;
    employees:any;
    
    cardHandheldFieldsitems:any;
    eventFieldsitems:any;
    cardHandheldTotal:number=0;
    cardHandheldFieldsDataArray:any;
    selectedTidNumber:any;
    otherChargesTotal:number=0;
    
   //Register Variables--------
    registerStartingGT: number;
    registerEndingGT: number;
    registerZNumber: string='';
    registerTranx: number;
    registerNoSales: number;
    registerReading: number;
   //--------------------------
   //Sales Summary Variables---
   //difference: number;
   totalPickup: number=0;
   totalDrawer: number=0;
   adjustments: number=0;
   adjustmentComments: string;
   ccAdjustments: number=0;
   ccAdjustmentComments: string;
   overShort: number=0;
   overShortComments: string;
   cashier: any;
   cashierDataArray: any;
   cashierArr: string[];
   cashierList:PickupEmployee[];
   //--------------------------
   isChange:boolean=false;
   //Error flags-------------------------
    noError:boolean=false;
    hundredsError:boolean=false;
    fiftiesError:boolean=false;
    twentiesError:boolean=false;
    tensError:boolean=false;
    fivesError:boolean=false;
    twosError:boolean=false;
    onesError:boolean=false;
    quartersError:boolean=false;
    dimesError:boolean=false;
    nicklesError:boolean=false;
    penniesError:boolean=false;
    
    hundredsBillError:boolean=false;
    fiftiesBillError:boolean=false;
    twentiesBillError:boolean=false;
    tensBillError:boolean=false;
    fivesBillError:boolean=false;
    twosBillError:boolean=false;
    onesBillError:boolean=false;
    quartersBillError:boolean=false;
    dimesBillError:boolean=false;
    nicklesBillError:boolean=false;
    penniesBillError:boolean=false;
    adjustmentCommentError:boolean=false;
    ccAdjustmentCommentError:boolean=false;
    overShortCommentError:boolean=false;
    ccHandheldBatchoutError:boolean=false;
    ccHandheldAmountError:boolean=false;
    ccHandheldTIDError:boolean=false;
    registerStartingGTError:boolean=false;
    registerEndingGTError:boolean=false;
    registerZNumberError:boolean=false;
    registerTranxError:boolean=false;
    registerNoSalesError:boolean=false;
    registerReadingError:boolean=false;
    //------------------------------------
   //denominations Variables---
    denominationDetailArray: any;
    hundreds: number=0;
    fifties: number=0;
    twenties: number=0;
    tens: number=0;
    fives: number=0;
    twos: number=0;
    ones: number=0;
    quarters: number=0.00;
    dimes: number=0.00;
    nickles: number=0.00;
    pennies: number=0.00;
    
    hundredsCount: number=0;
    fiftiesCount: number=0;
    twentiesCount: number=0;
    tensCount: number=0;
    fivesCount: number=0;
    twosCount: number=0;
    onesCount: number=0;
    quartersCount: number=0;
    dimesCount: number=0;
    nicklesCount: number=0;
    penniesCount: number=0;
    
    
    change: number=0;
    checks: number;
    total: number=0;
    denominationsTotalAmount: number=0;
   //-----------------------
   //POS form variables--------
   posSelectError: boolean;
   posBatchoutNumberError: boolean;
   posId: number;
   posName: string;
   BatchoutNumber: string;
   VisaAmount: number=0;
   MasterCardAmount: number=0;
   DiscoverAmount: number=0;
   AmexAmount: number=0;
   DebitAmount: number=0;
   totalPOS: number=0;
   posDetails : any;
   BankId : number=0;
   //--------------------------
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<SpecialBank>, @Inject(MAT_DIALOG_DATA) public data: '',public getEmployeeService: BankManagementService, public BankServiceData: BankManagementService) {}

    ngOnInit() {
    this.authUserId=JSON.parse(localStorage.getItem('UserId'));
    
    this.getSpecialBankDetails();
    this.getPosList();
   // this.getCCList();
    this.getEmployeeList();
    
    
    //Register Summary Form------------------------
    this.registerSummaryForm = new FormGroup({
      BankCloseCashId: new FormControl(this.BankCloseCashId),
      registerStartingGT: new FormControl(this.registerStartingGT,[Validators.required]),
      registerEndingGT: new FormControl(this.registerEndingGT,[Validators.required]),
      registerZNumber: new FormControl(this.registerZNumber,[Validators.required]),
      registerTranx: new FormControl(this.registerTranx,[Validators.required]),
      registerNoSales: new FormControl(this.registerNoSales,[Validators.required]),
      registerReading: new FormControl(this.registerReading,[Validators.required])
    });
    //---------------------------------------------
    //Sales Summary Form---------------------------
    this.salesSummaryForm = new FormGroup({
     // difference: new FormControl(this.difference),
      totalPickup: new FormControl(this.totalPickup),
      totalDrawer: new FormControl(this.totalDrawer),
      adjustments: new FormControl(this.adjustments),
      adjustmentComments: new FormControl(this.adjustmentComments),
      ccAdjustments: new FormControl(this.ccAdjustments),
      ccAdjustmentComments: new FormControl(this.ccAdjustmentComments),
      overShort: new FormControl(this.overShort),
      overShortComments: new FormControl(this.overShortComments),
      cashier: new FormControl(this.overShortComments)
    });
    //---------------------------------------------
    //denominationsForm----------------------------
     this.denominationsForm = new FormGroup({
      hundreds: new FormControl(this.hundreds),
      fifties: new FormControl(this.fifties),
      twenties: new FormControl(this.twenties),
      tens: new FormControl(this.tens),
      fives: new FormControl(this.fives),
      twos: new FormControl(this.twos),
      ones: new FormControl(this.ones),
      quarters: new FormControl(this.quarters),
      dimes: new FormControl(this.dimes),
      nickles: new FormControl(this.nickles),
      pennies: new FormControl(this.pennies),
      
      hundredsCount: new FormControl(this.hundredsCount),
      fiftiesCount: new FormControl(this.fiftiesCount),
      twentiesCount: new FormControl(this.twentiesCount),
      tensCount: new FormControl(this.tensCount),
      fivesCount: new FormControl(this.fivesCount),
      twosCount: new FormControl(this.twosCount),
      onesCount: new FormControl(this.onesCount),
      quartersCount: new FormControl(this.quartersCount),
      dimesCount: new FormControl(this.dimesCount),
      nicklesCount: new FormControl(this.nicklesCount),
      penniesCount: new FormControl(this.penniesCount),
      
      change: new FormControl(this.change),
      //checks: new FormControl(''),
      paymentFormfields: new FormControl(JSON.stringify(this.paymentFormfields))
    })
    //-------------------------------------------
    //POS Form-----------------------------------
   
    this.posForm = new FormGroup({
      posId: new FormControl(''),
      BatchoutNumber: new FormControl(""),
      VisaAmount: new FormControl(this.VisaAmount),
      MasterCardAmount: new FormControl(this.MasterCardAmount),
      DiscoverAmount: new FormControl(this.DiscoverAmount),
      AmexAmount: new FormControl(this.AmexAmount),
      DebitAmount: new FormControl(this.DebitAmount),
      TotalCardHandheldAmount: new FormControl(this.totalPOS)
    });
    
    this.posForm.get('posId').setValue('');
    //---------------------------------------------
      
    //CardHandheld Form----------------------------
    this.cardHandheldform = new FormGroup({
     CardHandheldTidNumber: new FormControl(this.selectedTidNumber),
     CloseCardHandheldID: new FormControl(0),
     BatchoutNumber: new FormControl(''),
     AmexAmount: new FormControl(0),
     DebitAmount: new FormControl(0),
     DiscoverAmount: new FormControl(0),
     MasterCardAmount: new FormControl(0),
     TotalCardHandheldAmount: new FormControl(0),
     VisaAmount: new FormControl(0)
     
    });
    
    this.cardHandheldform.get('CardHandheldTidNumber').setValue('');
    //---------------------------------------------
    //Other charges forms--------------------------
    this.otherChargesForm = this.fb.group({
      otherChargeFormfields: new FormControl(JSON.stringify(this.otherChargeFormfields))
    });
    //-------------------------------------------
    
     //Event Form----------------------------
    this.eventForm = this.fb.group({
      eventFieldsitems: this.fb.array([])
    });
    //---------------------------------------------
    
   }
   //---------------------------------------------------------
  
  //Get Special Bank Details----------------------------------
   getSpecialBankDetails(){
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var BankPaymentFormsArray=[];
    var cardhandheldFormsDataArray=[];
    var OtherChargeDetailsArray=[];
    var cashierDataArray=[];
    var cashierArr=[];
    var pickupDataArr=[];
    const bankDetails = {
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId': branchId,
      'authUserId': this.authUserId
     }
    // console.log(bankDetails);
     //call api------------
      this.BankServiceData.getSpecialBankData(bankDetails).subscribe(res=>{
      this.progressBarHide();
      //console.log(res);
      if(res.status=='success') {
          this.BankId = res.data.SpecialBankDetail.BankId;
          this.getCCList();
          this.BankCloseCashId = res.data.SpecialBankDetail.BankCloseCashId;
          this.registerSummaryForm.controls['BankCloseCashId'].setValue(res.data.SpecialBankDetail.BankCloseCashId);
          this.registerStartingGT = res.data.StartingGT;
          this.registerSummaryForm.controls['registerStartingGT'].setValue(res.data.SpecialBankDetail.StartingGT);
          this.registerEndingGT = res.data.EndingGT;
          this.registerSummaryForm.controls['registerEndingGT'].setValue(res.data.SpecialBankDetail.EndingGT);
          
          //this.registerZNumber = res.data.RegisterZNumber;
          var RegisterZNumber=null;
          
          if (res.data.SpecialBankDetail.RegisterZNumber=='null' || res.data.SpecialBankDetail.RegisterZNumber== null) {
          // RegisterZNumber = res.data.RegisterZNumber; 
          }
          else{
            RegisterZNumber = res.data.SpecialBankDetail.RegisterZNumber;
          }
          
          this.registerSummaryForm.controls['registerZNumber'].setValue(RegisterZNumber);
          
          this.registerTranx = res.data.SpecialBankDetail.RegisterTranx;
          this.registerSummaryForm.controls['registerTranx'].setValue(res.data.SpecialBankDetail.RegisterTranx);
          this.registerNoSales = res.data.SpecialBankDetail.RegisterNoSales;
          this.registerSummaryForm.controls['registerNoSales'].setValue(res.data.SpecialBankDetail.RegisterNoSales);
          this.registerReading = res.data.SpecialBankDetail.Sales;
          this.registerSummaryForm.controls['registerReading'].setValue(res.data.SpecialBankDetail.Sales);
          
          //this.difference = 0;
          this.totalPickup = res.data.SpecialBankDetail.TotalPickup;
          this.salesSummaryForm.controls['totalPickup'].setValue(res.data.SpecialBankDetail.TotalPickup);
          this.totalDrawer = res.data.SpecialBankDetail.TotalDrawer;
          this.salesSummaryForm.controls['totalDrawer'].setValue(res.data.SpecialBankDetail.TotalDrawer);
          this.adjustments = res.data.SpecialBankDetail.Adjustment;
          this.salesSummaryForm.controls['adjustments'].setValue(res.data.SpecialBankDetail.Adjustment);
          this.adjustmentComments = res.data.SpecialBankDetail.AdjustmentComment;
          var AdjustmentComment = null;
          if (res.data.SpecialBankDetail.AdjustmentComment!='null') {
            AdjustmentComment=res.data.SpecialBankDetail.AdjustmentComment;
          }
          this.salesSummaryForm.controls['adjustmentComments'].setValue(AdjustmentComment);
         
          this.ccAdjustments = res.data.SpecialBankDetail.CCAdjustment;
          this.salesSummaryForm.controls['ccAdjustments'].setValue(res.data.SpecialBankDetail.CCAdjustment);
          this.ccAdjustmentComments = res.data.SpecialBankDetail.CCAdjustmentComment;
          var CCAdjustmentComment = null;
          if (res.data.SpecialBankDetail.CCAdjustmentComment!='null') {
            CCAdjustmentComment = res.data.SpecialBankDetail.CCAdjustmentComment;
          }
          this.salesSummaryForm.controls['ccAdjustmentComments'].setValue(CCAdjustmentComment);
          
          this.overShort = res.data.SpecialBankDetail.OverShort;
          this.salesSummaryForm.controls['overShort'].setValue(res.data.SpecialBankDetail.OverShort);
          
          this.overShortComments = res.data.SpecialBankDetail.OverShortComment;
          var overShortComments=null;
          if (res.data.SpecialBankDetail.OverShortComment!='') {
            overShortComments=res.data.SpecialBankDetail.OverShortComment;
          }
          this.salesSummaryForm.controls['overShortComments'].setValue(overShortComments);
          
          //bank payment forms-----------------------
          res.data.SpecialBankDetail.BankPaymentForms.forEach(function (value) {
            //console.log(value);
            let bankFormsData =  {} as paymentFormfields;
                
                bankFormsData.type='text';
                bankFormsData.label=value.PaymentFormName;
                bankFormsData.name=value.PaymentFormId;
                bankFormsData.value=value.Amount;
                
                BankPaymentFormsArray.push(bankFormsData);
          });
          
          this.paymentFormfields=BankPaymentFormsArray;
          //console.log(BankPaymentFormsArray);
          for (let i = 0; i < BankPaymentFormsArray.length; i++) {
           // console.log(BankPaymentFormsArray[i].name);
           this.denominationsForm.addControl(BankPaymentFormsArray[i].name, new FormControl(BankPaymentFormsArray[i].Amount));
           this.denominationsForm.controls[BankPaymentFormsArray[i].name].setValue(BankPaymentFormsArray[i].value); 
          }
          //-----------------------------------------
          //Event Details----------------------------
          if (res.data.Events.length > 0) {
           var eventDataArr=[];
           res.data.Events.forEach(function (value) {
            let eventData =  {} as Event;
                eventData.EventId=value.EventId;
                eventData.Vendor=value.Vendor;
                eventData.Event=value.Event;
                eventData.Date=moment(value.Date).format(dateFormat);
                eventData.AmountReceived=value.AmountReceived;
                eventData.Notes=value.Notes;
                eventDataArr.push(eventData);
           });
           //console.log(eventDataArr);
           for (let i = 0; i < eventDataArr.length; i++) {
            this.addEventField(i,eventDataArr[i].EventId,eventDataArr[i].Vendor,eventDataArr[i].Event,eventDataArr[i].Date,eventDataArr[i].AmountReceived,eventDataArr[i].Notes);
            //this.eventForm.controls[OtherChargeDetailsArray[i].name].setValue(OtherChargeDetailsArray[i].value); 
           }
          }
          else{
           this.addEventField(0,0,'','','','','');
          }
          //-----------------------------------------
          //otherChargeFormfields--------------------
          var otherChargesTotal=0;
          res.data.SpecialBankDetail.BankOtherChargeDetails.forEach(function (value) {
            //BankOtherChargeDetailsArray
            let otherChargeFormsData =  {} as otherChargeFormfields;
                otherChargeFormsData.type='text';
                otherChargeFormsData.label=value.PaymentFormName;
                otherChargeFormsData.name=value.PaymentFormId;
                otherChargeFormsData.value=value.Amount;
                
                otherChargesTotal=otherChargesTotal+value.Amount;
                
                OtherChargeDetailsArray.push(otherChargeFormsData);
          });
          this.otherChargesTotal=+otherChargesTotal;
          this.otherChargeFormfields=OtherChargeDetailsArray;
          for (let i = 0; i < OtherChargeDetailsArray.length; i++) {
           this.otherChargesForm.addControl(OtherChargeDetailsArray[i].name, new FormControl(OtherChargeDetailsArray[i].Amount));
           this.otherChargesForm.controls[OtherChargeDetailsArray[i].name].setValue(OtherChargeDetailsArray[i].value); 
          }
          //-----------------------------------------
          
          //CardHandheld-----------------------------
          //var totalCardHandheldTotal=0;
          //res.data.SpecialBankDetail.CardHandheldDetails.forEach(function (value) {
          //  let cardhandheldFormsData =  {} as cardHandheldDetails;
          //    cardhandheldFormsData.AmexAmount = value.AmexAmount;
          //    cardhandheldFormsData.BatchoutNumber = value.BatchoutNumber;
          //    cardhandheldFormsData.CardHandheldTidNumber = value.CardHandheldTidNumber;
          //    cardhandheldFormsData.CloseCardHandheldID = value.CloseCardHandheldID;
          //    cardhandheldFormsData.DebitAmount = value.DebitAmount;
          //    cardhandheldFormsData.DiscoverAmount = value.DiscoverAmount;
          //    cardhandheldFormsData.MasterCardAmount = value.MasterCardAmount;
          //    cardhandheldFormsData.VisaAmount = value.VisaAmount;
          //    cardhandheldFormsData.TotalCardHandheldAmount = value.TotalCardHandheldAmount;
          //    totalCardHandheldTotal = totalCardHandheldTotal+value.TotalCardHandheldAmount;
          //    cardhandheldFormsDataArray.push(cardhandheldFormsData);
          //   
          //});
          //this.cardHandheldTotal=totalCardHandheldTotal;
          if (res.data.SpecialBankDetail.CardHandheldDetails.length > 0) {
          this.cardHandheldform.controls['CardHandheldTidNumber'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].CardHandheldTidNumber);
          this.cardHandheldform.controls['BatchoutNumber'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].BatchoutNumber);
          this.cardHandheldform.controls['VisaAmount'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].VisaAmount);
          this.cardHandheldform.controls['MasterCardAmount'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].MasterCardAmount);
          this.cardHandheldform.controls['DiscoverAmount'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].DiscoverAmount);
          this.cardHandheldform.controls['AmexAmount'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].AmexAmount);
          this.cardHandheldform.controls['DebitAmount'].setValue(res.data.SpecialBankDetail.CardHandheldDetails[0].DebitAmount);
          this.cardHandheldTotal=res.data.SpecialBankDetail.CardHandheldDetails[0].TotalCardHandheldAmount;
         } 
          
        //Denomination-----------------------------
         this.denominationsForm.controls['hundreds'].setValue(res.data.SpecialBankDetail.Denomination.HundredsAmount);
         this.denominationsForm.controls['fifties'].setValue(res.data.SpecialBankDetail.Denomination.FiftiesAmount);
         this.denominationsForm.controls['twenties'].setValue(res.data.SpecialBankDetail.Denomination.TwentiesAmount);
         this.denominationsForm.controls['tens'].setValue(res.data.SpecialBankDetail.Denomination.TensAmount);
         this.denominationsForm.controls['fives'].setValue(res.data.SpecialBankDetail.Denomination.FivesAmount);
         this.denominationsForm.controls['twos'].setValue(res.data.SpecialBankDetail.Denomination.TwosAmount);
         this.denominationsForm.controls['ones'].setValue(res.data.SpecialBankDetail.Denomination.OnesAmount);
         this.denominationsForm.controls['quarters'].setValue(res.data.SpecialBankDetail.Denomination.QuartersAmount);
         this.denominationsForm.controls['dimes'].setValue(res.data.SpecialBankDetail.Denomination.DimesAmount);
         this.denominationsForm.controls['nickles'].setValue(res.data.SpecialBankDetail.Denomination.NickelsAmount);
         this.denominationsForm.controls['pennies'].setValue(res.data.SpecialBankDetail.Denomination.PenniesAmount);
         this.denominationsForm.controls['change'].setValue(res.data.SpecialBankDetail.Denomination.ChangeAmount);
         this.denominationsTotalAmount=res.data.SpecialBankDetail.Denomination.TotalCash;
        //-----------------------------------------
        //Denomination Count-----------------------
        this.denominationsForm.controls['hundredsCount'].setValue(res.data.SpecialBankDetail.DenominationCount.HundredsCount);
        this.denominationsForm.controls['fiftiesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.FiftiesCount);
        this.denominationsForm.controls['twentiesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.TwentiesCount);
        this.denominationsForm.controls['tensCount'].setValue(res.data.SpecialBankDetail.DenominationCount.TensCount);
        this.denominationsForm.controls['fivesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.FivesCount);
        this.denominationsForm.controls['twosCount'].setValue(res.data.SpecialBankDetail.DenominationCount.TwosCount);
        this.denominationsForm.controls['onesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.OnesCount);
        this.denominationsForm.controls['quartersCount'].setValue(res.data.SpecialBankDetail.DenominationCount.QuartersCount);
        this.denominationsForm.controls['dimesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.DimesCount);
        this.denominationsForm.controls['nicklesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.NickelsCount);
        this.denominationsForm.controls['penniesCount'].setValue(res.data.SpecialBankDetail.DenominationCount.PenniesCount);
        //-----------------------------------------
        //POS--------------------------------------
          let posFormsData =  {} as POS;
          posFormsData.BatchoutNumber=res.data.SpecialBankDetail.PosDetails.BatchoutNumber;
          posFormsData.AmexAmount=res.data.SpecialBankDetail.PosDetails.AmexAmount;
          posFormsData.DebitAmount=res.data.SpecialBankDetail.PosDetails.DebitAmount;
          posFormsData.DiscoverAmount=res.data.SpecialBankDetail.PosDetails.DiscoverAmount;
          posFormsData.MasterCardAmount=res.data.SpecialBankDetail.PosDetails.MasterCardAmount;
          posFormsData.VisaAmount=res.data.SpecialBankDetail.PosDetails.VisaAmount;
          posFormsData.TotalCardHandheldAmount=res.data.SpecialBankDetail.PosDetails.TotalCardHandheldAmount;
          
          //set form value----------------------------
          this.posName=res.data.SpecialBankDetail.PosDetails.POSName;
          this.posForm.controls['posId'].setValue(res.data.SpecialBankDetail.PosDetails.PosId);
          this.posForm.controls['BatchoutNumber'].setValue(res.data.SpecialBankDetail.PosDetails.BatchoutNumber);
          this.posForm.controls['VisaAmount'].setValue(res.data.SpecialBankDetail.PosDetails.VisaAmount);
          this.posForm.controls['MasterCardAmount'].setValue(res.data.SpecialBankDetail.PosDetails.MasterCardAmount);
          this.posForm.controls['DiscoverAmount'].setValue(res.data.SpecialBankDetail.PosDetails.DiscoverAmount);
          this.posForm.controls['AmexAmount'].setValue(res.data.SpecialBankDetail.PosDetails.AmexAmount);
          this.posForm.controls['DebitAmount'].setValue(res.data.SpecialBankDetail.PosDetails.DebitAmount);
          
          this.totalPOS = res.data.SpecialBankDetail.PosDetails.TotalCardHandheldAmount;
          //------------------------------------------
          //console.log(posFormsData);
       
        //-----------------------------------------
        //Cashier----------------------------------
        res.data.SpecialBankDetail.Cashier.forEach(function (value) {
          let cashierData =  {} as PickupEmployee;
          cashierData.FK_Employee_ID=value.UserId;
          cashierArr.push(value.UserId);
          cashierDataArray.push(cashierData);
        });
        this.cashierList=cashierDataArray;
        this.cashierArr=cashierArr;
        //console.log(this.cashierList);
        //-----------------------------------------
        
      }
      else{
        this.getCCList();
        this.progressBarHide();
       //console.log('Error');
      }
      //console.log(bankDetailsArray);
    })
     
  }
  //----------------------------------------------------------
  
   //Get POS List----------------------------------
  getPosList(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const posDetails = {
        'branchId': branchId,
        'authUserId': this.authUserId
    }
    var posArray= [];
    //call api------------
      this.BankServiceData.getPOSList(posDetails).subscribe(res=>{
       //console.log(res);
         if(res.status=='success') {
          res.data.forEach(function (value) {
            let posData =  {} as POS;
            
            posData.PosId = value.POSId;
            posData.posName = value.POSName;
            
            posArray.push(posData);
           
            });
         
            this.posOptions = posArray;
            
        } 
      });
    //-------------------
  }
  //----------------------------------------------------------
  //Get POS List----------------------------------
  getCCList(){
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    const ccDetails = {
        'branchId': branchId,
        'authUserId': this.authUserId,
        'bankid': this.BankId,
        'bankIssueDate': JSON.parse(sessionStorage.getItem("bankIssueDate"))
    }
   // console.log(ccDetails);
    var ccArray= [];
    //call api------------
      this.BankServiceData.getCCList(ccDetails).subscribe(res=>{
       //console.log(res);
       
         if(res.status=='success') {
          res.data.forEach(function (value) {
            let ccData =  {} as CC;
            ccData.CardHandheldTidNumber = value.CardHandheldTidNumber;
            ccArray.push(ccData);
           
            });
            this.ccOptions = ccArray;
        }
         
      });
    //-------------------
  }
  //----------------------------------------------------------
  
  //hundreds----------------------------------------------
  //hundredsChange Count----------------------------------
  hundredsChangeCount(hundredsCount){
    if (hundredsCount == 0) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else if (hundredsCount > 0 && CheckIsInt(hundredsCount)) {
     this.noError=false;
     this.hundredsBillError=false;
     this.hundredsError=false;
     this.hundreds=(hundredsCount * 100);
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    else{
     this.noError=true;
     this.hundredsBillError=true;
     this.hundreds=0;
     this.denominationsForm.controls['hundreds'].setValue(this.hundreds);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //hundredsChange Amount---------------------------------
  hundredsChangeAmount(hundredsAmount){
    var hundredsMod= hundredsAmount % 100;
    var hundredsMod= hundredsAmount % 100;
    if (hundredsAmount == 0) {
     this.hundredsError=false;
     this.noError=false;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else if (hundredsAmount >= 100 && CheckIsInt(hundredsAmount) && hundredsMod==0) {
     this.hundredsError=false;
     this.hundredsBillError=false;
     this.noError=false;
     this.hundredsCount=(hundredsAmount / 100);
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    else{
     this.hundredsError=true;
     this.noError=true;
     this.hundredsCount=0;
     this.denominationsForm.controls['hundredsCount'].setValue(this.hundredsCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  //fifties----------------------------------------------
  //fiftiesChange Count----------------------------------
  fiftiesChangeCount(fiftiesCount){
     if (fiftiesCount == 0) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else if(fiftiesCount > 0 && CheckIsInt(fiftiesCount)) {
     this.noError=false;
     this.fiftiesBillError=false;
     this.fiftiesError=false;
     this.fifties=(fiftiesCount * 50);
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    else{
     this.noError=true;
     this.fiftiesBillError=true;
     this.fifties=0;
     this.denominationsForm.controls['fifties'].setValue(this.fifties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fiftiesChange Amount---------------------------------
  fiftiesChangeAmount(fiftiesAmount){
    var fiftiesMod= fiftiesAmount % 50;
     if (fiftiesAmount == 0) {
     this.fiftiesError=false;
     this.noError=false;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    if (fiftiesAmount >= 50 && CheckIsInt(fiftiesAmount) && fiftiesMod==0) {
     this.fiftiesError=false;
     this.fiftiesBillError=false;
     this.noError=false;
     this.fiftiesCount=(fiftiesAmount / 50);
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    else{
     this.fiftiesError=true;
     this.noError=true;
     this.fiftiesCount=0;
     this.denominationsForm.controls['fiftiesCount'].setValue(this.fiftiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twenties----------------------------------------------
  //twentiesChange Count----------------------------------
  twentiesChangeCount(twentiesCount){
    if (twentiesCount == 0) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else if(twentiesCount > 0 && CheckIsInt(twentiesCount)) {
     this.noError=false;
     this.twentiesBillError=false;
     this.twentiesError=false;
     this.twenties=(twentiesCount * 20);
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    else{
     this.noError=true;
     this.twentiesBillError=true;
     this.twenties=0;
     this.denominationsForm.controls['twenties'].setValue(this.twenties);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twentiesChange Amount---------------------------------
  twentiesChangeAmount(twentiesAmount){
    var twentiesMod= twentiesAmount % 20;
    if (twentiesAmount == 0) {
     this.twentiesError=false;
     this.noError=false;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else if (twentiesAmount >= 20 && CheckIsInt(twentiesAmount) && twentiesMod==0) {
     this.twentiesError=false;
     this.twentiesBillError=false;
     this.noError=false;
     this.twentiesCount=(twentiesAmount / 20);
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    else{
     this.twentiesError=true;
     this.noError=true;
     this.twentiesCount=0;
     this.denominationsForm.controls['twentiesCount'].setValue(this.twentiesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //tens--------------------------------------------------
  //tensChange Count--------------------------------------
  tensChangeCount(tensCount){
    if (tensCount == 0) {
     this.noError=false;
     this.tensBillError=false;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else if(tensCount > 0 && CheckIsInt(tensCount)) {
     this.noError=false;
     this.tensBillError=false;
     this.tensError=false;
     this.tens=(tensCount * 10);
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    else{
     this.noError=true;
     this.tensBillError=true;
     this.tens=0;
     this.denominationsForm.controls['tens'].setValue(this.tens);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //tensChange Amount-------------------------------------
  tensChangeAmount(tensAmount){
    var tensMod= tensAmount % 10;
    if (tensAmount == 0) {
     this.tensError=false;
     this.noError=false;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else if (tensAmount >= 10 && CheckIsInt(tensAmount) && tensMod==0) {
     this.tensError=false;
     this.tensBillError=false;
     this.noError=false;
     this.tensCount=(tensAmount / 10);
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    else{
     this.tensError=true;
     this.noError=true;
     this.tensCount=0;
     this.denominationsForm.controls['tensCount'].setValue(this.tensCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //fives-------------------------------------------------
  //fivesChange Count-------------------------------------
  fivesChangeCount(fivesCount){
    if (fivesCount == 0) {
     this.noError=false;
     this.fivesBillError=false;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else if(fivesCount > 0 && CheckIsInt(fivesCount)) {
     this.noError=false;
     this.fivesBillError=false;
     this.fivesError=false;
     this.fives=(fivesCount * 5);
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    else{
     this.noError=true;
     this.fivesBillError=true;
     this.fives=0;
     this.denominationsForm.controls['fives'].setValue(this.fives);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //fivesChange Amount-------------------------------------
  fivesChangeAmount(fivesAmount){
    var fivesMod= fivesAmount % 5;
    if (fivesAmount == 0) {
     this.fivesError=false;
     this.noError=false;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else if (fivesAmount >= 5 && CheckIsInt(fivesAmount) && fivesMod==0) {
     this.fivesError=false;
     this.fivesBillError=false;
     this.noError=false;
     this.fivesCount=(fivesAmount / 5);
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    else{
     this.fivesError=true;
     this.noError=true;
     this.fivesCount=0;
     this.denominationsForm.controls['fivesCount'].setValue(this.fivesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //twos-------------------------------------------------
  //twosChange Count-------------------------------------
  twosChangeCount(twosCount){
    if (twosCount == 0) {
     this.noError=false;
     this.twosBillError=false;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else if(twosCount > 0 && CheckIsInt(twosCount)) {
     this.noError=false;
     this.twosBillError=false;
     this.twosError=false;
     this.twos=(twosCount * 2);
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    else{
     this.noError=true;
     this.twosBillError=true;
     this.twos=0;
     this.denominationsForm.controls['twos'].setValue(this.twos);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //twosChange Amount-------------------------------------
  twosChangeAmount(twosAmount){
    var twosMod= twosAmount % 2;
    if (twosAmount == 0) {
     this.twosError=false;
     this.noError=false;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else if (twosAmount >= 2 && CheckIsInt(twosAmount) && twosMod==0) {
     this.twosError=false;
     this.twosBillError=false;
     this.noError=false;
     this.twosCount=(twosAmount / 2);
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    else{
     this.twosError=true;
     this.noError=true;
     this.twosCount=0;
     this.denominationsForm.controls['twosCount'].setValue(this.twosCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //ones-------------------------------------------------
  //onesChange Count-------------------------------------
  onesChangeCount(onesCount){
    if(onesCount == 0) {
     this.noError=false;
     this.onesBillError=false;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else if(onesCount > 0 && CheckIsInt(onesCount)) {
     this.noError=false;
     this.onesBillError=false;
     this.onesError=false;
     this.ones=(onesCount * 1);
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    else{
     this.noError=true;
     this.onesBillError=true;
     this.ones=0;
     this.denominationsForm.controls['ones'].setValue(this.ones);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //onesChange Amount-------------------------------------
  onesChangeAmount(onesAmount){
    var onesMod= onesAmount % 1;
    if(onesAmount == 0) {
     this.onesError=false;
     this.noError=false;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else if (onesAmount >= 1 && CheckIsInt(onesAmount) && onesMod==0) {
     this.onesError=false;
     this.onesBillError=false;
     this.noError=false;
     this.onesCount=(onesAmount / 1);
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    else{
     this.onesError=true;
     this.noError=true;
     this.onesCount=0;
     this.denominationsForm.controls['onesCount'].setValue(this.onesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //quarters----------------------------------------------
  //onesChange Count--------------------------------------
  quartersChangeCount(quartersCount){
    if(quartersCount == 0) {
     this.noError=false;
     this.quartersBillError=false;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else if(quartersCount > 0 && CheckIsInt(quartersCount)) {
     this.noError=false;
     this.quartersBillError=false;
     this.quartersError=false;
     this.quarters=(quartersCount * 0.25);
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    else{
     this.noError=true;
     this.quartersBillError=true;
     this.quarters=0.00;
     this.denominationsForm.controls['quarters'].setValue(this.quarters);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //quartersChange Amount---------------------------------
  quartersChangeAmount(quartersAmount){
    var quartersMod = quartersAmount % 0.25;
    if(quartersAmount == 0) {
     this.quartersError=false;
     this.noError=false;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else if (quartersAmount >= 0.25 && quartersMod==0) {
     this.quartersError=false;
     this.quartersBillError=false;
     this.noError=false;
     this.quartersCount=(quartersAmount / 0.25);
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    else{
     this.quartersError=true;
     this.noError=true;
     this.quartersCount=0;
     this.denominationsForm.controls['quartersCount'].setValue(this.quartersCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //dimes-------------------------------------------------
  //dimesChange Count-------------------------------------
  dimesChangeCount(dimesCount){
    if(dimesCount == 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else if(dimesCount > 0) {
     this.noError=false;
     this.dimesBillError=false;
     this.dimesError=false;
     var dimesAmt=(dimesCount * 0.10);
     this.dimes=parseFloat(dimesAmt.toFixed(2));
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    else{
     this.noError=true;
     this.dimesBillError=true;
     this.dimes=0.00;
     this.denominationsForm.controls['dimes'].setValue(this.dimes);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //dimesChange Amount-------------------------------------
  dimesChangeAmount(dimesAmount){
    var dimesN= dimesAmount * 100;
    var n  = dimesN.toFixed(2);
    var dimesMod = parseFloat(n) % 10;
    if(dimesAmount == 0) {
     this.dimesError=false;
     this.noError=false;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }     
    else if (dimesAmount >= 0.10 && dimesMod==0) {
     this.dimesError=false;
     this.dimesBillError=false;
     this.noError=false;
     this.dimesCount=(dimesAmount / 0.10);
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    else{
     this.dimesError=true;
     this.noError=true;
     this.dimesCount=0;
     this.denominationsForm.controls['dimesCount'].setValue(this.dimesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //nickles-----------------------------------------------
  //nicklesChange Count-----------------------------------
  nicklesChangeCount(nicklesCount){
    if(nicklesCount == 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }  
    else if(nicklesCount > 0) {
     this.noError=false;
     this.nicklesBillError=false;
     this.nicklesError=false;
     var nicklesAmt=(nicklesCount * 0.05);
     this.nickles=parseFloat(nicklesAmt.toFixed(2));
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    else{
     this.noError=true;
     this.nicklesBillError=true;
     this.nickles=0.00;
     this.denominationsForm.controls['nickles'].setValue(this.nickles);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //nicklesChange Amount-----------------------------------
  nicklesChangeAmount(nicklesAmount){
    var nicklesN= nicklesAmount * 100;
    var n  = nicklesN.toFixed(2);
    var nicklesMod = parseFloat(n) % 5;
    if(nicklesAmount == 0) {
     this.nicklesError=false;
     this.noError=false;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }    
    else if (nicklesAmount >= 0.05 && nicklesMod==0) {
     this.nicklesError=false;
     this.nicklesBillError=false;
     this.noError=false;
     this.nicklesCount=(nicklesAmount / 0.05);
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    else{
     this.nicklesError=true;
     this.noError=true;
     this.nicklesCount=0;
     this.denominationsForm.controls['nicklesCount'].setValue(this.nicklesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  //------------------------------------------------------
  
  //pennies-----------------------------------------------
  //penniesChange Count-----------------------------------
  penniesChangeCount(penniesCount){
    if (penniesCount == 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else if(penniesCount > 0) {
     this.noError=false;
     this.penniesBillError=false;
     this.penniesError=false;
     var penniesAmt=(penniesCount * 0.01);
     this.pennies=parseFloat(penniesAmt.toFixed(2));
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    else{
     this.noError=true;
     this.penniesBillError=true;
     this.pennies=0.00;
     this.denominationsForm.controls['pennies'].setValue(this.pennies);
    }
    this.denominationsFormSubmit();
  }
  //-------------------------------------------------------
  //penniesChange Amount-----------------------------------
  penniesChangeAmount(penniesAmount){
    var penniesN= penniesAmount * 100;
    var n  = penniesN.toFixed(2);
    var penniesMod = parseFloat(n) % 1;
    if (penniesAmount == 0) {
     this.penniesError=false;
     this.noError=false;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }    
    else if (penniesAmount >= 0.05 && penniesMod==0) {
     this.penniesError=false;
     this.penniesBillError=false;
     this.noError=false;
     this.penniesCount=(penniesAmount / 0.01);
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    else{
     this.penniesError=true;
     this.noError=true;
     this.penniesCount=0;
     this.denominationsForm.controls['penniesCount'].setValue(this.penniesCount);
    }
    this.denominationsFormSubmit();
  }
  //------------------------------------------------------
  
    
  //DenominationsFormSubmit-------------------------------
  denominationsFormSubmit(){
   // console.log(this.denominationsForm.value);
    if (!this.noError) {
    var frmVal=JSON.stringify(this.denominationsForm.value);
    var frmValObj=JSON.parse(frmVal);
    var denominationDetailArray = [];
    //console.log(frmValObj);
    let sum = 0;
    var countVariable = ["dimesCount","fiftiesCount","fivesCount","nicklesCount","onesCount","penniesCount","quartersCount","tensCount","twentiesCount","twosCount","hundredsCount"];
    for (var key in frmValObj) {
      if (frmValObj.hasOwnProperty(key)) {
        if (!countVariable.includes(key)) {
          //console.log(key + " -> " + frmValObj[key]);
          let amount=frmValObj[key];
          sum = sum + (+amount);
        }
          
      }
    }
    //Collection data---------------
    let denominationData =  {} as DenominationDetail;
      denominationData.HundredsCount = frmValObj['hundredsCount'];
      denominationData.FiftiesCount = frmValObj['fiftiesCount'];
      denominationData.TwentiesCount = frmValObj['twentiesCount'];
      denominationData.TensCount = frmValObj['tensCount'];
      denominationData.FivesCount = frmValObj['fivesCount'];
      denominationData.TwosCount = frmValObj['twosCount'];
      denominationData.OnesCount = frmValObj['onesCount'];
      denominationData.QuartersCount = frmValObj['quartersCount'];
      denominationData.NickelsCount = frmValObj['nicklesCount'];
      denominationData.DimesCount = frmValObj['dimesCount'];
      denominationData.PenniesCount = frmValObj['penniesCount'];
      denominationData.ChangeAmount = frmValObj['change'];
      
     // denominationDetailArray.push(denominationData);
      this.denominationDetailArray=denominationData;
    //------------------------------
    //Collecting payments form data---------
    var frmVal2=JSON.stringify(this.paymentFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var paymentFormSubmitDetailsArr=[];
   
    for (var key2 in frmValObj2) {
      let paymentFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      paymentFormSubmitDetails.PaymentFormId=keyName;
      paymentFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      paymentFormSubmitDetailsArr.push(paymentFormSubmitDetails);
    }
    this.BankPaymentFormsArray=paymentFormSubmitDetailsArr;
    //console.log(paymentFormSubmitDetailsArr);
    //--------------------------------------
    
    this.denominationsTotalAmount=+sum.toFixed(2);
    this.calculateOverShort();
    this.errorArray.splice(2, 1); 
    }
    else{
      this.errorArray[2]='Some error in denominations';
    }
    
  }
  //------------------------------------------------------
  //Register Summary Form Submit--------------------------
  registerSummaryFormSubmit(){
    //console.log(this.registerSummaryForm.valid);
    var registerStartingGT=this.registerSummaryForm.value.registerStartingGT;
    this.registerStartingGTError=false;
    if (registerStartingGT === null) {
     this.registerStartingGTError=true; 
    }
    
    var registerEndingGT=this.registerSummaryForm.value.registerEndingGT;
    this.registerEndingGTError=false;
    if (registerEndingGT === null) {
      this.registerEndingGTError=true;
    }
    
    var registerZNumber = this.registerSummaryForm.value.registerZNumber;
    this.registerZNumberError=false;
    if (registerZNumber == "" || registerZNumber === 'null') {
      this.registerZNumberError=true;
    }
    
    var registerTranx = this.registerSummaryForm.value.registerTranx;
    
    this.registerTranxError=false;
    if (registerTranx === null) {
      this.registerTranxError=true;
    }
    
    var registerNoSales = this.registerSummaryForm.value.registerNoSales;
    this.registerNoSalesError=false;
    if (registerNoSales === null) {
      this.registerNoSalesError=true;
    }
    
    var registerReading = this.registerSummaryForm.value.registerReading;
    this.registerReadingError=false;
    if (registerReading === null) {
      this.registerReadingError=true;
    }
    
    if(this.registerSummaryForm.valid) {
      
      if (registerStartingGT > registerEndingGT) {
        this.registerStartingGTError=true;
        this.errorArray[9]="Register Starting GT Can't be grater than Ending GT";
      }
      else{
       this.errorArray.splice(9, 1);
       this.registerStartingGTError=false; 
      }
      this.errorArray.splice(8, 1);
    }
    else{
     this.errorArray[8]='Please fill all register summary fields';
    }
    
    this.calculateOverShort();
   
  }
  //------------------------------------------------------
  //Sales Summary Form Submit-----------------------------
  salesSummaryFormSubmit(){
     
    //console.log(this.salesSummaryForm);
    var frmVal=JSON.stringify(this.salesSummaryForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var cashierDataArr=[];
    
    for (var key in frmValObj['cashier']) {
      let cashierData =  {} as Employee;
      cashierData.UserId = frmValObj['cashier'][key].FK_Employee_ID;
      cashierDataArr.push(cashierData);
    }
    
    //ccAdjustmentCommentError
    //CC adjustment comment null checking-----------------
    var ccAdjustmentAmt=this.salesSummaryForm.value.ccAdjustments;
    var ccAdjustmentComment=this.salesSummaryForm.value.ccAdjustmentComments;
    if ((ccAdjustmentAmt > 0 || ccAdjustmentAmt < 0 && ccAdjustmentAmt !='') && (ccAdjustmentComment=='' || ccAdjustmentComment==null)) {
      this.ccAdjustmentCommentError=true;
      this.errorArray[6]='Please enter CC Adjustment comment';
    }
    else{
     this.errorArray.splice(6, 1); 
     this.ccAdjustmentCommentError=false;
    }
    //----------------------------------------------------
    //Adjustment Comment null checking--------------------
    var adjustmentAmt=this.salesSummaryForm.value.adjustments;
    var adjustmentComment=this.salesSummaryForm.value.adjustmentComments;
    if ((adjustmentAmt > 0 || adjustmentAmt < 0 && adjustmentAmt !='') && (adjustmentComment=='' || adjustmentComment==null)) {
      this.adjustmentCommentError=true;
      this.errorArray[5]='Please enter Adjustment comment';
    }
    else{
     this.errorArray.splice(5, 1); 
     this.adjustmentCommentError=false;
    }
    //----------------------------------------------------
   //overshort null checking------------------------------
    var overShortAmt=this.salesSummaryForm.value.overShort;
    var overShortComment=this.salesSummaryForm.value.overShortComments;
    if ((overShortAmt >= 10) && (overShortComment=='' || overShortComment==null)) {
      this.overShortCommentError=true;
      this.errorArray[0]='Please enter over/short comments';
    }
    else{
     this.errorArray.splice(0, 1); 
     this.overShortCommentError=false;
    }
    //----------------------------------------------------
   
    this.cashierDataArray=cashierDataArr;
    this.calculateOverShort();
   
  }
  //------------------------------------------------------
  //POS Form submit---------------------------------------
  posFormSubmit(){
    //console.log(this.posForm.value);
    let posTotal=0;
    
    posTotal=(this.posForm.value.VisaAmount + this.posForm.value.MasterCardAmount + this.posForm.value.DiscoverAmount + this.posForm.value.AmexAmount + this.posForm.value.DebitAmount);
    
    //For collecting data----------
    var posDataArray=[];
    let posData =  {} as POS;
        
      if (this.posForm.value.BatchoutNumber == null && posTotal > 0) {
        this.errorArray[1]='Please enter POS Batch#';
        this.posBatchoutNumberError=true;
        //this.noError=true;
      }
      else{
        this.errorArray.splice(1, 1); 
        this.posBatchoutNumberError=false;
        //this.noError=false;
      }
      
      if (this.posForm.value.BatchoutNumber != null && posTotal > 0 && this.posForm.value.posId.length ===0 ) {
          var element = document.getElementById('pos-id');
          element.classList.add("error-input");
          
          this.errorArray[10]='Please select POS';
          this.posSelectError=true;
          
       }
       else{
          var element = document.getElementById('pos-id');
          element.classList.remove("error-input");
          this.posSelectError=false;
          this.errorArray.splice(10, 1); 
       }
     
      posData.PosId = this.posForm.value.posId;
      posData.BatchoutNumber = this.posForm.value.BatchoutNumber;
      posData.VisaAmount = this.posForm.value.VisaAmount;
      posData.MasterCardAmount = this.posForm.value.MasterCardAmount;
      posData.DiscoverAmount = this.posForm.value.DiscoverAmount;
      posData.AmexAmount = this.posForm.value.AmexAmount;
      posData.DebitAmount = this.posForm.value.DebitAmount;
     
      posData.OtherChargeAmount=0;
      posData.TotalPOSAmount=posTotal;
      posDataArray.push(posData);
      this.posDetails=posDataArray;
    
    //console.log(this.posDetails);
    //-----------------------------
   
    this.totalPOS=posTotal;
    this.posForm.controls['TotalCardHandheldAmount'].setValue(this.totalPOS);
    
    this.calculateOverShort();
    
  }
  //------------------------------------------------------
  //CardHandheld Form Submit------------------------------
  cardHandheldFormSubmit(){
    
   //console.log(this.cardHandheldform);
    let ccErrorCount=0;
    let ccAmtErrorCount=0;
    var cardHandheldFieldsArray=[];
    let cardHandheldTotal=0;
  
        //For collecting data---------------
        
        cardHandheldTotal=(+this.cardHandheldform.value.VisaAmount + this.cardHandheldform.value.MasterCardAmount + this.cardHandheldform.value.DiscoverAmount + this.cardHandheldform.value.AmexAmount + this.cardHandheldform.value.DebitAmount);

             if (this.cardHandheldform.value.CardHandheldTidNumber.length === 0 && cardHandheldTotal > 0) {
                var element2 = document.getElementById('cc-tid');
                element2.classList.add("error-input");
                
                this.errorArray[11]='Please select CC-Handheld TID';
                this.ccHandheldTIDError=true;
               // ccErrorCount++;
             }
             else{
                var element2 = document.getElementById('cc-tid');
                element2.classList.remove("error-input");
                this.ccHandheldTIDError=false;
                 this.errorArray.splice(11, 1); 
             }
             
             
            if (this.cardHandheldform.value.BatchoutNumber.length === 0 && cardHandheldTotal > 0) {
                var element = document.getElementById('cc-batch-out');
                element.classList.add("error-input");
                ccErrorCount++;
             }
             else{
                var element = document.getElementById('cc-batch-out');
                element.classList.remove("error-input");
             }
             
             if (this.cardHandheldform.value.BatchoutNumber.length!== 0 && cardHandheldTotal <= 0) {
              ccAmtErrorCount++;
             }
            
            
          let cardHandheldData =  {} as cardHandheldDetails;   
          
            cardHandheldData.CardHandheldTidNumber = this.cardHandheldform.value.CardHandheldTidNumber;
            cardHandheldData.BatchoutNumber = this.cardHandheldform.value.BatchoutNumber;
            cardHandheldData.AmexAmount = this.cardHandheldform.value.AmexAmount;
            cardHandheldData.DebitAmount = this.cardHandheldform.value.DebitAmount;
            cardHandheldData.DiscoverAmount = this.cardHandheldform.value.DiscoverAmount;
            cardHandheldData.MasterCardAmount = this.cardHandheldform.value.MasterCardAmount;
            cardHandheldData.VisaAmount = this.cardHandheldform.value.VisaAmount;
            cardHandheldData.TotalCardHandheldAmount = cardHandheldTotal;
          
      
        //----------------------------------
        cardHandheldFieldsArray.push(cardHandheldData);
        
      
    //console.log('ccErrorCount:'+ccErrorCount);
    if (ccErrorCount > 0) {
     this.errorArray[3]='Please enter CC-Handheld Batch#';
     this.ccHandheldAmountError=true;
    }
    else{
       this.errorArray.splice(3, 1); 
       this.ccHandheldAmountError=false;
    }
    //console.log('ccAmtErrorCount:'+ccAmtErrorCount);
    if (ccAmtErrorCount > 0) {
      this.errorArray[4]='Please enter cc handheld details';
      this.ccHandheldBatchoutError=true;
    }
    else{
       this.errorArray.splice(4, 1); 
       this.ccHandheldBatchoutError=false;
    }
    
    this.cardHandheldFieldsDataArray=cardHandheldFieldsArray;
    this.cardHandheldTotal=cardHandheldTotal;
    this.calculateOverShort();
    
  }
  //------------------------------------------------------
  //RegisterSummary on change submit----------------------
  registerSummaryChange(){
   this.registerSummaryFormSubmit();
  }
  //------------------------------------------------------
 

  calculateOverShort(){
    var totalDrawerC=0;
    var cardHandheldTotal= +this.cardHandheldTotal;
    var totalPOS = +this.totalPOS;
    var denominationsTotalAmount= +this.denominationsTotalAmount;
    
    totalDrawerC=(cardHandheldTotal + totalPOS + denominationsTotalAmount +this.otherChargesTotal);
     
    this.salesSummaryForm.controls['totalDrawer'].setValue(totalDrawerC.toFixed(2));
     
    var totalDrawer=this.salesSummaryForm.value.totalDrawer;
    var registerReading=this.registerSummaryForm.value.registerReading;
    var overshort=(parseFloat(totalDrawer) - parseFloat(registerReading));
   
    this.salesSummaryForm.controls['overShort'].setValue(overshort.toFixed(2));
  }
  //-------------------------------------------------------
  //Other charges form submit------------------------------
  otherChargesFormSubmit(){
  //Collecting payments form data---------
    var frmVal=JSON.stringify(this.otherChargesForm.value);
    var frmValObj=JSON.parse(frmVal);
    
    var frmVal2=JSON.stringify(this.otherChargeFormfields);
    var frmValObj2=JSON.parse(frmVal2);
    var otherChargesFormSubmitDetailsArr=[];
    var otherChargesTotal=0;
    for (var key2 in frmValObj2) {
      let otherChargesFormSubmitDetails = {} as PaymentFormSubmitDetails;
      var keyName=frmValObj2[key2].name;
      //console.log("keyName: "+keyName);
      otherChargesFormSubmitDetails.PaymentFormId=keyName;
      otherChargesFormSubmitDetails.Amount=frmValObj[JSON.stringify(keyName)];
      otherChargesTotal=otherChargesTotal+frmValObj[JSON.stringify(keyName)]
      
      otherChargesFormSubmitDetailsArr.push(otherChargesFormSubmitDetails);
    }
    this.otherChargesTotal=+otherChargesTotal;
    this.OtherChargesFormsArray=otherChargesFormSubmitDetailsArr;
    
    this.calculateOverShort();
    //console.log(otherChargesFormSubmitDetailsArr);
    //--------------------------------------
  }
  //-------------------------------------------------------
  //Event Form Submit--------------------------------------
  eventFormSubmit(){
   // console.log(this.eventForm.value);
    var eventDetailsArr=[];
    
     this.eventForm.value.eventFieldsitems.forEach(function (value) {
      let eventDetails = {} as Event;
      eventDetails.Vendor=value.Vendor;
      eventDetails.Event=value.Event;
      eventDetails.Date=moment(value.Date).format(dateFormat);
      eventDetails.AmountReceived=value.AmountReceived;
      eventDetails.Notes=value.Notes;
      
      eventDetailsArr.push(eventDetails);
      });
   // console.log(eventDetailsArr);
    this.eventFormsArray=eventDetailsArr;
    
  }
  //-------------------------------------------------------
  
  //Submit all forms---------------------------------------
  submitAllForms(){
    
    this.registerSummaryFormSubmit();
    this.salesSummaryFormSubmit();
    this.denominationsFormSubmit();
    this.cardHandheldFormSubmit();
    this.posFormSubmit();
    this.otherChargesFormSubmit();
    this.eventFormSubmit();
    //console.log(this.errorArray);
    
    var Uniqerror = this.errorArray.filter((v, i, a) => a.indexOf(v) === i);
    let error=JSON.stringify(Uniqerror);
    
    
    if (this.eventForm.valid && this.registerSummaryForm.valid && this.posForm.valid && !this.noError && !this.posBatchoutNumberError && !this.overShortCommentError && !this.ccHandheldAmountError && !this.ccHandheldBatchoutError && !this.adjustmentCommentError && !this.ccAdjustmentCommentError && !this.registerStartingGTError && !this.posSelectError && !this.ccHandheldTIDError) {
    this.progressBarShow();
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    
    var cashierDetails='';
    if (this.cashierDataArray.length > 0) {
      cashierDetails=JSON.stringify(this.cashierDataArray);
    }
    var isChange=false;
    if (this.registerSummaryForm.value.BankCloseCashId > 0 && this.isChange==true) {
      isChange=true;
    }
    this.showErrorMsg=false;
    //Close bank data--------------------
    const specialBankDetails = {
      'bankId' : this.BankId,
      'bankIssueDate' :JSON.parse(sessionStorage.getItem("bankIssueDate")),
      'branchId' : branchId,
      'createBy' : this.authUserId,
      'authUserId' : this.authUserId,
      'bankCloseCashId' : this.registerSummaryForm.value.BankCloseCashId,
      'startingGT' : this.registerSummaryForm.value.registerStartingGT,
      'endingGT' : this.registerSummaryForm.value.registerEndingGT,
      'registerTranx' : this.registerSummaryForm.value.registerTranx,
      'registerNoSales' : this.registerSummaryForm.value.registerNoSales,
      'sales' : this.registerSummaryForm.value.registerReading,
      'registerZNumber' : this.registerSummaryForm.value.registerZNumber,
      'totalDrawer' : this.salesSummaryForm.value.totalDrawer,
      'adjustment' : this.salesSummaryForm.value.adjustments,
      'cCAdjustment' : this.salesSummaryForm.value.ccAdjustments,
      'adjustmentComment' : this.salesSummaryForm.value.adjustmentComments,
      'ccAdjustmentComment' : this.salesSummaryForm.value.ccAdjustmentComments,
      'overShort' : this.salesSummaryForm.value.overShort,
      'overShortComment' : this.salesSummaryForm.value.overShortComments,
      'cashierDetails' : cashierDetails,
      'cardHandheldAmount' : JSON.stringify(this.cardHandheldFieldsDataArray),
      'denominationDetail' : JSON.stringify(this.denominationDetailArray),
      'paymentFormDetails' : JSON.stringify(this.BankPaymentFormsArray),
      'isChange' : isChange,
      'otherChargeDetails' : JSON.stringify(this.OtherChargesFormsArray),
      'posDetails' : JSON.stringify(this.posDetails),
      'eventDetail' : JSON.stringify(this.eventFormsArray)
     }
    //-----------------------------------
    //console.log(specialBankDetails);
    //API call---------------------------
    
    this.BankServiceData.submitSpecialBank(specialBankDetails).subscribe(res=>{
     this.progressBarHide();
     // console.log(res);
      if(res.status=='success') {
        this.showErrorMsg=false;
        sessionStorage.setItem("dialogOpend",'false');
        this.successmessage=res.message;
        setTimeout(() => {
          this.dialogRef.close();
         }, 2000);
      }
      else{
        this.showErrorMsg=true;
        this.errormessage=res.message;
      }
    });
    
    //-----------------------------------
    }
    else{
      this.showErrorMsg=true;
      this.errormessage=error;
      if (!this.eventForm.valid){
       this.errormessage='Some error in event form';
      }
    }
  }
  //--------------------------------------------------------
  //------------------------------------------------------
  
  //Adding Event form-----------------------------------------
  get EventFields() {
    return this.eventForm.get('eventFieldsitems') as FormArray
  }
  
  addEventField(i,EventId,Vendor,Event,EventDate,AmountReceived,Notes) {
     
    this.date = new FormControl(new Date(EventDate));
   
     const EventField = this.fb.group({ 
      Vendor: [Vendor],
      Event: [Event],
      Date: [this.date.value],
      AmountReceived: [AmountReceived],
      Notes: [Notes]
    })

    this.EventFields.push(EventField)
    
    window.onload = function(){
      document.getElementById('evt_add_btn_'+i).style.display="none";
      document.getElementById('evt_remove_btn_'+i).style.display="block";
    }
  }
  //----------------------------------------------------------
  //Remove event field----------------------------------------
  removeEvent(i){
     this.EventFields.removeAt(i)
  }
  //----------------------------------------------------------
   
  //Get Employee list-----------------------------------------
  getEmployeeList(){
    
    var branchId=JSON.parse(localStorage.getItem("currentBranchId"));
    var employeeArray= [];
    
    const employeeDetails = {
      'branchId': branchId,
      'authUserId': this.authUserId
     }
     
    //call api------------
      this.getEmployeeService.getEmployeeList(employeeDetails).subscribe(res=>{
     // console.log(res);
      if(res.status=='success') {
        res.data.forEach(function (value) {
          let empData =  {} as PickupEmployee;
          empData.FK_Employee_ID = value.UserId;
          empData.name = value.Name;
          employeeArray.push(empData);
        });
       this.employees=employeeArray;
      }
      else{
       //console.log('Error');
      }
      
    })
    //----------------------------------------------------
  }
  //--------------------------------------------------------------
  //Function for checking selected Cashier---------------
    comparer(o1: PickupEmployee, o2: PickupEmployee): boolean {
      // if possible compare by object's name, and not by reference.
      return o1 && o2 ? o1.FK_Employee_ID === o2.FK_Employee_ID : o2 === o2;
    }
  //----------------------------------------------------
  
  //Check for added Cashier-------------------------------
    checkDisabledCashier(FK_Employee_ID){
      if (this.cashierArr && this.cashierArr.includes(FK_Employee_ID)) {
       return true;
      }
      else{
       return false; 
      }
    }
    //-----------------------------------------------------
  //Is any changes made-------------------------------------------
  isAmountChange(){
    this.isChange=true;
  }
  //--------------------------------
    progressBarShow(){
     this.progressShow=true;
     this.mode="indeterminate"; 
    }
    progressBarHide(){
      this.mode="determinate";
      this.progressShow=false;
    }
  //--------------------------------
  onNoClick(): void {
    this.dialogRef.close();
  }
  

  onKeydown(event: any) {
    let pincode=event.target.value;
   
    if(event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 97 && event.keyCode <= 122 ||
      event.keyCode >= 32 && event.keyCode <= 46 ) {
      return false;
    }
    if(pincode.length+1 == 2 || pincode.length+1 == 3 || pincode.length+1 == 4 || pincode.length+1 == 6) {
      if(event.keyCode < 47 || event.keyCode > 57 ) {
        return false;
      }
    }else if(event.keyCode == 47) {
      return false;
    }
  }
}
//----------------------------------------------------------

