import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankManagementComponent } from './component/bank-management/bank-management.component';
import { SharedModule } from '../../shared/shared.module';
import {BankManagementRoutingModule} from './bank-management-routing/bank-management-routing.module';
import { IssueBankListComponent } from './component/issue-bank-list/issue-bank-list.component';
import { IssueBankBulkComponent } from './component/issue-bank-bulk/issue-bank-bulk.component';
import { BankToolbarComponent } from './component/bank-toolbar/bank-toolbar.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressBarModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
//import {DialogOverviewExampleDialog} from './component/bank-management/bank-management.component';

import {NewPickup} from './component/bank-management/bank-management.component';
import {EditNewPickup} from './component/bank-summary/bank-summary.component';
import {BankEdit} from './component/bank-management/bank-management.component';
import {BankClose} from './component/bank-management/bank-management.component';
import {EditBankClose} from './component/bank-summary/bank-summary.component';
import { BankSummaryComponent } from './component/bank-summary/bank-summary.component';

import {SpecialBank} from './component/bank-management/bank-management.component';

@NgModule({
  declarations: [BankManagementComponent, IssueBankListComponent, IssueBankBulkComponent, BankToolbarComponent, NewPickup,EditNewPickup,BankEdit,BankClose,EditBankClose,BankSummaryComponent,SpecialBank],
  imports: [
    CommonModule,
    SharedModule,
    BankManagementRoutingModule,
    MatDialogModule,
    MatProgressBarModule,
    MatExpansionModule
  ],
  entryComponents: [NewPickup,EditNewPickup,BankEdit,BankClose,EditBankClose,SpecialBank]
})
export class BankManagementModule { }
