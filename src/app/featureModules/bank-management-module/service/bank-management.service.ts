import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

//httpHeaders options------------------------------------
const httpOptions = {
  headers: new HttpHeaders({
    "Accept": "application/json"
  }),
 withCredentials: true
};
//-------------------------------------------------------
//API base url-------------------------------------------
var apiUrl=environment.apiHost;
//-------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class BankManagementService {

  constructor(private http: HttpClient) { }
  
  //Get Issued Bank list-------------------
  getIssuedBankList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+ "&date="+data.date+"&bankType="+data.bankType;
    return this.http.get(apiUrl+`branch/site/register/bank?`+body,httpOptions)
  }
  //---------------------------------------
  //Get Employee list----------------------
  getEmployeeList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId;
    return this.http.get(apiUrl+`branch/employee?`+body,httpOptions)
  }
  //---------------------------------------
  //Get Open site list for bank------------
  getOpenSiteList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+"&date="+data.date;
    return this.http.get(apiUrl+`branch/site/register/bank?`+body,httpOptions)
  }
  //---------------------------------------
  //Issue Bank-----------------------------
  issueBank(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('bankIssueDetails', data.bankIssueDetails);
    formData.append('branchId', data.branchId);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('signOutAccessTypeId', data.signOutAccessTypeId);
    formData.append('employeeId', data.employeeId);
    formData.append('issueDate', data.issueDate);
    formData.append('issueTime', data.issueTime);
    formData.append('signOutIpAddress', data.signOutIpAddress);
    formData.append('signOutIMEI', data.signOutIMEI);
    formData.append('signOutAppVersion', data.signOutAppVersion);
    formData.append('userImage', data.userImage);
    
    //console.log(data);
    return this.http.post(apiUrl+`branch/site/register/bank`,formData,httpOptions)
  }
  //---------------------------------------
  //Get bank data for new pickup-----------
  //getBankDataForPickup
  getBankDataForPickup(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+"&bankIssuedDate="+data.bankIssuedDate+"&bankId="+data.bankId+"&pickupListType="+data.pickupListType;
    return this.http.get(apiUrl+`branch/site/register/bank/pickup?`+body,httpOptions)
  }
  //---------------------------------------
  //New pickup-----------------------------
  //Get payment form list------------------
  getPaymentFormList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId="+data.authUserId+"&formType=7";
    return this.http.get(apiUrl+`branch/paymentform?`+body,httpOptions)
  }
  //---------------------------------------
  //Submit new pickup data-----------------
  submitNewPickup(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('denominationDetail', data.denominationDetail);
    formData.append('paymentFormDetail', data.paymentFormDetail);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('bankIssueDate', data.bankIssueDate);
    formData.append('bankId', data.bankId);
    formData.append('registerId', data.registerId);
    formData.append('siteId', data.siteId);
    formData.append('branchId', data.branchId);
    formData.append('verifiedBy', data.verifiedBy);
    formData.append('pickupBy', data.pickupBy);
    formData.append('accessTypeId', data.accessTypeId);
    
    //console.log(data);
    return this.http.post(apiUrl+`branch/site/register/bank/pickup`,formData,httpOptions)
  }
  //---------------------------------------
  
  //Submit edit pickup data-----------------
  submitEditPickup(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('pickupId', data.pickupId);
    formData.append('denominationDetail', data.denominationDetail);
    formData.append('paymentFormDetail', data.paymentFormDetail);
    formData.append('authUserId', data.authUserId);
    formData.append('createBy', data.createBy);
    formData.append('bankIssueDate', data.bankIssueDate);
    formData.append('bankId', data.bankId);
    formData.append('registerId', data.registerId);
    formData.append('siteId', data.siteId);
    formData.append('branchId', data.branchId);
    formData.append('verifiedBy', data.verifiedBy);
    formData.append('pickupBy', data.pickupBy);
    formData.append('accessTypeId', data.accessTypeId);
    formData.append('isChange', data.isChange);
    
    //console.log(data);
    return this.http.put(apiUrl+`branch/site/register/bank/pickup`,formData,httpOptions)
  }
  //---------------------------------------
   //Get Branch wise cardhandheld list-----
   getBranchWiseCardHandheldList(data):Observable<any>{
    var body ="bankId="+data.bankId+"&bankIssueDate=" + data.bankIssueDate + "&branchId=" + data.branchId + "&authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`branch/site/register/bank/card?`+body,httpOptions)
  }
  //------------------------------------------------------------
   //Get Bank wise cardhandheld list-----
  getBankWiseCardHandheldList(data):Observable<any>{
    var body = "bankId="+data.bankId+"&bankIssueDate=" + data.bankIssueDate + "&branchId=" + data.branchId + "&authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`branch/site/register/bank?`+body,httpOptions)
  }
  //------------------------------------------------------------
  //Edit bank---------------------------------------------------
  //Get site list----------------------------------
  getBranchWiseSiteList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId + "&siteListType=3";
    
    return this.http.get(apiUrl+`branch/site?`+body,httpOptions)
  }
  //-----------------------------------------------
  //Get Register list------------------------------
  getSiteWiseRegisterList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId + "&siteId=" +data.siteId+ "&registerType=3";
    
    return this.http.get(apiUrl+`branch/site/register?`+body,httpOptions)
  }
  //-----------------------------------------------
   //submit edit bank data--------------------------
    editBank(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    
    formData.append('cashierDetails', data.cashierDetails);
    formData.append('cardHandheldTidIdList', data.cardHandheldTidIdList);
    formData.append('authUserId', data.authUserId);
    formData.append('updateBy', data.updateBy);
    formData.append('bankIssueDate', data.bankIssueDate);
    formData.append('bankId', data.bankId);
    formData.append('registerId', data.registerId);
    formData.append('siteId', data.siteId);
    formData.append('branchId', data.branchId);
    
    //console.log(data);
    return this.http.put(apiUrl+`branch/site/register/bank`,formData,httpOptions)
  }
  //------------------------------
  //------------------------------------------------------------
  //Close Bank--------------------------------------------------
  //Get Close Bank Details----------------
  getCloseBankData(data):Observable<any>{

    var body = "bankId="+data.bankId+"&bankIssueDate=" + data.bankIssueDate + "&branchId=" + data.branchId + "&authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`branch/site/register/bank/close?`+body,httpOptions)
  }
  //--------------------------------------
  //Submit Close Bank---------------------
  submitCloseBank(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('bankId', data.bankId);
    formData.append('bankIssueDate', data.bankIssueDate);
    formData.append('branchId', data.branchId);
    formData.append('createBy', data.createBy);
    formData.append('authUserId', data.authUserId);
    formData.append('bankCloseCashId', data.bankCloseCashId);
    formData.append('startingGT', data.startingGT);
    formData.append('endingGT', data.endingGT);
    formData.append('registerTranx', data.registerTranx);
    formData.append('registerNoSales', data.registerNoSales);
    formData.append('sales', data.sales);
    formData.append('registerZNumber', data.registerZNumber);
    formData.append('totalDrawer', data.totalDrawer);
    formData.append('adjustment', data.adjustment);
    formData.append('cCAdjustment', data.cCAdjustment);
    formData.append('adjustmentComment', data.adjustmentComment);
    formData.append('ccAdjustmentComment', data.ccAdjustmentComment);
    formData.append('overShort', data.overShort);
    formData.append('overShortComment', data.overShortComment);
    formData.append('cashierDetails', data.cashierDetails);
    formData.append('cardHandheldAmount', data.cardHandheldAmount);
    formData.append('denominationDetail', data.denominationDetail);
    formData.append('paymentFormDetails', data.paymentFormDetails);
    formData.append('otherChargeDetails', data.otherChargeDetails);
    formData.append('isChange', data.isChange);
    formData.append('posDetails', data.posDetails);
    //console.log(data);
    return this.http.post(apiUrl+`branch/site/register/bank/close`,formData,httpOptions)
  }
  //------------------------------------------------------------
  //------------------------------------------------------------
  //Bank Summary------------------------------------------------
  getBankSummary(data):Observable<any>{
   var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId + "&bankIssueDate=" +data.bankIssueDate+ "&bankId="+data.bankId;
   return this.http.get(apiUrl+`branch/site/register/bank/summary?`+body,httpOptions) 
  }
  
  //Get Pickup Details------------------------------------------
  getPickupDetailsData(data):Observable<any>{
   var body = "branchId=" + data.branchId + "&authUserId=" +data.authUserId + "&bankIssueDate=" +data.bankIssueDate+"&pickupId="+data.pickupId;
   return this.http.get(apiUrl+`branch/site/register/bank/pickup?`+body,httpOptions) 
  }
  //------------------------------------------------------------
  //Get Special Bank Details------------------------------------
  getSpecialBankData(data):Observable<any>{

    var body = "bankIssueDate=" + data.bankIssueDate + "&branchId=" + data.branchId + "&authUserId=" +data.authUserId;
    
    return this.http.get(apiUrl+`branch/special/bank?`+body,httpOptions)
  }
  //------------------------------------------------------------
  //Get branch wise pos list------------------------------------
  getPOSList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" + data.authUserId;
    return this.http.get(apiUrl+`branch/pos?`+body,httpOptions)
  }
  //------------------------------------------------------------
  //Get CC-handheld List----------------------------------------
  getCCList(data):Observable<any>{
    var body = "branchId=" + data.branchId + "&authUserId=" + data.authUserId + "&bankid=" +data.bankid+ "&bankIssueDate="+data.bankIssueDate;
    return this.http.get(apiUrl+`branch/site/register/bank/card?`+body,httpOptions)
  }
  //------------------------------------------------------------
  //Submit special bank-----------------------------------------
  submitSpecialBank(data):Observable<any>{ 
    
    const formData: FormData = new FormData();
    formData.append('bankId', data.bankId);
    formData.append('bankIssueDate', data.bankIssueDate);
    formData.append('branchId', data.branchId);
    formData.append('createBy', data.createBy);
    formData.append('authUserId', data.authUserId);
    formData.append('bankCloseCashId', data.bankCloseCashId);
    formData.append('startingGT', data.startingGT);
    formData.append('endingGT', data.endingGT);
    formData.append('registerTranx', data.registerTranx);
    formData.append('registerNoSales', data.registerNoSales);
    formData.append('sales', data.sales);
    formData.append('registerZNumber', data.registerZNumber);
    formData.append('totalDrawer', data.totalDrawer);
    formData.append('adjustment', data.adjustment);
    formData.append('cCAdjustment', data.cCAdjustment);
    formData.append('adjustmentComment', data.adjustmentComment);
    formData.append('ccAdjustmentComment', data.ccAdjustmentComment);
    formData.append('overShort', data.overShort);
    formData.append('overShortComment', data.overShortComment);
    formData.append('cashierDetails', data.cashierDetails);
    formData.append('cardHandheldAmount', data.cardHandheldAmount);
    formData.append('denominationDetail', data.denominationDetail);
    formData.append('paymentFormDetails', data.paymentFormDetails);
    formData.append('otherChargeDetails', data.otherChargeDetails);
    formData.append('isChange', data.isChange);
    formData.append('posDetails', data.posDetails);
    formData.append('eventDetail', data.eventDetail);
    //console.log(data);
    return this.http.post(apiUrl+`branch/special/bank`,formData,httpOptions)
  }
  //------------------------------------------------------------
  //------------------------------------------------------------
}
