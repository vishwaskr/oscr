import { TestBed } from '@angular/core/testing';

import { BankManagementService } from './bank-management.service';

describe('BankManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BankManagementService = TestBed.get(BankManagementService);
    expect(service).toBeTruthy();
  });
});
