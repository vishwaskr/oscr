import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
//featured module
import {LoginModule} from './featureModules/login-module/login.module';
import { NgxMaskModule, IConfig } from 'ngx-mask';

//export const options: Partial<any> | (() => Partial<any>) = null;
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    LoginModule,
    LoadingBarRouterModule,
    LoadingBarModule,
    NgxMaskModule.forRoot(options)
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
