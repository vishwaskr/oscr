import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthState } from './core/auth/auth.model';
import {ActionAuthLogin, ActionAuthLogout} from './core/auth/auth.action';
import {Observable} from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username:String = 'Cash room manager';
  title = 'OSCR';
  isLogin:boolean;
  state:Observable<AuthState>;

  constructor(private store:Store<AuthState>, private router:Router){
    
    this.state = store.select('login'); 
    this.state.subscribe(state =>{
    
      this.isLogin = state.isAuthenticated;
      //if(this.isLogin){
      //  router.navigate(['/summary'])
      //}else{
      //  router.navigate(['/login'])
      //}   
    })    

    // user is login or not ?    
    if(localStorage.getItem('isLogin')){
      let sessionLoginObject = JSON.parse(localStorage.getItem('isLogin'));            
      if(sessionLoginObject['isLogin'] == 'yes'){
        this.store.dispatch(new ActionAuthLogin);
      }else if(sessionLoginObject['isLogin'] == 'no'){
        this.store.dispatch(new ActionAuthLogout);
      }
    }else{
      this.store.dispatch(new ActionAuthLogout);
    }
    
    
  }
  
  //ngOnInit() {
  //  this.router.navigate([''])
  //}
  
}
