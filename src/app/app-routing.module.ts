import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginModule} from './featureModules/login-module/login.module';
import {SummaryModule} from './featureModules/summary-module/summary.module';
import {BankManagementModule} from './featureModules/bank-management-module/bank-management.module';
import {DepositModule} from './featureModules/deposit-module/deposit.module';
import {DocumentModule} from './featureModules/document-module/document.module';
import {SettingsModule} from './featureModules/settings-module/settings.module';
import {ReportsModule} from './featureModules/reports-module/reports.module';
import {AuthLoginGuard} from './core/auth/auth-login.guard';

// importing guard
import {AuthGuard} from './core/auth/auth.guard';

const routes: Routes = [
  { path:'',
    redirectTo:'index',
    pathMatch:'full'
  },
  {
    path:'index',    
     loadChildren:"src/app/featureModules/login-module/login.module#LoginModule",
    //loadChildren: () => LoginModule,
    canLoad:[AuthLoginGuard]
  },
  {
    path:'summary',
    loadChildren: () => SummaryModule,
    canLoad:[AuthGuard]
  },
  {
    path:'bank-mangement',
    //loadChildren: () => BankManagementModule,
    //loadChildren: () => import('./featureModules/bank-management-module/bank-management.module').then(m => m.BankManagementModule),
    loadChildren: './featureModules/bank-management-module/bank-management.module#BankManagementModule',
    canLoad:[AuthGuard]
  },
  {
    path:'deposit',
    loadChildren: './featureModules/deposit-module/deposit.module#DepositModule',
    canLoad:[AuthGuard]
  },
  {
    path:'document',
    loadChildren: () => DocumentModule,
    canLoad:[AuthGuard]
  },
  {
    path:'setting',
    loadChildren: () => SettingsModule,
    canLoad:[AuthGuard]
  },
  {
    path:'reports',
    loadChildren: () => ReportsModule,
    canLoad:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
