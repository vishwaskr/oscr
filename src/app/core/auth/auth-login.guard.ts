import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthLoginGuard implements CanLoad  {
  
  constructor(private router:Router){}

  canLoad():Observable<boolean> | boolean {
    if(localStorage.getItem('isLogin')){
      let sessionLoginObject = JSON.parse(localStorage.getItem('isLogin'));            
      if(sessionLoginObject['isLogin'] == 'yes'){
        this.router.navigate(['/summary'])          
        return false;
      }else if(sessionLoginObject['isLogin'] == 'no'){        
        return true
      }
    }else{      
      return true
    }
    return false;
  }

}
