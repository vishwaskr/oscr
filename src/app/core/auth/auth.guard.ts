import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,CanActivate, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import {Router, Route} from '@angular/router';
import {Store} from '@ngrx/store';
import {AuthState} from '../../core/auth/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad  {
  
  state:Observable<AuthState>;
  isLogin:boolean;
  constructor(private router:Router, private store:Store<AuthState>){
    this.state = store.select('login');
    this.state.subscribe(state =>{
      this.isLogin = state.isAuthenticated
    })
  }

  canActivate(router:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> | boolean{
     
    if(router.component['name'] == 'LoginComponent'){     
      if(this.isLogin){
        this.router.navigate(['/summary'])
        return false;
      }else{
        return true
      }   
    }else{      
      if(this.isLogin) {        
        return true;
      }else {
        this.router.navigate(['/login'])
        return false
      } 
    }
    return false;
  }
  canLoad(route:Route):Observable<boolean> | boolean{
    if(this.isLogin){     
      return true;
    }else{
      this.router.navigate(['/login'])
      return false
    }
  }

}
