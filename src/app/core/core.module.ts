import { NgModule, Optional, SkipSelf, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import {authReducer} from './auth/auth.reducer';
// import { EffectsModule } from '@ngrx/effects';
// import { StoreDevtoolsModule } from '@ngrx/store-devtools';
// import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import {
//   StoreRouterConnectingModule,
//   RouterStateSerializer
// } from '@ngrx/router-store';

import { environment } from '../../environments/environment';


@NgModule({
  imports: [
    // angular
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot({login:authReducer})
    // // ngrx
    // StoreModule.forRoot(reducers, { metaReducers }),
    // StoreRouterConnectingModule.forRoot(),
    // EffectsModule.forRoot([AuthEffects, GoogleAnalyticsEffects]),
    // environment.production
    //   ? []
    //   : StoreDevtoolsModule.instrument({
    //       name: 'Angular NgRx Material Starter'
    //     }),

  ],
  declarations: [],
  providers: [],
  exports: []
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}



