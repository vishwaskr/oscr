import { Component, OnInit,Input } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as jq from 'jquery';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() username:string;
  
  fillerNav : Array<Object> = [{icon:'fa fa-file-text-o',text:'Summary',navigate:'/summary'},{icon:'fa fa-university', text:'Bank Management',navigate:'/bank-mangement'},{icon:'fa fa-money', text:'Deposit',navigate:'/deposit'},{icon:'fa fa-file-o',text:'Documents',navigate:'/document'},{icon:'fa fa-cog', text:'Settings',navigate:'/setting'}];
  fillerNavIcon : Array<String> = ['home','money','keyboard',, 'settings']
  constructor() { }

  ngOnInit() {
    
  }

}
