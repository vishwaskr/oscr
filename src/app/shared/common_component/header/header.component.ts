import { Component, AfterViewInit,OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthState } from '../../../core/auth/auth.model';
import {ActionAuthLogout} from '../../../core/auth/auth.action';
import { Router } from '@angular/router';
import * as jq from 'jquery';

declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  UserName: string;
  constructor(private store:Store<AuthState>,private router:Router) {
    store.select('login')
    this.UserName= sessionStorage.getItem("UserName")
   }

   logout(){
     this.store.dispatch(new ActionAuthLogout);
     localStorage.setItem('isLogin',JSON.stringify({isLogin:"no"}))
     this.router.navigate(['/login'])
   }
  
  ngOnInit() {
    if (this.UserName==null) {
      this.logout();
    }
  }
  
  side_slide(){
   let idd=+jq("#menu_toggle").attr("idd");
   //console.log(idd);
   if (idd==0) {
    jq("#slide-out").addClass("sm-sidebar");
    jq("#copy_text").hide();
    jq("#header_id").addClass("pl-50");
    jq("#main_id").addClass("pl-50");
    jq("#menu_toggle").attr("idd",1);
   }
   else{
    jq("#slide-out").removeClass("sm-sidebar");
    jq("#copy_text").show();
    jq("#header_id").removeClass("pl-50");
    jq("#main_id").removeClass("pl-50");
    jq("#menu_toggle").attr("idd",0);
   }
  }
  
}
